import { Component, ElementRef, Input, trigger, state, style, transition, animate, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import {Subscription} from 'rxjs/Subscription';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';
import { RequestFloatBoxComponent } from './request_float_box.component';
import { SubCategoriesService } from './subcategories.service';
import { AdsComponent } from './ads.component';
import { WindowRef } from './window.service';

@Component({
  selector: 'tradesmanlist',
  templateUrl: './app/HTML/tradesmanlist.html',
  styleUrls: ['css/tradesmanlist.css'],
  host: {'(window:scroll)': 'show_to_top_btn($event)'}
})

export class TradesmanListComponent {
  window: any;
  location="";
  tradesman_array = [];
  tradesman_array_print = [];
  search = "";
  errorMessage = "";
  category = "";
  rq_box_category = "";
  array_id_delete = [];
  no_tradesman = "Please wait. Fetching tradesmen.";
  tradesmanlist_container_id = "tradesmanlist_container";
  overlay_id= "overlay";
  t_id = 0;
  f_name = "";
  l_name = "";
  name = "";
  pic = "";
  array_index = 0;
  array_id = 0;
  window_width = 0;
  choice = 0;

  temp_type = 0;
  temp_first = "";
  temp_last = "";
  temp_image = "";
  temp_company = "";
  temp_avail = "";
  temp_rating = "";
  temp_location = "";
  temp_sub_cat = "";
  temp_id = 0;
  temp_job = "";
  chosen_sub_category = "";

  ad_login: Subscription;

  public DeleteFunction: Function;

  sub_cat_array: any;
  start_hour:any;
  start_minute:any;
  start_am_pm:any;
  end_hour:any;
  end_minute:any;
  end_am_pm:any;

  show_service_time = false;
  ad: any;
  loc_error = "";

  @ViewChild(RequestFloatBoxComponent) request:RequestFloatBoxComponent;
  @ViewChild(AdsComponent) ads:AdsComponent;

  constructor(private element: ElementRef, private jsonfetch: JsonFetchService, public router: Router, private datatransfer:DataTransferService, private sub_categories: SubCategoriesService, win: WindowRef) {
    this.window = win.nativeWindow;
    if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "2"){
      if(this.datatransfer.get_tradesman_values_json() == null){
        this.router.navigate(['./edit-profile']);
      }else{
        this.router.navigate(['./dashboard']);
      }
    }else if(this.datatransfer.is_category()){
      this.DeleteFunction= this.remove_item.bind(this);
      this.window_width = window.innerWidth;
      this.ad_login = this.datatransfer.ad_login
        .subscribe(value => this.set_ad_login(value));
      this.sub_cat_array = JSON.parse(this.datatransfer.get_sub_categories());
      if(this.datatransfer.is_service_time()){
        var time = JSON.parse(this.datatransfer.get_service_time());
        this.show_service_time = true;
        this.start_hour = time.start_hour;
        this.start_minute = time.start_minute;
        this.start_am_pm = time.start_am_pm;
        this.end_hour = time.end_hour;
        this.end_minute = time.end_minute;
        this.end_am_pm = time.end_am_pm;
      }
    }else{
      this.router.navigate(['./tradesmen']);
    }
  }

  set_ad_login(value){
    if(value != ""){
      var value_json = JSON.parse(value);
      if(value_json.state == "1"){
        this.view_profile(0, value_json.first_name, value_json.last_name, value_json.image, value_json.company, value_json.availability, value_json.rating, value_json.location, value_json.sub_category, value_json.id, value_json.job);
      }else{
        this.view_profile(1, value_json.name, value_json.contact, value_json.image, "", value_json.address_1, "", "", value_json.address_3, 0, value_json.address_2);
      }
    }
  }

  show_to_top_btn(event){
    var height = window.innerHeight;
    var width = window.innerWidth;
    var y = currentYPosition()*(100 / width);
    var offset = 1.5*(height) * (100 / width);
    if(y > offset){
      this.element.nativeElement.querySelector('#to_top_btn').style.opacity = 1;
    }else{
      this.element.nativeElement.querySelector('#to_top_btn').style.opacity = 0;
    }
  }

  loc_focus(){
    this.element.nativeElement.querySelector('#loc_image').style.display = "none";
    this.element.nativeElement.querySelector('#loc_search').style.display = "block";
    this.element.nativeElement.querySelector('#loc_phrase').removeAttribute("style");
    this.element.nativeElement.querySelector('#location').style.boxShadow = "0 0 11px rgba(0,0,0,.25)";
  }

  loc_focus_out(){
    this.element.nativeElement.querySelector('#loc_image').style.display = "block";
    this.element.nativeElement.querySelector('#loc_search').style.display = "none";
    this.element.nativeElement.querySelector('#location').style.boxShadow = "0 0 2px rgba(0,0,0,.2)";
    var testloc = /^[A-Z]+[0-9]+[A-Z]+ +[0-9]+[A-Z]+[0-9]$/i;
    var testloc_2 = /^[A-Z]+[0-9]+[A-Z]+[0-9]+[A-Z]+[0-9]$/i;
    var error = false;
    this.loc_error = "";


    if(!testloc.test(this.location) && !testloc_2.test(this.location)){
      this.loc_error = "*Enter a valid postal code*";
      this.element.nativeElement.querySelector('#loc_phrase').style.border = "1px solid #F27072";
      this.element.nativeElement.querySelector('#loc_phrase').style.borderLeft = "none";
      error = true;
    }

    if(this.location.length == 0){
      this.loc_error = "*Input a postal code*";
      this.element.nativeElement.querySelector('#loc_phrase').style.border = "1px solid #F27072";
      this.element.nativeElement.querySelector('#loc_phrase').style.borderLeft = "none";
      error = true;
    }

    if(error){
      return;
    }

    var gps = {
      "lon": null,
      "lat": null,
      "postal_code": this.location
    }
    this.datatransfer.set_gps(JSON.stringify(gps));

    this.reload_list();
  }

  ngOnInit() {
    this.category = this.datatransfer.get_category();
    this.array_id_delete = this.datatransfer.get_id_to_delete();
    var timeoutVal = 10 * 1000 * 1000;
    navigator.geolocation.getCurrentPosition(
      position => this.gps_found(position),
      error => this.gps_error(error),
      { enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 }
    );
  }

  gps_found(position){
    var gps = {
      "lon": position.coords.longitude,
      "lat": position.coords.latitude,
      "postal_code": null
    }
    this.datatransfer.set_gps(JSON.stringify(gps));
    this.getTradesman();
  }

  gps_error(error){
    console.log(error);
    this.getTradesman();
  }

  ngAfterViewInit(){
    this.element.nativeElement.querySelector('#to_top_btn').style.opacity = 0;
    if(this.datatransfer.get_request_item() != null){
      this.overlay_id = "overlay_active";
      this.datatransfer.set_blur(true);
      this.tradesmanlist_container_id = "tradesmanlist_container_blurred";
    }
  }

  getTradesman() {
    this.datatransfer.set_load(true);
    this.tradesman_array_print.length = 0;
    var lon:any = null;
    var lat:any = null;
    var code:any = null;
    var time:any = null;
    var start_hour;
    var end_hour;
    if(this.datatransfer.get_gps() != null){
      var gps = JSON.parse(this.datatransfer.get_gps());
      code = gps.postal_code;
      if(code == null){
        lon = gps.lon;
        lat = gps.lat;
      }
    }

    if(this.start_hour != null){
      start_hour = this.start_hour;
      end_hour = this.end_hour;
      if(this.start_am_pm == "pm"){
        if(this.start_hour != "12"){
          start_hour = parseInt(this.start_hour) + 12;
        }
      }

      if(this.end_am_pm == "pm"){
        if(this.end_hour != "12"){
          end_hour = parseInt(this.end_hour) + 12;
        }
      }

      time = start_hour + ":" + this.start_minute + "-" + end_hour + ":" + this.end_minute;
    }

    this.jsonfetch.tradesman_list(this.category, this.sub_cat_array, time, lon, lat, code)
                     .subscribe(
                       tradesman => this.lol(tradesman),
                       error =>  this.error_happened(error));
  }

  error_happened(error){
    console.log(error);
  }

  get_new_tradesman(){
    if(this.tradesman_array.length < 5){
      this.getTradesman();
    }else{
      this.tradesman_array_print = this.tradesman_array;
      for(var i = 0; i < this.array_id_delete.length; i++){
        for(var j = 0; j < this.tradesman_array_print.length; j++){
          if(this.tradesman_array_print[j].id == this.array_id_delete[i]){
              this.tradesman_array_print.splice(j, 1);
          }
        }
      }
      this.datatransfer.set_load(false);
    }
  }

  getRandomInt(min, max){
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

  change_margin_top(value, length){
    if(length != 0){
       return {'margin-top': '3px'}
     }
  }

  change_top(value, length){
    var val = length + 'px';
    if(length != 0){
      if(this.window_width <= 560){
        return {'top': '-3px'}
      }else{
        return {'top': '-15px'}
      }
     }
  }

  compute_width(rating, index){
    if(index <= rating){
      return{
        "width": "98%",
        "margin-left": "1%",
        "margin-right": "1%"
      };
    }else{
      if(rating- index < -1){
        return{
          "width": "0",
          "margin-left": "1%",
          "margin-right": "1%"
        };
      }else{
        var value = (1 + rating- index)*98;
        return{
          "width": value + "%",
          "margin-left": "1%",
          "margin-right": "1%"
        };
      }
    }
  }

  view_profile(type, first_name, last_name, image, company, availability, rating, location, sub_category, id, job){
    this.choice = 1;
    this.datatransfer.set_data(type, first_name, last_name, image, company, availability, rating, location, sub_category, id, job);
    this.router.navigate(['./tradesman']);
  }

  lol(res){
    this.no_tradesman = "Please wait. Fetching tradesmen.";
    console.log(res);
    if(res.STATUS == "OK"){
      for(var i = 0; i < res.DATA.length; i++){
        var sub_category = "";
        var location:any = null;
        var company:any = null;
        var rating:any = null;

        if(res.DATA[i].rating != null){
          rating = parseFloat(res.DATA[i].rating);
        }

        if(res.DATA[i].company_name != null){
          company = res.DATA[i].company_name;
        }

        if(res.DATA[i].distance != null){
          location = Math.round(res.DATA[i].distance/10)/100;
        }

        if(res.DATA[i].postal_code.length != 0){
          this.location = res.DATA[i].postal_code;
        }

        for(var y = 0; y < res.DATA[i].job_sub_type.length; y++){
          if(y != 0){
            sub_category += " | ";
          }
          sub_category += res.DATA[i].job_sub_type[y];
        }

        var image = res.DATA[i].profile_imgpath;

        if(image.length == 0){
          image = "./assets/images/icons/default_profile_pic.png";
        }

        var tradesman = {
          "id": res.DATA[i].tradesmen_id,
          "name": res.DATA[i].tr_first_name + " " + res.DATA[i].tr_last_name,
          "first_name": res.DATA[i].tr_first_name,
          "last_name": res.DATA[i].tr_last_name,
          "company": company,
          "image": image,
          "sub_category": sub_category,
          "rating": rating,
          "location": location,
          "availability": 'Available today from ' + res.DATA[i].start_hour + ':' + res.DATA[i].start_minute + ' ' + res.DATA[i].start_am_pm + ' to ' + res.DATA[i].end_hour + ':' + res.DATA[i].end_minute + ' ' + res.DATA[i].end_am_pm
        }

        this.tradesman_array_print.push(tradesman);
      }
    }else{
      this.no_tradesman = "No tradesman found. Please try a different search categories.";
    }

    this.datatransfer.set_load(false);
  }

  get_length(){
    if(this.tradesman_array_print.length == 0){
      if(this.array_id_delete.length > 1){
        this.no_tradesman = "Looks like all tradesmen is occupied at the moment. Please try again later.";
      }
      return true;
    }else{
      return false;
    }
  }

  remove_item(){
    this.search = "";
    if(this.array_index != null){
      setTimeout(() => {
        this.datatransfer.add_id_to_delete(this.array_id);
        this.tradesman_array_print.splice(this.array_index, 1);
      },10);
    }
  }

  request_pop(first, last, index, id, img, sub_category, category){
    this.rq_box_category = category;
    this.choice = 0;
    this.name = first + " " + last;
    this.f_name = first;
    this.l_name = last;
    this.t_id = id;
    this.pic = img;
    this.chosen_sub_category = sub_category;
    if(index != null){
      for(var i = 0; i < this.tradesman_array_print.length; i++){
        if(this.tradesman_array_print[i].first_name == first &&
           this.tradesman_array_print[i].last_name == last &&
           this.tradesman_array_print[i].id == id &&
           this.tradesman_array_print[i].image == img){
             this.array_index = i;
           }
      }
    }else{
      this.array_index = null;
    }
    this.array_id = id;
    this.overlay_id="overlay_active";
    this.datatransfer.set_blur(true);
    this.tradesmanlist_container_id = "tradesmanlist_container_blurred";
    this.request.request_pop();
  }

  close_request(){
    if(this.request.element.nativeElement.querySelector('#request_container').style.display == "none"){
      this.close();
    }
  }

  close(){
    this.request.close_request();
    setTimeout(() => {
      this.overlay_id="overlay";
      this.tradesmanlist_container_id="tradesmanlist_container";
      this.datatransfer.set_blur(false);
    },350);
  }

  box_closed(event){
    if(event){
      this.close();
    }else{
      this.request.close_request();
      setTimeout(() => {
        this.overlay_id="overlay";
        this.tradesmanlist_container_id="tradesmanlist_container";
        this.datatransfer.set_blur(false);
        setTimeout(() => {
          this.view_profile(this.temp_type, this.temp_first, this.temp_last, this.temp_image, this.temp_company,
                            this.temp_avail, this.temp_rating, this.temp_location, this.temp_sub_cat, this.temp_id, this.temp_job);
        },50);
      },350);
    }
  }

  hello(img, src){
    img.src = src;
  }

  set_off_on(id){
    if(id%2 == 0){
      return{
        "background-color": "#47b649"
      };
    }else{
      return{
        "background-color": "#f5845d"
      };
    }
  }

  set_checkboxes(category){
    if(this.sub_cat_array.includes(category)){
      return "./assets/images/check_box_v2.png";
    }else{
      return "./assets/images/uncheck_box.png";
    }
  }

  toggle_sub_category(category){
    if(this.sub_cat_array.includes(category)){
      this.sub_cat_array.splice(this.sub_cat_array.indexOf(category),1);
    }else{
      this.sub_cat_array.push(category);
    }

    this.reload_list();
  }

  toggle_drop_down_start(event){
    event.stopPropagation();
    var drop_down_box = this.element.nativeElement.querySelector('#drop_down_start');
    var am_pm = this.element.nativeElement.querySelector('#am_pm_start');

    if(drop_down_box.style.height == "19px"){
      drop_down_box.style.height = "0";
      am_pm.style.borderBottomLeftRadius = "0";
    }else{
      drop_down_box.style.height = "19px";
      am_pm.style.borderBottomLeftRadius = "4px";
    }
  }

  close_drop_down_start(){
    var drop_down_box = this.element.nativeElement.querySelector('#drop_down_start');
    var am_pm = this.element.nativeElement.querySelector('#am_pm_start');

    drop_down_box.style.height = "0";
    am_pm.style.borderBottomLeftRadius = "0";
  }

  change_value_start(value){
    this.start_am_pm = value;
  }

  toggle_drop_down_end(event){
    event.stopPropagation();
    var drop_down_box = this.element.nativeElement.querySelector('#drop_down_end');
    var am_pm = this.element.nativeElement.querySelector('#am_pm_end');

    if(drop_down_box.style.height == "19px"){
      drop_down_box.style.height = "0";
      am_pm.style.borderBottomLeftRadius = "0";
    }else{
      drop_down_box.style.height = "19px";
      am_pm.style.borderBottomLeftRadius = "4px";
    }
  }

  close_drop_down_end(){
    var drop_down_box = this.element.nativeElement.querySelector('#drop_down_end');
    var am_pm = this.element.nativeElement.querySelector('#am_pm_end');

    drop_down_box.style.height = "0";
    am_pm.style.borderBottomLeftRadius = "0";
  }

  change_value_end(value){
    this.end_am_pm = value;
  }

  check_hour(val){
    switch(val){
      case "0":
      case "00":
      case "1":
      case "01":
      case "2":
      case "02":
      case "3":
      case "03":
      case "4":
      case "04":
      case "5":
      case "05":
      case "6":
      case "06":
      case "7":
      case "07":
      case "8":
      case "08":
      case "9":
      case "09":
      case "10":
      case "11":
      case "12":
          return true;
      default: return false;
    }
  }

  check_minute(val){
    switch(val){
      case "0":
      case "00":
      case "1":
      case "01":
      case "2":
      case "02":
      case "3":
      case "03":
      case "4":
      case "04":
      case "5":
      case "05":
      case "6":
      case "06":
      case "7":
      case "07":
      case "8":
      case "08":
      case "9":
      case "09":
          return true;
      default:
        if(parseInt(val) != null && parseInt(val) < 61){
          return true;
        }else{
          return false;
        }
    }
  }

  update_service_time(){
    var box = this.element.nativeElement.querySelector('#time_error');
    box.style.opacity = 0;
    if(!(this.check_hour(this.start_hour) && this.check_hour(this.end_hour) && this.check_minute(this.start_minute) && this.check_minute(this.end_minute))){
      box.style.opacity = 1;
      return;
    }
    var time = {
      "start_hour": this.start_hour,
      "start_minute": this.start_minute,
      "start_am_pm": this.start_am_pm,
      "end_hour": this.end_hour,
      "end_minute": this.end_minute,
      "end_am_pm": this.end_am_pm
    }
    this.datatransfer.set_service_time(JSON.stringify(time));
    this.reload_list();
  }

  toggle_drop_down_category(event){
    event.stopPropagation();
    var drop_down_box = this.element.nativeElement.querySelector('#job_dropdown');
    var btn = this.element.nativeElement.querySelector('#category_drop_down_btn');
    var title = this.element.nativeElement.querySelector('#category_title');

    if(drop_down_box.style.maxHeight == "22vw"){
      drop_down_box.style.maxHeight = "0";
      title.style.borderBottomLeftRadius = "4px";
      btn.style.borderBottomRightRadius = "4px";
      setTimeout(() => {
        drop_down_box.style.border = "none";
        drop_down_box.style.borderTop = "none";
      },350);
    }else{
      drop_down_box.style.border = "1px solid #eaeaea";
      drop_down_box.style.borderTop = "none";
      drop_down_box.style.maxHeight= "22vw";
      title.style.borderBottomLeftRadius = "0";
      btn.style.borderBottomRightRadius = "0";
    }
  }

  close_drop_down_category(){
    var drop_down_box = this.element.nativeElement.querySelector('#job_dropdown');
    var btn = this.element.nativeElement.querySelector('#category_drop_down_btn');
    var title = this.element.nativeElement.querySelector('#category_title');

    drop_down_box.style.height = "0";
    title.style.borderBottomLeftRadius = "0";
    btn.style.borderBottomRightRadius = "0";
    setTimeout(() => {
      drop_down_box.style.border = "none";
      drop_down_box.style.borderTop = "none";
    },350);
  }

  change_value_category(value){
    if(this.category != value){
      this.category = value;
      this.sub_cat_array.length = 0;
      this.reload_list();
    }
  }

  reload_list(){
    this.datatransfer.set_category(this.category);
    this.datatransfer.set_sub_categories(JSON.stringify(this.sub_cat_array));
    this.tradesman_array.length = 0;
    this.tradesman_array_print.length = 0;
    this.datatransfer.set_load(true);
    this.getTradesman();
  }

  add_service_time(){
    this.show_service_time = true;
    var date = new Date();

    this.start_hour = date.getHours();
    this.start_minute = date.getMinutes();
    if(this.start_minute < 10){
      this.start_minute = "0" + this.start_minute.toString();
    }
    this.start_am_pm = "am";

    this.end_hour = date.getHours() + 5;
    this.end_minute = date.getMinutes();
    if(this.end_minute < 10){
      this.end_minute = "0" + this.end_minute.toString();
    }
    this.end_am_pm = "am";

    if(this.start_hour > 11){
      if(this.start_hour != 12){
        this.start_hour = this.start_hour - 12;
      }
      this.start_am_pm = "pm";
    }


    if(this.end_hour > 23){
      this.end_hour = this.end_hour - 24;
      this.end_am_pm = "am";
    }else if(this.end_hour > 11){
      if(this.end_hour != 12){
        this.end_hour = this.end_hour - 12;
      }
      this.end_am_pm = "pm";
    }

    this.start_hour = this.start_hour.toString();
    this.start_minute = this.start_minute.toString();
    this.end_hour = this.end_hour.toString();
    this.end_minute = this.end_minute.toString();
  }

  ads_array(i){
    var index = Math.ceil(i/3);
    if(i%3 == 0){
      index = index + 1;
    }
    this.ad = this.ads.ads_pool_copy[((index-1)*2)%this.ads.ads_pool_copy.length];
    if(this.ad.state == 1){
      return true;
    }else{
      return false;
    }
  }

  ad_view_profile_company(i){
    var index = Math.ceil(i/3);
    if(i%3 == 0){
      index = index + 1;
    }
    var ad_index = ((index-1)*2)%this.ads.ads_pool_copy.length;
    this.view_profile(1, this.ads.ads_pool_copy[ad_index].name, this.ads.ads_pool_copy[ad_index].contact, this.ads.ads_pool_copy[ad_index].image, '', this.ads.ads_pool_copy[ad_index].address_1, '', '',
                      this.ads.ads_pool_copy[ad_index].address_3, 0, this.ads.ads_pool_copy[ad_index].address_2);
  }

  ad_view_profile_tradesman(i){
    var index = Math.ceil(i/3);
    if(i%3 == 0){
      index = index + 1;
    }
    var ad_index = ((index-1)*2)%this.ads.ads_pool_copy.length;
    this.view_profile(0, this.ads.ads_pool_copy[ad_index].first_name, this.ads.ads_pool_copy[ad_index].last_name, this.ads.ads_pool_copy[ad_index].image, this.ads.ads_pool_copy[ad_index].company, this.ads.ads_pool_copy[ad_index].availability,
                      this.ads.ads_pool_copy[ad_index].rating, this.ads.ads_pool_copy[ad_index].location, this.ads.ads_pool_copy[ad_index].sub_category, this.ads.ads_pool_copy[ad_index].id, this.ads.ads_pool_copy[ad_index].job);
  }

  ad_request_tradesman(i){
    var index = Math.ceil(i/3);
    if(i%3 == 0){
      index = index + 1;
    }
    var ad_index = ((index-1)*2)%this.ads.ads_pool_copy.length;
    this.request_pop(this.ads.ads_pool_copy[ad_index].first_name, this.ads.ads_pool_copy[ad_index].last_name, null, this.ads.ads_pool_copy[ad_index].id,
                     this.ads.ads_pool_copy[ad_index].image, this.ads.ads_pool_copy[ad_index].sub_category, this.ads.ads_pool_copy[ad_index].job);
  }

  scrollTo(yPoint: number, duration: number) {
      setTimeout(() => {
          this.window.window.scrollTo(0, yPoint)
      }, duration);
      return;
  }

  smoothScroll(eID) {
      var startY = currentYPosition();
      var stopY = elmYPosition(eID) - 100;
      var distance = stopY > startY ? stopY - startY : startY - stopY;
      if (distance < 100) {
          this.window.window.scrollTo(0, stopY); return;
      }
      var speed = Math.round(distance / 100);
      if (speed >= 20) speed = 20;
      var step = Math.round(distance / 100);
      var leapY = stopY > startY ? startY + step : startY - step;
      var timer = 0;
      if (stopY > startY) {
          for (var i = startY; i < stopY; i += step) {
              this.scrollTo(leapY, timer * speed);
              leapY += step; if (leapY > stopY) leapY = stopY; timer++;
          } return;
      }
      for (var i = startY; i > stopY; i -= step) {
          this.scrollTo(leapY, timer * speed);
          leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
      }
    }
}

function elmYPosition(eID) {
    var elm = document.getElementById(eID);
    var y = elm.offsetTop;
    var node = elm;
    while (node.offsetParent && node.offsetParent != document.body) {
        node = <HTMLElement><any>node.offsetParent;
        y += node.offsetTop;
    } return y;
}

function currentYPosition() {
    // Firefox, Chrome, Opera, Safari
    if (self.pageYOffset) return self.pageYOffset;
    // Internet Explorer 6 - standards mode
    if (document.documentElement && document.documentElement.scrollTop)
        return document.documentElement.scrollTop;
    // Internet Explorer 6, 7 and 8
    if (document.body.scrollTop) return document.body.scrollTop;
    return 0;
}
