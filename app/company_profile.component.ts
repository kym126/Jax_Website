import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';
import { WindowRef } from './window.service';
import { JobSpecificFieldsComponent } from './job_specific_fields.component';
import {ImageCropperComponent, CropperSettings} from 'ng2-img-cropper';

@Component({
  selector: 'tradesman-settings',
  templateUrl: './app/HTML/company_profile.html',
  styleUrls: ['css/company_profile.css'],
  host: {'(window:scroll)': 'change_nav_btns($event)'}
})

export class CompanyProfileComponent {

  @ViewChild(ImageCropperComponent) cropper:ImageCropperComponent;
  cropperSettings: CropperSettings;
  data: any;

  content_id = "content";
  specialize_in_error_msg = "";
  about_your_company_error_msg = "";
  company_details_error_msg = "";

  company_profile_pic = "./assets/images/company.png";
  company_name = "";
  unit_number = "";
  street_address = "";
  city_town_village = "";
  postal_code = "";
  province = "";

  email = "";
  cell_number = "";
  office_number = "";
  new_skill = "";

  about_your_company = "";

  chosen_province = "ON";

  overlay_id = "overlay_hide";
  container_id = "container";
  navigation_id = "navigation";

  added_skills = [];
  rw_array = [];
  gallery_images = [];
  facebook = "";
  linkedin = "";
  twitter = "";
  website = "";
  profile_url: string = null;

  window: any;
  prev_image: any;

  provinces = ["AB", "BC", "MB", "NB", "NL", "NS", "NT", "NU", "ON", "PE", "QC", "SK", "YT"];

  @ViewChild(JobSpecificFieldsComponent) profile: JobSpecificFieldsComponent;

  constructor(private element: ElementRef, public router: Router, private datatransfer:DataTransferService, private jsonfetch: JsonFetchService, win: WindowRef) {
    if(!this.datatransfer.is_logged_in() || this.datatransfer.get_user_type() ==  "1"){
        this.router.navigate(['./home']);
    }
    this.window = win.nativeWindow;
    this.scrollTo(0,0);
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.width = 100;
    this.cropperSettings.height = 100;
    this.cropperSettings.croppedWidth =100;
    this.cropperSettings.croppedHeight = 100;
    this.cropperSettings.canvasWidth = 500;
    this.cropperSettings.canvasHeight = 300;
    if(window.innerWidth < 800){
      this.cropperSettings.canvasWidth = 600;
      this.cropperSettings.canvasHeight = 400;
    }

    this.data = {};
    this.data.image = "./assets/images/company.png";
    if(this.datatransfer.is_company_profile()){
      var company = JSON.parse(this.datatransfer.get_company_profile());
      this.company_name = company.company_name;
      if(company.company_pic != null){
          this.data.image = company.company_pic;
      }
      this.profile_url = company.profile_url;
      this.about_your_company = company.about_your_company;
      this.unit_number = company.address.unit_number;
      this.street_address = company.address.street_address;
      this.city_town_village = company.address.city_town_village;
      this.postal_code = company.address.postal_code;
      this.chosen_province = company.address.province;
      this.email = company.email;
      this.cell_number = company.cell_number;
      this.office_number = company.office_number;
      this.added_skills = company.specialize_in;
      if(company.recent_works != null){
          this.rw_array = company.recent_works;
      }
      if(company.gallery_images != null){
          this.gallery_images = company.gallery_images;
      }
      this.facebook = company.facebook;
      this.linkedin = company.linkedin;
      this.twitter = company.twitter;
      this.website = company.website;
    }else{
      this.company_name = this.datatransfer.get_company_name();
      if(this.datatransfer.is_office_number()){
        this.office_number = this.datatransfer.get_office_number();
      }
    }
  }

  scrollTo(yPoint: number, duration: number) {
      setTimeout(() => {
          this.window.window.scrollTo(0, yPoint)
      }, duration);
      return;
  }

  ngAfterViewInit(){
    var btn1 = this.element.nativeElement.querySelector('#company_details_nav_btn');
    var btn2 = this.element.nativeElement.querySelector('#about_your_company_nav_btn');
    var btn3 = this.element.nativeElement.querySelector('#specialize_in_nav_btn');
    var btn4 = this.element.nativeElement.querySelector('#recent_works_nav_btn');
    var btn5 = this.element.nativeElement.querySelector('#gallery_nav_btn');
    var btn6 = this.element.nativeElement.querySelector('#add_social_media_links_nav_btn');

    this.set_nav_btn(btn1, btn2, btn3, btn4, btn5, btn6);
  }

  change_nav_btns(event){
    var offset = 65;
    var section1 = elmYPosition('company_details_title') - offset;
    var section2 = elmYPosition('about_your_company_title') - offset;
    var section3 = elmYPosition('specialize_in_title') - offset;
    var section4 = elmYPosition('recent_works_title') - offset;
    var section5 = elmYPosition('gallery_title') - offset;
    var section6 = elmYPosition('add_social_media_links_title') - offset;

    var btn1 = this.element.nativeElement.querySelector('#company_details_nav_btn');
    var btn2 = this.element.nativeElement.querySelector('#about_your_company_nav_btn');
    var btn3 = this.element.nativeElement.querySelector('#specialize_in_nav_btn');
    var btn4 = this.element.nativeElement.querySelector('#recent_works_nav_btn');
    var btn5 = this.element.nativeElement.querySelector('#gallery_nav_btn');
    var btn6 = this.element.nativeElement.querySelector('#add_social_media_links_nav_btn');

    if(currentYPosition() >= section1 && currentYPosition() <= section2){
      this.set_nav_btn(btn1, btn2, btn3, btn4, btn5, btn6);
    }else if(currentYPosition() >= section2 && currentYPosition() <= section3){
      this.set_nav_btn(btn2, btn1, btn3, btn4, btn5, btn6);
    }else if(currentYPosition() >= section3 && currentYPosition() <= section4){
      this.set_nav_btn(btn3, btn1, btn2, btn4, btn5, btn6);
    }else if(currentYPosition() >= section4 && currentYPosition() <= section5){
      this.set_nav_btn(btn4, btn1, btn2, btn3, btn5, btn6);
    }else if(currentYPosition() >= section5 && currentYPosition() <= section6){
      this.set_nav_btn(btn5, btn1, btn2, btn3, btn4, btn6);
    }else if(currentYPosition() >= section6){
      this.set_nav_btn(btn6, btn1, btn2, btn3, btn4, btn5);
    }else{
      this.set_nav_btn(btn1, btn2, btn3, btn4, btn5, btn6);
    }
  }

  set_nav_btn(btn1, btn2, btn3, btn4, btn5, btn6){
    btn1.style.color = "#292C38";
    btn1.style.fontSize = "11pt";
    btn2.style.color = "#8C8C8C";
    btn2.style.fontSize = "9pt";
    btn3.style.color = "#8C8C8C";
    btn3.style.fontSize = "9pt";
    btn4.style.color = "#8C8C8C";
    btn4.style.fontSize = "9pt";
    btn5.style.color = "#8C8C8C";
    btn5.style.fontSize = "9pt";
    btn6.style.color = "#8C8C8C";
    btn6.style.fontSize = "9pt";
  }

  smoothScroll(eID) {
      var startY = currentYPosition();
      var stopY = elmYPosition(eID) - 63;
      var distance = stopY > startY ? stopY - startY : startY - stopY;
      if (distance < 100) {
          this.window.window.scrollTo(0, stopY); return;
      }
      var speed = Math.round(distance / 100);
      if (speed >= 20) speed = 20;
      var step = Math.round(distance / 100);
      var leapY = stopY > startY ? startY + step : startY - step;
      var timer = 0;
      if (stopY > startY) {
          for (var i = startY; i < stopY; i += step) {
              this.scrollTo(leapY, timer * speed);
              leapY += step; if (leapY > stopY) leapY = stopY; timer++;
          } return;
      }
      for (var i = startY; i > stopY; i -= step) {
          this.scrollTo(leapY, timer * speed);
          leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
      }
  }

  change_company_profile_pic(event){
    var reader = new FileReader();
    var _this = this;
    var image:any = new Image();
    this.prev_image = this.data.image;
    if(this.data.image == null){
      this.prev_image = "./assets/images/icons/default_profile_pic.png";
    }
    reader.onload = function(e) {
      var src = reader.result;
      _this.company_profile_pic = src;
      image.src = src;
      _this.cropper.setImage(image);
      _this.crop_image();

    };
    if(event.target.files[0] != null){
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  crop_image(){
    this.content_id = "content_blur";
    this.navigation_id = "navigation_blur";
    this.overlay_id = "overlay_active";
    this.datatransfer.set_blur(true);
    var pop_up = this.element.nativeElement.querySelector('#crop_pop_up');

    pop_up.style.opacity = 0;

    setTimeout(() => {
      pop_up.style.opacity = 1;
    },10);
  }

  close_pop_up(){
    var pop_up = this.element.nativeElement.querySelector('#crop_pop_up');
    pop_up.style.opacity = 0;
    setTimeout(() => {
      this.content_id = "content";
      this.overlay_id = "overlay_hide";
      this.navigation_id = "navigation";
      this.datatransfer.set_blur(false);
    },350);
  }

  give_border_bottom(i){
    if(i == this.provinces.length - 1){
      return{
        "border-bottom-left-radius": "3px",
        "border-bottom-right-radius": "3px",
        "border-bottom": "1px solid #b6b7b7"
      };
    }else{
      return{
        "border-bottom-left-radius": "0",
        "border-bottom-right-radius": "0",
        "border-bottom": "none"
      };
    }
  }

  remove_attribute(){
    var box = this.element.nativeElement.querySelector('#postal_code_input');
    box.removeAttribute("style");
  }

  save_profile(){
    var pic = "";
    var gallery_images = [];
    var recent_works = [];
    var error = false;
    var box1 = this.element.nativeElement.querySelector('#company_name_input');
    var box2 = this.element.nativeElement.querySelector('#street_address_input');
    var box3 = this.element.nativeElement.querySelector('#city_town_village_input');
    var box4 = this.element.nativeElement.querySelector('#postal_code_input');
    var box5 = this.element.nativeElement.querySelector('#email_input');
    var box6 = this.element.nativeElement.querySelector('#mobile_number_input');
    var box7 = this.element.nativeElement.querySelector('#about_you_text_area');

    var testloc = /^[A-Z]+[0-9]+[A-Z]+ +[0-9]+[A-Z]+[0-9]$/i;
    var testloc_2 = /^[A-Z]+[0-9]+[A-Z]+[0-9]+[A-Z]+[0-9]$/i;
    var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    var testPhone = /^\(+([0-9]{3})+\)+\ +([0-9]{3})[-]?([0-9]{4})$/;

    this.about_your_company_error_msg = "";
    this.company_details_error_msg = "";
    this.specialize_in_error_msg = "";

    if(!testloc.test(this.postal_code) && !testloc_2.test(this.postal_code)){
      this.smoothScroll('company_details_title');
      box4.style.border = "1px solid #F27072";
      this.company_details_error_msg = "*Please input a valid postal code*";
      error = true;
    }

    if(!testPhone.test(this.cell_number)){
      this.smoothScroll('company_details_title');
      box6.style.border = "1px solid #F27072";
      this.company_details_error_msg = "*Please input a valid phone number*";
      error = true;
    }

    if(!testEmail.test(this.email)){
      this.smoothScroll('company_details_title');
      box5.style.border = "1px solid #F27072";
      this.company_details_error_msg = "*Please input a valid email address*";
      error = true;
    }

    if(this.company_name.length == 0){
      this.smoothScroll('company_details_title');
      box1.style.border = "1px solid #F27072";
      this.company_details_error_msg = "*Please fill up all fields highlighted in red*";
      error = true;
    }

    if(this.street_address.length == 0){
      this.smoothScroll('company_details_title');
      box2.style.border = "1px solid #F27072";
      this.company_details_error_msg = "*Please fill up all fields highlighted in red*";
      error = true;
    }

    if(this.city_town_village.length == 0){
      this.smoothScroll('company_details_title');
      box3.style.border = "1px solid #F27072";
      this.company_details_error_msg = "*Please fill up all fields highlighted in red*";
      error = true;
    }

    if(this.postal_code.length == 0){
      this.smoothScroll('company_details_title');
      box4.style.border = "1px solid #F27072";
      this.company_details_error_msg = "*Please fill up all fields highlighted in red*";
      error = true;
    }

    if(this.email.length == 0){
      this.smoothScroll('company_details_title');
      box5.style.border = "1px solid #F27072";
      this.company_details_error_msg = "*Please fill up all fields highlighted in red*";
      error = true;
    }


    if(this.cell_number.length == 0){
      this.smoothScroll('company_details_title');
      box6.style.border = "1px solid #F27072";
      this.company_details_error_msg = "*Please fill up all fields highlighted in red*";
      error = true;
    }

    if(this.about_your_company.length == 0){
      this.about_your_company_error_msg = "*Please provide a short description about your company*";
      this.smoothScroll('about_your_company_title');
      box7.style.border = "1px solid #F27072";
      error = true;
    }

    if(this.profile.added_skills.length == 0){
      this.specialize_in_error_msg = "*Please provide at least 1 specialization for your company*";
      this.smoothScroll('specialize_in_title');
      error = true;
    }

    if(error){
      return;
    }

    if(this.profile.gallery_images.length != 0){
      gallery_images = this.gallery_images;
    }

    if(this.profile.rw_array.length != 0){
      recent_works = this.rw_array;
    }

    if(this.data.image != "./assets/images/company.png"){
      pic = this.data.image;
    }

    var company = {
      "company_pic": pic,
      "company_name": this.company_name,
      "about_your_company": this.about_your_company,
      "address": {
        "unit_number": this.unit_number,
        "street_address": this.street_address,
        "city_town_village": this.city_town_village,
        "postal_code": this.postal_code,
        "province": this.chosen_province
      },
      "email": this.email,
      "cell_number": this.cell_number,
      "office_number": this.office_number,
      "specialize_in": this.profile.added_skills,
      "recent_works": recent_works,
      "gallery_images": gallery_images,
      "facebook": this.profile.facebook,
      "linkedin": this.profile.linkedin,
      "twitter": this.profile.twitter,
      "website": this.profile.website
    }

    var rw_title = [];
    var rw_desc = [];
    var rw_images = [];
    var rw_id = [];
    var rw_images_file_index = [];

    if(recent_works == null){
      recent_works = new Array();
    }

    for(var z = 0; z < recent_works.length; z++){
      rw_title.push(recent_works[z].title);
      rw_desc.push(recent_works[z].description);
      if(recent_works[z].id == null){
        rw_id.push(0);
      }else{
        rw_id.push(parseInt(recent_works[z].id));
      }
      var rw_image_small = new Array();
      var rw_images_file_index_small = new Array();
      for(var y = 0; y < recent_works[z].images.length; y++){
        if(typeof recent_works[z].images_file[y] == "string"){
          rw_image_small.push(recent_works[z].rw_image_url[y]);
          rw_images_file_index_small.push(0);
        }else{
          rw_image_small.push(recent_works[z].images_file[y]);
          rw_images_file_index_small.push(1);
        }
      }

      for(var x = rw_images_file_index_small.length; x < 3; x++){
        rw_images_file_index_small.push(null);
      }

      rw_images_file_index.push(rw_images_file_index_small);
      rw_images.push(rw_image_small);
    }

    if(pic.substr(0,4) == "data"){
      this.profile_url = null;
    }else{
      pic = null;
    }

    this.datatransfer.set_load(true);
    this.jsonfetch.initial_company_profile(this.datatransfer.get_user_id(), this.street_address, this.about_your_company, pic, this.city_town_village, this.profile.added_skills,
        this.chosen_province, this.postal_code, this.profile.facebook, this.profile.twitter, this.profile.linkedin, this.profile.website, this.profile.gallery_images_file, rw_title, rw_desc, rw_images,
        this.cell_number, this.company_name, this.email, rw_id, this.profile_url, rw_images_file_index)
        .map(res => res)
            .subscribe(
                res => this.check_result(res),
                error => this.error_happened(error)
            );
  }

  error_happened(res){
    this.datatransfer.set_load(false);
    console.log(res);
  }

  getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  check_result(res){
    this.datatransfer.set_load(false);
    console.log(res);
    if(res.STATUS == "OK"){

      var rw_array = [];

      for(var x = 0; x < res.DATA[0].recentwork.length; x++){
        var title_shown = "";
        var desc_shown = "";
        var rw_img_file = [];
        var rw_img_url = [];

        if(res.DATA[0].recentwork[x].recentwork_title.length > 15){
          title_shown = res.DATA[0].recentwork[x].recentwork_title.substr(0, 11) + "...";
        }else{
          title_shown = res.DATA[0].recentwork[x].recentwork_title;
        }

        if(res.DATA[0].recentwork[x].recentwork_desc.length > 100){
          desc_shown = res.DATA[0].recentwork[x].recentwork_desc.substr(0, 96) + "...";
        }else{
          desc_shown = res.DATA[0].recentwork[x].recentwork_desc;
        }

        for(var y = 0; y < res.DATA[0].recentwork[x].recentwork_image.length; y++){
          rw_img_url.push(res.DATA[0].recentwork[x].recentwork_image[y]);
          res.DATA[0].recentwork[x].recentwork_image[y] = 'http://jaxoftrades.com/api/' + res.DATA[0].recentwork[x].recentwork_image[y] + "?" + this.getRandomInt(90000000000, 100000000000);
          rw_img_file.push(res.DATA[0].recentwork[x].recentwork_image[y]);
        }

        var rw = {
          "title": res.DATA[0].recentwork[x].recentwork_title,
          "description": res.DATA[0].recentwork[x].recentwork_desc,
          "images": res.DATA[0].recentwork[x].recentwork_image,
          "images_file": rw_img_file,
          "title_shown": title_shown,
          "description_shown": desc_shown,
          "chosen_bg": 0,
          "cost": 0,
          "id": res.DATA[0].recentwork[x].recentwork_id,
          "rw_image_url": rw_img_url
        }

        rw_array.push(rw);

      }

      var gallery = [];

      for(var y = 0; y < res.DATA[0].gallery_images.length; y++){
        gallery.push(res.DATA[0].gallery_images[y]);
      }

      var company = {
        "profile_url": res.DATA[0].c_photo,
        "company_pic": 'http://jaxoftrades.com/api/' + res.DATA[0].c_photo + "?" +this.getRandomInt(90000000000, 100000000000),
        "company_name": res.DATA[0].c_name,
        "about_your_company": res.DATA[0].c_about,
        "address": {
          "unit_number": "",
          "street_address": res.DATA[0].c_add,
          "city_town_village": res.DATA[0].c_city,
          "postal_code": res.DATA[0].c_postalcode,
          "province": res.DATA[0].c_province
        },
        "email": res.DATA[0].c_email,
        "cell_number": res.DATA[0].c_number,
        "office_number": res.DATA[0].c_number,
        "specialize_in": res.DATA[0].c_spec,
        "recent_works": rw_array,
        "gallery_images": gallery,
        "facebook": res.DATA[0].facebook_link,
        "linkedin": res.DATA[0].linked_in,
        "twitter": res.DATA[0].twitter_link,
        "website": res.DATA[0].website
      }
      this.datatransfer.set_company_profile(JSON.stringify(company));
      this.router.navigate(['./dashboard']);
    }
  }

  open_choices(){
    var drop = this.element.nativeElement.querySelector('#drop_down');
    var box = this.element.nativeElement.querySelector('#province_text');
    if(window.innerWidth < 800){
      drop.style.height = "15vw";
    }else{
      drop.style.height = "100px";
    }
    box.style.borderBottomLeftRadius = "0";
    box.style.borderBottomRightRadius = "0";
    drop.style.border = "1px solid #b6b7b7";
    drop.style.borderTop = "none";
  }

  close_choices(){
    var drop = this.element.nativeElement.querySelector('#drop_down');
    var box = this.element.nativeElement.querySelector('#province_text');
    drop.style.height = "0";
    setTimeout(() => {
      box.style.borderBottomLeftRadius = "3px";
      box.style.borderBottomRightRadius = "3px";
      drop.style.border = "none";
    },350);
  }

  toggle_choices(){
    var drop = this.element.nativeElement.querySelector('#drop_down');
    if(drop.style.height == "100px" || drop.style.height == "15vw"){
      this.close_choices();
    }else{
      this.open_choices();
    }
  }

  change_province(i, event){
    event.stopPropagation();
    this.chosen_province = this.provinces[i];
    this.close_choices();
  }

  set_bottom_height(){
    var height = window.innerHeight;
    var value = height - 710;

    if(window.innerWidth < 800){
      return{
        "min-height": "3vw"
      };
    }else{
      return{
        "min-height": value + "px"
      };
    }

  }

  set_blur(event){
    if(event){
      this.container_id = "container_blur";
    }else{
      this.container_id = "container";
    }
  }

  remove_style(id){
    this.element.nativeElement.querySelector(id).removeAttribute("style");
  }

  contact_change(event){
    if(event.length == 4){
      this.cell_number = "(" + event.substr(0,3)+ ") " + event[3];
    }else if(event.length == 6){
      this.cell_number = event.substr(1,3);
    }else if(event.length == 10 && event[9] != "-"){
      this.cell_number = event.substr(0,9) + "-" + event[9];
    }else if(event.length == 10 && event[9] == "-"){
      this.cell_number = event.substr(0,9);
    }
  }

  set_predefined_area_code(){
    var code = this.element.nativeElement.querySelector('#predefined_area_code');
    code.style.opacity = 1;
  }

  remove_predefined_area_code(input){
    var code = this.element.nativeElement.querySelector('#predefined_area_code');

    if(input.length == 0){
      code.style.opacity = 0;
    }
  }

  style_predefined_area_code(input){
    if(input.length > 0){
      return{
        "opacity": 1
      }
    }else{
      return{
        "opacity": 0
      }
    }
  }

}

function currentYPosition() {
    // Firefox, Chrome, Opera, Safari
    if (self.pageYOffset) return self.pageYOffset;
    // Internet Explorer 6 - standards mode
    if (document.documentElement && document.documentElement.scrollTop)
        return document.documentElement.scrollTop;
    // Internet Explorer 6, 7 and 8
    if (document.body.scrollTop) return document.body.scrollTop;
    return 0;
}
function elmYPosition(eID) {
    var elm = document.getElementById(eID);
    var y = elm.offsetTop;
    var node = elm;
    while (node.offsetParent && node.offsetParent != document.body) {
        node = <HTMLElement><any>node.offsetParent;
        y += node.offsetTop;
    } return y;
}
