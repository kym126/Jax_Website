"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
require("./rxjs-operators");
var JsonFetch_service_1 = require("./JsonFetch.service");
var data_transfer_service_1 = require("./data_transfer.service");
var window_service_1 = require("./window.service");
var ng2_img_cropper_1 = require("ng2-img-cropper");
var UserHistoryComponent = (function () {
    function UserHistoryComponent(element, router, datatransfer, win, jsonfetch) {
        this.element = element;
        this.router = router;
        this.datatransfer = datatransfer;
        this.win = win;
        this.jsonfetch = jsonfetch;
        this.first_name = "";
        this.last_name = "";
        this.email = "";
        this.phone = "";
        this.old_pass = "";
        this.new_pass = "";
        this.c_new_pass = "";
        this.prof_pic = "./assets/images/icons/default_profile_pic.png";
        this.testimonial_title = "";
        this.testimonial_content = "";
        this.chosen_history = 0;
        this.range_1 = 0;
        this.range_2 = 0;
        this.range_3 = 0;
        this.range_4 = 0;
        this.word_value_1 = "Poor";
        this.word_value_2 = "Poor";
        this.word_value_3 = "Poor";
        this.word_value_4 = "Poor";
        this.review_title = "";
        this.review_content = "";
        this.review_rating = 0.0;
        this.review_rating_string = "0.0";
        this.review_images = [];
        this.review_images_file = [];
        this.event = [];
        this.userhistory_container_id = "userhistory_container";
        this.overlay_id = "overlay";
        this.lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
        this.pop_up_active = false;
        this.histories = []; /*[
      
          0: in progress,
          1: completed,
          2: requested ratings,
          3: already rated (can edit)
          {
            "image": "./assets/images/icons/avatar/barry_allen.png",
            "first_name": "Barry",
            "last_name": "Allen",
            "status": "In progress",
            "job": "Plumbing",
            "date": "3rd September 2016",
            "state": 0,
            "rating": 0.0,
            "review":{
                "range_1": 0,
                "range_2": 0,
                "range_3": 0,
                "range_4": 0,
                "rating": 0.0,
                "title": "",
                "content": "",
                "images":[]
            }
          },
          {
            "image": "./assets/images/icons/avatar/david_smith.png",
            "first_name": "David",
            "last_name": "Smith",
            "status": "Completed",
            "job": "Electrician",
            "date": "1st September 2016",
            "state": 1,
            "rating": 0.0,
            "review":{
                "range_1": 0,
                "range_2": 0,
                "range_3": 0,
                "range_4": 0,
                "rating": 0.0,
                "title": "",
                "content": "",
                "images":[]
            }
          },
          {
            "image": "./assets/images/icons/avatar/felicity_smoak.jpg",
            "first_name": "Felicity",
            "last_name": "Smoak",
            "status": "Completed",
            "job": "Painter",
            "date": "20th July 2016",
            "state": 2,
            "rating": 0.0,
            "review":{
                "range_1": 0,
                "range_2": 0,
                "range_3": 0,
                "range_4": 0,
                "rating": 0.0,
                "title": "",
                "content": "",
                "images":[]
            }
          },
          {
            "image": "./assets/images/icons/avatar/harrison_wells.png",
            "first_name": "Harrison",
            "last_name": "Wells",
            "status": "Completed",
            "job": "Gardening",
            "date": "10th July 2016",
            "state": 3,
            "rating": 2.8,
            "review":{
                "range_1": 2,
                "range_2": 5,
                "range_3": 1,
                "range_4": 3,
                "rating": 2.8,
                "title": "Run Barry, RUN!",
                "content": this.lorem,
                "images":[
                  "./assets/images/icons/avatar/barry_allen.png",
                  "./assets/images/icons/avatar/felicity_smoak.jpg"
                ]
            }
          },
        ];*/
        this.profile_error = "";
        this.pass_error = "";
        this.testimonial_error = "";
        this.review_errors = "";
        this.overlay_2_id = "overlay_2";
        if (this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() == "1") {
            this.first_name = this.datatransfer.get_user_first_name();
            this.last_name = this.datatransfer.get_user_last_name();
            this.email = this.datatransfer.get_user_email();
            this.phone = this.datatransfer.get_user_contact();
            this.data = {};
            if (this.datatransfer.is_homeowner_profile_image()) {
                this.data.image = 'http://jaxoftrades.com/api/' + this.datatransfer.get_homeowner_profile_image() + '?' + this.getRandomInt(90000000000, 100000000000);
            }
            if (this.datatransfer.get_testimonial() != null) {
                this.testimonial_content = this.datatransfer.get_testimonial();
            }
            this.window = win.nativeWindow;
            this.cropperSettings = new ng2_img_cropper_1.CropperSettings();
            this.cropperSettings.noFileInput = true;
            this.cropperSettings.width = 100;
            this.cropperSettings.height = 100;
            this.cropperSettings.croppedWidth = 100;
            this.cropperSettings.croppedHeight = 100;
            this.cropperSettings.canvasWidth = 500;
            this.cropperSettings.canvasHeight = 300;
            if (window.innerWidth < 800) {
                this.cropperSettings.canvasWidth = 600;
                this.cropperSettings.canvasHeight = 400;
            }
        }
        else if (this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() == "2") {
            if (this.datatransfer.get_tradesman_values_json() == null) {
                this.router.navigate(['./edit-profile']);
            }
            else {
                this.router.navigate(['./dashboard']);
            }
        }
        else if (!this.datatransfer.is_logged_in()) {
            this.router.navigate(['./home']);
        }
    }
    UserHistoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.datatransfer.set_load(true);
        this.jsonfetch.fetch_h_history(this.datatransfer.get_user_id())
            .map(function (res) { return res; })
            .subscribe(function (res) { return _this.history_recv(res); }, function (error) { return _this.history_error(error); });
    };
    UserHistoryComponent.prototype.history_recv = function (res) {
        this.datatransfer.set_load(false);
        console.log(res);
        if (res.STATUS == "OK") {
            for (var i = 0; i < res.DATA.length; i++) {
                var state = 0;
                var rating = 0.0;
                if (res.DATA[i].status == "In Progress") {
                    state = 0;
                }
                else if (res.DATA[i].avg_rating != null) {
                    state = 3;
                }
                else if (res.DATA[i].req_rating == 1) {
                    state = 2;
                }
                else {
                    state = 1;
                }
                var review = {
                    "range_1": 0,
                    "range_2": 0,
                    "range_3": 0,
                    "range_4": 0,
                    "rating": 0.0,
                    "title": "",
                    "content": "",
                    "images": [],
                    "images_file": []
                };
                var img_with_urls = [];
                if (state == 3) {
                    review.range_1 = res.DATA[i].rate1;
                    review.range_2 = res.DATA[i].rate2;
                    review.range_3 = res.DATA[i].rate3;
                    review.range_4 = res.DATA[i].rate4;
                    review.rating = parseFloat(res.DATA[i].avg_rating);
                    review.title = res.DATA[i].review_title;
                    review.content = res.DATA[i].review_desc;
                    review.images = img_with_urls;
                    review.images_file = res.DATA[i].review_image;
                    rating = parseFloat(res.DATA[i].avg_rating);
                    if (res.DATA[i].review_image != null) {
                        for (var x = 0; x < res.DATA[i].review_image.length; x++) {
                            img_with_urls.push('http://jaxoftrades.com/api/' + res.DATA[i].review_image[x] + "?" + this.getRandomInt(90000000000, 100000000000));
                        }
                    }
                }
                var history = {
                    "image": 'http://jaxoftrades.com/api/' + res.DATA[i].profile_imgpath + "?" + this.getRandomInt(90000000000, 100000000000),
                    "first_name": res.DATA[i].tr_first_name,
                    "last_name": res.DATA[i].tr_last_name,
                    "status": res.DATA[i].status,
                    "job": res.DATA[i].job_type,
                    "date": res.DATA[i].work_date,
                    "state": state,
                    "rating": rating,
                    "review": review,
                    "t_id": res.DATA[i].tradesmen_id,
                    "r_id": res.DATA[i].request_id,
                };
                this.histories.splice(0, 0, history);
            }
        }
    };
    UserHistoryComponent.prototype.history_error = function (error) {
        this.datatransfer.set_load(false);
        console.log(error);
    };
    UserHistoryComponent.prototype.ngAfterViewInit = function () {
        var width = window.innerWidth;
        var box = this.element.nativeElement.querySelector('#scroll_overlay');
        if (width < 800) {
            box.style.display = "block";
        }
        else {
            box.style.display = "none";
        }
    };
    UserHistoryComponent.prototype.set_profile_pic = function () {
        var image = this.element.nativeElement.querySelector('#profile_picture');
        if (this.data.image == null) {
            image.style.backgroundImage = "url('./assets/images/icons/default_profile_pic.png')";
        }
        else {
            image.style.backgroundImage = "url('" + this.data.image + "')";
        }
    };
    UserHistoryComponent.prototype.change_prof_pic = function (event) {
        var reader = new FileReader();
        var _this = this;
        var image = new Image();
        this.prev_image = this.data.image;
        if (this.data.image == null) {
            this.prev_image = "./assets/images/icons/default_profile_pic.png";
        }
        reader.onload = function (e) {
            var src = reader.result;
            image.src = src;
            _this.cropper.setImage(image);
            _this.prof_pic = src;
            _this.crop_image();
        };
        if (event.target.files[0] != null) {
            reader.readAsDataURL(event.target.files[0]);
            this.profile_pic_data = event.target.files[0];
        }
    };
    UserHistoryComponent.prototype.compute_width = function (rating, index) {
        if (index <= rating) {
            return {
                "width": "100%",
            };
        }
        else {
            if (rating - index < -1) {
                return {
                    "width": "0",
                };
            }
            else {
                var value = (1 + rating - index) * 100;
                return {
                    "width": value + "%",
                };
            }
        }
    };
    UserHistoryComponent.prototype.set_margin_top_bottom = function (i) {
        if (i == 0) {
            return {
                "margin-top": "5px",
                "margin-bottom": "15px"
            };
        }
        else if (i == this.histories.length - 1) {
            return {
                "margin-top": "15px",
                "margin-bottom": "5px"
            };
        }
    };
    UserHistoryComponent.prototype.remove_style = function (id) {
        this.element.nativeElement.querySelector(id).removeAttribute("style");
    };
    UserHistoryComponent.prototype.update_password = function () {
        var _this = this;
        var error = false;
        var old_pass = this.element.nativeElement.querySelector('#old_pass');
        var pass = this.element.nativeElement.querySelector('#new_pass');
        var new_pass = this.element.nativeElement.querySelector('#c_new_pass');
        this.pass_error = "";
        this.element.nativeElement.querySelector('#new_pass').removeAttribute("style");
        this.element.nativeElement.querySelector('#c_new_pass').removeAttribute("style");
        if (this.old_pass.length == 0) {
            this.pass_error = "*Please fill up all fields outlined with red*";
            old_pass.style.border = "1px solid #F27072";
            error = true;
        }
        if (this.new_pass.length == 0) {
            this.pass_error = "*Please fill up all fields outlined with red*";
            pass.style.border = "1px solid #F27072";
            error = true;
        }
        if (this.c_new_pass.length == 0) {
            this.pass_error = "*Please fill up all fields outlined with red*";
            new_pass.style.border = "1px solid #F27072";
            error = true;
        }
        if (this.new_pass != this.c_new_pass) {
            this.pass_error = "*The two new passwords don't match*";
            pass.style.border = "1px solid #F27072";
            new_pass.style.border = "1px solid #F27072";
            error = true;
        }
        if (error) {
            return;
        }
        this.datatransfer.set_load(true);
        this.jsonfetch.send_new_password(this.datatransfer.get_user_id(), this.old_pass, this.new_pass)
            .map(function (res) { return res; })
            .subscribe(function (res) { return _this.check_result_2(res); }, function (error) { return _this.error_happened_2(error); });
    };
    UserHistoryComponent.prototype.error_happened_2 = function (res) {
        this.datatransfer.set_load(false);
        this.pass_error = "*Please check your internet connection and try again*";
    };
    UserHistoryComponent.prototype.check_result_2 = function (res) {
        this.datatransfer.set_load(false);
        console.log(res);
        if (res.STATUS == "OK") {
            this.old_pass = "";
            this.new_pass = "";
            this.c_new_pass = "";
        }
        else {
            this.pass_error = "*" + res.ERR_MSG + "*";
        }
    };
    UserHistoryComponent.prototype.update_changes = function () {
        var _this = this;
        var error = false;
        var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        var testPhone = /^\(+([0-9]{3})+\)+\ +([0-9]{3})[-]?([0-9]{4})$/;
        var email_box = this.element.nativeElement.querySelector('#email');
        var fname_box = this.element.nativeElement.querySelector('#fname');
        var lname_box = this.element.nativeElement.querySelector('#lname');
        var contact_box = this.element.nativeElement.querySelector('#mobile_number');
        this.profile_error = "";
        if (this.first_name.length == 0) {
            this.profile_error = "*Please fill up all fields outlined with red*";
            fname_box.style.border = "1px solid #F27072";
            error = true;
        }
        if (this.last_name.length == 0) {
            this.profile_error = "*Please fill up all fields outlined with red*";
            lname_box.style.border = "1px solid #F27072";
            error = true;
        }
        if (this.email.length == 0) {
            this.profile_error = "*Please fill up all fields outlined with red*";
            email_box.style.border = "1px solid #F27072";
            error = true;
        }
        if (!testEmail.test(this.email)) {
            this.profile_error = "*Please use a valid email address*";
            email_box.style.border = "1px solid #F27072";
            error = true;
        }
        if (!testPhone.test(this.phone)) {
            this.profile_error = "*Please use a valid contact number*";
            contact_box.style.border = "1px solid #F27072";
            error = true;
        }
        if (error) {
            return;
        }
        this.datatransfer.set_load(true);
        var prof_url = null;
        var prof_img = null;
        if (this.data.image.substr(0, 7) == "http://") {
            prof_url = this.datatransfer.get_homeowner_profile_image();
        }
        else {
            prof_img = this.data.image;
        }
        console.log(this.data.image, prof_url);
        this.jsonfetch.send_homeowner_edit_profile(this.datatransfer.get_user_id(), this.email, this.first_name, this.last_name, this.phone, prof_img, prof_url)
            .map(function (res) { return res; })
            .subscribe(function (res) { return _this.check_result(res); }, function (error) { return _this.error_happened(error); });
    };
    UserHistoryComponent.prototype.error_happened = function (res) {
        this.datatransfer.set_load(false);
        this.profile_error = "*Please check your internet connection and try again*";
    };
    UserHistoryComponent.prototype.getRandomInt = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    UserHistoryComponent.prototype.check_result = function (res) {
        this.datatransfer.set_load(false);
        console.log(res);
        if (res.STATUS == "OK") {
            this.datatransfer.user_login(this.datatransfer.get_user_id(), this.first_name, this.last_name, this.email, 1, this.phone);
            this.datatransfer.set_homeowner_profile_image(res.profile_image);
            this.data.image = 'http://jaxoftrades.com/api/' + res.profile_image + '?' + this.getRandomInt(90000000000, 100000000000);
        }
        else {
            this.profile_error = "*" + res.Message + "*";
        }
    };
    UserHistoryComponent.prototype.clear_history = function () {
        var _this = this;
        var r_id = [];
        for (var i = 0; i < this.histories.length; i++) {
            if (this.histories[i].state == 3) {
                r_id.push(this.histories[i].r_id);
                //this.histories.splice(i,1);
                //i--;
            }
        }
        this.jsonfetch.clear_h_history(r_id).subscribe(function (res) { return _this.clear_history_2(res); }, function (error) { return _this.clear_history_err(error); });
    };
    UserHistoryComponent.prototype.clear_history_2 = function (res) {
        console.log(res);
        if (res.STATUS == "OK") {
            for (var i = 0; i < this.histories.length; i++) {
                if (this.histories[i].state == 3) {
                    this.histories.splice(i, 1);
                    i--;
                }
            }
        }
    };
    UserHistoryComponent.prototype.clear_history_err = function (error) {
        console.log(error);
    };
    UserHistoryComponent.prototype.delete_history = function (i) {
        var _this = this;
        var r_id = [];
        r_id.push(this.histories[i].r_id);
        this.jsonfetch.clear_h_history(r_id).subscribe(function (res) { return _this.clear_history_3(res, i); }, function (error) { return _this.clear_history_err(error); });
        this.histories.splice(i, 1);
    };
    UserHistoryComponent.prototype.clear_history_3 = function (res, i) {
        console.log(res);
        if (res.STATUS == "OK") {
            this.histories.splice(i, 1);
        }
    };
    UserHistoryComponent.prototype.convert_value_to_word = function (value) {
        if (value <= 5000) {
            return "Poor";
        }
        else if (value <= 15000) {
            return "Mediocre";
        }
        else if (value <= 25000) {
            return "Average";
        }
        else if (value <= 35000) {
            return "Good";
        }
        else if (value <= 45000) {
            return "Outstanding";
        }
        else {
            return "Excellent";
        }
    };
    UserHistoryComponent.prototype.magnet_value = function (value) {
        if (value <= 5000) {
            return 0;
        }
        else if (value <= 15000) {
            return 10000;
        }
        else if (value <= 25000) {
            return 20000;
        }
        else if (value <= 35000) {
            return 30000;
        }
        else if (value <= 45000) {
            return 40000;
        }
        else {
            return 50000;
        }
    };
    UserHistoryComponent.prototype.magnet_color_value_2 = function (value) {
        if (value <= 5000) {
            return "#FF474D";
        }
        else if (value <= 15000) {
            return "#FF6135";
        }
        else if (value <= 25000) {
            return "#FFCC33";
        }
        else if (value <= 35000) {
            return "#009800";
        }
        else if (value <= 45000) {
            return "#10D51C";
        }
        else {
            return "#86D5E3";
        }
    };
    UserHistoryComponent.prototype.magnet_color_value = function (value) {
        var color_1_red = 0;
        var color_1_yellow = 0;
        var color_1_blue = 0;
        var color_2_red = 0;
        var color_2_yellow = 0;
        var color_2_blue = 0;
        var mult_1 = 1;
        var mult_2 = 0;
        if (value <= 10000) {
            mult_1 = (10000 - value) / 10000;
            mult_2 = 1 - mult_1;
            color_1_red = 255 * mult_1;
            color_1_yellow = 71 * mult_1;
            color_1_blue = 77 * mult_1;
            color_2_red = 255 * mult_2;
            color_2_yellow = 110 * mult_2;
            color_2_blue = 53 * mult_2;
        }
        else if (value <= 20000) {
            mult_1 = (20000 - value) / 10000;
            mult_2 = 1 - mult_1;
            color_1_red = 255 * mult_1;
            color_1_yellow = 110 * mult_1;
            color_1_blue = 53 * mult_1;
            color_2_red = 118 * mult_2;
            color_2_yellow = 255 * mult_2;
            color_2_blue = 51 * mult_2;
        }
        else if (value <= 30000) {
            mult_1 = (30000 - value) / 10000;
            mult_2 = 1 - mult_1;
            color_1_red = 118 * mult_1;
            color_1_yellow = 255 * mult_1;
            color_1_blue = 51 * mult_1;
            color_2_red = 0 * mult_2;
            color_2_yellow = 152 * mult_2;
            color_2_blue = 152 * mult_2;
        }
        else if (value <= 40000) {
            mult_1 = (40000 - value) / 10000;
            mult_2 = 1 - mult_1;
            color_1_red = 0 * mult_1;
            color_1_yellow = 152 * mult_1;
            color_1_blue = 152 * mult_1;
            color_2_red = 16 * mult_2;
            color_2_yellow = 201 * mult_2;
            color_2_blue = 213 * mult_2;
        }
        else {
            mult_1 = (50000 - value) / 10000;
            mult_2 = 1 - mult_1;
            color_1_red = 16 * mult_1;
            color_1_yellow = 201 * mult_1;
            color_1_blue = 213 * mult_1;
            color_2_red = 86 * mult_2;
            color_2_yellow = 153 * mult_2;
            color_2_blue = 227 * mult_2;
        }
        var iRed = (color_1_red + color_2_red);
        var iYellow = (color_1_yellow + color_2_yellow);
        var iBlue = (color_1_blue + color_2_blue);
        var iWhite = Math.min(iRed, iYellow, iBlue);
        iRed -= iWhite;
        iYellow -= iWhite;
        iBlue -= iWhite;
        var iMaxYellow = Math.max(iRed, iYellow, iBlue);
        var iGreen = Math.min(iYellow, iBlue);
        iYellow -= iGreen;
        iBlue -= iGreen;
        if (iBlue > 0 && iGreen > 0) {
            iBlue *= 2.0;
            iGreen *= 2.0;
        }
        iRed += iYellow;
        iGreen += iYellow;
        var iMaxGreen = Math.max(iRed, iGreen, iBlue);
        if (iMaxGreen > 0) {
            var iN = iMaxYellow / iMaxGreen;
            iRed *= iN;
            iGreen *= iN;
            iBlue *= iN;
        }
        iRed += iWhite;
        iGreen += iWhite;
        iBlue += iWhite;
        var r_hex = Math.floor(iRed).toString(16);
        var g_hex = Math.floor(iGreen).toString(16);
        var b_hex = Math.floor(iBlue).toString(16);
        if (r_hex.length == 1) {
            r_hex = "0" + r_hex;
        }
        if (g_hex.length == 1) {
            g_hex = "0" + g_hex;
        }
        if (b_hex.length == 1) {
            b_hex = "0" + b_hex;
        }
        var hex_code = "#" + r_hex + g_hex + b_hex;
        return hex_code;
    };
    UserHistoryComponent.prototype.compute_ave_rating = function () {
        var num = 0;
        num = this.range_1 / 4000 + this.range_2 / 4000 + this.range_3 / 4000 + this.range_4 / 4000;
        num = 5 * Math.round(num / 5);
        this.review_rating = (num / 10);
        this.review_rating_string = this.review_rating.toFixed(1);
    };
    UserHistoryComponent.prototype.set_range_value_1 = function (event) {
        this.range_1 = event;
        var val = this.range_1 / 50000;
        var track = this.element.nativeElement.querySelector('#range_1');
        track.style.backgroundImage = "-webkit-gradient(linear, left top, right top, " + "color-stop(" + val + "," + this.magnet_color_value(event) + " ), " + "color-stop(" + val + ", #fcfcfc)" + ")";
        this.word_value_1 = this.convert_value_to_word(event);
        this.compute_ave_rating();
    };
    UserHistoryComponent.prototype.saverange_1 = function () {
        this.set_range_value_1(this.magnet_value(this.range_1));
    };
    UserHistoryComponent.prototype.set_range_value_2 = function (event) {
        this.range_2 = event;
        var val = this.range_2 / 50000;
        var track = this.element.nativeElement.querySelector('#range_2');
        track.style.backgroundImage = "-webkit-gradient(linear, left top, right top, " + "color-stop(" + val + "," + this.magnet_color_value(event) + " ), " + "color-stop(" + val + ", #fcfcfc)" + ")";
        this.word_value_2 = this.convert_value_to_word(event);
        this.compute_ave_rating();
    };
    UserHistoryComponent.prototype.saverange_2 = function () {
        this.set_range_value_2(this.magnet_value(this.range_2));
    };
    UserHistoryComponent.prototype.set_range_value_3 = function (event) {
        this.range_3 = event;
        var val = this.range_3 / 50000;
        var track = this.element.nativeElement.querySelector('#range_3');
        track.style.backgroundImage = "-webkit-gradient(linear, left top, right top, " + "color-stop(" + val + "," + this.magnet_color_value(event) + " ), " + "color-stop(" + val + ", #fcfcfc)" + ")";
        this.word_value_3 = this.convert_value_to_word(event);
        this.compute_ave_rating();
    };
    UserHistoryComponent.prototype.saverange_3 = function () {
        this.set_range_value_3(this.magnet_value(this.range_3));
    };
    UserHistoryComponent.prototype.set_range_value_4 = function (event) {
        this.range_4 = event;
        var val = this.range_4 / 50000;
        var track = this.element.nativeElement.querySelector('#range_4');
        track.style.backgroundImage = "-webkit-gradient(linear, left top, right top, " + "color-stop(" + val + "," + this.magnet_color_value(event) + " ), " + "color-stop(" + val + ", #fcfcfc)" + ")";
        this.word_value_4 = this.convert_value_to_word(event);
        this.compute_ave_rating();
    };
    UserHistoryComponent.prototype.saverange_4 = function () {
        this.set_range_value_4(this.magnet_value(this.range_4));
    };
    UserHistoryComponent.prototype.add_image_to_review = function (event) {
        console.log(this.review_images.length);
        var input = this.element.nativeElement.querySelector('#add_review_photo');
        this.event.length = 0;
        /*if(input.files.length + this.rw_images.length > 3){
          this.rw_images_error = "*You tried to upload more then 3 images. Maximum of 3 images per recent work.*";
        }*/
        for (var j = 0; j < input.files.length && j < 3 - this.review_images.length; j++) {
            this.event.push(input.files[j]);
        }
        var _this = this;
        var i = 0;
        if (this.event.length != 0) {
            this.push_review_image(i, this);
        }
    };
    UserHistoryComponent.prototype.push_review_image = function (i, _this) {
        var reader = new FileReader();
        reader.onloadend = function (e) {
            var src = reader.result;
            _this.review_images.push(src);
            i++;
            if (i < _this.event.length) {
                _this.push_review_image(i, _this);
            }
        };
        _this.review_images_file.push(_this.event[i]);
        reader.readAsDataURL(_this.event[i]);
    };
    UserHistoryComponent.prototype.review_choose_bg = function (i) {
        return {
            "background-image": "url('" + this.review_images[i] + "')"
        };
    };
    UserHistoryComponent.prototype.close_pop_up = function () {
        var _this = this;
        var pop_up = this.element.nativeElement.querySelector('#rating_pop_up');
        pop_up.style.opacity = 0;
        setTimeout(function () {
            _this.userhistory_container_id = "userhistory_container";
            _this.overlay_id = "overlay";
            _this.pop_up_active = false;
            _this.review_errors = "";
        }, 350);
    };
    UserHistoryComponent.prototype.close_pop_up_2 = function () {
        var _this = this;
        var pop_up = this.element.nativeElement.querySelector('#crop_pop_up');
        pop_up.style.opacity = 0;
        setTimeout(function () {
            _this.userhistory_container_id = "userhistory_container";
            _this.overlay_2_id = "overlay_2";
        }, 350);
    };
    UserHistoryComponent.prototype.crop_image = function () {
        this.userhistory_container_id = "userhistory_container_blurred";
        this.overlay_2_id = "overlay_2_active";
        var pop_up = this.element.nativeElement.querySelector('#crop_pop_up');
        pop_up.style.opacity = 0;
        setTimeout(function () {
            pop_up.style.opacity = 1;
        }, 10);
    };
    UserHistoryComponent.prototype.rate_work = function (i) {
        this.chosen_history = i;
        this.pop_up_active = true;
        this.userhistory_container_id = "userhistory_container_blurred";
        this.overlay_id = "overlay_active";
        var pop_up = this.element.nativeElement.querySelector('#rating_pop_up');
        pop_up.style.opacity = 0;
        setTimeout(function () {
            pop_up.style.opacity = 1;
        }, 10);
        this.review_images.length = 0;
        this.review_images_file.length = 0;
        if (this.histories[i].state == 3) {
            this.set_range_value_1(this.histories[this.chosen_history].review.range_1 * 10000);
            this.set_range_value_2(this.histories[this.chosen_history].review.range_2 * 10000);
            this.set_range_value_3(this.histories[this.chosen_history].review.range_3 * 10000);
            this.set_range_value_4(this.histories[this.chosen_history].review.range_4 * 10000);
            this.review_title = this.histories[this.chosen_history].review.title;
            this.review_content = this.histories[this.chosen_history].review.content;
            this.review_rating = this.histories[this.chosen_history].review.rating;
            this.review_rating_string = (this.histories[this.chosen_history].review.rating).toFixed(1);
            for (var j = 0; j < this.histories[this.chosen_history].review.images.length; j++) {
                this.review_images.push(this.histories[this.chosen_history].review.images[j]);
                this.review_images_file.push(this.histories[this.chosen_history].review.images_file[j]);
            }
        }
        else {
            this.set_range_value_1(0);
            this.set_range_value_2(0);
            this.set_range_value_3(0);
            this.set_range_value_4(0);
            this.review_title = "";
            this.review_content = "";
            this.review_rating = 0.0;
            this.review_rating_string = "0.0";
        }
        this.event.length = 0;
    };
    UserHistoryComponent.prototype.save_review = function () {
        var _this = this;
        var images = [];
        this.review_errors = "";
        for (var i = 0; i < this.review_images.length; i++) {
            images.push(this.review_images[i]);
        }
        if (images.length != 0 && (this.review_title.length == 0 || this.review_content.length == 0)) {
            this.review_errors = "*Please provide a title and content to your review*";
            return;
        }
        if (images.length != 0 && this.review_title.length == 0 && this.review_content.length != 0) {
            this.review_errors = "*Please provide a title to your review*";
            return;
        }
        if (images.length == 0 && this.review_title.length == 0 && this.review_content.length != 0) {
            this.review_errors = "*Please provide a title to your review*";
            return;
        }
        this.datatransfer.set_load(true);
        console.log(this.review_images_file);
        this.jsonfetch.review_a_tradesman(this.datatransfer.get_user_id(), this.histories[this.chosen_history].t_id, this.range_1 / 10000, this.range_2 / 10000, this.range_3 / 10000, this.range_4 / 10000, this.review_rating, this.histories[this.chosen_history].job, this.review_title, this.review_content, this.review_images_file, this.histories[this.chosen_history].r_id)
            .subscribe(function (tradesman) { return _this.save_review_2(tradesman); }, function (error) { return _this.save_review_error(error); });
    };
    UserHistoryComponent.prototype.save_review_error = function (error) {
        this.datatransfer.set_load(false);
        console.log(error);
    };
    UserHistoryComponent.prototype.save_review_2 = function (res) {
        this.datatransfer.set_load(false);
        console.log(res);
        if (res.STATUS = "Update Successful") {
            var img_with_urls = [];
            for (var x = 0; x < res.DATA[0].review_image.length; x++) {
                img_with_urls.push('http://jaxoftrades.com/api/' + res.DATA[0].review_image[x] + "?" + this.getRandomInt(90000000000, 100000000000));
            }
            var history = {
                "image": this.histories[this.chosen_history].image,
                "first_name": this.histories[this.chosen_history].first_name,
                "last_name": this.histories[this.chosen_history].last_name,
                "status": this.histories[this.chosen_history].status,
                "job": this.histories[this.chosen_history].job,
                "date": this.histories[this.chosen_history].date,
                "state": 3,
                "rating": this.review_rating,
                "review": {
                    "range_1": this.range_1 / 10000,
                    "range_2": this.range_2 / 10000,
                    "range_3": this.range_3 / 10000,
                    "range_4": this.range_4 / 10000,
                    "rating": this.review_rating,
                    "title": this.review_title,
                    "content": this.review_content,
                    "images": img_with_urls,
                    "images_file": res.DATA[0].review_image
                },
                "t_id": this.histories[this.chosen_history].t_id,
                "r_id": this.histories[this.chosen_history].r_id
            };
        }
        this.histories.splice(this.chosen_history, 1, history);
        this.close_pop_up();
    };
    UserHistoryComponent.prototype.delete_review_image = function (i) {
        this.review_images.splice(i, 1);
        this.review_images_file.splice(i, 1);
    };
    UserHistoryComponent.prototype.contact_change = function (event) {
        if (event.length == 4) {
            this.phone = "(" + event.substr(0, 3) + ") " + event[3];
        }
        else if (event.length == 6) {
            this.phone = event.substr(1, 3);
        }
        else if (event.length == 10 && event[9] != "-") {
            this.phone = event.substr(0, 9) + "-" + event[9];
        }
        else if (event.length == 10 && event[9] == "-") {
            this.phone = event.substr(0, 9);
        }
    };
    UserHistoryComponent.prototype.smoothScroll = function (eID) {
        var startY = currentYPosition();
        var stopY = elmYPosition(eID) - 72;
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            this.window.window.scrollTo(0, stopY);
            return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20)
            speed = 20;
        var step = Math.round(distance / 100);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for (var i = startY; i < stopY; i += step) {
                this.scrollTo(leapY, timer * speed);
                leapY += step;
                if (leapY > stopY)
                    leapY = stopY;
                timer++;
            }
            return;
        }
        for (var i = startY; i > stopY; i -= step) {
            this.scrollTo(leapY, timer * speed);
            leapY -= step;
            if (leapY < stopY)
                leapY = stopY;
            timer++;
        }
    };
    UserHistoryComponent.prototype.scrollTo = function (yPoint, duration) {
        var _this = this;
        setTimeout(function () {
            _this.window.window.scrollTo(0, yPoint);
        }, duration);
        return;
    };
    UserHistoryComponent.prototype.set_scroll_overlay = function (event) {
        var height = window.innerHeight;
        var width = window.innerWidth;
        var currY = currentYPosition();
        var stopY = elmYPosition('history_container') - (.5 * (height));
        var box = this.element.nativeElement.querySelector('#scroll_overlay');
        if (width < 800) {
            if (currY < stopY) {
                box.style.display = "block";
            }
            else {
                box.style.display = "none";
            }
        }
        else {
            box.style.display = "none";
        }
    };
    UserHistoryComponent.prototype.send_testimonial = function () {
        var _this = this;
        this.testimonial_error = "";
        if (this.testimonial_content.length == 0) {
            this.testimonial_error = "*Please provide content for your testimonial*";
        }
        else {
            this.datatransfer.set_load(true);
            this.jsonfetch.testimonial(1, this.datatransfer.get_user_id(), this.testimonial_content, this.datatransfer.get_user_first_name(), this.datatransfer.get_user_last_name())
                .map(function (res) { return res; })
                .subscribe(function (res) { return _this.check_result_3(res); }, function (error) { return _this.error_happened_3(error); });
        }
    };
    UserHistoryComponent.prototype.error_happened_3 = function (res) {
        this.datatransfer.set_load(false);
        console.log(res);
        this.testimonial_error = "*Please check your internet connection and try again*";
    };
    UserHistoryComponent.prototype.check_result_3 = function (res) {
        this.datatransfer.set_load(false);
        console.log(res);
        if (res.STATUS == "OK") {
            this.datatransfer.set_testimonial(this.testimonial_content);
        }
        else {
            this.testimonial_error = "*" + res.ERR_MSG + "*";
        }
    };
    __decorate([
        core_1.ViewChild(ng2_img_cropper_1.ImageCropperComponent),
        __metadata("design:type", ng2_img_cropper_1.ImageCropperComponent)
    ], UserHistoryComponent.prototype, "cropper", void 0);
    UserHistoryComponent = __decorate([
        core_1.Component({
            selector: 'tradesman',
            templateUrl: './app/HTML/userhistory.html',
            styleUrls: ['css/userhistory.css'],
            host: { '(window:scroll)': 'set_scroll_overlay($event)' },
        }),
        __metadata("design:paramtypes", [core_1.ElementRef, router_1.Router, data_transfer_service_1.DataTransferService, window_service_1.WindowRef, JsonFetch_service_1.JsonFetchService])
    ], UserHistoryComponent);
    return UserHistoryComponent;
}());
exports.UserHistoryComponent = UserHistoryComponent;
function currentYPosition() {
    // Firefox, Chrome, Opera, Safari
    if (self.pageYOffset)
        return self.pageYOffset;
    // Internet Explorer 6 - standards mode
    if (document.documentElement && document.documentElement.scrollTop)
        return document.documentElement.scrollTop;
    // Internet Explorer 6, 7 and 8
    if (document.body.scrollTop)
        return document.body.scrollTop;
    return 0;
}
function elmYPosition(eID) {
    var elm = document.getElementById(eID);
    var y = elm.offsetTop;
    var node = elm;
    while (node.offsetParent && node.offsetParent != document.body) {
        node = node.offsetParent;
        y += node.offsetTop;
    }
    return y;
}
//# sourceMappingURL=userhistory.component.js.map