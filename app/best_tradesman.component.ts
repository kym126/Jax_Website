import { Component, ElementRef, Input, trigger, state, style, transition, animate, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import {Subscription} from 'rxjs/Subscription';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';
import { RequestFloatBoxComponent } from './request_float_box.component';
import { AdsComponent } from './ads.component';
import { SubCategoriesService } from './subcategories.service';


@Component({
  selector: 'best-tradesman',
  templateUrl: './app/HTML/best_tradesman.html',
  styleUrls: ['css/best_tradesman.css']
})

export class BestTradesmanComponent {
  best_tradesmen = [];/*[
    {
      "id": 0,
      "name": "Oliver Queen",
      "first_name": "Oliver",
      "last_name": "Queen",
      "sub_category": "Home/Commercial",
      "company": "Jax of Trades",
      "image": "./assets/images/icons/avatar/oliver_queen.jpg",
      "rating": 4.8,
      "location": 2,
      "availability": "Available from 6am to 5pm",
      "job": "Plumber"
    },
    {
      "id": 0,
      "name": "Harrison Wells",
      "first_name": "Harrison",
      "last_name": "Wells",
      "sub_category": "Home/Commercial",
      "company": "Jax of Trades",
      "image": "./assets/images/icons/avatar/harrison_wells.png",
      "rating": 4.8,
      "location": 2,
      "availability": "Available from 6am to 5pm",
      "job": "Electrician"
    },
    {
      "id": 0,
      "name": "John Diggle",
      "first_name": "John",
      "last_name": "Diggle",
      "sub_category": "Home/Commercial",
      "company": "",
      "image": "./assets/images/icons/avatar/john_diggle.png",
      "rating": 4.8,
      "location": 2,
      "availability": "Available from 6am to 5pm",
      "job": "Electrician"
    },
    {
      "id": 0,
      "name": "Felicity Smoak",
      "first_name": "Felicity",
      "last_name": "Smoak",
      "sub_category": "Home/Commercial",
      "company": "Jax of Trades",
      "image": "./assets/images/icons/avatar/felicity_smoak.jpg",
      "rating": 4.7,
      "location": 3,
      "availability": "Available from 6am to 5pm",
      "job": "Carpenter"
    },
    {
      "id": 0,
      "name": "Barry Allen",
      "first_name": "Barry",
      "last_name": "Allen",
      "sub_category": "Home/Commercial",
      "company": "Jax of Trades",
      "image": "./assets/images/icons/avatar/barry_allen.png",
      "rating": 4.9,
      "location": 2,
      "availability": "Available from 6am to 5pm",
      "job": "Painter"
    },
    {
      "id": 0,
      "name": "Joe West",
      "first_name": "Joe",
      "last_name": "West",
      "sub_category": "Home/Commercial",
      "company": "",
      "image": "./assets/images/icons/avatar/joe_west.png",
      "rating": 4.8,
      "location": 2,
      "availability": "Available from 6am to 5pm",
      "job": "Mover"
    },
    {
      "id": 0,
      "name": "James Gordon",
      "first_name": "Gordon",
      "last_name": "Gordon",
      "sub_category": "Home/Commercial",
      "company": "",
      "image": "./assets/images/icons/avatar/James_Gordon.jpg",
      "rating": 4.5,
      "location": 4.4,
      "availability": "Available from 6am to 5pm",
      "job": "Roofing"
    },
    {
      "id": 0,
      "name": "Oswald Cobblepot",
      "first_name": "Oswald",
      "last_name": "Cobblepot",
      "sub_category": "Home/Commercial",
      "company": "",
      "image": "./assets/images/icons/avatar/oswald.jpg",
      "rating": 4.0,
      "location": 1.4,
      "availability": "Available from 6am to 5pm",
      "job": "Flooring"
    },
    {
      "id": 0,
      "name": "Iris West",
      "first_name": "Iris",
      "last_name": "West",
      "sub_category": "Home/Commercial",
      "company": "",
      "image": "./assets/images/icons/avatar/iris_west.jpg",
      "rating": 4.5,
      "location": 3.6,
      "availability": "Available from 6am to 5pm",
      "job": "General Labour"
    }
  ];*/

  tradesmanlist_container_id = "tradesmanlist_container";
  overlay_id= "overlay";
  public DeleteFunction: Function;
  category = "";

  name = "";
  f_name = "";
  l_name = "";
  t_id= 0;
  pic = "";
  array_index = 0;
  array_id = 0;

  ads_array = [];
  choice = 0;

  temp_type = 0;
  temp_first = "";
  temp_last = "";
  temp_image = "";
  temp_company = "";
  temp_avail = "";
  temp_rating = "";
  temp_location = "";
  temp_sub_cat = "";
  temp_id = 0;
  temp_job = "";
  chosen_sub_category = "";

  ad_1:any;
  ad_2:any;

  @ViewChild(RequestFloatBoxComponent) request:RequestFloatBoxComponent;
  @ViewChild(AdsComponent) ads:AdsComponent;

  constructor(private element: ElementRef, private jsonfetch: JsonFetchService, public router: Router, private datatransfer:DataTransferService, private sub_categories: SubCategoriesService) {
    if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "2"){
      if(this.datatransfer.get_tradesman_values_json() == null){
        this.router.navigate(['./edit-profile']);
      }else{
        this.router.navigate(['./dashboard']);
      }
    }
    this.DeleteFunction= this.remove_item.bind(this);
  }

  ngOnInit(){
    this.datatransfer.set_load(true);
    this.jsonfetch.best_tradesman_list()
     .subscribe(
       res => this.lol(res),
       error =>  this.error_happened(error));
  }

  ngAfterViewInit(){
    if(this.datatransfer.get_request_item() != null){
      this.overlay_id = "overlay_active";
      this.datatransfer.set_blur(true);
      this.tradesmanlist_container_id = "tradesmanlist_container_blurred";
    }
  }

  lol(res){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      for(var i = 0; i < res.DATA.length; i++){
        var sub_category = "";
        var location:any = null;
        var company:any = null;
        var rating:any = null;

        if(res.DATA[i].rating != null){
          rating = parseFloat(res.DATA[i].rating);
        }

        if(res.DATA[i].company_name != null){
          company = res.DATA[i].company_name;
        }

        if(res.DATA[i].location != null){
          location = res.DATA[i].location;
        }

        for(var y = 0; y < res.DATA[i].job_sub_type.length; y++){
          if(y != 0){
            sub_category += " | ";
          }
          sub_category += res.DATA[i].job_sub_type[y];
        }

        var tradesman = {
          "id": res.DATA[i].tradesmen_id,
          "name": res.DATA[i].tr_first_name + " " + res.DATA[i].tr_last_name,
          "first_name": res.DATA[i].tr_first_name,
          "last_name": res.DATA[i].tr_last_name,
          "company": company,
          "image": res.DATA[i].profile_imgpath,
          "sub_category": sub_category,
          "rating": rating,
          "location": location,
          "availability": "",
          "job": res.DATA[i].job_type
        }

        this.best_tradesmen.push(tradesman);
      }
    }
  }

  error_happened(error){
    this.datatransfer.set_load(false);
    console.log(error);
  }

  compute_width(rating, index){
    if(index <= rating){
      return{
        "width": "98%",
        "margin-left": "1%",
        "margin-right": "1%"
      };
    }else{
      if(rating- index < -1){
        return{
          "width": "0",
          "margin-left": "1%",
          "margin-right": "1%"
        };
      }else{
        var value = (1 + rating- index)*98;
        return{
          "width": value + "%",
          "margin-left": "1%",
          "margin-right": "1%"
        };
      }
    }
  }

  set_off_on(id){
    if(id%2 == 0){
      return{
        "background-color": "#47b649"
      };
    }else{
      return{
        "background-color": "#f5845d"
      };
    }
  }

  view_profile(type, first_name, last_name, image, company, availability, rating, location, sub_category, id, job){
    this.choice = 1;
    this.f_name = first_name;
    this.l_name = last_name;
    this.t_id = id;
    this.category = job;
    if(this.datatransfer.is_logged_in()){
      this.datatransfer.clear_delete_id();
      var cat_array = []
      this.datatransfer.delete_data_local_storage();
      if(type == 0){
        this.datatransfer.set_category(job);
        this.datatransfer.set_sub_categories(JSON.stringify(cat_array));
        this.datatransfer.remove_service_time();
      }
      this.datatransfer.set_data(type, first_name, last_name, image, company, availability, rating, location, sub_category, id, job);
      this.router.navigate(['./tradesman']);
    }else{
      this.temp_type = type;
      this.temp_first = first_name;
      this.temp_last = last_name;
      this.temp_image = image;
      this.temp_company = company;
      this.temp_avail = availability;
      this.temp_rating = rating;
      this.temp_location = location;
      this.temp_sub_cat = sub_category;
      this.temp_id = id;
      this.temp_job = job;
      this.overlay_id="overlay_active";
      this.datatransfer.set_blur(true);
      this.tradesmanlist_container_id = "tradesmanlist_container_blurred";
      this.request.request_pop();
    }
  }

  remove_item(){
  }

  request_pop(first, last, index, id, img, sub_category, job){
    this.choice = 0;
    this.name = first + " " + last;
    this.pic = img;
    this.chosen_sub_category = sub_category;
    this.array_index = id;
    this.array_id = id;
    this.overlay_id="overlay_active";
    this.datatransfer.set_blur(true);
    this.tradesmanlist_container_id = "tradesmanlist_container_blurred";
    this.category = job;
    this.request.request_pop();
  }

  close_request(){
    if(this.request.element.nativeElement.querySelector('#request_container').style.display == "none"){
      this.close();
    }
  }

  close(){
    this.request.close_request();
    setTimeout(() => {
      this.overlay_id="overlay";
      this.tradesmanlist_container_id="tradesmanlist_container";
      this.datatransfer.set_blur(false);
    },350);
  }

  box_closed(event){
    if(event){
      this.close();
    }else{
      this.request.close_request();
      setTimeout(() => {
        this.overlay_id="overlay";
        this.tradesmanlist_container_id="tradesmanlist_container";
        this.datatransfer.set_blur(false);
        setTimeout(() => {
          this.view_profile(this.temp_type, this.temp_first, this.temp_last, this.temp_image, this.temp_company,
                            this.temp_avail, this.temp_rating, this.temp_location, this.temp_sub_cat, this.temp_id, this.temp_job);
        },50);
      },350);
    }
  }

  ads_array_1(i){
    this.ad_1 = this.ads.ads_pool_copy[(((i/4)-1)*2)%this.ads.ads_pool_copy.length];
    if(this.ad_1.state == 1){
      return true;
    }else{
      return false;
    }
  }

  ads_array_2(i){
    this.ad_2 = this.ads.ads_pool_copy[((((i/4)-1)*2)+1)%this.ads.ads_pool_copy.length];
    if(this.ad_2.state == 1){
      return true;
    }else{
      return false;
    }
  }
}
