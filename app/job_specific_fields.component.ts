import { Component, ElementRef, Input, Output, EventEmitter, ViewChildren, QueryList, ViewChild, NgZone } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';
import { LoginFloatBoxComponent } from './login_float_box.component';
import { TimeInputComponent } from './time_input.component';
import { EditProfileComponent_Personal } from './editprofilepersonal.component';
import { SubCategoriesService } from './subcategories.service';

@Component({
  selector: 'job-specific-fields',
  templateUrl: './app/HTML/job_specific_fields.html',
  styleUrls: ['css/edit_profile.css'],

})

export class JobSpecificFieldsComponent {

  @ViewChildren(TimeInputComponent) Time_Components: QueryList<TimeInputComponent>;

  @Output()
  blur = new EventEmitter();

  @Input() company_name = "";
  @Input() office_number = "";
  @Input() job = "";
  @Input() sub_categs = [];

  sub_categories = [];

  add_skill_error = "";
  new_skill = "";
  @Input() location = "";

  @Input() start_hour = "10";
  @Input() start_minute = "00";
  @Input() start_am_pm = "am";

  @Input() end_hour = "05";
  @Input() end_minute = "00";
  @Input() end_am_pm = "pm";

  pre_skills = ["Skill 1", "Skill 2", "Skill 3", "Skill 4", "Skill 5", "Skill 6", "Skill 7", "Skill 8"];

  @Input() chosen_pre_skill=[];
  @Input() added_skills = [];
  @Input() gallery_images = [];
  @Input() gallery_images_file = [];

  gallery_images_to_display = [];

  add_image_id = "add_image_centered";
  event = [];

  rw_title = "";
  rw_description = "";
  rw_images = [];
  rw_images_file = [];
  rw_cost = "";
  temp_rw_images: any;
  temp_rw_images_file: any;
  @Input() rw_array = [];

  temp_edit_rw: any;
  temp_edit_rw_images = [];
  temp_edit_rw_images_file = [];
  temp_edit_rw_image_url = [];

  edit_rw_img_array = [];
  edit_rw_img_array_file = [];
  edit_rw_title = "";
  edit_rw_description = "";
  edit_rw_chosen_bg = 0;
  edit_rw_cost = "";
  edit_rw_image_url = [];

  edit_rw_images_error = "";

  rw_title_error = "";
  rw_description_error = "";
  rw_images_error = "";

  @Input() facebook = "";
  @Input() linkedin = "";
  @Input() website = "";
  @Input() twitter = "";

  add_recent_works_id = "add_recent_works_active";

  overlay_id = "overlay_hide";
  container_id = "container";
  jobs = [];

  location_error = "";
  time_error = "";
  rw_to_delete = 0;

  constructor(private element: ElementRef, public router: Router, private datatransfer:DataTransferService, private jsonfetch: JsonFetchService, private zone: NgZone,  private sub_cat: SubCategoriesService) {
    if(this.datatransfer.is_tradesman_profile()){
      this.jobs = JSON.parse(this.datatransfer.get_tradesman_values_json()).jobs;
    }

    if (this.company_name == ""){
      this.company_name = this.datatransfer.get_company_name();
    }

    if (this.office_number == "" && this.datatransfer.is_office_number()){
      this.office_number = this.datatransfer.get_office_number();
    }

    var data = this.datatransfer.get_new_tm_loc();
    if(data != null){
      this.location = data;
    }
  }

  ngAfterViewInit(){
    setTimeout(() => {
      this.sub_categories = this.sub_cat.get_row_for_category(this.job);
      if(this.rw_array.length != 0){
          this.add_recent_works_id = "add_recent_works_hide";
      }

      if(this.gallery_images.length != 0){
          this.add_image_id = "add_image_float";
      }

      for(var i = 0; i < this.chosen_pre_skill.length; i++){
        for(var j = 0; j < this.pre_skills.length; j++){
          if(this.pre_skills[j] == this.chosen_pre_skill[i]){
            var chk_box = this.element.nativeElement.querySelector('#pre_skill_chk_' + j);
            chk_box.src = "./assets/images/check_box_v2.png";
          }
        }
      }

      for(var x = 0; x < this.gallery_images.length; x++){
        this.gallery_images_file.push(this.gallery_images[x]);
        this.gallery_images_to_display.push('http://jaxoftrades.com/api/' + this.gallery_images[x] + "?" + this.getRandomInt(90000000000, 100000000000));
      }

      setTimeout(() => {
        if(this.jobs.length > 1){
          for(var i = 0; i < this.sub_categs.length; i++){
            for(var j = 0; j < this.sub_categories.length; j++){
              if(this.sub_categs[i] == this.sub_categories[j]){
                var checkbox = this.element.nativeElement.querySelector('#check_box_' + j);
                checkbox.src="./assets/images/check_box_v2.png";
              }
            }
          }
        }
      },10);
    },10);
  }

  toggle_pre_skill(value){
    var chk_box = this.element.nativeElement.querySelector('#pre_skill_chk_' + value);
    for(var i = 0; i < this.chosen_pre_skill.length; i++){
      if(this.chosen_pre_skill[i] == this.pre_skills[value]){
        chk_box.src = "./assets/images/uncheck_box.png";
        this.chosen_pre_skill.splice(i, 1);
        return;
      }
    }

    this.chosen_pre_skill.push(this.pre_skills[value]);
    chk_box.src = "./assets/images/check_box_v2.png";
  }

  add_skill(value){
    var error = this.element.nativeElement.querySelector('#add_skill_error');
    this.new_skill = "";
    if(value.length == 0){
      this.add_skill_error = "You cannot add a blank skill";
      error.style.opacity = 1;
      return;
    }

    for(var i = 0; i < this.added_skills.length; i ++){
      if(this.added_skills[i] == value){
        this.add_skill_error = "You cannot add a duplicate skill";
        error.style.opacity = 1;
        return;
      }
    }

    this.added_skills.push(value);
    error.style.opacity = 0;
  }

  delete_skill(index){
    this.added_skills.splice(index, 1);
  }

  add_skill_keypress(value){
    if(value == 13){
      this.add_skill(this.new_skill);
    }
  }

  onDrop(event: any) {
    event.preventDefault();
    event.stopPropagation();
    this.event.length = 0;
    var imageTypes = ['image/png', 'image/gif', 'image/bmp', 'image/jpg'];

    for(var j = 0; j < event.dataTransfer.files.length; j ++){
      var fileType = event.dataTransfer.files[j].type;
      if (imageTypes.includes(fileType)) {
        this.event.push(event.dataTransfer.files[j]);
      }
    }

    var _this = this;
    var i = 0;

    if(this.event.length != 0){
      this.push_image(i, this);
    }

  }

  add_image_to_gallery(event){
    var input = this.element.nativeElement.querySelector('#addpicbtn');

    this.event.length = 0;

    for(var j = 0; j < input.files.length; j ++){
      this.event.push(input.files[j]);
    }

    var _this = this;
    var i = 0;

    if(this.event.length != 0){
      this.push_image(i, this);
    }
  }

  push_image(i, _this){

    var reader = new FileReader();
    reader.onloadend = function(e) {
        var src = reader.result;
        _this.gallery_images_file.push(_this.event[i]);
        _this.gallery_images_to_display.push(src);
        _this.gallery_images.push(src);
        _this.add_image_id = "add_image_float";
        i++;
        if(i < _this.event.length){
          _this.push_image(i, _this);
        }
    };
    reader.readAsDataURL(_this.event[i]);
  }

  onDrag(event: any) {
    event.preventDefault();
  }

  getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }


  gallery_choose_bg(i){
    return{
      "background-image": "url('" + this.gallery_images_to_display[i] + "')"
    }
  }

  remove_image(i){
    this.gallery_images.splice(i, 1);
    this.gallery_images_file.splice(i, 1);
    this.gallery_images_to_display.splice(i, 1);
    if(this.gallery_images.length == 0){
      this.add_image_id = "add_image_centered";
    }
  }

  hide_images_section(){
    if(this.rw_images_error.length <= 2 && this.rw_images.length == 0){
      return true;
    }else{
      return false;
    }
  }

  add_image_to_rw(event){
    var input = this.element.nativeElement.querySelector('#add_rw_photo');

    this.rw_images_error = "";
    this.event.length = 0;

    if(input.files.length + this.rw_images.length > 3){
      this.rw_images_error = "*You tried to upload more then 3 images. Maximum of 3 images per recent work.*";
    }

    for(var j = 0; j < input.files.length && j < 3 - this.rw_images.length; j ++){
      this.event.push(input.files[j]);
    }

    var _this = this;
    var i = 0;

    if(this.event.length != 0){
      this.push_rw_image(i, this);
    }
  }

  push_rw_image(i, _this){

    var reader = new FileReader();
    reader.onloadend = function(e) {
        var src = reader.result;
        _this.rw_images.push(src);
        i++;
        if(i < _this.event.length){
          _this.push_rw_image(i, _this);
        }
    };
    _this.rw_images_file.push(_this.event[i]);
    reader.readAsDataURL(_this.event[i]);
  }

  rw_choose_bg(i){
    return{
      "background-image": "url('" + this.rw_images[i] + "')"
    }
  }

  remove_rw_image(i){
    this.rw_images.splice(i, 1);
    this.rw_images_file.splice(i, 1);
  }

  rw_save(){

    this.rw_title_error = "";

    if(this.rw_title.length == 0 && this.rw_description.length == 0){
      this.rw_title_error = "*Please provide a title or a description to your project*";
      return;
    }

    this.add_recent_works_id = "add_recent_works_hide";

    if(this.rw_title.length > 15){
      var title_shown = this.rw_title.substr(0, 11) + "...";
    }else{
      var title_shown = this.rw_title;
    }

    if(this.rw_description.length > 100){
      var description_shown = this.rw_description.substr(0, 96) + "...";
    }else{
      var description_shown = this.rw_description;
    }

    this.temp_rw_images = new Array();
    this.temp_rw_images_file = new Array();
    var temp_rw_images_url = new Array();

    for(var i = 0; i < this.rw_images.length; i++){
      this.temp_rw_images.push(this.rw_images[i]);
      this.temp_rw_images_file.push(this.rw_images_file[i]);
      temp_rw_images_url.push("");
    }

    var rw = {
      "title": this.rw_title,
      "description": this.rw_description,
      "images": this.temp_rw_images,
      "images_file": this.temp_rw_images_file,
      "title_shown": title_shown,
      "description_shown": description_shown,
      "chosen_bg": 0,
      "cost": this.rw_cost,
      "rw_image_url": temp_rw_images_url
    }

    this.rw_array.push(rw);

    this.rw_title = "";
    this.rw_description = "";
    this.rw_images.length = 0;
    this.rw_images_file.length = 0;
    this.rw_cost = "";
  }

  add_border(i){
    if(i == 0){
      return{
        "border": "1px solid #999"
      }
    }
  }

  rw_container_bg(i){
    if(this.rw_array[i].images.length != 0){
      return{
        "background-image": "url('" + this.rw_array[i].images[this.rw_array[i].chosen_bg] + "')",
        "background-color": "transparent"
      }
    }else{
      return{
        "background-image": "none",
        "background-color": "rgba(0,0,0,.8)"
      }
    }
  }

  add_more_recent_work(){
    this.add_recent_works_id = "add_recent_works_active";
  }

  delete_rw(){
    this.close_overlay();
    setTimeout(() => {
      this.rw_array.splice(this.rw_to_delete, 1);
      if(this.rw_array.length == 0){
        this.add_recent_works_id = "add_recent_works_active";
      }
    },450);
  }

  show_popup_delete(i){
    this.rw_to_delete = i;
    this.container_id="container_blur";
    this.overlay_id="overlay_active";
    var pop = this.element.nativeElement.querySelector('#edit_rw_overlay');
    var pop_2 = this.element.nativeElement.querySelector('#edit_rw_overlay_2');
    this.datatransfer.set_blur(true);
    this.blur.emit(true);

    pop.style.opacity = 0;
    pop_2.style.opacity = 0;
    pop.style.display = "none";
    pop_2.style.display = "block";
    setTimeout(() => {
      pop_2.style.opacity = 1;
    },10);
  }

  edit_rw(i){
    this.container_id="container_blur";
    this.overlay_id="overlay_active";
    var pop = this.element.nativeElement.querySelector('#edit_rw_overlay');
    var pop_2 = this.element.nativeElement.querySelector('#edit_rw_overlay_2');
    this.datatransfer.set_blur(true);
    this.blur.emit(true);

    pop.style.opacity = 0;
    pop_2.style.opacity = 0;
    pop_2.style.display = "none";
    pop.style.display = "block";
    setTimeout(() => {
      pop.style.opacity = 1;
    },10);

    this.temp_edit_rw_images = new Array();
    this.temp_edit_rw_images_file = new Array();
    this.temp_edit_rw_image_url = new Array();

    for(var j = 0; j < this.rw_array[i].images.length; j++){
      this.temp_edit_rw_images.push(this.rw_array[i].images[j]);
      this.temp_edit_rw_images_file.push(this.rw_array[i].images_file[j]);
    }

    for(var j = 0; j < this.rw_array[i].rw_image_url.length; j++){
      this.temp_edit_rw_image_url.push(this.rw_array[i].rw_image_url[j]);
    }

    this.temp_edit_rw = {
      "title": this.rw_array[i].title,
      "description": this.rw_array[i].description,
      "images": this.rw_array[i].images,
      "images_file": this.rw_array[i].images_file,
      "title_shown": this.rw_array[i].title_shown,
      "description_shown": this.rw_array[i].description_shown,
      "chosen_bg": this.rw_array[i].chosen_bg,
      "cost": this.rw_array[i].cost,
      "rw_image_url": this.rw_array[i].rw_image_url
    };

    this.edit_rw_img_array = this.rw_array[i].images;
    this.edit_rw_img_array_file = this.rw_array[i].images_file;
    this.edit_rw_title = this.rw_array[i].title;
    this.edit_rw_description = this.rw_array[i].description;
    this.edit_rw_chosen_bg = i;
    this.edit_rw_cost = this.rw_array[i].cost;
    this.edit_rw_image_url = this.rw_array[i].rw_image_url;
  }

  close_overlay(){
    var pop = this.element.nativeElement.querySelector('#edit_rw_overlay');
    var pop_2 = this.element.nativeElement.querySelector('#edit_rw_overlay_2');

    pop.style.opacity = 0;
    pop_2.style.opacity = 0;
    setTimeout(() => {
      this.container_id="container";
      this.overlay_id="overlay_hide";
      this.datatransfer.set_blur(false);
      this.blur.emit(false);
    },350);
  }

  edit_rw_img_bg(i){
    return{
      "background-image": "url('" + this.edit_rw_img_array[i] + "')"
    }
  }

  delete_edit_rw_img(i){
    this.edit_rw_img_array.splice(i,1);
    this.edit_rw_image_url.splice(i,1);
    this.edit_rw_img_array_file.splice(i,1);
    if(i == this.rw_array[this.edit_rw_chosen_bg].chosen_bg){
      this.rw_array[this.edit_rw_chosen_bg].chosen_bg = 0;
    }
  }

  stop_prop(event){
    event.stopPropagation();
  }

  add_more_rw_img(){
    var input = this.element.nativeElement.querySelector('#add_edit_rw_photo');

    this.edit_rw_images_error = "";
    this.event.length = 0;

    if(input.files.length + this.edit_rw_img_array.length > 3){
      this.edit_rw_images_error = "*You tried to upload more then 3 images. Maximum of 3 images per recent work.*";
    }

    for(var j = 0; j < input.files.length && j < 3 - this.edit_rw_img_array.length; j ++){
      this.event.push(input.files[j]);
    }

    var _this = this;
    var i = 0;

    if(this.event.length != 0){
      this.push_edit_rw_image(i, this);
    }
  }

  push_edit_rw_image(i, _this){

    var reader = new FileReader();
    reader.onloadend = function(e) {
        var src = reader.result;
        _this.edit_rw_img_array.push(src);
        i++;
        if(i < _this.event.length){
          _this.push_edit_rw_image(i, _this);
        }
    };
    reader.readAsDataURL(_this.event[i]);
    _this.edit_rw_img_array_file.push(_this.event[i]);
    _this.edit_rw_image_url.push("");
  }

  choose_cover(i){
    this.rw_array[this.edit_rw_chosen_bg].chosen_bg = i;
  }

  if_checked(i){
    if(i == this.rw_array[this.edit_rw_chosen_bg].chosen_bg){
      return "./assets/images/check_box.png";
    }else{
      return "./assets/images/uncheck_box_v2.png";
    }
  }

  edit_rw_save(){

    if(this.edit_rw_title.length == 0 && this.edit_rw_description.length == 0){
      this.edit_rw_images_error = "*Please provide a title or a description to your project*";
      return;
    }

    this.rw_array[this.edit_rw_chosen_bg].title = this.edit_rw_title;
    this.rw_array[this.edit_rw_chosen_bg].description = this.edit_rw_description;
    this.rw_array[this.edit_rw_chosen_bg].cost = this.edit_rw_cost;
    this.rw_array[this.edit_rw_chosen_bg].rw_image_url = this.edit_rw_image_url

    if(this.edit_rw_title.length > 15){
      var title_shown = this.edit_rw_title.substr(0, 11) + "...";
    }else{
      var title_shown = this.edit_rw_title;
    }

    if(this.edit_rw_description.length > 100){
      var description_shown = this.edit_rw_description.substr(0, 96) + "...";
    }else{
      var description_shown = this.edit_rw_description;
    }

    setTimeout(() => {

      var temp = this.edit_rw_img_array[this.rw_array[this.edit_rw_chosen_bg].chosen_bg];
      var temp_2 = this.edit_rw_img_array_file[this.rw_array[this.edit_rw_chosen_bg].chosen_bg];
      var temp_3 = this.edit_rw_image_url[this.rw_array[this.edit_rw_chosen_bg].chosen_bg];
      var temp2, temp2_2, temp2_3;

      for(var i = 0; i < this.edit_rw_img_array.length; i ++){
        temp2 = this.edit_rw_img_array[i];
        temp2_2 = this.edit_rw_img_array_file[i];
        temp2_3 = this.edit_rw_image_url[i];

        this.edit_rw_img_array[i] = temp;
        this.edit_rw_img_array_file[i] = temp_2;
        this.edit_rw_image_url[i] = temp_3;

        temp = temp2;
        temp_2 = temp2_2;
        temp_3 = temp2_3;

        if(i == this.rw_array[this.edit_rw_chosen_bg].chosen_bg){
          break;
        }
      }

      console.log(this.rw_array[this.edit_rw_chosen_bg].images);
      console.log(this.rw_array[this.edit_rw_chosen_bg].images_file);
      console.log(this.rw_array[this.edit_rw_chosen_bg].rw_image_url);

      this.rw_array[this.edit_rw_chosen_bg].chosen_bg = 0;
      this.rw_array[this.edit_rw_chosen_bg].title_shown = title_shown;
      this.rw_array[this.edit_rw_chosen_bg].description_shown = description_shown;

    },350);

    this.close_overlay();
  }

  edit_rw_cancel(){
    this.rw_array[this.edit_rw_chosen_bg].title = this.temp_edit_rw.title;
    this.rw_array[this.edit_rw_chosen_bg].title_shown = this.temp_edit_rw.title_shown;
    this.rw_array[this.edit_rw_chosen_bg].description = this.temp_edit_rw.description;
    this.rw_array[this.edit_rw_chosen_bg].description_shown = this.temp_edit_rw.description_shown;
    this.rw_array[this.edit_rw_chosen_bg].images = this.temp_edit_rw_images;
    this.rw_array[this.edit_rw_chosen_bg].images_file = this.temp_edit_rw_images_file;
    this.rw_array[this.edit_rw_chosen_bg].chosen_bg = this.temp_edit_rw.chosen_bg;
    this.rw_array[this.edit_rw_chosen_bg].cost = this.temp_edit_rw.cost;
    this.rw_array[this.edit_rw_chosen_bg].rw_image_url = this.temp_edit_rw_image_url;

    console.log(this.rw_array[this.edit_rw_chosen_bg].rw_image_url);

    this.close_overlay();
  }

  set_opacity(value){
    if(value == ""){
      return{
        "opacity": "0"
      };
    }else{
      return{
        "opacity": "1"
      }
    };
  }

  toggle_item(x){
    var checkbox = this.element.nativeElement.querySelector('#check_box_' + x);

    for(var i = 0; i < this.sub_categs.length; i++){
      if(this.sub_categs[i] == this.sub_categories[x]){
        checkbox.src="./assets/images/uncheck_box.png";
        this.sub_categs.splice(i,1);
        return;
      }
    }
    checkbox.src="./assets/images/check_box_v2.png";
    this.sub_categs.push(this.sub_categories[x]);
  }

  sub_categ_show(){
    var location = this.router.url;

    if(location == '/add-trade'){
      return false;
    }else{
      return true;
    }
  }

  set_blur(value){
    if(value){
      this.container_id="container_blur";
    }else{
      this.container_id="container";
    }
  }

  add_recent_works_activate(){
    if(this.add_recent_works_id == 'add_recent_works_active'){
      return{
        "display": "block"
      };
    }else{
      return{
        "display": "none"
      };
    }
  }

  check_location(){
    var box = this.element.nativeElement.querySelector('#location');
    var err = this.element.nativeElement.querySelector('#location_error');
    var testloc = /^[A-Z]+[0-9]+[A-Z]+ +[0-9]+[A-Z]+[0-9]$/i;
    var testloc_2 = /^[A-Z]+[0-9]+[A-Z]+[0-9]+[A-Z]+[0-9]$/i;

    this.location_error = "";
    if(this.location.length == 0){
      box.style.border = "1px solid #F27072";
      err.style.opacity = 1;
      this.location_error = "*Please input a location*";
      return false;
    }else if(!testloc.test(this.location) && !testloc_2.test(this.location)){
      box.style.border = "1px solid #F27072";
      err.style.opacity = 1;
      this.location_error = "*Please input a valid location*";
      return false;
    }else{
      this.remove_attribute();
      return true;
    }
  }

  remove_attribute(){
    var box = this.element.nativeElement.querySelector('#location');
    var err = this.element.nativeElement.querySelector('#location_error');
    box.removeAttribute("style");
    err.style.opacity = 0;
  }

  check_hour(val){
    switch(val){
      case "0":
      case "00":
      case "1":
      case "01":
      case "2":
      case "02":
      case "3":
      case "03":
      case "4":
      case "04":
      case "5":
      case "05":
      case "6":
      case "06":
      case "7":
      case "07":
      case "8":
      case "08":
      case "9":
      case "09":
      case "10":
      case "11":
      case "12":
          return true;
      default: return false;
    }
  }

  check_minute(val){
    switch(val){
      case "0":
      case "00":
      case "1":
      case "01":
      case "2":
      case "02":
      case "3":
      case "03":
      case "4":
      case "04":
      case "5":
      case "05":
      case "6":
      case "06":
      case "7":
      case "07":
      case "8":
      case "08":
      case "9":
      case "09":
          return true;
      default:
        if(parseInt(val) != null && parseInt(val) < 61){
          return true;
        }else{
          return false;
        }
    }
  }

  check_time(){
    var box = this.element.nativeElement.querySelector('#time_error');
    var T_comp = this.Time_Components.toArray();
    this.time_error = "";
    if(!(this.check_hour(T_comp[0].hour) && this.check_hour(T_comp[1].hour) && this.check_minute(T_comp[0].minute) && this.check_minute(T_comp[1].minute))){
      box.style.opacity = 1;
      this.time_error = "*Please use a valid time range*";
      return false;
    }

    return true;
  }
}
