import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent }      from './login.component';
import { SignUpComponent }      from './signup.component';
import { HomeComponent }      from './home.component';
import { TradesmanListComponent }      from './tradesmanlist.component';
import { TradesmanComponent } from './tradesman.component';
import { UserHistoryComponent } from './userhistory.component';
import { EditProfileComponent } from './editprofile.component';
import { DashboardComponent } from './dashboard.component';
import { TradesmanSettingsComponent } from './tradesman_settings.component';
import { TradesmanHistoryComponent } from './tradesman_history.component';
import { TradesmanNotificationsComponent } from './tradesman_notifications.component';
import { CompanyProfileComponent } from './company_profile.component';
import { TradesmanEditProfileComponent } from './tradesman_edit_profile.component';
import { AddTradeComponent } from './add_trade.component';
import { SubscribeCompanyComponent } from './subscribe_company.component';
import { SubscribePersonalComponent } from './subscribe_personal.component';
import { LearnMoreTradesman } from './learn_more_tradesman.component';
import { LearnMoreHomeowner } from './learn_more_homeowner.component';
import { BestTradesmanComponent } from './best_tradesman.component';
import { AboutUsComponent } from './about_us.component';
import { OurTeamComponent } from './our_team.component';
import { ContactUsComponent } from './contact_us.component';
import { PrivacyPolicyComponent } from './privacy_policy.component';
import { TermsOfUseComponent } from './terms_of_use.component';
import { ResetPasswordComponent } from './reset_password.component';
import { ViewCompanyComponent } from './view_company.component';

const appRoutes: Routes = [
  {
    path: 'view-company',
    component: ViewCompanyComponent
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent
  },
  {
    path: 'terms-of-use',
    component: TermsOfUseComponent
  },
  {
    path: 'about-us',
    component: AboutUsComponent
  },
  {
    path: 'our-team',
    component: OurTeamComponent
  },
  {
    path: 'contact-us',
    component: ContactUsComponent
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'sign-up',
    component: SignUpComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'tradesmanlist',
    component: TradesmanListComponent
  },
  {
    path: 'tradesman',
    component: TradesmanComponent
  },
  {
    path: 'user-history',
    component: UserHistoryComponent
  },
  {
    path: 'edit-profile',
    component: EditProfileComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'tradesman-settings',
    component: TradesmanSettingsComponent
  },
  {
    path: 'tradesman-history',
    component: TradesmanHistoryComponent
  },
  {
    path: 'tradesman-notifications',
    component: TradesmanNotificationsComponent
  },
  {
    path: 'company-profile',
    component: CompanyProfileComponent
  },
  {
    path: 'tradesman-edit-profile',
    component: TradesmanEditProfileComponent
  },
  {
    path: 'add-trade',
    component: AddTradeComponent
  },
  {
    path: 'subscribe-company',
    component: SubscribeCompanyComponent
  },
  {
    path: 'subscribe-personal',
    component: SubscribePersonalComponent
  },
  {
    path: 'learn-more-tradesman',
    component: LearnMoreTradesman
  },
  {
    path: 'learn-more-homeowner',
    component: LearnMoreHomeowner
  },
  {
    path: 'best-tradesman',
    component: BestTradesmanComponent
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
