import { Component, ElementRef, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Router} from '@angular/router';
import { DataTransferService } from './data_transfer.service';
import { DashboardComponent } from './dashboard.component';
import { RequestService } from './request.service';
import { PushNotificationsService } from 'angular2-notifications';
import { WindowRef } from './window.service';
import { JsonFetchService } from './JsonFetch.service';

@Component({
  selector: 'body',
  templateUrl: './app/HTML/navbar.html',
  styleUrls: ['css/navbar.css'],
  host: {
    "[style.background-color]":"bodyBackgroundColor()",
    '(window:scroll)': 'hide_dropdown(); close_menu(); close_notif_box(); close_tman_navs();'
  },
})
export class AppComponent {
  @ViewChild(DashboardComponent) dashboard:DashboardComponent;

  date =  new Date();
  months = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"];
  homeowner_image_2_data = "";
  notif_box = false;
  menu_open = false;
  sliding_menu_id ="sliding_menu";
  missed_reqs_sub: Subscription;
  missed_reqs = 0;
  errorMessage: string;
  subscription: Subscription;
  blur_sub: Subscription;
  load_sub: Subscription;
  t_man_profile: Subscription;
  notif_sub: Subscription;
  login: boolean;
  name = "";
  full_name = "";
  dropdown_id= "dropdown";
  window_width = 0;
  ads_id = "ads";
  content_id = "content";
  overlay_id = "overlay_hidden";
  overlay_1_id = "overlay_1_hidden";
  job = "";
  prof_pic = "./assets/images/icons/default_profile_pic.png";
  navbar_container_id = "navbar_container";
  tman_navbar_id = "tman_navbar";
  requests = [];
  notif_icon = "./assets/images/icons/notification.png";
  lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et cursus felis. Fusce vulputate libero quis velit porttitor egestas. Aliquam volutpat et libero vitae lobortis. Nam eget consectetur nulla. In id sapien ac mauris venenatis viverra sed in mi. Nam sit amet turpis id arcu congue dictum. Donec a felis ut leo venenatis viverra vitae gravida nisl. Donec tincidunt lacus sapien, bibendum faucibus quam pharetra eu. Sed aliquet eget tellus rhoncus pulvinar. Pellentesque varius sapien turpis."
  inc_request = [
                  {
                    "state": 0,
                    "first_name": "John",
                    "last_name": "Doe",
                    "job": "Plumbing",
                    "location": "North York, ON",
                    "precise_loc": "2km away",
                    "time": 120,
                    "description": this.lorem,
                    "phone_number": "123-123-1234",
                    "email": "asd@asd.com"
                  },

                  {
                    "state": 0,
                    "first_name": "James",
                    "last_name": "Cameron",
                    "job": "Electrician",
                    "location": "",
                    "precise_loc": "",
                    "time": 120,
                    "description": this.lorem,
                    "phone_number": "123-123-1234",
                    "email": "asd@asd.com"
                  },

                  {
                    "state": 0,
                    "first_name": "Clinton",
                    "last_name": "Loomis",
                    "job": "Plumbing",
                    "location": "North York, ON",
                    "precise_loc": "2km away",
                    "time": 120,
                    "description": "",
                    "phone_number": "123-123-1234",
                    "email": "asd@asd.com"
                  },

                  {
                    "state": 0,
                    "first_name": "Artour",
                    "last_name": "Babaev",
                    "job": "Plumbing",
                    "location": "North York, ON",
                    "precise_loc": "2km away",
                    "time": 120,
                    "description": this.lorem,
                    "phone_number": "",
                    "email": "asd@asd.com"
                  },

                  {
                    "state": 0,
                    "first_name": "Jacky",
                    "last_name": "Mao",
                    "job": "Plumbing",
                    "location": "North York, ON",
                    "precise_loc": "2km away",
                    "time": 120,
                    "description": this.lorem,
                    "phone_number": "123-123-1234",
                    "email": ""
                  },
                ];

  notifications = [];
    /*{
      "state": "1",
      "icon": "./assets/images/icons/missed.png",
      "text": "You missed a request from John Doe",
      "time": "9 minutes ago"
    },
    {
      "state": "2",
      "icon": "./assets/images/icons/ratings_1.png",
      "text": "Amanda rated your work",
      "time": "20 hours ago"
    },
    {
      "state": "3",
      "icon": "./assets/images/icons/history_dark.png",
      "text": "You haven't marked \"Start work\" for John Taylor's plumbing request.",
      "button": "Mark as start work",
      "bg_color": "#7dc469"
    },
    {
      "state": "3",
      "icon": "./assets/images/icons/history_dark.png",
      "text": "Have you completed your plumbing works for Dennis?",
      "button": "Mark as completed",
      "bg_color": "#2fb5bf",
      "start_time": "15th November 2016"
    },
    {
      "state": "3",
      "icon": "./assets/images/icons/Calendar.png",
      "text": "You have been in \"Do not disturb mode\" for 2 days now. Do you wish to turn it off?",
      "button": "Turn off DND mode",
      "bg_color": "#f2653c"
    },
    {
      "state": "1",
      "icon": "./assets/images/icons/missed.png",
      "text": "You missed a request from John Doe",
      "time": "3 days ago"
    },
    {
      "state": "2",
      "icon": "./assets/images/icons/ratings_1.png",
      "text": "Amanda rated your work",
      "time": "2 months ago"
    },
    {
      "state": "3",
      "icon": "./assets/images/icons/history_dark.png",
      "text": "You haven't marked \"Start work\" for John Taylor's plumbing request.",
      "button": "Mark as start work",
      "bg_color": "#7dc469"
    },
    {
      "state": "3",
      "icon": "./assets/images/icons/history_dark.png",
      "text": "Have you completed your plumbing works for Dennis?",
      "button": "Mark as completed",
      "bg_color": "#2fb5bf",
      "start_time": "12th September 2016"
    },
    {
      "state": "3",
      "icon": "./assets/images/icons/Calendar.png",
      "text": "You have been in \"Do not disturb mode\" for 2 days now. Do you wish to turn it off?",
      "button": "Turn off DND mode",
      "bg_color": "#f2653c"
    },
  ];*/

  window: Window;

  constructor(private element: ElementRef, private datatransfer:DataTransferService, public router: Router, private request_service: RequestService, private push_notif: PushNotificationsService, win: WindowRef, private jsonfetch: JsonFetchService) {
    this.window = win.nativeWindow;
    this.notifications = JSON.parse(this.datatransfer.get_tradesman_notifications());
    if(this.datatransfer.get_homeowner_profile_image() != null){
      this.homeowner_image_2_data = 'http://jaxoftrades.com/api/' + this.datatransfer.get_homeowner_profile_image() + '?' + this.getRandomInt(90000000000, 100000000000);
    }
  }

  ngOnInit(){
    this.subscription = this.datatransfer.login
       .subscribe(value => this.sub(value));

    this.blur_sub = this.datatransfer.blur
       .subscribe(value => this.set_blur(value));

    this.load_sub = this.datatransfer.load
      .subscribe(value => this.set_loading(value));

    this.t_man_profile = this.datatransfer.t_man_profile
      .subscribe(value => this.set_tradesman_values(value));

    this.notif_sub = this.datatransfer.notif
       .subscribe(value => this.notif_sub_fxn(value));

    this.window_width = window.innerWidth;

    if(this.datatransfer.is_job_string()){
      this.job = this.datatransfer.get_job_string();
    }

    if(this.datatransfer.get_tradesman_values_json() != null){
      var img = JSON.parse(this.datatransfer.get_tradesman_values_json()).profile_pic;
      if(img != null){
        this.prof_pic = img;
      }
    }

    this.requests = this.request_service.get_inc_requests();

    this.datatransfer.set_tradesman_notifications(JSON.stringify(this.notifications));

    this.missed_reqs_sub = this.request_service.missed_reqs
      .subscribe(value => this.set_missed_reqs(value));

    if(this.show_tman_navs()){
      this.get_inc_req();
    }

  }

  notif_sub_fxn(value){
    this.notif_icon = "./assets/images/icons/notification.png";
    var notif = JSON.parse(this.datatransfer.get_tradesman_notifications());
    if(notif == null){
      this.notifications = [];
    }else{
      this.notifications = notif;
    }
  }


  set_missed_reqs(value){
    this.missed_reqs = value;
  }

  ngAfterViewChecked(){
    var location = this.router.url;

    switch(location) {
    case "/dashboard":
        this.change_highlights(0);
        break;
    case "/tradesman-edit-profile":
        this.change_highlights(1);
        break;
    case "/tradesman-history":
        this.change_highlights(2);
        break;
    case "/company-profile":
        this.change_highlights(3);
        break;
    case "/tradesman-settings":
        this.change_highlights(4);
        break;
    default:
        this.change_highlights(5);
    }
  }

  set_blur_2(value){
    if(value == true){
      this.content_id = "content_blurred";
    }else{
      this.content_id = "content";
    }
  }

  set_blur(value){
    if(value == true){
      this.ads_id = "ads_blurred";
      this.navbar_container_id = "navbar_container_blurred";
      this.tman_navbar_id = "tman_navbar_blurred";
    }else{
      this.ads_id = "ads";
      this.navbar_container_id = "navbar_container";
      this.tman_navbar_id = "tman_navbar";
    }
  }

  register_as_tradesman(){
    this.datatransfer.register_as_tradesman();
    this.router.navigate(['./sign-up']);
  }

  set_loading(value){
    var loading = this.element.nativeElement.querySelector('#loading');

    if(value == true){
      loading.style.opacity = 0;
      this.content_id = "content_blurred";
      this.overlay_id = "overlay";
      this.set_blur(value);
      setTimeout(() => {
        loading.style.opacity = 1;
      },10);
    }else{
      loading.style.opacity = 1;
      setTimeout(() => {
        loading.style.opacity = 0;
        setTimeout(() => {
          this.content_id = "content";
          this.set_blur(value);
          this.overlay_id = "overlay_hidden";
        },350);
      },10);
    }
  }

  sub(value){
    this.login = value;
    this.name = this.datatransfer.get_user_first_name();
    this.full_name = this.datatransfer.get_user_first_name() + " " + this.datatransfer.get_user_last_name();
    if(this.datatransfer.get_homeowner_profile_image() != null){
      this.homeowner_image_2_data = 'http://jaxoftrades.com/api/' + this.datatransfer.get_homeowner_profile_image() + '?' + this.getRandomInt(90000000000, 100000000000);
    }
  }

  toggle_menu(){
    if(this.menu_open){
      this.sliding_menu_id = "sliding_menu";
    }else{
      this.sliding_menu_id = "sliding_menu_active";
    }

    this.menu_open = !this.menu_open;
  }

  close_menu(){
    this.sliding_menu_id = "sliding_menu";
    this.menu_open = false;
  }

  set_nav_bg(){
    var location = this.router.url;

    if((location == '/learn-more-tradesman' || location == '/learn-more-homeowner')&&(window.innerWidth > 800)){
      return{
        "background-color": "transparent"
      }
    }else{
      return{
        "background-color": "rgba(0, 0, 0, .5)"
      }
    }
  }

  set_nav_btn_bg(){
    var location = this.router.url;

    if(location == '/learn-more-tradesman' || location == '/learn-more-homeowner'){
      return{
        "border": "1px solid #7b7a7a",
        "color": "#7b7a7a"
      }
    }else{
      return{
        "border": "1px solid #fff",
        "color": "#fff"
      }
    }
  }

  set_app_icon(){
    var location = this.router.url;

    if(location == '/learn-more-tradesman' || location == '/learn-more-homeowner'){
      return "./assets/images/icons/get_our_app_icon_2.png";
    }else{
      return "./assets/images/icons/get_our_app.png";
    }
  }

  toggle_user_opts(event){
    this.close_notif_box()
    event.stopPropagation();
    if(this.dropdown_id == "dropdown_active"){
      this.dropdown_id = "dropdown";
    }else{
      this.dropdown_id = "dropdown_active";
    }
  }

  logout(){
    this.datatransfer.user_logout();
    this.dropdown_id = "dropdown";
    this.router.navigate(['./home']);
  }

  hide_dropdown(){
    this.dropdown_id = "dropdown";
  }

  check_route(){
    var location = this.router.url;
    if(location == "/tradesmen" || location == "/tradesman" || location == "/tradesmanlist" || location == "/view-company"){
      return true;
    }else{
      return false;
    }
  }

  adjust_width(event){
    var location = this.router.url;

    if(this.show_tman_navs()){
      if(window.innerWidth < 800){
        return {
          "width": "100%",
          "padding-left": "0%",
          "padding-right": "0%"
        }
      }else{
        return {
          "width": "85%",
          "padding-left": "0%",
          "padding-right": "0%"
        }
      }
    }else if(location == "/tradesmen" || location == "/tradesmanlist" || location == "/view-company"){
      if(this.window_width <= 981){
        return {
          "width": "100%",
          "padding-left": "0%",
          "padding-right": "0%"
        }
      }else{
        if(location != "/view-company"){
          return {
            "width": "70%",
            "padding-left": "5%",
            "padding-right": "0%"
          }
        }else{
          return {
            "width": "68%",
            "padding-left": "5%",
            "padding-right": "0%"
          }
        }
      }
    }else if(location == "/tradesman" || location == "/view-company"){
      if(this.window_width <= 981){
        return {
          "width": "100%",
          "padding-left": "0%",
          "padding-right": "0%"
        }
      }else{
        return {
          "width": "68%",
          "padding-left": "5%",
          "padding-right": "0%"
        }
      }
    }else{
      return {
        "width": "100%",
        "padding-left": "0%",
        "padding-right": "0%"
      }
    }
  }

  adjust_ads_width(){
    var location = this.router.url;

    if(location == "/tradesmanlist"){
      return {
        "width": "20%",
        "padding-right": "5%"
      }
    }else{
      return {
        "width": "22%",
        "padding-right": "5%"
      }
    }
  }

  onResize(event) {
    var box = this.element.nativeElement.querySelector('#tman_navbar');
    var box_2 = this.element.nativeElement.querySelector('#tman_navbar_blurred');
    var icon = this.element.nativeElement.querySelector('#menu_button_tradesman');

    this.window_width = event.target.innerWidth;

    if(this.show_tman_navs()){
      if(box != null){
        box.removeAttribute('style');
      }else if(box_2 != null){
        box_2.removeAttribute('style');
      }
      icon.removeAttribute('style');
    }
  }

  click_event(event){
    event.stopPropagation();
  }

  is_homeowner(){
    if(this.datatransfer.is_logged_in()){
      if(this.datatransfer.get_user_type() == "1"){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

  is_tradesman(){
    if(this.datatransfer.is_logged_in()){
      if(this.datatransfer.get_user_type() == "2"){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

  show_tman_navs(){
    var location = this.router.url;
    if(this.is_tradesman()){
      if(location == "/edit-profile" || location == "/home"){
        return false;
      }else{
        return true;
      }
    }else{
      return false;
    }
  }

  change_highlights(val){
    if(this.show_tman_navs()){
      for(var i = 0; i < 5; i++){
        var btn = this.element.nativeElement.querySelector('#t_nav_btn_' + i);
        if(i == val){
          btn.style.backgroundColor = "#20222e";
        }else{
          btn.style.backgroundColor = "transparent";
        }
      }
    }
  }

  set_tradesman_values(value){
    if(value){
        this.job = "";
        var jobs = JSON.parse(this.datatransfer.get_job_json());
        for(var i = 0; i < jobs.jobs.length; i++){
          this.job += jobs.jobs[i].job;
          if(i != jobs.jobs.length - 1){
            this.job += " | ";
          }
        }
        this.datatransfer.set_job_string(this.job);
        this.prof_pic = JSON.parse(this.datatransfer.get_tradesman_values_json()).profile_pic;
        if(this.prof_pic == null){
          this.prof_pic = "./assets/images/icons/default_profile_pic.png";
        }
        setTimeout(() => {
          this.get_inc_req();
        },500);
    }else{
      this.job = "";
    }
  }

  get_inc_req(){
    if(this.show_tman_navs()){
      this.jsonfetch.get_inc_request(this.datatransfer.get_user_id(), this.datatransfer.get_user_email())
         .subscribe(
           data => this.inc_req_data(data),
           error =>  this.inc_req_error(error));
    }
  }

  inc_req_error(error){
    console.log(error);
  }

  inc_req_data(res){
    //console.log(res);
    var histories = JSON.parse(this.datatransfer.get_tradesman_history());
    for(var y = 0; y < res.incoming_requests.length; y++){
      for(var z = 0; z < this.request_service.requests.length; z++){
        if(res.incoming_requests[y].request_id == this.request_service.requests[z].id){
          break;
        }
      }
      if(res.incoming_requests[y].h_first_name != null && z == this.request_service.requests.length){
        for(var z = 0; z < histories.length; z++){
          if(res.incoming_requests[y].request_id == histories[z].id){
            break;
          }
        }

        if(z != histories.length){
          continue;
        }

        var req = {
          "first_name": res.incoming_requests[y].h_first_name,
          "last_name": res.incoming_requests[y].h_last_name,
          "job": res.incoming_requests[y].job_type,
          "location": "",
          "precise_loc": "",
          "time": res.incoming_requests[y].remaining_second,
          "description": res.incoming_requests[y].short_desc,
          "phone_number": res.incoming_requests[y].contact_number,
          "email": res.incoming_requests[y].email,
          "id": res.incoming_requests[y].request_id,
          "h_id": res.incoming_requests[y].homeowner_id
        }
        this.got_request(req);
      }
    }
    for(var y = 0; y < res.notifications.length; y++){
      for(var x = 0; x < this.notifications.length; x++){
        if(this.notifications[x].id == res.notifications[y].request_id){
          break;
        }
      }
      if(x != this.notifications.length){
        continue;
      }

      var state = 0;
      var text = "";
      var icon = "";
      var time = "";
      var button = "";
      var bg_color = "";
      var start_time = "";

      if(res.notifications[y].status == "Mark as start work"){
        state = 3;
        icon = "./assets/images/icons/history_dark.png";
        text = "You haven't marked \"Start work\" for " + res.notifications[y].h_first_name + " " + res.notifications[y].h_last_name + "'s " + res.notifications[y].job_type + " request.",
        button = "Mark as start work";
        bg_color = "#7dc469";
      }else if(res.notifications[y].status == "Mark as completed"){
        state = 3;
        icon = "./assets/images/icons/history_dark.png";
        text = "Have you completed your " + res.notifications[y].job_type + " works for " + res.notifications[y].h_first_name + "?"
        button = "Mark as completed";
        bg_color = "#2fb5bf";
        start_time = res.notifications[y].timing;
      }else if(res.notifications[y].status == "Turn off DND mode"){
        state = 3;
      }else if(res.notifications[y].status == "Rated"){
        state = 2;
        text = res.notifications[y].h_first_name + " rated your work";
        icon = "./assets/images/icons/ratings_1.png";
        time = res.notifications[y].timing;
      }else if(res.notifications[y].status == "Missed Request"){
        state = 1;
        text = "You missed a request from " + res.notifications[y].h_first_name + " " + res.notifications[y].h_last_name;
        icon = "./assets/images/icons/missed.png";
        time = res.notifications[y].timing;
      }

      var notif = {
        "state": state,
        "icon": icon,
        "text": text,
        "button": button,
        "bg_color": bg_color,
        "start_time": start_time,
        "time": time + " ago",
        "id": res.notifications[y].request_id
      }

      this.notifications.splice(0,0,notif);
      this.datatransfer.set_tradesman_notifications(JSON.stringify(this.notifications));
      this.datatransfer.set_notif();
      this.notif_icon = "./assets/images/icons/notification_new.png";
    }
    setTimeout(() => {
      this.get_inc_req();
    },5000);
  }

  pop_up_close(){
    this.overlay_1_id = "overlay_1_hidden";
    this.set_blur(false);
    this.set_blur_2(false);
    this.request_service.drop_inc_reqs();
  }

  go_to_dashboard(){
    this.router.navigate(['./dashboard']);
    this.pop_up_close();
  }

  accept_popup(i){
    this.request_service.pop_set_state(i, 2);
    this.go_to_dashboard();
  }

  decline_popup(i){
    this.request_service.pop_set_state(i, 3);
    this.pop_up_close();
  }

  decline_popup_1(i){
    this.request_service.pop_set_state(i, 3);
    this.requests.splice(i,1);
  }

  recv_inc_req(){
    this.overlay_1_id = "overlay_1";
    this.set_blur(true);
    this.set_blur_2(true);
  }

  getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  got_request(req){
    if(req.first_name == null){
      return;
    }
    this.request_service.add_request(req.first_name, req.last_name, req.job, req.location, req.precise_loc, req.time, req.description, req.phone_number, req.email, req.id, req.h_id);
    if(this.requests.length == 1){
      this.recv_inc_req();
    }
    if(req.location.length == 0){
      this.push_notif.create('Jax of Trades', {body: req.first_name+" "+req.last_name+" has sent you a request for "+req.job+"." , icon: './assets/images/logo_box.png'}).subscribe(
        res => console.log(res),
        err => console.log(err)
      );
    }else{
      this.push_notif.create('Jax of Trades', {body: req.first_name+" "+req.last_name+" has sent you a request for "+req.job+" at "+req.location+". ("+req.precise_loc+")" , icon: './assets/images/logo_box.png'}).subscribe(
        res => console.log(res),
        err => console.log(err)
      );
    }
  }

  set_desc_height(i){
    if(this.requests[i].pop_state == 0){
      return {
        "max-height": "0"
      }
    }else if(this.requests[i].pop_state == 1){
      if(window.innerWidth < 800){
        return {
          "max-height": "25vw"
        }
      }else{
        return {
          "max-height": "100"
        }
      }

    }
  }

  close_desc(i){
    this.requests[i].pop_state = 0;
  }

  open_desc(i){
    this.requests[i].pop_state = 1;
  }

  add_margin(i){
    if(this.requests.length - 1 == i){
      return{
        "margin-bottom": "0"
      }
    }else{
      return{
        "margin-bottom": "3px"
      }
    }
  }

  set_bg(i){
    return{
      "background-color": this.requests[i].bg_color
    }
  }

  show_company(){
    if(this.datatransfer.is_company_profile()){
      return{
        "display": "block"
      }
    }else{
      return{
        "display": "none"
      }
    }
  }

  toggle_notif_box(event){
    event.stopPropagation();
    this.notif_box = !this.notif_box;
    this.notif_icon = "./assets/images/icons/notification.png";
    this.hide_dropdown();
  }

  close_notif_box(){
    this.notif_box = false;
  }

  set_white_spaces(){
    var location = this.router.url;
    if(location == '/tradesmanlist'){
      return{
        "max-width": "1500px"
      }
    }else if(location == '/tradesman' || location == "/view-company"){
      return{
        "max-width": "1100px"
      }
    }else{
      return{
        "max-width": "none"
      };
    }
  }

  bodyBackgroundColor() {
    var location = this.router.url;
    if(location == '/home'){
      return "#282828";
    }else if(location == '/about-us' || location == '/contact-us' || location == '/our-team' || location == '/privacy-policy' || location == '/terms-of-use'){
      return "#ffffff";
    }else{
      return "#f1f1f1";
    }
  }

  scroll_up(){
    var location = this.router.url;

    if(location == "/home"){
      var startY = currentYPosition();
      var stopY = 0;
      var distance = stopY > startY ? stopY - startY : startY - stopY;
      if (distance < 100) {
          this.window.window.scrollTo(0, stopY); return;
      }
      var speed = Math.round(distance / 100);
      if (speed >= 20) speed = 20;
      var step = Math.round(distance / 100);
      var leapY = stopY > startY ? startY + step : startY - step;
      var timer = 0;
      if (stopY > startY) {
          for (var i = startY; i < stopY; i += step) {
              this.scrollTo(leapY, timer * speed);
              leapY += step; if (leapY > stopY) leapY = stopY; timer++;
          } return;
      }
      for (var i = startY; i > stopY; i -= step) {
          this.scrollTo(leapY, timer * speed);
          leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
      }
    }
  }

  scrollTo(yPoint: number, duration: number) {
      setTimeout(() => {
          this.window.window.scrollTo(0, yPoint)
      }, duration);
      return;
  }

  use_homeowner_image(){
    if(this.datatransfer.is_homeowner_profile_image()){
      return true;
    }else{
      return false;
    }
  }

  use_homeowner_image_2(){
    return this.homeowner_image_2_data;
  }

  use_tradesman_image(){
    if(this.show_tman_navs()){
      if(this.prof_pic != "./assets/images/icons/default_profile_pic.png"){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

  tradesman_edit_profile(){
    if(this.router.url == '/edit-profile'){
      return;
    }else{
      this.router.navigate(['/tradesman-edit-profile']);
    }
  }

  toggle_tman_navs(){
    var box = this.element.nativeElement.querySelector('#tman_navbar');
    var icon = this.element.nativeElement.querySelector('#menu_button_tradesman');

    if(box.style.transform == "translateX(0%)"){
      this.close_tman_navs();
    }else{
      box.style.transform = "translateX(0%)";
      box.style.boxShadow = "2px 0px 5px 1px rgba(0,0,0,.3)";
      icon.style.transform = "rotateZ(-90deg)";
    }
  }

  close_tman_navs(){
    var box = this.element.nativeElement.querySelector('#tman_navbar');
    var box_2 = this.element.nativeElement.querySelector('#tman_navbar_blurred');
    var icon = this.element.nativeElement.querySelector('#menu_button_tradesman');
    if(window.innerWidth < 800 && this.show_tman_navs() && box != null){
      box.style.transform = "translateX(-100%)";
      box.style.boxShadow = "none";
      icon.style.transform = "rotateZ(0deg)";
    }
  }

  notif_btn(i){
    this.datatransfer.set_load(true);
    if(this.notifications[i].button == "Mark as start work"){
      this.jsonfetch.start_work(this.notifications[i].id, 1)
        .map(res => res)
            .subscribe(
                res => this.add_to_history_2(res, i),
                error => this.error_happened(error)
            );
    }else if(this.notifications[i].button == "Mark as completed"){
      this.jsonfetch.history_status(this.notifications[i].id, 1)
        .map(res => res)
            .subscribe(
                res => this.completed_2(res, i),
                error => this.error_happened(error)
            );
    }else if(this.notifications[i].button == "Turn off DND mode"){

    }
  }

  error_happened(res){
    this.datatransfer.set_load(false);
    console.log(res);
  }

  add_to_history_2(res, i){
    console.log(res);
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      var day;

      switch(this.date.getDate()){
        case 1:
          day = this.date.getDate() + "st";
          break;
        case 2:
          day = this.date.getDate() + "nd";
          break;
        case 3:
          day = this.date.getDate() + "rd";
          break;
        case 21:
          day = this.date.getDate() + "st";
          break;
        case 22:
          day = this.date.getDate() + "nd";
          break;
        case 23:
          day = this.date.getDate() + "rd";
          break;
        case 31:
          day = this.date.getDate() + "st";
          break;
        default:
          day = this.date.getDate() + "th";
          break;
      }

      for(var x = 0; x < this.request_service.requests.length; x++){
        if(this.notifications[i].id == this.request_service.requests[x].id){
          break;
        }
      }

      var history = {
        "state": 0,
        "first_name": this.request_service.requests[x].first_name,
        "last_name": this.request_service.requests[x].last_name,
        "date": day + " of " + this.months[this.date.getMonth()]+ " " + this.date.getFullYear(),
        "job": this.request_service.requests[x].job,
        "h_id": this.request_service.requests[x].h_id,
        "id": this.request_service.requests[x].id
      }

      setTimeout(() => {
        this.request_service.delete_req(x);
      }, 100);
      var histories = JSON.parse(this.datatransfer.get_tradesman_history());
      histories.splice(0,0, history);
      this.datatransfer.set_tradesman_history(JSON.stringify(histories));
      this.request_service.drop_reqs();
      this.notifications.splice(i, 1);
      this.datatransfer.set_tradesman_notifications(JSON.stringify(this.notifications));
      this.datatransfer.set_notif();
      this.datatransfer.set_history_notif();
    }
  }

  completed_2(res, i){
    this.datatransfer.set_load(false);
    var histories = JSON.parse(this.datatransfer.get_tradesman_history());
    for(var x = 0; x < histories.length; x++){
      if(this.notifications[i].id == histories[x].id){
        break;
      }
    }
    if(res.STATUS == "OK"){
      histories[x].state = 1;
    }
    this.datatransfer.set_tradesman_history(JSON.stringify(histories));
    this.notifications.splice(i, 1);
    this.datatransfer.set_tradesman_notifications(JSON.stringify(this.notifications));
    this.datatransfer.set_notif();
    this.datatransfer.set_history_notif();
  }

}

function currentYPosition() {
    // Firefox, Chrome, Opera, Safari
    if (self.pageYOffset) return self.pageYOffset;
    // Internet Explorer 6 - standards mode
    if (document.documentElement && document.documentElement.scrollTop)
        return document.documentElement.scrollTop;
    // Internet Explorer 6, 7 and 8
    if (document.body.scrollTop) return document.body.scrollTop;
    return 0;
}
