import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { WindowRef } from './window.service';
import { DataTransferService } from './data_transfer.service';

@Component({
  selector: 'about-us',
  templateUrl: './app/HTML/about_us.html',
  styleUrls: ['css/misc_pages.css']
})

export class AboutUsComponent {
  banner_img = "./assets/images/misc_pages_banners/about_us_banner.jpg";
  opening_msg_1="About Us";
  opening_msg_2="Jax of Trades is not your average business directory. In fact, it is so much more! The Jax of Trades web and mobile application is an interactive tool that is focused on helping tradesmen find work and manage their business while also providing a tool for home and property owners who are searching for the best local tradesmen in the GTA.";

  constructor(public router: Router, private datatransfer:DataTransferService) {
    if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "2"){
      if(this.datatransfer.get_tradesman_values_json() == null){
        this.router.navigate(['./edit-profile']);
      }else{
        this.router.navigate(['./dashboard']);
      }
    }
    window.scrollTo(0, 0);
  }
}
