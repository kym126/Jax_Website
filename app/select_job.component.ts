import { Component, ElementRef, Input, Output, EventEmitter, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';

@Component({
  selector: 'select-job',
  templateUrl: './app/HTML/select_job.html',
  styleUrls: ['css/edit_profile.css'],

})

export class SelectJobComponent {

  categories=["Plumber", "Electrician", "Gardener", "Cleaners", "Painter", "Carpenter", "Roofing", "Flooring", "Movers", "Handyman"];
  @Input() job:string = "Please select one";
  @Output() changed_job = new EventEmitter();

  isopen = false;

  constructor(private element: ElementRef, private datatransfer:DataTransferService){
    var jobs = JSON.parse(this.datatransfer.get_job_json()).jobs;

    for(var i = 0; i < jobs.length; i++){
      for(var j = 0; j < this.categories.length; j++){
        if(this.categories[j] == jobs[i].job){
          this.categories.splice(j,1);
          break;
        }
      }
    }
  }

  toggle_container(){
    if(this.isopen){
      if(window.innerWidth < 800){
        return{
          "max-height": "43vw"
        };
      }else{
        return{
          "max-height": "220px"
        };
      }
    }else{
      return{
        "max-height": "0"
      };
    }
  }

  toggle_left_corner(){
    if(this.isopen){
      return{
        "border-bottom-left-radius": "0"
      };
    }else{
      return{
        "border-bottom-left-radius": "3px"
      };
    }
  }

  toggle_right_corner(){
    if(this.isopen){
      return{
        "border-bottom-right-radius": "0"
      };
    }else{
      return{
        "border-bottom-right-radius": "3px"
      };
    }
  }

  set_border_bottom(i){
    if(i == this.categories.length - 1){
      return{
        "border-bottom": "1px solid #b6b7b7",
        "border-radius": "3px",
        "border-top-right-radius": "0",
        "border-top-left-radius": "0"
      };
    }else{
      return{
        "border-bottom": "none",
        "border-radius": "0",
        "border-top-right-radius": "0",
        "border-top-left-radius": "0"
      };
    }
  }

  change_value(category){
    this.isopen = false;
    this.job = category;
    this.changed_job.emit(category);
  }

  show_category(){

  }

}
