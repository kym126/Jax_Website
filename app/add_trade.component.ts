import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';
import { WindowRef } from './window.service';
import { JobSpecificFieldsComponent } from './job_specific_fields.component';
import { SubCategoriesService } from './subcategories.service';

@Component({
  selector: 'add-trade',
  templateUrl: './app/HTML/add_trade.html',
  styleUrls: ['css/add_trade.css'],
  host: {'(window:scroll)': 'change_nav_btns($event)'}
})

export class AddTradeComponent {
  window: any;
  sub_categories = [];
  sub_categs = [];
  job = "";
  open_categs = false;
  error_category = "";
  container_id = "container";
  location = "";

  @ViewChild(JobSpecificFieldsComponent) job_specific: JobSpecificFieldsComponent;

  constructor(private element: ElementRef, public router: Router, private datatransfer:DataTransferService, private jsonfetch: JsonFetchService, win: WindowRef, private sub_cat: SubCategoriesService) {
    if(!this.datatransfer.is_logged_in() || this.datatransfer.get_user_type() ==  "1"){
        this.router.navigate(['./home']);
    }
    this.window = win.nativeWindow;
    this.scrollTo(0,0);
  }

  ngAfterViewInit(){
    var btn1 = this.element.nativeElement.querySelector('#category_nav_btn');
    var btn2 = this.element.nativeElement.querySelector('#company_nav_btn');
    var btn3 = this.element.nativeElement.querySelector('#skills_nav_btn');
    var btn4 = this.element.nativeElement.querySelector('#default_daily_time_availability_nav_btn');
    var btn5 = this.element.nativeElement.querySelector('#default_location_nav_btn');
    var btn6 = this.element.nativeElement.querySelector('#recent_works_nav_btn');
    var btn7 = this.element.nativeElement.querySelector('#gallery_nav_btn');
    var btn8 = this.element.nativeElement.querySelector('#add_social_media_links_nav_btn');

    this.set_nav_btn(btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8);

    this.datatransfer.set_load(true);
    var timeoutVal = 10 * 1000 * 1000;
    navigator.geolocation.getCurrentPosition(
      position => this.gps_found(position),
      error => this.nav_err(error),
      { enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 }
    );
  }

  gps_found(position){
    this.jsonfetch.get_postal_code(position.coords.longitude, position.coords.latitude)
      .map(res => res)
          .subscribe(
              res => this.got_gps(res),
              error => this.error_happened(error)
          );
  }

  got_gps(res){
    this.datatransfer.set_load(false);
    console.log(res);
    if(res.STATUS == "OK"){
      this.location = res.postal_code;
    }
  }

  nav_err(error){
    this.datatransfer.set_load(false);
  }

  change_nav_btns(event){
    var offset = 100;
    var section1 = elmYPosition('category_title') - offset;
    var section2 = elmYPosition('company_title') - offset;
    var section3 = elmYPosition('skills_title') - offset;
    var section4 = elmYPosition('default_daily_time_availability_title') - offset;
    var section5 = elmYPosition('default_location_title') - offset;
    var section6 = elmYPosition('recent_works_title') - offset;
    var section7 = elmYPosition('gallery_title') - offset;
    var section8 = elmYPosition('add_social_media_links_title') - offset;

    var btn1 = this.element.nativeElement.querySelector('#category_nav_btn');
    var btn2 = this.element.nativeElement.querySelector('#company_nav_btn');
    var btn3 = this.element.nativeElement.querySelector('#skills_nav_btn');
    var btn4 = this.element.nativeElement.querySelector('#default_daily_time_availability_nav_btn');
    var btn5 = this.element.nativeElement.querySelector('#default_location_nav_btn');
    var btn6 = this.element.nativeElement.querySelector('#recent_works_nav_btn');
    var btn7 = this.element.nativeElement.querySelector('#gallery_nav_btn');
    var btn8 = this.element.nativeElement.querySelector('#add_social_media_links_nav_btn');

    if(currentYPosition() >= section1 && currentYPosition() <= section2){
      this.set_nav_btn(btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8);
    }else if(currentYPosition() >= section2 && currentYPosition() <= section3){
      this.set_nav_btn(btn2, btn1, btn3, btn4, btn5, btn6, btn7, btn8);
    }else if(currentYPosition() >= section3 && currentYPosition() <= section4){
      this.set_nav_btn(btn3, btn1, btn2, btn4, btn5, btn6, btn7, btn8);
    }else if(currentYPosition() >= section4 && currentYPosition() <= section5){
      this.set_nav_btn(btn4, btn1, btn2, btn3, btn5, btn6, btn7, btn8);
    }else if(currentYPosition() >= section5 && currentYPosition() <= section6){
      this.set_nav_btn(btn5, btn1, btn2, btn3, btn4, btn6, btn7, btn8);
    }else if(currentYPosition() >= section6 && currentYPosition() <= section7){
      this.set_nav_btn(btn6, btn1, btn2, btn3, btn4, btn5, btn7, btn8);
    }else if(currentYPosition() >= section7 && currentYPosition() <= section8){
      this.set_nav_btn(btn7, btn1, btn2, btn3, btn4, btn5, btn6, btn8);
    }else if(currentYPosition() >= section7){
      this.set_nav_btn(btn8, btn1, btn2, btn3, btn4, btn5, btn6, btn7);
    }else{
      this.set_nav_btn(btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8);
    }
  }

  set_nav_btn(btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8){
    btn1.style.color = "#292C38";
    btn1.style.fontSize = "11pt";
    btn2.style.color = "#8C8C8C";
    btn2.style.fontSize = "9pt";
    btn3.style.color = "#8C8C8C";
    btn3.style.fontSize = "9pt";
    btn4.style.color = "#8C8C8C";
    btn4.style.fontSize = "9pt";
    btn5.style.color = "#8C8C8C";
    btn5.style.fontSize = "9pt";
    btn6.style.color = "#8C8C8C";
    btn6.style.fontSize = "9pt";
    btn7.style.color = "#8C8C8C";
    btn7.style.fontSize = "9pt";
    btn8.style.color = "#8C8C8C";
    btn8.style.fontSize = "9pt";
  }

  smoothScroll(eID) {
      var startY = currentYPosition();
      var stopY = elmYPosition(eID) - 63;
      var distance = stopY > startY ? stopY - startY : startY - stopY;
      if (distance < 100) {
          this.window.window.scrollTo(0, stopY); return;
      }
      var speed = Math.round(distance / 100);
      if (speed >= 20) speed = 20;
      var step = Math.round(distance / 100);
      var leapY = stopY > startY ? startY + step : startY - step;
      var timer = 0;
      if (stopY > startY) {
          for (var i = startY; i < stopY; i += step) {
              this.scrollTo(leapY, timer * speed);
              leapY += step; if (leapY > stopY) leapY = stopY; timer++;
          } return;
      }
      for (var i = startY; i > stopY; i -= step) {
          this.scrollTo(leapY, timer * speed);
          leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
      }
  }

  scrollTo(yPoint: number, duration: number) {
      setTimeout(() => {
          this.window.window.scrollTo(0, yPoint)
      }, duration);
      return;
  }

  toggle_item(x){
    var checkbox = this.element.nativeElement.querySelector('#check_box_' + x);

    for(var i = 0; i < this.sub_categs.length; i++){
      if(this.sub_categs[i] == this.sub_categories[x]){
        checkbox.src="./assets/images/uncheck_box.png";
        this.sub_categs.splice(i,1);
        return;
      }
    }
    checkbox.src="./assets/images/check_box_v2.png";
    this.sub_categs.push(this.sub_categories[x]);
  }

  changed_job(event){
    if(this.job != event && this.job.length != 0){

      for(var i = 0; i < this.sub_categories.length; i++){
        var checkbox = this.element.nativeElement.querySelector('#check_box_' + i);
        checkbox.src="./assets/images/uncheck_box.png";
      }

      this.sub_categs.length = 0;
    }

    this.sub_categories = this.sub_cat.get_row_for_category(event);
    this.job = event;
    this.open_categs = true;
  }

  add_trade(){
    this.error_category = "";
    var profile_json = JSON.parse(this.datatransfer.get_tradesman_values_json());
    var jobs = JSON.parse(this.datatransfer.get_tradesman_values_json()).jobs;
    var jobs_json = JSON.parse(this.datatransfer.get_job_json()).jobs;
    var error = false;

    if(!this.job_specific.check_time()){
      error = true;
      this.smoothScroll('default_daily_time_availability_title');
    }

    if(!this.job_specific.check_location()){
      error = true;
      this.smoothScroll('default_location_title');
    }

    if(this.job == ""){
      this.smoothScroll('category_title');
      this.error_category = "*Please choose a job category*";
      error = true;
    }

    if(error){
      return;
    }

    var job_json = {
      job: this.job,
      sub_categs: this.sub_categs
    }

    jobs_json.push(job_json);

    var json_value = {
      jobs: jobs_json
    }

    this.datatransfer.set_SU_extra_info(this.datatransfer.get_company_name(), JSON.stringify(json_value));

    var T_comp = this.job_specific.Time_Components.toArray();
    var job = {
      "company_name": this.job_specific.company_name,
      "office_number": this.job_specific.office_number,
      "active": true,
      "job": this.job,
      "sub_categories": this.sub_categs,
      "pre_defined_skills": this.job_specific.chosen_pre_skill,
      "added_skills": this.job_specific.added_skills,
      "default_time_availability":{
          "start_time": T_comp[0].hour + ":" + T_comp[0].minute + T_comp[0].am_pm,
          "end_time": T_comp[1].hour + ":" + T_comp[1].minute + T_comp[1].am_pm,
          "start_hour": T_comp[0].hour,
          "start_minute": T_comp[0].minute,
          "start_am_pm": T_comp[0].am_pm,
          "end_hour": T_comp[1].hour,
          "end_minute": T_comp[1].minute,
          "end_am_pm": T_comp[1].am_pm
        },
      "location": this.job_specific.location,
      "recent_works": this.job_specific.rw_array,
      "gallery": this.job_specific.gallery_images,
      "facebook": this.job_specific.facebook,
      "linkedin": this.job_specific.linkedin,
      "twitter": this.job_specific.twitter,
      "website": this.job_specific.website
    }

    //jobs.push(job);

    if(this.datatransfer.is_dnd() && jobs.length == 2){
      jobs[0].active = !this.datatransfer.get_dnd();
    }

    var profile={
      "profile_pic": profile_json.profile_pic,
      "about_you": profile_json.about_you,
      "jobs":jobs
    }

    var rw_title = [];
    var rw_id = [];
    var rw_desc = [];
    var rw_images = [];
    var time = T_comp[0].hour + ":" + T_comp[0].minute + T_comp[0].am_pm + "-" + T_comp[1].hour + ":" + T_comp[1].minute + T_comp[1].am_pm;

    for(var z = 0; z < this.job_specific.rw_array.length; z++){
      rw_title.push(this.job_specific.rw_array[z].title);
      rw_desc.push(this.job_specific.rw_array[z].description);
      if(this.job_specific.rw_array[z].id == null){
        rw_id.push(0);
      }else{
        rw_id.push(this.job_specific.rw_array[z].id);
      }
      rw_id.push(this.job_specific.rw_array[z].id);
      var rw_image_small = new Array();
      var rw_images_file_index_small = new Array();
      for(var y = 0; y < this.job_specific.rw_array[z].images.length; y++){
        if(typeof this.job_specific.rw_array[z].images_file[y] == "string"){
          rw_image_small.push(this.job_specific.rw_array[z].rw_image_url[y]);
        }else{
          rw_image_small.push(this.job_specific.rw_array[z].images_file[y]);
        }
      }


      rw_images.push(rw_image_small);
    }

    this.datatransfer.set_load(true);
    this.jsonfetch.add_trade(this.datatransfer.get_user_id(), this.job, this.sub_categs, this.job_specific.added_skills, this.job_specific.chosen_pre_skill, this.job_specific.company_name,
              time, this.job_specific.location, this.job_specific.facebook, this.job_specific.twitter, this.job_specific.linkedin, this.job_specific.website,
              this.job_specific.gallery_images_file, rw_title, rw_desc, rw_images, rw_id)
        .map(res => res)
            .subscribe(
                res => this.check_result(res),
                error => this.error_happened(error)
            );
  }

  error_happened(res){
    this.datatransfer.set_load(false);
    console.log(res);
  }

  getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  check_result(res){
    this.datatransfer.set_load(false);
    console.log(res);
    if(res.STATUS == "OK"){
      var start_am_pm = res.DATA[0].start_minute.substring(res.DATA[0].start_minute.length - 2, res.DATA[0].start_minute.length);
      var start_minute = res.DATA[0].start_minute.substring(0, res.DATA[0].start_minute.length - 2);
      var end_am_pm = res.DATA[0].end_minute.substring(res.DATA[0].end_minute.length - 2, res.DATA[0].end_minute.length);
      var end_minute = res.DATA[0].end_minute.substring(0, res.DATA[0].end_minute.length - 2);
      var rw_array = [];
      var jobs = JSON.parse(this.datatransfer.get_tradesman_values_json()).jobs;

      for(var x = 0; x < res.DATA[0].recentwork.length; x++){
        var title_shown = "";
        var desc_shown = "";
        var rw_img_file = [];
        var rw_img_url = [];

        if(res.DATA[0].recentwork[x].recentwork_title.length > 15){
          title_shown = res.DATA[0].recentwork[x].recentwork_title.substr(0, 11) + "...";
        }else{
          title_shown = res.DATA[0].recentwork[x].recentwork_title;
        }

        if(res.DATA[0].recentwork[x].recentwork_desc.length > 100){
          desc_shown = res.DATA[0].recentwork[x].recentwork_desc.substr(0, 96) + "...";
        }else{
          desc_shown = res.DATA[0].recentwork[x].recentwork_desc;
        }

        for(var y = 0; y < res.DATA[0].recentwork[x].recentwork_image.length; y++){
          rw_img_url.push(res.DATA[0].recentwork[x].recentwork_image[y]);
          res.DATA[0].recentwork[x].recentwork_image[y] = 'http://jaxoftrades.com/api/' + res.DATA[0].recentwork[x].recentwork_image[y] + "?" + this.getRandomInt(90000000000, 100000000000);
          rw_img_file.push(res.DATA[0].recentwork[x].recentwork_image[y]);
        }

        var rw = {
          "title": res.DATA[0].recentwork[x].recentwork_title,
          "description": res.DATA[0].recentwork[x].recentwork_desc,
          "images": res.DATA[0].recentwork[x].recentwork_image,
          "images_file": rw_img_file,
          "title_shown": title_shown,
          "description_shown": desc_shown,
          "chosen_bg": 0,
          "cost": 0,
          "id": res.DATA[0].recentwork[x].recentwork_id,
          "rw_image_url": rw_img_url
        }

        rw_array.push(rw);

      }

      var gallery = [];

      for(var y = 0; y < res.DATA[0].gallery_images.length; y++){
        gallery.push(res.DATA[0].gallery_images[y]);
      }

      var job = {
        "company_name": res.DATA[0].company_name,
        "office_number": "(000) -000-0000",
        "active": false,
        "job": res.DATA[0].job_type,
        "sub_categories": res.DATA[0].job_sub_type,
        "pre_defined_skills": res.DATA[0].predefined_skills,
        "added_skills": res.DATA[0].skills,
        "default_time_availability":{
            "start_time": res.DATA[0].start_hour + ":" + start_minute + start_am_pm,
            "end_time": res.DATA[0].end_hour + ":" + end_minute + end_am_pm,
            "start_hour": res.DATA[0].start_hour,
            "start_minute": start_minute,
            "start_am_pm": start_am_pm,
            "end_hour": res.DATA[0].end_hour,
            "end_minute": end_minute,
            "end_am_pm": end_am_pm
          },
        "location": res.DATA[0].default_location,
        "recent_works": rw_array,
        "gallery": gallery,
        "facebook": res.DATA[0].facebook_link,
        "linkedin": res.DATA[0].linked_in,
        "twitter": res.DATA[0].twitter_link,
        "website": res.DATA[0].website
      }

      jobs.push(job);

      var profile2={
        "profile_pic": JSON.parse(this.datatransfer.get_tradesman_values_json()).profile_pic,
        "profile_url": JSON.parse(this.datatransfer.get_tradesman_values_json()).profile_url,
        "about_you": JSON.parse(this.datatransfer.get_tradesman_values_json()).about_you,
        "jobs":jobs
      }

      this.datatransfer.save_tradesman_values_json(JSON.stringify(profile2));
      this.router.navigate(['./tradesman-edit-profile']);
    }
  }

  set_blur(event){
    if(event){
      this.container_id = "container_blur";
    }else{
      this.container_id = "container";
    }
  }

  set_bottom_height(){
    var height = window.innerHeight;
    var value = height - 290;

    if(window.innerHeight < 800){
      return{
        "min-height": "3vw"
      }
    }else{
      return{
        "min-height": value + "px"
      }
    }
  }
}

function currentYPosition() {
    // Firefox, Chrome, Opera, Safari
    if (self.pageYOffset) return self.pageYOffset;
    // Internet Explorer 6 - standards mode
    if (document.documentElement && document.documentElement.scrollTop)
        return document.documentElement.scrollTop;
    // Internet Explorer 6, 7 and 8
    if (document.body.scrollTop) return document.body.scrollTop;
    return 0;
}
function elmYPosition(eID) {
    var elm = document.getElementById(eID);
    var y = elm.offsetTop;
    var node = elm;
    while (node.offsetParent && node.offsetParent != document.body) {
        node = <HTMLElement><any>node.offsetParent;
        y += node.offsetTop;
    } return y;
}
