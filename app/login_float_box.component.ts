import { Component, ElementRef, Input, trigger, state, style, transition, animate } from '@angular/core';
import { Router} from '@angular/router';
import { DataTransferService } from './data_transfer.service';
import { JsonFetchService } from './JsonFetch.service';
import { PushNotificationsService } from 'angular2-notifications';

@Component({
  selector: 'login-float',
  templateUrl: './app/HTML/login_float_box.html',
  styleUrls: ['css/login.css'],
})

export class LoginFloatBoxComponent {

  remember_state = false;
  error_string:string = "";
  logo_id = "logo";
  title_id = "login_title";
  divider_id = "login_section_divider";
  page_state = "active";

  email = "";
  password = "";

  email_focus = false;
  password_focus = false;

  forgot_pass_email = "";
  forgot_pass_email_focus = false;

  forgot_pass_error = "*Please enter the email you used for creating an account*";

  @Input()
  public correct: Function;

  @Input()
  public sign_up: Function;

  constructor(private element: ElementRef, public router: Router, private datatransfer:DataTransferService, private jsonfetch: JsonFetchService, private push_notif: PushNotificationsService) {}

  ngAfterViewInit(){
    if(this.datatransfer.get_redir_forgot()){
      this.forgot_pop();
    }
  }

  forgot_pop(){
    var login = this.element.nativeElement.querySelector('#login_box');
    var forgot = this.element.nativeElement.querySelector('#forgot_password');
    var err = this.element.nativeElement.querySelector('#forgot_error');

    err.style.opacity = 0;
    login.style.opacity = 0;
    setTimeout(() => {
      login.style.display = "none";
      forgot.style.display= "block";
      forgot.style.opacity = "1";
    },350);
  }

  set_bg(value){
    var text = '#' + value;
    var box = this.element.nativeElement.querySelector(text);
    return{
      "background-color": box.style.backgroundColor
    }
  }

  forgot_close(){
    var err = this.element.nativeElement.querySelector('#forgot_error');
    var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

    if(!testEmail.test(this.forgot_pass_email)){
      this.forgot_pass_error = "*Please enter a valid email address*";
      err.style.opacity = 1;
    }else{
      this.datatransfer.set_load(true);
      this.jsonfetch.send_forgot_password(this.forgot_pass_email)
        .map(res => res)
        .subscribe(
            res => this.check_forgot_result(res),
            error => this.error_happened_forgot()
        );
    }
  }

  check_forgot_result(res){
    var err = this.element.nativeElement.querySelector('#forgot_error');
    this.datatransfer.set_load(false);
    console.log(res);
    if(res.STATUS == "ERROR"){
      this.forgot_pass_error = "*No such email found*";
      err.style.opacity = 1;
    }else{
      console.log(res);
      this.forgot_back();
    }
  }

  error_happened_forgot(){
    var err = this.element.nativeElement.querySelector('#forgot_error');
    this.datatransfer.set_load(false);
    this.forgot_pass_error = "*Please check your internet connection and try again*";
    err.style.opacity = 1;
  }



  forgot_back(){
    var login = this.element.nativeElement.querySelector('#login_box');
    var forgot = this.element.nativeElement.querySelector('#forgot_password');
    var err = this.element.nativeElement.querySelector('#forgot_error');

    err.style.opacity = 0;
    forgot.style.opacity = 0;
    setTimeout(() => {
      forgot.style.display = "none";
      login.style.display= "block";
      login.style.opacity = "1";
    },350);
  }

  tick_remember(){
    this.remember_state = !this.remember_state;
    var image = this.element.nativeElement.querySelector('#check_box_ic');
    if(this.remember_state){
      image.src = "./assets/images/check_box_v2.png";
    }else{
      image.src = "./assets/images/uncheck_box.png"
    }
  }

  remove_error(){
    var error_box = this.element.nativeElement.querySelector('#error_container');
    error_box.style.opacity = "0";
    setTimeout(() => {
      this.logo_id = "logo";
      this.title_id = "login_title";
      this.divider_id = "login_section_divider";
    },500);
  }

  login(){
    if(this.email == "" && this.password == ""){
      this.error_string = "*Email and password is empty*";
      this.open_error();
    }else if(this.email == ""){
      this.error_string = "*Email is empty*";
      this.open_error();
    }else if(this.password == ""){
      this.error_string = "*Password is empty*";
      this.open_error();
    }else{
      this.datatransfer.set_load(true);
      var location = this.router.url;
      var require_homeowner = false;
      if(location == "/login"){
        require_homeowner = false;
      }else{
        require_homeowner = true;
      }
      this.jsonfetch.send_login("http://10.10.35.148/jaxoftrades/API/login.PHP", this.email, this.password, require_homeowner)
        .map(res => res)
            .subscribe(
                res => this.check_result(res),
                error => this.error_happened()
            );
      /*if(this.email == "asd" && this.password == "asd"){
        this.correct();
      }else{
        this.wrong();
      }*/
    }

  }

  error_happened(){

    this.datatransfer.set_load(false);
    this.error_string = "*Error connecting to server, check your connection and try again*";
    this.open_error();
  }

  getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  check_result(res){
    this.datatransfer.set_load(false);
    console.log(res);
    if(res.STATUS == "OK"){
      this.push_notif.requestPermission();
      if(res.DATA[0].t_text != null){
        this.datatransfer.set_testimonial(res.DATA[0].t_text);
      }
      if(res.DATA[0].user_type== "1"){
        if(res.DATA[0].profile_imgpath.length != 0){
          this.datatransfer.set_homeowner_profile_image(res.DATA[0].profile_imgpath);
        }
        this.datatransfer.user_login(res.DATA[0].homeowner_id, res.DATA[0].h_first_name, res.DATA[0].h_last_name, res.DATA[0].email , 1, res.DATA[0].contact_number);
      }else if(res.DATA[0].user_type== "2"){
        this.datatransfer.set_inc_req_transfer(JSON.stringify(res.incoming_requests));
        this.datatransfer.set_tradesman_rating(res.DATA[0].avg_rating);
        this.datatransfer.set_tradesman_rating_num(res.DATA[0].total_rated);

        var mult_category_array = new Array();
        var mult_job_array = new Array();
        var about_you= res.DATA[0].category[0].about_you;
        var profile_pic = null;
        var dnd_array = [];

        for(var i = 0; i < res.DATA[0].category.length; i++){
          var recent_works = [];
          if(res.DATA[0].category[i].dnd == "1"){
            dnd_array.push(false);
          }else{
            dnd_array.push(true);
          }
          var job = {
            "company_name": res.DATA[0].company_name,
            "office_number": res.DATA[0].contact_number,
            "active": true,
            "job": res.DATA[0].category[i].job_type,
            "sub_categories": res.DATA[0].category[i].job_sub_type,
            "pre_defined_skills": res.DATA[0].category[i].predefined_skills,
            "added_skills": res.DATA[0].category[i].skills,
            "default_time_availability":{
                "start_time": res.DATA[0].category[i].start_hour + ":" + res.DATA[0].category[i].start_minute + res.DATA[0].category[i].start_am_pm,
                "end_time": res.DATA[0].category[i].end_hour + ":" + res.DATA[0].category[i].end_minute + res.DATA[0].category[i].end_am_pm,
                "start_hour": res.DATA[0].category[i].start_hour,
                "start_minute": res.DATA[0].category[i].start_minute,
                "start_am_pm": res.DATA[0].category[i].start_am_pm,
                "end_hour": res.DATA[0].category[i].end_hour,
                "end_minute": res.DATA[0].category[i].end_minute,
                "end_am_pm": res.DATA[0].category[i].end_am_pm,
              },
            "location": res.DATA[0].category[i].Default_location,
            "recent_works": recent_works,
            "gallery": res.DATA[0].category[i].gallery,
            "facebook": res.DATA[0].category[i].facebook_link,
            "linkedin": res.DATA[0].category[i].linked_in,
            "twitter": res.DATA[0].category[i].twitter_link,
            "website": res.DATA[0].category[i].website
          }

          for(var x = 0; x < res.DATA[0].category[i].recentwork.length; x++){

            var title_shown = "";
            var description_shown = "";
            var rw_img_file = [];
            var rw_img_url = [];

            if(res.DATA[0].category[i].recentwork[x].recentwork_title > 15){
              title_shown = res.DATA[0].category[i].recentwork[x].recentwork_title.substr(0, 11) + "...";
            }else{
              title_shown = res.DATA[0].category[i].recentwork[x].recentwork_title;
            }

            if(res.DATA[0].category[i].recentwork[x].recentwork_desc > 100){
              description_shown = res.DATA[0].category[i].recentwork[x].recentwork_desc.substr(0, 96) + "...";
            }else{
              description_shown = res.DATA[0].category[i].recentwork[x].recentwork_desc;
            }

            for(var y = 0; y < res.DATA[0].category[i].recentwork[x].recentwork_image.length; y++){
              rw_img_url.push(res.DATA[0].category[i].recentwork[x].recentwork_image[y]);
              res.DATA[0].category[i].recentwork[x].recentwork_image[y] = 'http://jaxoftrades.com/api/' + res.DATA[0].category[i].recentwork[x].recentwork_image[y] + "?" + this.getRandomInt(90000000000, 100000000000);
              rw_img_file.push(res.DATA[0].category[i].recentwork[x].recentwork_image[y]);
            }

            var rw = {
              "title": res.DATA[0].category[i].recentwork[x].recentwork_title,
              "description": res.DATA[0].category[i].recentwork[x].recentwork_desc,
              "images": res.DATA[0].category[i].recentwork[x].recentwork_image,
              "images_file": rw_img_file,
              "title_shown": title_shown,
              "description_shown": description_shown,
              "chosen_bg": 0,
              "id": res.DATA[0].category[i].recentwork[x].recentwork_id,
              "rw_image_url": rw_img_url
            }

            recent_works.push(rw);
          }

          var job_object = {
            "job": res.DATA[0].category[i].job_type,
            "sub_categs": res.DATA[0].category[i].job_sub_type,
          }

          mult_category_array.push(job);
          mult_job_array.push(job_object);

        }
        this.datatransfer.set_dnd(JSON.stringify(dnd_array));

        var jobs_array = {
          "jobs": mult_job_array
        }

        if(res.DATA[0].profile_imgpath.length != 0){
          profile_pic = 'http://jaxoftrades.com/api/' + res.DATA[0].profile_imgpath + "?" + this.getRandomInt(123215125612, 51521535135125125);
        }

        var profile={
          "profile_pic": profile_pic,
          "profile_url": res.DATA[0].profile_imgpath,
          "about_you": about_you,
          "jobs": mult_category_array
        }

        this.datatransfer.set_SU_extra_info(res.DATA[0].company_name, JSON.stringify(jobs_array));
        this.datatransfer.save_tradesman_values_json(JSON.stringify(profile));

        if(res.DATA[0].company.length != 0){
          var rw_array = [];

          for(var x = 0; x < res.DATA[0].company[0].recentwork.length; x++){
            var title_shown = "";
            var desc_shown = "";
            var rw_img_file = new Array();
            var rw_img_url = new Array();

            if(res.DATA[0].company[0].recentwork[x].recentwork_title.length > 15){
              title_shown = res.DATA[0].company[0].recentwork[x].recentwork_title.substr(0, 11) + "...";
            }else{
              title_shown = res.DATA[0].company[0].recentwork[x].recentwork_title;
            }

            if(res.DATA[0].company[0].recentwork[x].recentwork_desc.length > 100){
              desc_shown = res.DATA[0].company[0].recentwork[x].recentwork_desc.substr(0, 96) + "...";
            }else{
              desc_shown = res.DATA[0].company[0].recentwork[x].recentwork_desc;
            }

            for(var y = 0; y < res.DATA[0].company[0].recentwork[x].recentwork_image.length; y++){
              rw_img_url.push(res.DATA[0].company[0].recentwork[x].recentwork_image[y]);
              res.DATA[0].company[0].recentwork[x].recentwork_image[y] = 'http://jaxoftrades.com/api/' + res.DATA[0].company[0].recentwork[x].recentwork_image[y] + "?" + this.getRandomInt(90000000000, 100000000000);
              rw_img_file.push(res.DATA[0].company[0].recentwork[x].recentwork_image[y]);
            }

            var rw_2 = {
              "title": res.DATA[0].company[0].recentwork[x].recentwork_title,
              "description": res.DATA[0].company[0].recentwork[x].recentwork_desc,
              "images": res.DATA[0].company[0].recentwork[x].recentwork_image,
              "images_file": rw_img_file,
              "title_shown": title_shown,
              "description_shown": desc_shown,
              "chosen_bg": 0,
              "cost": 0,
              "id": res.DATA[0].company[0].recentwork[x].recentwork_id,
              "rw_image_url": rw_img_url
            }

            rw_array.push(rw_2);

          }

          var gallery = [];

          for(var y = 0; y < res.DATA[0].company[0].gallery_images.length; y++){
            gallery.push(res.DATA[0].company[0].gallery_images[y]);
          }

          var company = {
            "company_pic": 'http://jaxoftrades.com/api/' + res.DATA[0].company[0].c_photo + "?" + this.getRandomInt(90000000000, 100000000000),
            "company_name": res.DATA[0].company[0].c_name,
            "about_your_company": res.DATA[0].company[0].c_about,
            "address": {
              "unit_number": "",
              "street_address": res.DATA[0].company[0].c_add,
              "city_town_village": res.DATA[0].company[0].c_city,
              "postal_code": res.DATA[0].company[0].c_postalcode,
              "province": res.DATA[0].company[0].c_province
            },
            "email": res.DATA[0].company[0].c_email,
            "cell_number": res.DATA[0].company[0].c_number,
            "office_number": res.DATA[0].company[0].c_number,
            "specialize_in": res.DATA[0].company[0].c_spec,
            "recent_works": rw_array,
            "gallery_images": gallery,
            "facebook": res.DATA[0].company[0].facebook_link,
            "linkedin": res.DATA[0].company[0].linked_in,
            "twitter": res.DATA[0].company[0].twitter_link,
            "website": res.DATA[0].company[0].website
          }
          this.datatransfer.set_company_profile(JSON.stringify(company));
        }

        var histories = [];
        for(var x = 0; x < res.History_data.length; x++){
          var state = 0;
          if(res.History_data[x].req_rating == 2){
            state = 3;
          }else if(res.History_data[x].req_rating == 1){
            state = 1;
          }else if(res.History_data[x].req_rating == 0 && res.History_data[x].job_status == 1){
            state = 2;
          }else if(res.History_data[x].job_status == 0){
            state = 0;
          }else{
            continue;
          }

          var history = {
            "state": state,
            "first_name": res.History_data[x].h_first_name,
            "last_name": res.History_data[x].h_last_name,
            "date": res.History_data[x].work_date,
            "job": res.History_data[x].job_type
          }

          histories.splice(0, 0, history);
        }

        this.datatransfer.set_tradesman_history(JSON.stringify(histories));
        this.datatransfer.user_login(res.DATA[0].tradesmen_id, res.DATA[0].tr_first_name, res.DATA[0].tr_last_name, res.DATA[0].email , 2, res.DATA[0].contact_number);
      }

      this.correct();
    }else{
      if(res.DATA[0] != null){
        this.error_string = "*Please use homeowner credentials*"
      }else{
        this.error_string = "*Username and password mismatch*";
      }
      this.open_error();
    }
  }

  open_error(){
    var error_box = this.element.nativeElement.querySelector('#error_box');

    this.logo_id = "error_logo";
    this.title_id = "error_login_title";
    this.divider_id = "error_login_section_divider";

    setTimeout(() => {
      error_box.style.opacity = "1";
    },800);
  }

  signup(){
    this.sign_up();
  }

  hide_label(value, value_2){
    if(value.length == 0 && value_2 == false){
      return{
        "font-size": "12pt",
        "top": "10px"
      }
    }else{
      return{
        "font-size": "7pt",
        "top": "6px"
      }
    }
  }

  keypress(value){
    if(value == 13){
      this.login();
    }
  }

}
