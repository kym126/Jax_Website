import { Component, ElementRef, Input, trigger, state, style, transition, animate } from '@angular/core';
import { Router} from '@angular/router';
import { DataTransferService } from './data_transfer.service';
import { JsonFetchService } from './JsonFetch.service';
import { PushNotificationsService } from 'angular2-notifications';
import { SubCategoriesService } from './subcategories.service';

@Component({
  selector: 'sign-up',
  templateUrl: './app/HTML/signup.html',
  styleUrls: ['css/signup.css'],
  /*host: {
     '[@routeAnimation]': 'true',
     '[style.display]': "'block'",
     '[style.position]': "'absolute'",
     '[style.height]': "'100%'",
     '[style.width]': "'100%'"
  },
  animations: [
    trigger('routeAnimation', [
      state('*', style({transform: 'translateX(0)'})),
      transition('void => *', [
        style({transform: 'translateX(100%)'}),
        animate(700)
      ]),
      transition('* => void', animate(700, style({transform: 'translateX(100%)'})))
    ])
  ]*/
})

export class SignUpComponent {

  lat = null;
  long = null;
  choice = 0;
  logo_id = "logo";
  fill_sheet_id = "fill_sheet";
  form_container_id = "form_container";
  title_id = "title";
  form_id = "form";
  signup_container_id = "signup_container";
  signup_title_id = "signup_title";
  section_title_id = "section_title";
  shadow_id = "shadow";
  quote = "ASD";
  email = "";
  firstname = "";
  lastname = "";
  password = "";
  c_password = "";
  p_number = "";
  c_name = "";
  email_focus = false;
  firstname_focus = false;
  lastname_focus = false;
  password_focus = false;
  c_password_focus = false;
  p_number_focus = false;
  c_name_focus = false;
  error_title = "";
  submitted = false;
  select_val = "Select a category";
  job_menu = false;
  jobs = ["Plumber", "Electrician", "Gardener", "Cleaners", "Painter", "Carpenter", "Roofing", "Flooring", "Movers", "Handyman",];
  sub_categories = [" sub-category 1", " sub-category 2", " sub-category 3", " sub-category 4"];

  chosen_sub_categs = [];

  homeOwner:Homeowner;

  constructor(private element: ElementRef, public router: Router, private datatransfer:DataTransferService, private jsonfetch: JsonFetchService, private push_notif: PushNotificationsService, private sub_categories_service: SubCategoriesService) {
    if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "2"){
      if(this.datatransfer.get_tradesman_values_json() == null){
        this.router.navigate(['./edit-profile']);
      }else{
        this.router.navigate(['./dashboard']);
      }
    }else if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "1"){
      this.router.navigate(['./home']);
    }
  }

  ngAfterViewInit(){
    if(this.datatransfer.is_register_as_tradesman()){
      setTimeout(() => {
        this.tradesman();
      },100);
    }
  }

  go_back(){
    this.router.navigate(['./login']);
  }

  reset_boxes(){
    var email_box = this.element.nativeElement.querySelector('#email');
    var fname_box = this.element.nativeElement.querySelector('#fname');
    var lname_box = this.element.nativeElement.querySelector('#lname');
    var pass_box = this.element.nativeElement.querySelector('#password');
    var cpass_box = this.element.nativeElement.querySelector('#confirm_password');
    var contact_box = this.element.nativeElement.querySelector('#mobile_number');
    var err_title = this.element.nativeElement.querySelector('#error_title');

    err_title.style.opacity = 0;
    pass_box.style.border = "1px solid #d6d7d7";
    cpass_box.style.border = "1px solid #d6d7d7";
    email_box.style.border = "1px solid #d6d7d7";
    fname_box.style.border = "1px solid #d6d7d7";
    lname_box.style.border = "1px solid #d6d7d7";
    contact_box.style.border = "1px solid #d6d7d7";
  }

  homeowner(){
    var homeowner = this.element.nativeElement.querySelector('#homeowner');
    var tradesman = this.element.nativeElement.querySelector('#tradesman');
    var subtitle = this.element.nativeElement.querySelector('#sub_title');
    var tradesman_fields = this.element.nativeElement.querySelector('#tradesman_fields');
    var email = this.element.nativeElement.querySelector('#email_title');
    var contact_optional = this.element.nativeElement.querySelector('#contact_optional');

    if(this.choice == 2 || this.choice == 0){
      subtitle.style.opacity = "0";

      this.reset_boxes();
      this.form_id = "form";
      tradesman_fields.style.maxHeight = "0";

      setTimeout(() => {
        this.quote = "Hiring workers is just a form away.";
        subtitle.style.opacity = "1";
        this.form_id = "form_active";
        this.section_title_id="section_title";
        contact_optional.style.opacity = 1;
      },500);

      this.choice = 1;
      this.update_style();

      homeowner.style.backgroundColor = "#fff";
      homeowner.style.color = "#525250";
      tradesman.style.backgroundColor = "transparent";
      tradesman.style.color = "#fff";
    }
  }

  tradesman(){
    var homeowner = this.element.nativeElement.querySelector('#homeowner');
    var tradesman = this.element.nativeElement.querySelector('#tradesman');
    var subtitle = this.element.nativeElement.querySelector('#sub_title');
    var tradesman_fields = this.element.nativeElement.querySelector('#tradesman_fields');
    var email = this.element.nativeElement.querySelector('#email_title');
    var contact_optional = this.element.nativeElement.querySelector('#contact_optional');

    if(this.choice == 1 || this.choice == 0){
      subtitle.style.opacity = "0";
      this.reset_boxes();

      this.form_id = "form";
      contact_optional.style.opacity = 0;

      setTimeout(() => {
        email.style.marginTop = "0px";
        this.quote = "Let's create your profile.";
        subtitle.style.opacity = "1";
        this.form_id = "form_active";
        tradesman_fields.style.maxHeight = "700px";
        this.section_title_id="section_title_active";
      },500);

      this.choice = 2;
      this.update_style();

      tradesman.style.backgroundColor = "#fff";
      tradesman.style.color = "#525250";
      homeowner.style.backgroundColor = "transparent";
      homeowner.style.color = "#fff";
    }
  }

  update_style(){
    this.logo_id = "logo_active";
    this.fill_sheet_id="fill_sheet_active";
    this.form_container_id = "form_container_active";
    this.signup_container_id = "signup_container_active";
    this.signup_title_id = "signup_title_active";
    this.shadow_id = "shadow_active";
    this.title_id = "title_active";

    setTimeout(() => {
      this.title_id = "title_active";
      this.form_id = "form_active";
    },800);

  }

  hide_name(input, input_2){
    if(input.length == 0 && input_2 == false){
      return{
        "font-size": "10pt",
        "top": "13px"
      }
    }else{
      return{
        "font-size": "7pt",
        "top": "6px"
      }
    }
  }

  /*choice_status(val){
    var home = this.element.nativeElement.querySelector('#home');
    var work = this.element.nativeElement.querySelector('#work');
    var mobile = this.element.nativeElement.querySelector('#mobile');
    var home_img = this.element.nativeElement.querySelector('#home_img');
    var work_img = this.element.nativeElement.querySelector('#work_img');
    var mobile_img = this.element.nativeElement.querySelector('#mobile_img');


    if(val == 0){
      home.style.backgroundColor = "#8BC34A";
      home.style.color = "#fff";
      home_img.src = "./assets/images/checked_btn.png";
      home.style.boxShadow = "inset 0 2px 2px 1px rgba(0,0,0,0.3)";
      work.style.backgroundColor = "#fff";
      work.style.color = "#777";
      work_img.src = "./assets/images/unchecked_btn.png";
      work.style.boxShadow = "none";
      mobile.style.backgroundColor = "#fff";
      mobile.style.color = "#777";
      mobile_img.src = "./assets/images/unchecked_btn.png";
      mobile.style.boxShadow = "none";
    }else if(val == 1){
      work.style.backgroundColor = "#8BC34A";
      work.style.color = "#fff";
      work_img.src = "./assets/images/checked_btn.png";
      work.style.boxShadow = "inset 0 2px 2px 1px rgba(0,0,0,0.3)";
      home.style.backgroundColor = "#fff";
      home.style.color = "#777";
      home_img.src = "./assets/images/unchecked_btn.png";
      home.style.boxShadow = "none";
      mobile.style.backgroundColor = "#fff";
      mobile.style.color = "#777";
      mobile_img.src = "./assets/images/unchecked_btn.png";
      mobile.style.boxShadow = "none";
    }else{
      mobile.style.backgroundColor = "#8BC34A";
      mobile.style.color = "#fff";
      mobile_img.src = "./assets/images/checked_btn.png";
      mobile.style.boxShadow = "inset 0 2px 2px 1px rgba(0,0,0,0.3)";
      home.style.backgroundColor = "#fff";
      home.style.color = "#777";
      home_img.src = "./assets/images/unchecked_btn.png";
      home.style.boxShadow = "none";
      work.style.backgroundColor = "#fff";
      work.style.color = "#777";
      work_img.src = "./assets/images/unchecked_btn.png";
      work.style.boxShadow = "none";
    }
  }*/

  /*c_name_title(){
    if(this.c_name.length == 0){
      return "(Optional)";
    }else{
      return "Company name (Optional)";
    }
  }

  c_web_title(){
    if(this.c_website.length == 0){
      return "(Optional)";
    }else{
      return "Company website (Optional)";
    }
  }*/

  remove_style(id){
    this.element.nativeElement.querySelector(id).removeAttribute("style");
  }

  remove_conf_pass_del(){
    var conf_pass_del = this.element.nativeElement.querySelector('#conf_pass_del');
    conf_pass_del.style.display = "none";
  }

  conf_pass_del(event){
    this.c_password = "";
  }

  signup(){
    this.submitted = true;
    var err_title = this.element.nativeElement.querySelector('#error_title');
    var error = false;
    var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    var testPhone = /^\(+([0-9]{3})+\)+\ +([0-9]{3})[-]?([0-9]{4})$/;

    var email_box = this.element.nativeElement.querySelector('#email');
    var fname_box = this.element.nativeElement.querySelector('#fname');
    var lname_box = this.element.nativeElement.querySelector('#lname');
    var pass_box = this.element.nativeElement.querySelector('#password');
    var cpass_box = this.element.nativeElement.querySelector('#confirm_password');
    var contact_box = this.element.nativeElement.querySelector('#mobile_number');
    var value = this.element.nativeElement.querySelector('#select_value_wrapper');
    var conf_pass_del = this.element.nativeElement.querySelector('#conf_pass_del');

    if(this.select_val == "Select a category" && this.choice == 2){
      this.error_title = "Please select a category";
      error = true;
      value.style.border = "1px solid #F27072";
    }

    if(this.password.length != 0 && this.c_password.length != 0 && this.password != this.c_password){
      this.error_title = "Passwords mismatch";
      error = true;
      pass_box.style.border = "1px solid #F27072";
      cpass_box.style.border = "1px solid #F27072";
      conf_pass_del.style.display= "block";
    }

    if(!testPhone.test(this.p_number) && this.choice == 2){
      this.error_title = "Please input a valid contact number";
      error = true;
      contact_box.style.border = "1px solid #F27072";
    }

    if(!testEmail.test(this.email)){
      this.error_title = "Please input a valid email address";
      error = true;
      email_box.style.border = "1px solid #F27072";
    }

    if( this.password.length == 0 ){
      this.error_title = "Please fill up all fields outlined with red and resubmit";
      error = true;
      pass_box.style.border = "1px solid #F27072";
    }

    if(this.c_password.length == 0){
      this.error_title = "Please fill up all fields outlined with red and resubmit";
      error = true;
      cpass_box.style.border = "1px solid #F27072";
    }

    if(this.email.length == 0){
      this.error_title = "Please fill up all fields outlined with red and resubmit";
      error = true;
      email_box.style.border = "1px solid #F27072";
    }

    if(this.firstname.length == 0){
      this.error_title = "Please fill up all fields outlined with red and resubmit";
      error = true;
      fname_box.style.border = "1px solid #F27072";
    }

    if(this.lastname.length == 0){
      this.error_title = "Please fill up all fields outlined with red and resubmit";
      error = true;
      lname_box.style.border = "1px solid #F27072";
    }

    if(this.p_number.length == 0 && this.choice == 2){
      this.error_title = "Please fill up all fields outlined with red and resubmit";
      error = true;
      contact_box.style.border = "1px solid #F27072";
    }

    if(error == false){
         err_title.style.opacity = 0;
         this.datatransfer.set_load(true);
         var timeoutVal = 10 * 1000 * 1000;
         navigator.geolocation.getCurrentPosition(
           position => this.gps_found(position),
           error => this.send_signup_datas(error),
           { enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 }
         );

    }else{
      err_title.style.opacity = 1;
    }
  }

  gps_found(position){
    this.lat = position.coords.latitude;
    this.long = position.coords.longitude;
    this.send_signup_datas(null);
  }

  send_signup_datas(error){
    this.jsonfetch.send_xhr("http://192.168.0.127/jaxoftrades/API/signup.PHP", this.choice, this.email, this.firstname, this.lastname, this.password, this.p_number, this.c_name, this.select_val, this.chosen_sub_categs, this.lat, this.long)
      .map(res => res)
          .subscribe(
              res => this.check_result(res),
              error => this.error_happened(error)
          );
  }

  error_happened(res){
    console.log(res);
    var email_box = this.element.nativeElement.querySelector('#email');
    var err_title = this.element.nativeElement.querySelector('#error_title');
    this.datatransfer.set_load(false);
    if(res.Message == "This email is already used"){
      this.error_title = "This email has already been used. Please try a different email address.";
      email_box.style.border = "1px solid #F27072";
    }else{
      this.error_title = "Error connecting to server, check your connection and try again";
    }
    err_title.style.opacity = 1;
  }

  check_result(res){
    this.datatransfer.set_load(false);
    console.log(res);
    if(res.STATUS == "OK"){
      this.push_notif.requestPermission();
      if(this.choice == 1){
        this.datatransfer.user_login(res.DATA[0].homeowner_id, this.firstname, this.lastname, this.email, this.choice, this.p_number);
        this.router.navigate(['./home']);
      }else{
        this.datatransfer.user_login(res.DATA[0].tradesmen_id, this.firstname, this.lastname, this.email, this.choice, this.p_number);
        var job = {
          jobs: [
            {
              job: this.select_val,
              sub_categs: this.chosen_sub_categs
            }
          ]
        }
        this.datatransfer.set_SU_extra_info(this.c_name, JSON.stringify(job));
        this.datatransfer.set_new_tm_loc(res.DATA[0].postal_code);
        this.router.navigate(['./edit-profile']);
      }
    }else{
      this.error_happened(res);
    }
  }

  keypress(value){
    if(value == 13 && this.choice == 1){
      this.signup();
    }
  }

  close_jobs(){
    var container = this.element.nativeElement.querySelector('#options');
    var value = this.element.nativeElement.querySelector('#select_value_wrapper');
    this.job_menu = false;
    container.style.maxHeight = "0px";
    setTimeout(() => {
      value.style.borderBottomLeftRadius = "5px";
      value.style.borderBottomRightRadius = "5px";
    },500);
  }

  toggle_jobs(event){
    var container = this.element.nativeElement.querySelector('#options');
    var value = this.element.nativeElement.querySelector('#select_value_wrapper');
    value.style.border = "1px solid #d6d7d7";
    event.stopPropagation();

    if(this.job_menu){
        this.close_jobs();
    }else{
      value.style.borderBottomLeftRadius = "0";
      value.style.borderBottomRightRadius = "0";
      container.style.maxHeight = "411px";
      this.job_menu = true;
    }

  }

  set_job(job){
    if(this.select_val != job && this.select_val != "Select a category"){
      this.reset_sub_categs();
    }
    this.sub_categories = this.sub_categories_service.get_row_for_category(job);
    this.select_val = job;
  }

  set_last_item(value){
    if(value == this.jobs.length - 1){
      return{
        "border-bottom-left-radius": "5px",
        "border-bottom-right-radius": "5px",
        "border-bottom": "1px solid #d6d7d7"
      }
    }
  }

  cat_chosen(){
    if(this.select_val != "Select a category"){
      return true;
    }else{
      return false;
    }
  }

  reset_sub_categs(){
    for(var i = 0; i < this.sub_categories.length; i++){
      var img = this.element.nativeElement.querySelector('#sub-categ'+i);
      img.src = "./assets/images/uncheck_box.png";
    }
    this.chosen_sub_categs.length = 0;
  }

  toggle_sub_categ(index){
    var img = this.element.nativeElement.querySelector('#sub-categ'+index);
    for(var i = 0; i < this.chosen_sub_categs.length; i++){
      if(this.chosen_sub_categs[i] == this.sub_categories[index]){
        this.chosen_sub_categs.splice(i, 1);
        img.src = "./assets/images/uncheck_box.png";
        return;
      }
    }
    this.chosen_sub_categs.push(this.sub_categories[index]);
    img.src = "./assets/images/check_box_v2.png";
  }

  set_categ_color(input){
    if(input == "Select a category"){
      return{
        "color": "#929290"
      };
    }else{
      return{
        "color": "#525250"
      };
    }
  }

  contact_change(event){
    if(event.length == 4){
      this.p_number = "(" + event.substr(0,3)+ ") " + event[3];
    }else if(event.length == 6){
      this.p_number = event.substr(1,3);
    }else if(event.length == 10 && event[9] != "-"){
      this.p_number = event.substr(0,9) + "-" + event[9];
    }else if(event.length == 10 && event[9] == "-"){
      this.p_number = event.substr(0,9);
    }
  }

  set_predefined_area_code(){
    var code = this.element.nativeElement.querySelector('#predefined_area_code');
    code.style.opacity = 1;
  }

  remove_predefined_area_code(input){
    var code = this.element.nativeElement.querySelector('#predefined_area_code');

    if(input.length == 0){
      code.style.opacity = 0;
    }
  }

  scroll_to_bottom(){
    setTimeout(() => {
      var objDiv = this.element.nativeElement.querySelector('#form_container_active');
      objDiv.scrollTop = objDiv.scrollHeight;
      setTimeout(() => {
        var objDiv = this.element.nativeElement.querySelector('#form_container_active');
        objDiv.scrollTop = objDiv.scrollHeight;
        setTimeout(() => {
          var objDiv = this.element.nativeElement.querySelector('#form_container_active');
          objDiv.scrollTop = objDiv.scrollHeight;
          setTimeout(() => {
            var objDiv = this.element.nativeElement.querySelector('#form_container_active');
            objDiv.scrollTop = objDiv.scrollHeight;
            setTimeout(() => {
              var objDiv = this.element.nativeElement.querySelector('#form_container_active');
              objDiv.scrollTop = objDiv.scrollHeight;
              setTimeout(() => {
                var objDiv = this.element.nativeElement.querySelector('#form_container_active');
                objDiv.scrollTop = objDiv.scrollHeight;
              },30);
            },30);
          },30);
        },30);
      },30);
    },30);
  }

}

export class Homeowner{
  type: number;
  email: string;
  first_name: string;
  last_name: string;
  password: string;
  contact_number: string;

  constructor(type: number, email: string, first_name: string, last_name: string, password: string, contact_number: string) {
    this.type = type;
    this.email = email;
    this.first_name = first_name;
    this.last_name = last_name;
    this.password = password;
    this.contact_number = contact_number;
  }
}
