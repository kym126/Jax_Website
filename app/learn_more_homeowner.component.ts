import { Component, ElementRef, Input, trigger, state, style, transition, animate } from '@angular/core';
import './rxjs-operators';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';
import { Router} from '@angular/router';

@Component({
  selector: 'learn-more-homeowner',
  templateUrl: './app/HTML/learn_more_homeowner.html',
  styleUrls: ['css/learn_more_homeowner.css'],
})

export class LearnMoreHomeowner {
  title = "Find the very best local tradesmen in the GTA";
  sub_title = "Jax of Trades allows you to find the best local tradesmen in the Greater Toronto Area. From plumbing emergencies to researching the best home/office cleaning service, finding local tradespeople has never been easier or more convenient."
  explanation = "Duis non tincidunt orci, in mattis felis. Duis mollis vestibulum nibh, a molestie velit tincidunt sed. Aliquam vestibulum suscipit ante.";
  perks = [
    {
      "image": "./assets/images/icons/location.png",
      "title": "Location",
      "description": "Once you have chosen your category, Jax of Trades will then present you with a listing of contractors who are located in the closest proximity to your location. This is accomplished using the GPS coordinates from your smartphone which allows us to help you find tradesmen who are nearby and available to serve you faster!"
    },
    {
      "image": "./assets/images/icons/time.png",
      "title": "Time",
      "description": "You choose the time when you want the work done. This way, you can locate a contractor who is available to work in the time frame that works best for you."
    },
    {
      "image": "./assets/images/icons/profile.png",
      "title": "Profile",
      "description": "The Jax of Trades database contains the most reputable contractors in the GTA. You can browse through their profiles, read reviews, view previous work – and when you find the one you like – you can contact them directly on Jax of Trades!"
    },
  ]

  constructor(public router: Router, private datatransfer:DataTransferService){
    if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "2"){
      if(this.datatransfer.get_tradesman_values_json() == null){
        this.router.navigate(['./edit-profile']);
      }else{
        this.router.navigate(['./dashboard']);
      }
    }
  }
}
