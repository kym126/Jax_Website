import { Component, ElementRef, Input, trigger, state, style, transition, animate } from '@angular/core';
import { Router} from '@angular/router';
import { DataTransferService } from './data_transfer.service';
import { JsonFetchService } from './JsonFetch.service';

@Component({
  selector: 'reset-password',
  templateUrl: './app/HTML/reset_password.html',
  styleUrls: ['css/login.css'],
  /*host: {
     '[@routeAnimation]': "active",
     '[style.display]': "'block'",
     '[style.position]': "'absolute'",
     '[style.height]': "'100%'",
     '[style.width]': "'100%'"
   },
   animations: [
     trigger('routeAnimation', [
       state('*', style({transform: 'translateX(0)'})),
       transition('void => *', [
         style({transform: 'translateX(-100%)'}),
         animate(700)
       ]),
       transition('* => void', animate(700, style({transform: 'translateX(-100%)'})))
     ])
   ]*/
})

export class ResetPasswordComponent {


  pass = "";
  pass_focus = false;
  c_pass = "";
  c_pass_focus = false;
  forgot_pass_error = "hello";
  message = false;

  constructor(private element: ElementRef, private datatransfer:DataTransferService, private router:Router, private jsonfetch: JsonFetchService) {
    if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "2"){
      if(this.datatransfer.get_tradesman_values_json() == null){
        this.router.navigate(['./edit-profile']);
      }else{
        this.router.navigate(['./dashboard']);
      }
    }else if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "1"){
      this.router.navigate(['./home']);
    }
    if(this.getParameterByName("message") != null){
      this.message = true
    }
  }

  hide_label(value, value_2){
    if(value.length == 0 && value_2 == false){
      return{
        "font-size": "12pt",
        "top": "10px"
      }
    }else{
      return{
        "font-size": "7pt",
        "top": "6px"
      }
    }
  }

  send_pass(){
    var error = false;
    var pass = this.element.nativeElement.querySelector('#forgot_pass_email');
    var c_pass = this.element.nativeElement.querySelector('#forgot_pass_email_2');
    var f_error = this.element.nativeElement.querySelector('#forgot_error');

    this.forgot_pass_error = "";
    f_error.style.opacity = 0;

    if(this.pass.length == 0 || this.c_pass.length == 0){
      this.forgot_pass_error = "*Please fill up both fields*";
      error = true;
    }else if(this.pass != this.c_pass){
      this.forgot_pass_error = "*Passwords don't match*";
      error = true;
    }

    if(error){
      f_error.style.opacity = 1;
      pass.style.border = "1px solid #F27072";
      c_pass.style.border = "1px solid #F27072";
      return;
    }

    if(this.getParameterByName("encrypt") == null || this.getParameterByName("user_type") == null){
      f_error.style.opacity = 1;
      this.forgot_pass_error = "*Please close this page*";
      return;
    }

    this.datatransfer.set_load(true);
    this.jsonfetch.send_reset_password(this.pass, this.getParameterByName("encrypt") , this.getParameterByName("user_type"))
      .map(res => res)
          .subscribe(
              res => this.check_result_2(res),
              error => this.error_happened_2(error)
          );
  }



  getParameterByName(name) {
      var url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  error_happened_2(res){
    this.datatransfer.set_load(false);
    this.forgot_pass_error = "*Please check your internet connection and try again*";
  }

  check_result_2(res){
    console.log(res);
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.router.navigate(['./login']);
    }
  }

  keypress(value){
    if(value == 13){
      this.send_pass();
    }
  }

  redir_forgot(){
    this.datatransfer.set_redir_forgot();
    this.router.navigate(['./login']);
  }
}
