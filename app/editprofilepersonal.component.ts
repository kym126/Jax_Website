import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';
import { SubCategoriesService } from './subcategories.service';
import {ImageCropperComponent, CropperSettings} from 'ng2-img-cropper';

@Component({
  selector: 'edit-profile-personal',
  templateUrl: './app/HTML/edit_profile_personal.html',
  styleUrls: ['css/edit_profile.css'],

})

export class EditProfileComponent_Personal {

  @ViewChild(ImageCropperComponent) cropper:ImageCropperComponent;
  cropperSettings: CropperSettings;
  data: any;

  edit_profile_personal_container_id = "edit_profile_personal_container";

  first_name:string;
  last_name:string;
  name = "";
  email = "";
  mobile_number = "";
  office_number = "";
  @Input() job:string;
  @Input() sub_categ:string;
  @Input() c_name:string;
  @Input() image_input = "./assets/images/icons/default_profile_pic.png";
  sub_categs = [];
  sub_categories = [];
  profile_error = "";

  crop_image_overlay_id = "crop_image_overlay_hide";
  prev_image: any;

  @Output()
  blur = new EventEmitter();


  constructor(private element: ElementRef, private datatransfer:DataTransferService, private router: Router, private sub_cat: SubCategoriesService) {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.width = 100;
    this.cropperSettings.height = 100;
    this.cropperSettings.croppedWidth =100;
    this.cropperSettings.croppedHeight = 100;
    this.cropperSettings.canvasWidth = 500;
    this.cropperSettings.canvasHeight = 300;
    if(window.innerWidth < 800){
      this.cropperSettings.canvasWidth = 600;
      this.cropperSettings.canvasHeight = 400;
    }

    this.data = {};

    this.data.image = "./assets/images/icons/default_profile_pic.png";

    this.first_name = this.datatransfer.get_user_first_name();
    this.last_name = this.datatransfer.get_user_last_name();
    this.name = this.first_name + " " + this.last_name;
    this.mobile_number = this.datatransfer.get_user_contact();
    this.email = this.datatransfer.get_user_email();
    if(this.datatransfer.is_office_number()){
      this.office_number = this.datatransfer.get_office_number();
    }
    this.sub_categs = JSON.parse(this.datatransfer.get_job_json()).jobs[0].sub_categs;
  }

  ngAfterViewInit(){
    if(this.datatransfer.is_tradesman_profile() && !this.multiple_edit_values()){
      this.populate_sub_cat();
      setTimeout(() => {
        this.set_checkboxes();
        if(this.image_input != null){
          this.data.image = this.image_input;
        }
      },50);
    }
  }

  populate_sub_cat(){
    for(var x = 0; x < this.sub_cat.get_row_for_category(this.job).length; x++){
      this.sub_categories.push(this.sub_cat.get_row_for_category(this.job)[x]);
    }
  }

  set_checkboxes(){
    for(var i = 0; i < this.sub_categs.length; i++){
      for(var j = 0; j < this.sub_categories.length; j++){
        if(this.sub_categs[i] == this.sub_categories[j]){
          var checkbox = this.element.nativeElement.querySelector('#check_box_' + j);
          checkbox.src="./assets/images/check_box_v2.png";
        }
      }
    }
  }

  change_prof_pic(event){
    var reader = new FileReader();
    var _this = this;
    var image:any = new Image();
    this.prev_image = this.data.image;
    if(this.data.image == null){
      this.prev_image = "./assets/images/icons/default_profile_pic.png";
    }
    reader.onload = function(e) {
        var src = reader.result;
        image.src = src;
        _this.image_input = src;
        _this.cropper.setImage(image);
        _this.crop_image();
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  get_image(){
    if(this.data.image == "./assets/images/icons/default_profile_pic.png"){
      return null;
    }else{
      return this.data.image;
    }
  }

  get_name(){
    return this.name;
  }

  multiple_edit_values(){
    if(this.datatransfer.is_tradesman_profile()){
      var jobs = JSON.parse(this.datatransfer.get_tradesman_values_json()).jobs;
      if(jobs.length != 1){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

  change_opacity(value){
    if(value.length == 0){
      return{
        "opacity": 0
      }
    }else{
      return{
        "opacity": 1
      }
    }
  }

  toggle_item(x){
    var checkbox = this.element.nativeElement.querySelector('#check_box_' + x);

    for(var i = 0; i < this.sub_categs.length; i++){
      if(this.sub_categs[i] == this.sub_categories[x]){
        checkbox.src="./assets/images/uncheck_box.png";
        this.sub_categs.splice(i,1);
        return;
      }
    }
    checkbox.src="./assets/images/check_box_v2.png";
    this.sub_categs.push(this.sub_categories[x]);
  }

  changed_job(event){
    if(this.job != event){
      this.job = event;

      for(var i = 0; i < this.sub_categories.length; i++){
        var checkbox = this.element.nativeElement.querySelector('#check_box_' + i);
        checkbox.src="./assets/images/uncheck_box.png";
      }

      this.sub_categs.length = 0;
    }
  }

  error_1(){
    var email_box = this.element.nativeElement.querySelector('#email_input');
    var fname_box = this.element.nativeElement.querySelector('#first_name_input');
    var lname_box = this.element.nativeElement.querySelector('#last_name_input');
    var contact_box = this.element.nativeElement.querySelector('#mobile_number_input');

    if(this.email.length == 0){
      email_box.style.border = "1px solid #F27072";
    }

    if(this.first_name.length == 0){
      fname_box.style.border = "1px solid #F27072";
    }

    if(this.last_name.length == 0){
      lname_box.style.border = "1px solid #F27072";
    }

    if(this.mobile_number.length == 0){
      contact_box.style.border = "1px solid #F27072";
    }

    this.profile_error = "*Please fill up all fields highlighted with red*"
  }

  error_2(){
    var email_box = this.element.nativeElement.querySelector('#email_input');

    if(this.email.length == 0){
      email_box.style.border = "1px solid #F27072";
    }

    this.profile_error = "*Please use a valid email address*"
  }

  error_3(){
    var contact_box = this.element.nativeElement.querySelector('#mobile_number_input');

    if(this.mobile_number.length == 0){
      contact_box.style.border = "1px solid #F27072";
    }

    this.profile_error = "*Please use a valid contact number*"
  }

  remove_style(id){
    this.element.nativeElement.querySelector(id).removeAttribute("style");
  }

  contact_change(event){
    if(event.length == 4){
      this.mobile_number = "(" + event.substr(0,3)+ ") " + event[3];
    }else if(event.length == 6){
      this.mobile_number = event.substr(1,3);
    }else if(event.length == 10 && event[9] != "-"){
      this.mobile_number = event.substr(0,9) + "-" + event[9];
    }else if(event.length == 10 && event[9] == "-"){
      this.mobile_number = event.substr(0,9);
    }
  }

  set_predefined_area_code(){
    var code = this.element.nativeElement.querySelector('#predefined_area_code');
    code.style.opacity = 1;
  }

  remove_predefined_area_code(input){
    var code = this.element.nativeElement.querySelector('#predefined_area_code');

    if(input.length == 0){
      code.style.opacity = 0;
    }
  }

  style_predefined_area_code(input){
    if(input.length > 0){
      return{
        "opacity": 1
      }
    }else{
      return{
        "opacity": 0
      }
    }
  }

  crop_image(){
    this.blur.emit(true);
    this.edit_profile_personal_container_id = "edit_profile_personal_container_blur";
    this.crop_image_overlay_id = "crop_image_overlay";
    var pop_up = this.element.nativeElement.querySelector('#crop_pop_up');

    pop_up.style.opacity = 0;

    setTimeout(() => {
      pop_up.style.opacity = 1;
    },10);
  }

  close_pop_up(){
    var pop_up = this.element.nativeElement.querySelector('#crop_pop_up');
    pop_up.style.opacity = 0;
    setTimeout(() => {
      this.crop_image_overlay_id = "crop_image_overlay_hide";
      this.edit_profile_personal_container_id = "edit_profile_personal_container";
      this.blur.emit(false);
    },350);
  }

}
