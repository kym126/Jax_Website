"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
var md5_1 = require("ts-md5/dist/md5");
var JsonFetchService = (function () {
    function JsonFetchService(http, jsonp) {
        this.http = http;
        this.jsonp = jsonp;
        this.offline_signup = false;
        this.offline_login = false;
    }
    JsonFetchService.prototype.getPages = function (Url) {
        return this.http.get(Url)
            .map(this.extractData)
            .catch(this.handleError);
    };
    JsonFetchService.prototype.getTradesman = function (Url) {
        return this.http.get(Url)
            .map(this.extractData)
            .catch(this.handleError);
    };
    JsonFetchService.prototype.get_testimonials = function () {
        return this.http.get("http://jaxoftrades.com/api/testimonials_hl.php")
            .map(this.extractData)
            .catch(this.handleError);
    };
    JsonFetchService.prototype.get_testimonials_2 = function () {
        return this.http.get("http://jaxoftrades.com/api/testimonials_tl.php")
            .map(this.extractData)
            .catch(this.handleError);
    };
    JsonFetchService.prototype.send_post = function (Url, body) {
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(Url, body, options)
            .map(this.extractData_1)
            .catch(this.handleError);
    };
    JsonFetchService.prototype.send_forgot_password = function (email) {
        var _this = this;
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var res = {
                STATUS: "OK",
                DATA: "candidate registered"
            };
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("email", email.toLowerCase());
            if (_this.offline_signup == false) {
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            resolve(JSON.parse(xhr.response));
                        }
                        else {
                            reject(xhr.response);
                        }
                    }
                };
                xhr.open("POST", "http://jaxoftrades.com/api/forgotpassword.php", true);
                xhr.send(formData);
            }
            else {
                resolve(res);
            }
        }));
    };
    JsonFetchService.prototype.send_xhr = function (url, type, email, first, last, password, contact, c_name, job, sub_categs, lat, long) {
        var _this = this;
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var res = {
                STATUS: "OK",
                DATA: "candidate registered"
            };
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            var encrypted_pass = md5_1.Md5.hashStr(password);
            if (type == 1) {
                formData.append("h_first_name", first);
                formData.append("h_last_name", last);
            }
            else {
                formData.append("tr_first_name", first);
                formData.append("tr_last_name", last);
            }
            formData.append("type", type);
            formData.append("email", email.toLowerCase());
            formData.append("password", encrypted_pass);
            formData.append("contact_number", contact);
            formData.append("company_name", c_name);
            formData.append("job_type", job);
            formData.append("job_sub_type", sub_categs);
            formData.append("geo_lat", lat);
            formData.append("geo_long", long);
            if (_this.offline_signup == false) {
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            resolve(JSON.parse(xhr.response));
                        }
                        else {
                            reject(xhr.response);
                        }
                    }
                };
                xhr.open("POST", "http://www.jaxoftrades.com/api/signup.php", true);
                xhr.send(formData);
            }
            else {
                resolve(res);
            }
        }));
    };
    JsonFetchService.prototype.send_login = function (url, email, pass, req_home) {
        var _this = this;
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var res = {
                STATUS: "OK",
                DATA: "login successful"
            };
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            var encrypted_pass = md5_1.Md5.hashStr(pass);
            formData.append("email", email.toLowerCase());
            formData.append("password", encrypted_pass);
            formData.append("key", req_home);
            if (_this.offline_login == false) {
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            resolve(JSON.parse(xhr.response));
                        }
                        else {
                            reject(xhr.response);
                        }
                    }
                };
                xhr.open("POST", "http://www.jaxoftrades.com/api/login.php", true);
                xhr.send(formData);
            }
            else {
                resolve(res);
            }
        }));
    };
    JsonFetchService.prototype.send_homeowner_edit_profile = function (id, email, first, last, contact, image, image_url) {
        var _this = this;
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var res = {
                STATUS: "OK",
                DATA: "login successful"
            };
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("homeowner_id", id);
            formData.append("email", email.toLowerCase());
            formData.append("h_first_name", first);
            formData.append("h_last_name", last);
            formData.append("contact_number", contact);
            formData.append("profile_image", image);
            formData.append("profile_url", image_url);
            console.log(image);
            if (_this.offline_login == false) {
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            resolve(JSON.parse(xhr.response));
                        }
                        else {
                            reject(xhr.response);
                        }
                    }
                };
                xhr.open("POST", "http://www.jaxoftrades.com/api/editprofile_h.php", true);
                xhr.send(formData);
            }
            else {
                resolve(res);
            }
        }));
    };
    JsonFetchService.prototype.send_tradesman_new_password = function (id, old, new_pass) {
        var _this = this;
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var res = {
                STATUS: "OK",
                DATA: "login successful"
            };
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            var encrypted_pass = md5_1.Md5.hashStr(old);
            var encrypted_pass2 = md5_1.Md5.hashStr(new_pass);
            formData.append("tradesmen_id", id);
            formData.append("old_password", encrypted_pass);
            formData.append("new_password", encrypted_pass2);
            if (_this.offline_login == false) {
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            resolve(JSON.parse(xhr.response));
                        }
                        else {
                            reject(xhr.response);
                        }
                    }
                };
                xhr.open("POST", "http://www.jaxoftrades.com/api/settings_changepassword.php", true);
                xhr.send(formData);
            }
            else {
                resolve(res);
            }
        }));
    };
    JsonFetchService.prototype.send_new_password = function (id, old, new_pass) {
        var _this = this;
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var res = {
                STATUS: "OK",
                DATA: "login successful"
            };
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            var encrypted_pass = md5_1.Md5.hashStr(old);
            var encrypted_pass2 = md5_1.Md5.hashStr(new_pass);
            formData.append("homeowner_id", id);
            formData.append("old_password", encrypted_pass);
            formData.append("new_password", encrypted_pass2);
            if (_this.offline_login == false) {
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            resolve(JSON.parse(xhr.response));
                        }
                        else {
                            reject(xhr.response);
                        }
                    }
                };
                xhr.open("POST", "http://www.jaxoftrades.com/api/cpassword_h.php", true);
                xhr.send(formData);
            }
            else {
                resolve(res);
            }
        }));
    };
    JsonFetchService.prototype.testimonial = function (type, id, text, firstname, lastname) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("type", type);
            if (type == 1) {
                formData.append("homeowner_id", id);
                formData.append("t_text", text);
                formData.append("h_first_name", firstname);
                formData.append("h_last_name", lastname);
            }
            else {
                formData.append("tradesmen_id", id);
                formData.append("t_text", text);
                formData.append("tr_first_name", firstname);
                formData.append("tr_last_name", lastname);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/testimonials.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.tradesman_list = function (job, sub, time, long, lat, postal_code) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("job_type", job);
            formData.append("job_sub_type", sub);
            if (time != null) {
                formData.append("service_time", time);
            }
            if (long != null && lat != null) {
                formData.append("geo_long", long);
                formData.append("geo_lat", lat);
            }
            if (postal_code != null) {
                formData.append("postal_code", postal_code);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/tr_list.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.request_rating = function (id) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("request_id", id);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/req_rating.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.fetch_t_settings = function (id) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("tradesmen_id", id);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        console.log(xhr.response);
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/d_notifications.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.send_t_settings = function (id, v_email, e_davail, e_ireq, e_other, v_sms, s_davail, s_ireq, s_other) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("tradesmen_id", id);
            formData.append("viaemail", v_email);
            formData.append("daily_aval", e_davail);
            formData.append("incom_req", e_ireq);
            formData.append("other", e_other);
            formData.append("viasms", v_sms);
            formData.append("sdaily_aval", s_davail);
            formData.append("sincom_req", s_ireq);
            formData.append("sother", s_other);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/s_notification.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.best_tradesman_list_home = function (long, lat) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("type", "home");
            if (long != null && lat != null) {
                formData.append("geo_long", long);
                formData.append("geo_lat", lat);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/tr_list.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.delete_trade = function (id, job) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("tradesmen_id", id);
            formData.append("job_type", job);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/delete_trade.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.best_tradesman_list = function () {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("type", "home");
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/tr_list.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.get_postal_code = function (lon, lat) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("geo_long", lon);
            formData.append("geo_lat", lat);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        console.log(xhr.response);
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/postalcode.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.request = function (h_id, t_id, f_name, l_name, email, contact, desc, job, sub_cat) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("homeowner_id", h_id);
            formData.append("tradesmen_id", t_id);
            formData.append("h_first_name", f_name);
            formData.append("h_last_name", l_name);
            formData.append("email", email.toLowerCase());
            formData.append("contact_number", contact);
            formData.append("short_desc", desc);
            formData.append("job_type", job);
            formData.append("job_sub_type", sub_cat);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/request.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.check_request_status = function (h_id, t_id) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("homeowner_id", h_id);
            formData.append("tradesmen_id", t_id);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/h_req_status.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.cancel_request = function (h_id, t_id) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("homeowner_id", h_id);
            formData.append("tradesmen_id", t_id);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/cancel_request.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.view_tr_profile = function (id, job, long, lat) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("tradesmen_id", id);
            formData.append("job_type", job);
            if (long != null && lat != null) {
                formData.append("geo_long", long);
                formData.append("geo_lat", lat);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/view_tr.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.fetch_h_history = function (id) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("homeowner_id", id);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/h_history.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.clear_t_notif = function (r_id) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("request_id[]", r_id);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/cle_notifications.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.send_reset_password = function (password, encrypt, user_type) {
        var _this = this;
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var res = {
                STATUS: "OK",
                DATA: "login successful"
            };
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            var encrypted_pass = md5_1.Md5.hashStr(password);
            formData.append("password", encrypted_pass);
            formData.append("encrypt", encrypt);
            formData.append("user_type", user_type);
            if (_this.offline_login == false) {
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            resolve(JSON.parse(xhr.response));
                        }
                        else {
                            reject(xhr.response);
                        }
                    }
                };
                xhr.open("POST", "http://www.jaxoftrades.com/api/passwordset.php", true);
                xhr.send(formData);
            }
            else {
                resolve(res);
            }
        }));
    };
    JsonFetchService.prototype.clear_h_history = function (r_id) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("request_id[]", r_id);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/h_cle_history.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.clear_t_history = function (r_id) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("request_id[]", r_id);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/tr_cle_history.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.get_inc_request = function (id, email) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("tradesmen_id", id);
            formData.append("email", email.toLowerCase());
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        //console.log(xhr.response);
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/login_after_req.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.req_status_1 = function (id, value) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("request_id", id);
            formData.append("request_status", value);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/request_status.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.req_status_2 = function (id, value) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("request_id", id);
            formData.append("request_status", value);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/incoming_req_status.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.review_a_tradesman = function (h_id, t_id, r1, r2, r3, r4, avg_rating, job, title, desc, img, r_id) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("homeowner_id", h_id);
            formData.append("tradesmen_id", t_id);
            formData.append("request_id", r_id);
            formData.append("rate1", r1);
            formData.append("rate2", r2);
            formData.append("rate3", r3);
            formData.append("rate4", r4);
            formData.append("avg_rating", avg_rating);
            formData.append("job_type", job);
            formData.append("review_title", title);
            formData.append("review_desc", desc);
            for (var x = 0; x < img.length; x++) {
                formData.append("review_image[]", img[x]);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/rating.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.initial_tr_profile = function (id, job, about_you, image, pd_skills, a_skills, time, loc, fb, tw, linked_in, web, gallery, rw_title, rw_desc, rw_img, sub_categ, company, rw_id, rw_index) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("tradesmen_id", id);
            formData.append("profile_image", image);
            formData.append("job_type", job);
            formData.append("about_you", about_you);
            formData.append("predefined_skills", pd_skills);
            formData.append("skills", a_skills);
            formData.append("daily_time", time);
            formData.append("default_location", loc);
            formData.append("facebook_link", fb);
            formData.append("twitter_link", tw);
            formData.append("linked_in", linked_in);
            formData.append("website", web);
            formData.append("job_sub_type", sub_categ);
            formData.append("company_name", company);
            for (var x = 0; x < rw_id.length; x++) {
                formData.append("recentwork_id[]", rw_id[x]);
            }
            for (var x = 0; x < gallery.length; x++) {
                formData.append("gallery_image[]", gallery[x]);
            }
            for (var x = 0; x < rw_title.length; x++) {
                formData.append("recentwork_title[]", rw_title[x]);
            }
            for (var x = 0; x < rw_desc.length; x++) {
                formData.append("recentwork_desc[]", rw_desc[x]);
            }
            for (var x = 0; x < rw_img.length; x++) {
                for (var y = 0; y < rw_img[x].length; y++) {
                    var key = 'recentwork_image' + x + '[]';
                    formData.append(key, rw_img[x][y]);
                }
            }
            for (var x = 0; x < rw_index.length; x++) {
                for (var y = 0; y < rw_index[x].length; y++) {
                    var key = 'rw' + x + '[]';
                    formData.append(key, rw_index[x][y]);
                }
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/update_tr.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.update_tr_profile = function (id, job, about_you, image, pd_skills, a_skills, time, loc, fb, tw, linked_in, web, gallery, rw_title, rw_desc, rw_img, sub_categ, company, rw_id, contact, email, fname, lname, profile_url, rw_index) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("contact_number", contact);
            formData.append("email", email.toLowerCase());
            formData.append("tr_first_name", fname);
            formData.append("tr_last_name", lname);
            formData.append("tradesmen_id", id);
            formData.append("profile_image", image);
            formData.append("profile_url", profile_url);
            formData.append("job_type", job);
            formData.append("about_you", about_you);
            formData.append("predefined_skills", pd_skills);
            formData.append("skills", a_skills);
            formData.append("daily_time", time);
            formData.append("default_location", loc);
            formData.append("facebook_link", fb);
            formData.append("twitter_link", tw);
            formData.append("linked_in", linked_in);
            formData.append("website", web);
            formData.append("job_sub_type", sub_categ);
            formData.append("company_name", company);
            for (var x = 0; x < rw_id.length; x++) {
                formData.append("recentwork_id[]", rw_id[x]);
            }
            for (var x = 0; x < gallery.length; x++) {
                formData.append("gallery_image[]", gallery[x]);
            }
            for (var x = 0; x < rw_title.length; x++) {
                formData.append("recentwork_title[]", rw_title[x]);
            }
            for (var x = 0; x < rw_desc.length; x++) {
                formData.append("recentwork_desc[]", rw_desc[x]);
            }
            for (var x = 0; x < rw_img.length; x++) {
                for (var y = 0; y < rw_img[x].length; y++) {
                    var key = 'recentwork_image' + x + '[]';
                    formData.append(key, rw_img[x][y]);
                }
            }
            for (var x = 0; x < rw_index.length; x++) {
                for (var y = 0; y < rw_index[x].length; y++) {
                    var key = 'rw' + x + '[]';
                    formData.append(key, rw_index[x][y]);
                }
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/dupdate_tr.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.add_trade = function (id, job, categs, a_skills, p_skills, company, time, loc, fb, tw, linked_in, web, gallery, rw_title, rw_desc, rw_img, rw_id) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("tradesmen_id", id);
            formData.append("job_type", job);
            formData.append("job_sub_type", categs);
            formData.append("predefined_skills", p_skills);
            formData.append("skills", a_skills);
            formData.append("company_name", company);
            formData.append("daily_time", time);
            formData.append("default_location", loc);
            formData.append("facebook_link", fb);
            formData.append("twitter_link", tw);
            formData.append("linked_in", linked_in);
            formData.append("website", web);
            for (var x = 0; x < rw_id.length; x++) {
                formData.append("recentwork_id[]", rw_id[x]);
            }
            for (var x = 0; x < gallery.length; x++) {
                console.log(gallery[x]);
                formData.append("gallery_image[]", gallery[x]);
            }
            for (var x = 0; x < rw_title.length; x++) {
                formData.append("recentwork_title[]", rw_title[x]);
            }
            for (var x = 0; x < rw_desc.length; x++) {
                formData.append("recentwork_desc[]", rw_desc[x]);
            }
            for (var x = 0; x < rw_img.length; x++) {
                for (var y = 0; y < rw_img[x].length; y++) {
                    var key = 'recentwork_image' + x + '[]';
                    formData.append(key, rw_img[x][y]);
                }
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/trade.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.dnd_toggle = function (id, job, dnd) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            console.log(id, job, dnd);
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("tradesmen_id", id);
            formData.append("job_type", job);
            formData.append("dnd", dnd);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/dnd.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.start_work = function (id, status) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            console.log(id, status);
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("request_id", id);
            formData.append("work_status", status);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/incoming_req_status.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.history_status = function (id, status) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("request_id", id);
            formData.append("job_status", status);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/history_status.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.initial_company_profile = function (id, address, about_you, image, city, a_skills, province, loc, fb, tw, linked_in, web, gallery, rw_title, rw_desc, rw_img, contact, company, email, rw_id, prof_url, rw_index) {
        return Observable_1.Observable.fromPromise(new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("tradesmen_id", id);
            formData.append("profile_image", image);
            formData.append("profile_url", prof_url);
            formData.append("c_add", address);
            formData.append("c_about", about_you);
            formData.append("c_city", city);
            formData.append("c_spec", a_skills);
            formData.append("c_province", province);
            formData.append("c_postalcode", loc);
            formData.append("facebook_link", fb);
            formData.append("twitter_link", tw);
            formData.append("linked_in", linked_in);
            formData.append("website", web);
            formData.append("c_name", company);
            formData.append("c_number", contact);
            formData.append("c_email", email.toLowerCase());
            for (var x = 0; x < rw_id.length; x++) {
                formData.append("recentwork_id[]", rw_id[x]);
            }
            for (var x = 0; x < gallery.length; x++) {
                formData.append("gallery_image[]", gallery[x]);
            }
            for (var x = 0; x < rw_title.length; x++) {
                formData.append("recentwork_title[]", rw_title[x]);
            }
            for (var x = 0; x < rw_desc.length; x++) {
                formData.append("recentwork_desc[]", rw_desc[x]);
            }
            for (var x = 0; x < rw_img.length; x++) {
                for (var y = 0; y < rw_img[x].length; y++) {
                    var key = 'recentwork_image' + x + '[]';
                    formData.append(key, rw_img[x][y]);
                }
            }
            for (var x = 0; x < rw_index.length; x++) {
                for (var y = 0; y < rw_index[x].length; y++) {
                    var key = 'rw' + x + '[]';
                    formData.append(key, rw_index[x][y]);
                }
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", "http://www.jaxoftrades.com/api/company_tr.php", true);
            xhr.send(formData);
        }));
    };
    JsonFetchService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    JsonFetchService.prototype.extractData_1 = function (res) {
        return res;
    };
    JsonFetchService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable_1.Observable.throw(errMsg);
    };
    JsonFetchService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, http_1.Jsonp])
    ], JsonFetchService);
    return JsonFetchService;
}());
exports.JsonFetchService = JsonFetchService;
//# sourceMappingURL=JsonFetch.service.js.map