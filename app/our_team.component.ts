import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { WindowRef } from './window.service';
import { DataTransferService } from './data_transfer.service';

@Component({
  selector: 'our-team',
  templateUrl: './app/HTML/our_team.html',
  styleUrls: ['css/misc_pages.css']
})

export class OurTeamComponent {
  team = [
      {
        "name": "Philip Morris",
        "position": "Founder & President",
        "image": "./assets/images/headshots/mr_morris.png",
        "sml": []
      },
      {
        "name": "Krunal Desai",
        "position": "Android Developer & Team Lead",
        "image": "./assets/images/headshots/krunal.png",
        "sml": [
          {
            "icon": "./assets/images/icons/s_m_icons/linkedin.png",
            "link": "https://ca.linkedin.com/in/krunaldesai"
          }
        ]
      },
      {
        "name": "Janki Modi",
        "position": "Full Stack Developer",
        "image": "./assets/images/headshots/janki.png",
        "sml": [
          {
            "icon": "./assets/images/icons/s_m_icons/linkedin.png",
            "link": "https://ca.linkedin.com/in/janki-modi-8b494ab5"
          }
        ]
      },
      {
        "name": "Kym Derrick Uy",
        "position": "Front End Web App Developer",
        "image": "./assets/images/headshots/kym.png",
        "sml": [
          {
            "icon": "./assets/images/icons/s_m_icons/linkedin.png",
            "link": "https://ca.linkedin.com/in/kym-derrick-uy"
          }
        ]
      },
      {
        "name": "Rony Stephen",
        "position": "Product Designer",
        "image": "./assets/images/headshots/rony.jpg",
        "sml": [
          {
            "icon": "./assets/images/icons/s_m_icons/linkedin.png",
            "link": "https://www.linkedin.com/in/ronystephen/"
          },
          {
            "icon": "./assets/images/icons/s_m_icons/behance.png",
            "link": "https://www.behance.net/ronystephen"
          }
        ]
      }
  ];

  banner_img = "./assets/images/misc_pages_banners/our_team_banner.jpg";
  opening_msg_1="A PASSIONATE TEAM";
  opening_msg_2="The friendly team at Jax of Trades is here to assist you! We help tradesmen in growing their business while helping homeowners find the best local tradesmen in the GTA.";

  constructor(public router: Router, private datatransfer:DataTransferService) {
    if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "2"){
      if(this.datatransfer.get_tradesman_values_json() == null){
        this.router.navigate(['./edit-profile']);
      }else{
        this.router.navigate(['./dashboard']);
      }
    }
    window.scrollTo(0, 0);
  }
}
