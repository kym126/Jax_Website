import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';

@Component({
  selector: 'time_input',
  templateUrl: './app/HTML/time_input.html',
  styleUrls: ['css/time_input.css'],

})

export class TimeInputComponent {
  @Input() hour:string;
  @Input() minute:string;
  @Input() am_pm:string;

  drop_down = false;

  constructor(private element: ElementRef){}

  toggle_drop_down(event){
    event.stopPropagation();
    var drop_down_box = this.element.nativeElement.querySelector('#drop-down');
    var arrow = this.element.nativeElement.querySelector('#am_pm_text');
    var value = this.element.nativeElement.querySelector('#am_pm_button');

    if(this.drop_down){
      this.close_drop_down();
    }else{
      drop_down_box.style.height = "22px";
      this.drop_down = true;
      value.style.borderBottomRightRadius = "0px";
      value.style.borderBottomLeftRadius = "0px";
    }
  }

  close_drop_down(){
    var drop_down_box = this.element.nativeElement.querySelector('#drop-down');
    var arrow = this.element.nativeElement.querySelector('#am_pm_text');
    var value = this.element.nativeElement.querySelector('#am_pm_button');

    drop_down_box.style.height = "0px";
    this.drop_down = false;
    value.style.borderBottomRightRadius = "3px";
    value.style.borderBottomLeftRadius = "3px";
  }

  change_value(value){
    this.am_pm = value;
  }

  get_hour(){
    return this.hour;
  }

  border_blue(){
    var box = this.element.nativeElement.querySelector('#time_input');
    box.style.border = "1px solid #50b6e8";
  }

  border_gray(){
    var box = this.element.nativeElement.querySelector('#time_input');
    box.style.border = "1px solid #b6b7b7";
  }
}
