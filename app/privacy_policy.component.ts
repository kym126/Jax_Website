import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { WindowRef } from './window.service';
import { DataTransferService } from './data_transfer.service';

@Component({
  selector: 'privacy-policy',
  templateUrl: './app/HTML/privacy_policy.html',
  styleUrls: ['css/misc_pages.css']
})

export class PrivacyPolicyComponent {
  constructor(public router: Router, private datatransfer:DataTransferService) {
    if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "2"){
      if(this.datatransfer.get_tradesman_values_json() == null){
        this.router.navigate(['./edit-profile']);
      }else{
        this.router.navigate(['./dashboard']);
      }
    }
    window.scrollTo(0, 0);
  }
}
