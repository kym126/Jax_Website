import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';
import { LoginFloatBoxComponent } from './login_float_box.component';
import { PushNotificationsService } from 'angular2-notifications';
@Component({
  selector: 'request_float_box',
  templateUrl: './app/HTML/request_float_box.html',
  styleUrls: ['css/request_float_box.css'],
})
export class RequestFloatBoxComponent {
  path="M190,80 L190,14 A66,66 1 0,1 190,14 z";
  path2="M190,80 L190,147 A67,67 1 0,1 190,147 z";
  ticks = 0;
  orig_time = null;
  timer: Observable<number>;
  private subscription;
  response_title = "";
  response_value = 0;
  response_message = "";
  response_button = "";
  info_error = "";
  email = true;
  phone = true;
  info_email = "";
  info_phone = "";
  short_desc = "";
  public HideFunction: Function;
  public SignupFunction: Function;
  homeOwner:Homeowner;
  @Input()
  public remove_item: Function;
  @Input()
  public name: string;
  @Input()
  public pic: string = "./assets/images/logo_box.png";
  @Input()
  public choice: number;
  @Input()
  public category: string = "";
  @Input()
  public sub_category: string = "";
  @Input()
  public t_id: number;
  @Input()
  public f_name: string = "";
  @Input()
  public l_name: string = "";
  @Output()
  box_closed = new EventEmitter();
  SU_email = "";
  SU_firstname = "";
  SU_lastname = "";
  SU_contact = "";
  SU_password = "";
  SU_confirm_pass = "";
  SU_email_focus = false;
  SU_firstname_focus = false;
  SU_lastname_focus = false;
  SU_contact_focus = false;
  SU_password_focus = false;
  SU_confirm_pass_focus = false;
  signup_error = "";
  submitted = false;
  @ViewChild(LoginFloatBoxComponent) login:LoginFloatBoxComponent;
  constructor(public element: ElementRef, private jsonfetch: JsonFetchService, public router: Router, private datatransfer:DataTransferService,  private push_notif: PushNotificationsService) {
    this.HideFunction = this.correct.bind(this);
    this.SignupFunction = this.signup.bind(this);
  }
  request_pop(){
    var login_box = this.element.nativeElement.querySelector('#login_box');
    var request_box = this.element.nativeElement.querySelector('#request_overlay');
    var request_cont = this.element.nativeElement.querySelector('#request_container');
    request_cont.style.display = "none";
   if(!this.datatransfer.is_logged_in()){
     login_box.style.display = "block";
     setTimeout(() => {
       login_box.style.opacity = "1";
     },50);
     request_box.style.opacity = "0";
     request_box.style.display = "none";
   }else{
     this.info_email = this.datatransfer.get_user_email();
     this.info_phone = this.datatransfer.get_user_contact();
     if(this.info_phone.length == 0){
       this.phone = false;
     }
     login_box.style.display = "none";
     request_box.style.opacity = "0";
     request_box.style.display = "block";
     setTimeout(() => {
       request_box.style.opacity = "1";
     },50);
   }
  }
  ngAfterViewInit(){
    if(this.datatransfer.get_request_item() != null){
      var data = JSON.parse(this.datatransfer.get_request_item());
      this.name = data.name;
      this.pic = data.pic;
      this.choice = data.choice;
      this.category = data.category;
      this.sub_category = data.sub_category;
      this.t_id = data.t_id;
      this.f_name = data.f_name;
      this.l_name = data.l_name;
      this.request_pop();
      this.request(null);
    }
  }
  close_request(){
   var login_box = this.element.nativeElement.querySelector('#login_box');
   var signup_box = this.element.nativeElement.querySelector('#signup_box');
   var request_box = this.element.nativeElement.querySelector('#request_overlay');
   var info_box = this.element.nativeElement.querySelector('#send_info');
   var request_container = this.element.nativeElement.querySelector('#request_container');
   var response_container = this.element.nativeElement.querySelector('#response_container');
   login_box.style.opacity = "0";
   request_box.style.opacity = "0";
   signup_box.style.opacity = "0";
   this.datatransfer.remove_request_item();
   setTimeout(() => {
     if(response_container.style.display == "block"){
       this.remove_item();
     }
     this.login.forgot_back();
     info_box.style.display = "block";
     info_box.style.opacity = "1";
     request_container.style.display = "none";
     request_container.style.opacity = "0";
     response_container.style.display = "none";
     response_container.style.opacity = "0";
     signup_box.style.display = "none";
     if(this.subscription != null){
       this.subscription.unsubscribe();
     }
     this.ticks = 240;
     this.orig_time = 240;
   },350);
  }
  getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  change_info_to_request_check(){
    var info_err = this.element.nativeElement.querySelector('#info_error_title');
    if (this.email == false && this.phone == false){
      this.info_error = "Please select 1 mode of communication";
      info_err.style.opacity = 1;
    }else{
      var error = false;
      var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
      var testPhone = /^\(+([0-9]{3})+\)+\ +([0-9]{3})[-]?([0-9]{4})$/i;
      if(this.email == true){
        if(!testEmail.test(this.info_email)){
          this.info_error = "Please input a valid email address";
          info_err.style.opacity = 1;
          error = true;
        }
      }
      if(this.phone == true){
        if(!testPhone.test(this.info_phone)){
          this.info_error = "Please input a valid contact number";
          info_err.style.opacity = 1;
          error = true;
        }
      }
      if(error == false){
        this.change_info_to_request();
      }
    }
  }

  change_info_to_request(){
    this.datatransfer.set_load(true);
    this.jsonfetch.request(this.datatransfer.get_user_id(), this.t_id, this.datatransfer.get_user_first_name(), this.datatransfer.get_user_last_name(), this.info_email, this.info_phone, this.short_desc, this.category, this.sub_category)
     .subscribe(
       res => this.request(res),
       error =>  this.error_happened(error));
  }

  error_happened(error){
    this.datatransfer.set_load(false);
    console.log(error);
  }

  request(res){
    this.datatransfer.set_load(false);
    var info_err = this.element.nativeElement.querySelector('#info_error_title');
    var info_box = this.element.nativeElement.querySelector('#send_info');
    var request_container = this.element.nativeElement.querySelector('#request_container');
    info_err.style.opacity = 0;
    info_box.style.opacity = "0";
    var data = {
      "r_id": 0,
      "name": this.name,
      "pic": this.pic,
      "category": this.category,
      "sub_category": this.sub_category,
      "t_id": this.t_id,
      "f_name": this.f_name,
      "l_name": this.l_name,
      "status": "none"
    }
    this.datatransfer.set_request_item(JSON.stringify(data));
    setTimeout(() => {
      info_box.style.display = "none";
      this.timer = Observable.timer(0,10);
      this.subscription = this.timer.subscribe(t=>this.calculate_circular_timer(t));
      request_container.style.opacity = "0";
      request_container.style.display = "block";
      setTimeout(() => {
        request_container.style.opacity = "1";
      },50);
    },350);
    this.jsonfetch.check_request_status(this.datatransfer.get_user_id(), this.t_id)
     .subscribe(
       res => this.status_received(res),
       error =>  this.status_error(error));
  }

  status_received(res){
    if(res.STATUS == "OK"){
      if(this.datatransfer.get_request_item() != null){
        status = JSON.parse(this.datatransfer.get_request_item()).status;
      }else{
        status = "none"
      }
      if(res.DATA[0].request_status == "Time Out"){
        status = "unanswered";
      }else if(res.DATA[0].request_status == "Accepted"){
        status = "accepted";
      }else if(res.DATA[0].request_status == "Declined"){
        status = "rejected";
      }
      var data = {
        "r_id": 0,
        "name": this.name,
        "pic": this.pic,
        "category": this.category,
        "sub_category": this.sub_category,
        "t_id": this.t_id,
        "f_name": this.f_name,
        "l_name": this.l_name,
        "status": status
      }
      this.datatransfer.set_request_item(JSON.stringify(data));
      if(status == "accepted"){
        this.accepted();
      }else if(status == "rejected"){
        this.rejected();
      }else if(status == "unanswered"){
        this.unanswered();
      }else{
        if(this.orig_time == null){
          this.orig_time = res.DATA[0].remaining_second;
        }
        setTimeout(() => {
          this.jsonfetch.check_request_status(this.datatransfer.get_user_id(), this.t_id)
           .subscribe(
             res => this.status_received(res),
             error =>  this.status_error(error));
        }, 3000);
      }
    }
  }

  status_error(error){
    console.log(error);
  }

  cancel_request(){
    this.datatransfer.set_load(true);
    this.jsonfetch.cancel_request(this.datatransfer.get_user_id(), this.t_id)
     .subscribe(
       res => this.req_cancel(res),
       error =>  this.error_happened_2(error));
  }

  error_happened_2(error){
    console.log(error);
    this.datatransfer.set_load(false);
  }

  req_cancel(res){
    console.log(res);
    this.datatransfer.set_load(false);
    this.box_closed.emit(true);
    this.datatransfer.remove_request_item();
  }

  calculate_circular_timer(time){
    var time_left = this.orig_time - time/100;
    this.datatransfer.set_blur(true);
    this.ticks = Math.ceil(time_left);
    if(time_left == 0){
      this.unanswered();
    }/*else if(time_left == 115){
      var random= this.getRandomInt(0, 2);
      if(random == 0){
        this.accepted();
      }else if(random == 1){
        this.rejected();
      }else{
        this.unanswered();
      }
    }*/

    var full_speed = 1.8;
    var half_speed = full_speed/2;
    var current_value = (time/100) % full_speed;
    var computed_value = current_value;
    if(computed_value >= half_speed){
      var computed_value = 2 * half_speed - current_value;
    }

    var circle_1_max = 27;
    var circle_2_max = 44;
    var circle_3_max = 61;

    var circle_min = 110;

    var circle_1_size = circle_min + (computed_value/half_speed)*circle_1_max;
    var circle_1_margin = -(computed_value/half_speed)*(circle_1_max/2) + 5;
    var circle_2_size = circle_min + (computed_value/half_speed)*circle_2_max;
    var circle_2_margin = -(computed_value/half_speed)*(circle_2_max/2) + 5;
    var circle_3_size = circle_min + (computed_value/half_speed)*circle_3_max;
    var circle_3_margin = -(computed_value/half_speed)*(circle_3_max/2) + 5;

    var circle_1 = this.element.nativeElement.querySelector('#request_circle_1');
    var circle_2 = this.element.nativeElement.querySelector('#request_circle_2');
    var circle_3 = this.element.nativeElement.querySelector('#request_circle_3');

    circle_1.style.height = circle_1_size;
    circle_1.style.width = circle_1_size;
    circle_1.style.top = circle_1_margin;
    circle_1.style.left = circle_1_margin;

    circle_2.style.height = circle_2_size;
    circle_2.style.width = circle_2_size;
    circle_2.style.top = circle_2_margin;
    circle_2.style.left = circle_2_margin;

    circle_3.style.height = circle_3_size;
    circle_3.style.width = circle_3_size;
    circle_3.style.top = circle_3_margin;
    circle_3.style.left = circle_3_margin;

  }
  unanswered(){
    if(window.innerWidth > 800){
      this.push_notif.create('Jax of Trades', {body: this.name + ' is not available at the moment', icon: this.pic}).subscribe(
          res => console.log(res),
          err => console.log(err)
        );
    }
    var data = {
      "r_id": 0,
      "name": this.name,
      "pic": this.pic,
      "category": this.category,
      "sub_category": this.sub_category,
      "t_id": this.t_id,
      "f_name": this.f_name,
      "l_name": this.l_name,
      "status": "unanswered"
    }
    this.datatransfer.set_request_item(JSON.stringify(data));
    this.response_title="Request not answered";
    this.response_message = "We're sorry, it seems like " + this.name + " is not available at the moment";
    this.response_value = 0;
    this.response_button = "Find another tradesman";
    this.request_to_response();
  }
  accepted(){
    if(window.innerWidth > 800){
      this.push_notif.create('Jax of Trades', {body: this.name + ' accepted the job. Please wait for him to reach out to you', icon: this.pic}).subscribe(
          res => console.log(res),
          err => console.log(err)
        );
    }
    var data = {
      "r_id": 0,
      "name": this.name,
      "pic": this.pic,
      "category": this.category,
      "sub_category": this.sub_category,
      "t_id": this.t_id,
      "f_name": this.f_name,
      "l_name": this.l_name,
      "status": "accepted"
    }
    this.datatransfer.set_request_item(JSON.stringify(data));
    this.response_title="Request accepted";
    this.response_message = "Please wait until " + this.name + " reaches out to you";
    this.response_value = 1;
    this.response_button = "OK";
    this.request_to_response();
  }
  rejected(){
    if(window.innerWidth > 800){
      this.push_notif.create('Jax of Trades', {body: this.name + ' is not available at the moment', icon: this.pic}).subscribe(
          res => console.log(res),
          err => console.log(err)
        );
    }
    var data = {
      "r_id": 0,
      "name": this.name,
      "pic": this.pic,
      "category": this.category,
      "sub_category": this.sub_category,
      "t_id": this.t_id,
      "f_name": this.f_name,
      "l_name": this.l_name,
      "status": "rejected"
    }
    this.datatransfer.set_request_item(JSON.stringify(data));
    this.response_title="Request rejected";
    this.response_message = "We're sorry, it seems like " + this.name + " is not available at the moment";
    this.response_value = -1;
    this.response_button = "Find another tradesman";
    this.request_to_response();
  }
  request_to_response(){
    var request_container = this.element.nativeElement.querySelector('#request_container');
    var response_container = this.element.nativeElement.querySelector('#response_container');
    request_container.style.opacity = "0";
    setTimeout(() => {
      request_container.style.display = "none";
      if(this.subscription != null){
        this.subscription.unsubscribe();
      }
      response_container.style.display = "block";
      response_container.style.opacity = "0";
      setTimeout(() => {
        response_container.style.opacity = "1";
      },50);
    },350);
  }
  click_event(event){
    event.stopPropagation();
  }
  circle_color(value){
    if(value == 0){
      return{
        "background-color": "#fcb32b"
      }
    }else if(value == -1){
      return{
        "background-color": "#f05354"
      }
    }else{
      return{
        "background-color": "#2bb34b"
      }
    }
  }
  correct(){
    console.log(this.choice);
    if(this.choice == 0){
      var login_box = this.element.nativeElement.querySelector('#login_box');
      var request_box = this.element.nativeElement.querySelector('#request_overlay');
      login_box.style.opacity = "0";
      this.info_email = this.datatransfer.get_user_email();
      this.info_phone = this.datatransfer.get_user_contact();
      setTimeout(() => {
        this.datatransfer.set_blur(true);
        login_box.style.display = "none";
        request_box.style.opacity = "0";
        request_box.style.display = "block";
        setTimeout(() => {
          request_box.style.opacity = "1";
        },50);
      },450);
    }else{
      this.box_closed.emit(false);
    }
  }
  remove_style(id){
    this.element.nativeElement.querySelector(id).removeAttribute("style");
  }
  check_reg(){
    this.submitted = true;
    var err_title = this.element.nativeElement.querySelector('#sgn_up_error_title');
    var email_box = this.element.nativeElement.querySelector('#SU_email');
    var fname_box = this.element.nativeElement.querySelector('#SU_fname');
    var lname_box = this.element.nativeElement.querySelector('#SU_lname');
    var pass_box = this.element.nativeElement.querySelector('#SU_password');
    var cpass_box = this.element.nativeElement.querySelector('#confirm_password');
    var cnum_box = this.element.nativeElement.querySelector('#mobile_number');
    var conf_pass_del = this.element.nativeElement.querySelector('#sgn_up_conf_pass_del');
    var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    var testPhone = /^\(+([0-9]{3})+\)+\ +([0-9]{3})[-]?([0-9]{4})$/;
    var error = false;
    if(!testPhone.test(this.SU_contact) && this.SU_contact.length != 0){
      this.signup_error = "Please use a valid contact number";
      error = true;
      cnum_box.style.border = "1px solid #F27072";
    }
    if(!testEmail.test(this.SU_email)){
      this.signup_error = "Please input a valid email address";
      error = true;
      email_box.style.border = "1px solid #F27072";
    }
    if(this.SU_password.length != 0 && this.SU_confirm_pass.length != 0 && this.SU_password != this.SU_confirm_pass){
      this.signup_error = "Passwords mismatch";
      error = true;
      pass_box.style.border = "1px solid #F27072";
      cpass_box.style.border = "1px solid #F27072";
      conf_pass_del.style.display = "block";
    }
    if(this.SU_password.length == 0){
      this.signup_error = "Please fill up all fields outlined with red and resubmit";
      error = true;
      pass_box.style.border = "1px solid #F27072";
    }
    if(this.SU_confirm_pass.length == 0){
      this.signup_error = "Please fill up all fields outlined with red and resubmit";
      error = true;
      cpass_box.style.border = "1px solid #F27072";
    }
    if(this.SU_email.length == 0){
      this.signup_error = "Please fill up all fields outlined with red and resubmit";
      error = true;
      email_box.style.border = "1px solid #F27072";
    }
    if(this.SU_firstname.length == 0){
      this.signup_error = "Please fill up all fields outlined with red and resubmit";
      error = true;
      fname_box.style.border = "1px solid #F27072";
    }
    if(this.SU_lastname.length == 0){
      this.signup_error = "Please fill up all fields outlined with red and resubmit";
      error = true;
      lname_box.style.border = "1px solid #F27072";
    }
    if(error == false){
       err_title.style.opacity = 0;
       this.set_load(true);
       this.jsonfetch.send_xhr("http://www.jaxoftrades/API/signup.PHP", 1, this.SU_email, this.SU_firstname, this.SU_lastname, this.SU_password, this.SU_contact, "", "", null, null, null)
         .map(res => res)
             .subscribe(
                 res => this.reg(res),
                 error => this.error_happened_3()
             );
       /*this.homeOwner = new Homeowner(1, this.SU_email, this.SU_firstname, this.SU_lastname, this.SU_password, "");
       let body = JSON.stringify(this.homeOwner);
      console.log(body);
       this.jsonfetch.send_post("http://192.168.0.127/jaxoftrades/API/signup.PHP", body)
                        .subscribe(
                          msg => this.reg(),
                          error =>  console.log(error));*/
    }else{
       err_title.style.opacity = 1;
    }
  }
  error_happened_3(){
    this.signup_error  = "Error connecting to server, check your connection and try again";
    this.err();
  }

  err(){
    var err_title = this.element.nativeElement.querySelector('#sgn_up_error_title');
    this.set_load(false);
    err_title.style.opacity = 1;
  }

  reg(res){
    if(res.STATUS == "OK"){
      this.set_load(false);
      this.datatransfer.user_login(res.DATA.homeowner_id, this.SU_firstname, this.SU_lastname, this.SU_email, 1, this.SU_contact);
      this.push_notif.requestPermission();
      if(this.choice == 0){
        var login_box = this.element.nativeElement.querySelector('#signup_box');
        var request_box = this.element.nativeElement.querySelector('#request_overlay');
        this.info_email = this.datatransfer.get_user_email();
        this.info_phone = this.datatransfer.get_user_contact();
        login_box.style.opacity = "0";
        setTimeout(() => {
          login_box.style.display = "none";
          request_box.style.opacity = "0";
          request_box.style.display = "block";
          setTimeout(() => {
            request_box.style.opacity = "1";
          },50);
        },450);
      }else{
        this.box_closed.emit(false);
      }
    }else{
      this.signup_error  = res.Message;
      this.err();
    }
  }
  toggle_chk_box(value){
    if(!value){
      return "/assets/images/uncheck_box.png"
    }else{
      return "/assets/images/check_box_v2.png"
    }
  }
  go_back_to_login(){
    var login_box = this.element.nativeElement.querySelector('#login_box');
    var sign_box = this.element.nativeElement.querySelector('#signup_box');
    sign_box.style.opacity = "0";
    setTimeout(() => {
      sign_box.style.display = "none";
      login_box.style.opacity = "0";
      login_box.style.display = "block";
      setTimeout(() => {
        login_box.style.opacity = "1";
      },10);
    },450);
  }
  signup(){
    var login_box = this.element.nativeElement.querySelector('#login_box');
    var sign_box = this.element.nativeElement.querySelector('#signup_box');
    login_box.style.opacity = "0";
    setTimeout(() => {
      login_box.style.display = "none";
      sign_box.style.opacity = "0";
      sign_box.style.display = "block";
      setTimeout(() => {
        sign_box.style.opacity = "1";
      },10);
    },450);
  }
  hide_field_label(value){
    if(value.length == 0){
      return{
        "opacity": 0
      }
    }else{
      return{
        "opacity": 1
      }
    }
  }
  keypress(value){
    if(value == 13){
      this.check_reg();
    }
  }
  keypress_info(value){
    if(value == 13){
      this.change_info_to_request_check();
    }
  }

  contact_change(event){

    var code = this.element.nativeElement.querySelector('#predefined_area_code');
    var box = this.element.nativeElement.querySelector('#contact_no_info_field');

    if(event.length != 0){
      box.style.paddingLeft = "50px";
      code.style.opacity = 1;
    }else{
      code.style.opacity = 0;
      box.style.paddingLeft = "33px";
    }

    if(event.length == 4){
      this.info_phone = "(" + event.substr(0,3)+ ") " + event[3];
    }else if(event.length == 6){
      this.info_phone = event.substr(1,3);
    }else if(event.length == 10 && event[9] != "-"){
      this.info_phone = event.substr(0,9) + "-" + event[9];
    }else if(event.length == 10 && event[9] == "-"){
      this.info_phone = event.substr(0,10);
    }
  }

  contact_change_2(event){
    if(event.length == 4){
      this.SU_contact = "(" + event.substr(0,3)+ ") " + event[3];
    }else if(event.length == 6){
      this.SU_contact = event.substr(1,3);
    }else if(event.length == 10 && event[9] != "-"){
      this.SU_contact = event.substr(0,9) + "-" + event[9];
    }else if(event.length == 10 && event[9] == "-"){
      this.SU_contact = event.substr(0,9);
    }
  }

  hide_name(input, input_2){
    if(input.length == 0 && input_2 == false){
      return{
        "font-size": "10pt",
        "top": "13px"
      }
    }else{
      return{
        "font-size": "7pt",
        "top": "6px"
      }
    }
  }

  set_predefined_area_code(){
    var code = this.element.nativeElement.querySelector('#sgn_up_predefined_area_code');
    code.style.opacity = 1;
  }

  remove_conf_pass_del(){
    var conf_pass_del = this.element.nativeElement.querySelector('#sgn_up_conf_pass_del');
    conf_pass_del.style.display = "none";
  }

  conf_pass_del(event){
    this.SU_confirm_pass = "";
  }

  remove_predefined_area_code(input){
    var code = this.element.nativeElement.querySelector('#sgn_up_predefined_area_code');

    if(input.length == 0){
      code.style.opacity = 0;
    }
  }

  set_load(value){
    var overlay = this.element.nativeElement.querySelector('#loading_overlay');
    var loading = this.element.nativeElement.querySelector('#loading_box');
    if(value){
      overlay.style.display = "block";
      loading.style.opacity = 1;
    }else{
      loading.style.opacity = 0;
      setTimeout(() => {
        overlay.style.display = "none";
      },350);
    }
  }

  click_event_loading(event){
    event.stopPropagation();
  }
}
export class Homeowner{
  type: number;
  email: string;
  first_name: string;
  last_name: string;
  password: string;
  contact_number: string;
  constructor(type: number, email: string, first_name: string, last_name: string, password: string, contact_number: string) {
    this.type = type;
    this.email = email;
    this.first_name = first_name;
    this.last_name = last_name;
    this.password = password;
    this.contact_number = contact_number;
  }
}
