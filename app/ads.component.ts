import { Component, ElementRef, Input, trigger, state, style, transition, animate } from '@angular/core';
import './rxjs-operators';
import { Router} from '@angular/router';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';

@Component({
  selector: 'ads',
  templateUrl: './app/HTML/ads.html',
  styleUrls: ['css/ads.css'],
})

export class AdsComponent {
  lorem = "Plumbing, Drainage Services, Drainage Contractors, Waterproofing...";
  ads = [];
  ads_pool_copy = [];
  cat_array = [];

  ads_pool = [
    {
      "state": 0,
      "image": "./assets/images/icons/company_logos/d&g_plumbing.png",
      "name": "D&G Plumbing",
      "description": this.lorem,
      "bg": "./assets/images/ads_bg/PlumbingBCk.jpg",
      "street_address_1": "899 Danforth Rd",
      "street_address_2": "Toronto, ON, M1G 1A5",
      "address_1": "899 Danforth Rd",
      "address_2": "Toronto, ON",
      "address_3": "M1G 1A5",
      "contact": "+1(345)765-3212"
    },
    {
      "state": 1,
      "image": "./assets/images/icons/avatar/plumber.png",
      "name": "Allen Harper",
      "description": "Plumber - Home/Commercial",
      "bg": "./assets/images/ads_bg/ToolsBck.jpg",
      "id": 0,
      "first_name": "Allen",
      "last_name": "Harper",
      "sub_category": "Home/Commercial",
      "company": "",
      "rating": 4.8,
      "location": 2,
      "availability": "Available from 6am to 5pm",
      "job": "Plumber"
    },
    {
      "state": 1,
      "image": "./assets/images/icons/avatar/david_smith.png",
      "name": "James Gordon",
      "description": "Electrician - Home/Commercial",
      "bg": "./assets/images/ads_bg/ToolsBck.jpg",
      "id": 0,
      "first_name": "James",
      "last_name": "Gordon",
      "sub_category": "Home/Commercial",
      "company": "",
      "rating": 4.8,
      "location": 2,
      "availability": "Available from 6am to 5pm",
      "job": "Electrician"
    },
    {
      "state": 0,
      "image": "./assets/images/icons/company_logos/maple_leaf_plumbing.png",
      "name": "Maple Leaf Plumbing",
      "description": this.lorem,
      "bg": "./assets/images/ads_bg/WoodenTexture-1.jpg",
      "street_address_1": "234 Bayview Avenue",
      "street_address_2": "Toronto, ON, M3H 5J4",
      "address_1": "234 Bayview Avenue",
      "address_2": "Toronto, ON",
      "address_3": "M3H 5J4",
      "contact": "+1(312)351-3865"
    }
  ];

  constructor(public router: Router, private datatransfer:DataTransferService){
    for(var i = 0; i < this.ads_pool.length; i++){
      this.ads_pool_copy.push(this.ads_pool[i]);
    }
    this.set_ads();
  }

  set_ads(){
    this.ads.length = 0;
    for(var i = 0; i < 3; i++){
      var item = this.getRandomInt(0, this.ads_pool.length - 1);
      this.ads.push(this.ads_pool[item]);
      this.ads_pool.splice(item, 1);
    }
    for(var i = 0; i < 3; i++){
      this.ads_pool.push(this.ads[i]);
    }
    setTimeout(() => {
      this.set_ads();
    },20000);
  }

  getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  go_to_tradesman(value){
    if(this.datatransfer.is_logged_in()){
      this.datatransfer.delete_data_local_storage();
      if(this.ads[value].state == 1){
        this.datatransfer.set_data(0, this.ads[value].first_name, this.ads[value].last_name, this.ads[value].image, this.ads[value].company, this.ads[value].availability, this.ads[value].rating, this.ads[value].location, this.ads[value].sub_category, this.ads[value].id, this.ads[value].job);
        this.datatransfer.clear_delete_id();
        this.cat_array.length = 0;
        this.datatransfer.set_sub_categories(JSON.stringify(this.cat_array));
        this.datatransfer.set_category(this.ads[value].job);
        this.datatransfer.remove_service_time();
      }else{
        this.datatransfer.set_data(1, this.ads[value].name, this.ads[value].contact, this.ads[value].image, "", this.ads[value].address_1, "", "", this.ads[value].address_3, 0, this.ads[value].address_2);
      }
      this.router.navigate(['./tradesman']);
      this.datatransfer.set_ad_load();
    }else{
      this.datatransfer.set_ad_open_login(JSON.stringify(this.ads[value]));
    }
  }

  change_margin_top(){
    var location = this.router.url;

    if(location == "/tradesman"){
      return{
        "margin-top": "87.5px"
      }
    }else{
      return{
        "margin-top": "135px"
      }
    }
  }

}
