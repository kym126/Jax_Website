import { Injectable } from '@angular/core';

@Injectable()
export class SubCategoriesService {

  jobs = ["Plumber", "Electrician", "Gardener", "Painter", "Carpenter",  "Cleaners", "Movers", "Flooring", "Roofing", "Handyman"];

  sub_categories = [
    ["Drain Services", "Water Filtration & Purification", "Septic Tank Cleaning", "Repair & Installation", "Sprinkler Systems"],
    ["Alarm Systems", "Lighting", "Appliance Repair", "Installation/electrical wiring"],
    ["Fences & Gates", "Decks", "Lawn Maintenance", "Sprinkler Systems", "Swimming Pools", "Spas & Hot Tubs", "Gazebos & Sheds"],
    ["Exterior Painting", "Interior Painting"],
    ["Woodworking", "Stairs and Railings", "Cabinetry & Millwork", "Framing", "Bathroom & Kitchen"],
    ["Blinds Cleaning/Repairing", "Carpet & Rug Cleaning/Repairing", "Duct Cleaning", "House & Apartment Cleaning/Maid Service", "Window & Gutter Cleaning"],
    ["Moving & Storage", "Junk Removal"],
    ["Carpet Installers", "Floor Laying & Refinishing", "Tile & Stone/ Masonry & Bricklaying"],
    ["Gutters & Eavestroughs", "Siding and Insulation", "Skylight installation/repairs"],
    ["category 1", "category 2", "category 3", "category 4"]
  ];

  get_sub_cat(row, col){
    return this.sub_categories[row][col];
  }

  get_row_length(row){
    return this.sub_categories[row].length;
  }

  get_row_for_category(value){
    var selector = value.toLowerCase();
    switch(selector) {
      case "plumber":
        return this.sub_categories[0];
      case "electrician":
        return this.sub_categories[1];
      case "gardener":
        return this.sub_categories[2];
      case "painter":
        return this.sub_categories[3];
      case "carpenter":
        return this.sub_categories[4];
      case "cleaners":
        return this.sub_categories[5];
      case "movers":
        return this.sub_categories[6];
      case "flooring":
        return this.sub_categories[7];
      case "roofing":
        return this.sub_categories[8];
      case "handyman":
        return this.sub_categories[9];
    }
  }
}
