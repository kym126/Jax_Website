import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';
import { WindowRef } from './window.service';

@Component({
  selector: 'tradesman-settings',
  templateUrl: './app/HTML/tradesman_settings.html',
  styleUrls: ['css/tradesman_settings.css'],
  host: {'(window:scroll)': 'change_nav_btns($event)'}
})

export class TradesmanSettingsComponent {
  email_notifs = false;
  sms_notifs = false;
  window: Window;
  email_daily = true;
  email_request = true;
  email_general = true;
  sms_daily = true;
  sms_request = true;
  sms_general = true;

  current_email = "";
  new_email = "";

  current_password = "";
  new_password = "";
  confirm_password = "";

  temp_email_daily = "";
  temp_email_request = "";
  temp_email_general = "";
  temp_sms_daily = "";
  temp_sms_request = "";
  temp_sms_general = "";
  email_notif = "";
  sms_notif = "";
  pass_error = "";

  ngAfterViewInit(){
    var btn2 = this.element.nativeElement.querySelector('#change_password_nav_btn');
    var btn3 = this.element.nativeElement.querySelector('#notifications_nav_btn');
    this.set_btn(btn2, btn3);
    this.datatransfer.set_load(true);
    this.jsonfetch.fetch_t_settings(this.datatransfer.get_user_id()).subscribe(
         res => this.got_notif_settings(res),
         error =>  this.error_happened(error)
    );
  }

  error_happened(error){
    console.log(error);
  }

  got_notif_settings(res){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      if(res.notifications[0].daily_aval == "1"){
        this.email_daily = true;
        this.temp_email_daily = "1";
      }else{
        this.email_daily = false;
        this.temp_email_daily = "2";
      }

      if(res.notifications[0].incom_req == "1"){
        this.email_request = true;
        this.temp_email_request = "1";
      }else{
        this.email_request = false;
        this.temp_email_request = "2";
      }

      if(res.notifications[0].other == "1"){
        this.email_general = true;
        this.temp_email_general = "1";
      }else{
        this.email_general = false;
        this.temp_email_general = "2";
      }

      if(res.notifications[0].sdaily_aval == "1"){
        this.sms_daily = true;
        this.temp_sms_daily = "1";
      }else{
        this.sms_daily = false;
        this.temp_sms_daily = "2";
      }

      if(res.notifications[0].sincom_req == "1"){
        this.sms_request = true;
        this.temp_sms_request = "1";
      }else{
        this.sms_request = false;
        this.temp_sms_request = "2";
      }

      if(res.notifications[0].sother == "1"){
        this.sms_general = true;
        this.temp_sms_general = "1";
      }else{
        this.sms_general = false;
        this.temp_sms_general = "2";
      }

      if(res.notifications[0].viaemail == "1"){
        this.email_notifs = true;
        this.email_notif = "1";
      }else{
        this.email_notifs = false;
        this.email_notif = "2";
      }

      if(res.notifications[0].viasms == "1"){
        this.sms_notifs = true;
        this.sms_notif = "1";
      }else{
        this.sms_notifs = false;
        this.sms_notif = "2";
      }
    }
  }

  constructor(private element: ElementRef, public router: Router, private datatransfer:DataTransferService, private jsonfetch: JsonFetchService, win: WindowRef) {
    if(!this.datatransfer.is_logged_in() || this.datatransfer.get_user_type() ==  "1"){
        this.router.navigate(['./home']);
    }
    this.window = win.nativeWindow;
    this.current_email = this.datatransfer.get_user_email();
    this.scrollTo(0,0);
  }

  scrollTo(yPoint: number, duration: number) {
      setTimeout(() => {
          this.window.window.scrollTo(0, yPoint)
      }, duration);
      return;
  }

  change_nav_btns(event){
    var section2 = elmYPosition('change_password') - 174;
    var section3 = elmYPosition('notifications') - 174;

    var btn2 = this.element.nativeElement.querySelector('#change_password_nav_btn');
    var btn3 = this.element.nativeElement.querySelector('#notifications_nav_btn');

    if(currentYPosition() >= section2 && currentYPosition() <= section3){
      this.set_btn(btn2, btn3);
    }else{
      this.set_btn(btn3,btn2);
    }
  }

  set_btn(btn1, btn2){
    btn1.style.color = "#292C38";
    btn1.style.fontSize = "11pt";
    btn2.style.color = "#8C8C8C";
    btn2.style.fontSize = "8pt";
  }

  smoothScroll(eID) {
      var startY = currentYPosition();
      var stopY = elmYPosition(eID) - 72;
      var distance = stopY > startY ? stopY - startY : startY - stopY;
      if (distance < 100) {
          this.window.window.scrollTo(0, stopY); return;
      }
      var speed = Math.round(distance / 100);
      if (speed >= 20) speed = 20;
      var step = Math.round(distance / 100);
      var leapY = stopY > startY ? startY + step : startY - step;
      var timer = 0;
      if (stopY > startY) {
          for (var i = startY; i < stopY; i += step) {
              this.scrollTo(leapY, timer * speed);
              leapY += step; if (leapY > stopY) leapY = stopY; timer++;
          } return;
      }
      for (var i = startY; i > stopY; i -= step) {
          this.scrollTo(leapY, timer * speed);
          leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
      }
  }

  notif_btn(val){
    if(val == 0){
      if(this.email_notif){
        return{
          "display": "none"
        }
      }else{
        return{
          "display": "block"
        }
      }
    }else{
      if(this.sms_notif){
        return{
          "display": "none"
        }
      }else{
        return{
          "display": "block"
        }
      }
    }
  }

  set_height(val){
    /*if(val == 0){
      if(this.email_notif){
        if(window.innerWidth < 800){
          return{
            "max-height": "31vw"
          }
        }else{
          return{
            "max-height": "132px"
          }
        }
      }else{
        return{
          "max-height": "0"
        }
      }
    }else{
      if(this.sms_notif){
        if(window.innerWidth < 800){
          return{
            "max-height": "31vw"
          }
        }else{
          return{
            "max-height": "132px"
          }
        }
      }else{
        return{
          "max-height": "0"
        }
      }
    }*/

    return{
      "max-height": "none"
    }
  }

  set_chkbox(val){
    if(val){
      return "./assets/images/check_box_v2.png";
    }else{
      return "./assets/images/uncheck_box.png";
    }
  }

  set_bottom_height(){
    var height = window.innerHeight;
    var value = height - 382;

    if(window.innerWidth < 800){
      return{
        "min-height": "3vw"
      }
    }else{
      return{
        "min-height": value + "px"
      }
    }
  }

  change_password(){
    var error = false;
    var old_pass = this.element.nativeElement.querySelector('#old_pass');
    var pass = this.element.nativeElement.querySelector('#new_pass');
    var new_pass = this.element.nativeElement.querySelector('#c_new_pass');

    this.pass_error = "";
    if(this.new_password != this.confirm_password){
      this.pass_error = "*New passwords dont match*";
      pass.style.border = "1px solid #F27072";
      new_pass.style.border = "1px solid #F27072";
      error = true;
    }

    if(this.current_password.length == 0){
      this.pass_error = "*Please fill up all fields outlined with red*";
      old_pass.style.border = "1px solid #F27072";
      error = true;
    }

    if(this.new_password.length == 0){
      this.pass_error = "*Please fill up all fields outlined with red*";
      pass.style.border = "1px solid #F27072";
      error = true;
    }

    if(this.confirm_password.length == 0){
      this.pass_error = "*Please fill up all fields outlined with red*";
      new_pass.style.border = "1px solid #F27072";
      error = true;
    }

    if(error){
      return;
    }

    this.datatransfer.set_load(true);
    this.jsonfetch.send_tradesman_new_password(this.datatransfer.get_user_id(), this.current_password, this.new_password)
      .map(res => res)
          .subscribe(
              res => this.check_result_2(res),
              error => this.error_happened_2(error)
          );
  }

  error_happened_2(res){
    this.datatransfer.set_load(false);
    this.pass_error = "*Please check your internet connection and try again*";
  }

  check_result_2(res){
    this.datatransfer.set_load(false);
    console.log(res);
    if(res.STATUS == "OK"){
      this.new_password = "";
      this.current_password = "";
      this.confirm_password = "";
    }else{
      this.pass_error = "*" + res.ERR_MSG + "*";
    }
  }

  remove_style(id){
    this.element.nativeElement.querySelector(id).removeAttribute("style");
  }

  set_email_daily(){
    if(this.email_daily){
      this.temp_email_daily = "2";
    }else{
      this.temp_email_daily = "1";
    }
    this.datatransfer.set_load(true);
    this.jsonfetch.send_t_settings(this.datatransfer.get_user_id(), this.email_notif, this.temp_email_daily, this.temp_email_request, this.temp_email_general,
     this.sms_notif, this.temp_sms_daily, this.temp_sms_request, this.temp_sms_general).subscribe(
          res => this.set_email_daily_2(res),
          error =>  this.error_happened(error)
     );
  }

  set_email_daily_2(res){
    this.datatransfer.set_load(false);
    console.log(res);
    if(res.STATUS == "OK"){
      this.email_daily = !this.email_daily;
    }
  }

  set_email_request(){
    if(this.email_request){
      this.temp_email_request = "2";
    }else{
      this.temp_email_request = "1";
    }
    this.datatransfer.set_load(true);
    this.jsonfetch.send_t_settings(this.datatransfer.get_user_id(), this.email_notif, this.temp_email_daily, this.temp_email_request, this.temp_email_general,
     this.sms_notif, this.temp_sms_daily, this.temp_sms_request, this.temp_sms_general).subscribe(
          res => this.set_email_request_2(res),
          error =>  this.error_happened(error)
     );
  }

  set_email_request_2(res){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.email_request = !this.email_request;
    }
  }

  set_email_general(){
    if(this.email_general){
      this.temp_email_general = "2";
    }else{
      this.temp_email_general = "1";
    }
    this.datatransfer.set_load(true);
    this.jsonfetch.send_t_settings(this.datatransfer.get_user_id(), this.email_notif, this.temp_email_daily, this.temp_email_request, this.temp_email_general,
     this.sms_notif, this.temp_sms_daily, this.temp_sms_request, this.temp_sms_general).subscribe(
          res => this.set_email_general_2(res),
          error =>  this.error_happened(error)
     );
  }

  set_email_general_2(res){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.email_general = !this.email_general;
    }
  }

  set_sms_daily(){
    if(this.sms_daily){
      this.temp_sms_daily = "2";
    }else{
      this.temp_sms_daily = "1";
    }
    this.datatransfer.set_load(true);
    this.jsonfetch.send_t_settings(this.datatransfer.get_user_id(), this.email_notif, this.temp_email_daily, this.temp_email_request, this.temp_email_general,
     this.sms_notif, this.temp_sms_daily, this.temp_sms_request, this.temp_sms_general).subscribe(
          res => this.set_sms_daily_2(res),
          error =>  this.error_happened(error)
     );
  }

  set_sms_daily_2(res){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.sms_daily = !this.sms_daily;
    }
  }

  set_sms_request(){
    if(this.sms_request){
      this.temp_sms_request = "2";
    }else{
      this.temp_sms_request = "1";
    }
    this.datatransfer.set_load(true);
    this.jsonfetch.send_t_settings(this.datatransfer.get_user_id(), this.email_notif, this.temp_email_daily, this.temp_email_request, this.temp_email_general,
     this.sms_notif, this.temp_sms_daily, this.temp_sms_request, this.temp_sms_general).subscribe(
          res => this.set_sms_request_2(res),
          error =>  this.error_happened(error)
     );
  }

  set_sms_request_2(res){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.sms_request = !this.sms_request;
    }
  }

  set_sms_general(){
    if(this.sms_general){
      this.temp_sms_general = "2";
    }else{
      this.temp_sms_general = "1";
    }
    this.datatransfer.set_load(true);
    this.jsonfetch.send_t_settings(this.datatransfer.get_user_id(), this.email_notif, this.temp_email_daily, this.temp_email_request, this.temp_email_general,
     this.sms_notif, this.temp_sms_daily, this.temp_sms_request, this.temp_sms_general).subscribe(
          res => this.set_sms_general_2(res),
          error =>  this.error_happened(error)
     );
  }

  set_sms_general_2(res){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.sms_general = !this.sms_general;
    }
  }

  set_sms_notif(){
    if(!this.sms_notifs){
      this.sms_notif = "1";
    }else{
      this.sms_notif  = "2";
    }
    this.datatransfer.set_load(true);
    this.jsonfetch.send_t_settings(this.datatransfer.get_user_id(), this.email_notif, this.temp_email_daily, this.temp_email_request, this.temp_email_general,
     this.sms_notif, this.temp_sms_daily, this.temp_sms_request, this.temp_sms_general).subscribe(
          res => this.set_sms_notif_2(res),
          error =>  this.error_happened(error)
     );
  }

  set_sms_notif_2(res){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.sms_notifs = !this.sms_notifs;
    }
  }

  set_email_notif(){
    if(!this.email_notifs){
      this.email_notif = "1";
    }else{
      this.email_notif  = "2";
    }
    this.datatransfer.set_load(true);
    this.jsonfetch.send_t_settings(this.datatransfer.get_user_id(), this.email_notif, this.temp_email_daily, this.temp_email_request, this.temp_email_general,
     this.sms_notif, this.temp_sms_daily, this.temp_sms_request, this.temp_sms_general).subscribe(
          res => this.set_email_notif_2(res),
          error =>  this.error_happened(error)
     );
  }

  set_email_notif_2(res){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.email_notifs = !this.email_notifs;
    }
  }
}

function currentYPosition() {
    // Firefox, Chrome, Opera, Safari
    if (self.pageYOffset) return self.pageYOffset;
    // Internet Explorer 6 - standards mode
    if (document.documentElement && document.documentElement.scrollTop)
        return document.documentElement.scrollTop;
    // Internet Explorer 6, 7 and 8
    if (document.body.scrollTop) return document.body.scrollTop;
    return 0;
}
function elmYPosition(eID) {
    var elm = document.getElementById(eID);
    var y = elm.offsetTop;
    var node = elm;
    while (node.offsetParent && node.offsetParent != document.body) {
        node = <HTMLElement><any>node.offsetParent;
        y += node.offsetTop;
    } return y;
}
