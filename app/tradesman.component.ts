import { Component, ElementRef, Input, trigger, state, style, transition, animate, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';
import { RequestFloatBoxComponent } from './request_float_box.component';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'tradesman',
  templateUrl: './app/HTML/tradesman.html',
  styleUrls: ['css/tradesman.css'],
})

export class TradesmanComponent {
  id = 0;
  gallery_num_rows = 1;
  recent_work_num_rows = 1;
  type = 0;
  fb_link = "";
  tw_link = "";
  ln_link = "";
  web_link = "";
   first_name = "";
   last_name = "";
   image = "";
   company = null;
   availability = "";
   rating = null;
   location = null;
   sub_category = "";
   category = "";

   about = "";
   specialize = [];
   choice = 0;

   recent_works = [];
   gallery = [];
   company_logo = "";
   company_street = null;
   company_province = "";
   company_postal_code = "";
   company_number = "";
   company_rating = 0;

   overlay_id= "overlay";
   tradesman_container_id="tradesman_container";
   overlay_1_id = "overlay_1";
   overlay_2_id = "overlay_2";
   overlay_3_id = "overlay_3";
   overlay_array_id = 0;
   overlay_array_1_id = 0;
   overlay_array_2_id = 0;
   overlay_array_3_id = 0;
   overlay_array_4_id = 0;

   ad_load: Subscription;

   rw_view_more_less = "more";

   gallery_view_more_less = "more";

   public DeleteFunction: Function;

   reviews = [];/*[
     {
       "name": "Walter White",
       "rating": 2.4,
       "date": "3rd November 2016",
       "title": "If you don't know who I am - then maybe your best course is to tread lightly.",
       "review": this.about,
       "images": []
     },
     {
       "name": "Saul Goodman",
       "rating": 4.3,
       "date": "21st October 2016",
       "title": "Better call Saul!",
       "review": this.about + this.about + this.about,
       "images": ["http://www.porkdisco.com/wp-content/uploads/2015/11/plumber.jpg"]
     },
     {
       "name": "Jesse Pinkman",
       "rating": 3.2,
       "date": "12th October 2016",
       "title": "Yeah Mr. White! You really do have a plan! Yeah science!",
       "review": this.about,
       "images": ["http://www.tubliners.com/wp-content/uploads/bigstock-Plumber-16576664.jpg",
                  "http://www.porkdisco.com/wp-content/uploads/2015/11/plumber.jpg",
                  "http://nextech2u.com/megajayarenovation2.com/images/Plumbing_Works.jpg"]
     }
   ];*/

   @ViewChild(RequestFloatBoxComponent) request:RequestFloatBoxComponent;

   ngOnInit(){
     window.scrollTo(0,0);
   }

  constructor(private element: ElementRef, private jsonfetch: JsonFetchService, public router: Router, private datatransfer:DataTransferService) {

    if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "2"){
      if(this.datatransfer.get_tradesman_values_json() == null){
        this.router.navigate(['./edit-profile']);
      }else{
        this.router.navigate(['./dashboard']);
      }
    }else if(this.datatransfer.is_data_from_list()){
      this.set_values();
      this.DeleteFunction= this.remove_item.bind(this);
      this.datatransfer.set_ad_open_login("");
      this.ad_load = this.datatransfer.ad_load
        .subscribe(value => this.set_ad_load(value));
    }else{
      if(this.datatransfer.is_category()){
        this.router.navigate(['./tradesmanlist']);
      }else{
        this.router.navigate(['./tradesmen']);
      }
    }
  }

  ngAfterViewInit(){
    if(this.datatransfer.get_request_item() != null){
      this.overlay_id = "overlay_active";
      this.datatransfer.set_blur(true);
      this.tradesman_container_id="tradesman_container_blurred";
    }
  }

  set_ad_load(value){
    if(value){
      this.set_values();
    }
  }

  set_off_on(id){
    if(id%2 == 0){
      return{
        "background-color": "#47b649"
      };
    }else{
      return{
        "background-color": "#f5845d"
      };
    }
  }

  set_values(){
    this.datatransfer.set_load(true);
    var timeoutVal = 10 * 1000 * 1000;
    navigator.geolocation.getCurrentPosition(
      position => this.gps_found(position),
      error => this.gps_error(error),
      { enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 }
    );
  }

  gps_found(position){
    this.jsonfetch.view_tr_profile(this.datatransfer.get_tr_id(), this.datatransfer.get_tradesman_cat(), position.coords.longitude, position.coords.latitude)
       .subscribe(
         tradesman => this.lol(tradesman),
         error =>  this.error_happened(error));
  }

  gps_error(error){
    this.jsonfetch.view_tr_profile(this.datatransfer.get_tr_id(), this.datatransfer.get_tradesman_cat(), null, null)
       .subscribe(
         tradesman => this.lol(tradesman),
         error =>  this.error_happened(error));
  }

  error_happened(error){
    console.log(error);
  }

  lol(res){
    this.datatransfer.set_load(false);
    console.log(res);
    if(res.STATUS == "OK"){

      var rw_array = [];
      var image = 'http://jaxoftrades.com/api/' + res.DATA[0].profile_imgpath + "?" + this.getRandomInt(90000000000, 100000000000);

      if(res.DATA[0].profile_imgpath == 0){
        image = "./assets/images/icons/default_profile_pic.png";
      }

        this.id = parseInt(this.datatransfer.get_tr_id());
        this.first_name =  res.DATA[0].tr_first_name;
        this.last_name = res.DATA[0].tr_last_name;
        this.company = res.DATA[0].company_name;
        this.image = image;

        for(var y = 0; y < res.DATA[0].job_sub_type.length; y++){
          if(res.DATA[0].job_sub_type[y].length != 0){
            if(y != 0){
              this.sub_category += " | ";
            }
            this.sub_category += res.DATA[0].job_sub_type[y];
          }
        }

        this.category = res.DATA[0].job_type;
        if(res.DATA[0].avg_rating != null){
          this.rating = parseFloat(res.DATA[0].avg_rating);
        }else{
          this.rating = null;
        }
        this.location = Math.round(res.DATA[0].distance/10)/100;
        this.availability = res.DATA[0].start_hour + ':' + res.DATA[0].start_minute + ' ' + res.DATA[0].start_am_pm + ' to ' + res.DATA[0].end_hour + ':' + res.DATA[0].end_minute + ' ' + res.DATA[0].end_am_pm;
        this.type = parseInt(this.datatransfer.get_view_profile_type(),10);
        this.about = res.DATA[0].about_you;
        this.specialize = [];
        if(res.DATA[0].predefined_skills == null || res.DATA[0].predefined_skills == ""){
          this.specialize = [];
        }else{
          this.specialize = res.DATA[0].predefined_skills;
        }
        
        for(var i = 0; i < res.DATA[0].skills.length; i++){
          this.specialize.push(res.DATA[0].skills[i]);
        }

        for(var x = 0; x < res.DATA[0].recentwork.length; x++){
          var title_shown = "";
          var desc_shown = "";
          var rw_img_file = [];
          var rw_img_url = [];

          if(res.DATA[0].recentwork[x].recentwork_title.length > 15){
            title_shown = res.DATA[0].recentwork[x].recentwork_title.substr(0, 11) + "...";
          }else{
            title_shown = res.DATA[0].recentwork[x].recentwork_title;
          }

          if(res.DATA[0].recentwork[x].recentwork_desc.length > 100){
            desc_shown = res.DATA[0].recentwork[x].recentwork_desc.substr(0, 96) + "...";
          }else{
            desc_shown = res.DATA[0].recentwork[x].recentwork_desc;
          }

          for(var y = 0; y < res.DATA[0].recentwork[x].recentwork_image.length; y++){
            rw_img_url.push(res.DATA[0].recentwork[x].recentwork_image[y]);
            res.DATA[0].recentwork[x].recentwork_image[y] = 'http://jaxoftrades.com/api/' + res.DATA[0].recentwork[x].recentwork_image[y] + "?" + this.getRandomInt(90000000000, 100000000000);
            rw_img_file.push(res.DATA[0].recentwork[x].recentwork_image[y]);
          }

          var rw = {
            "title": res.DATA[0].recentwork[x].recentwork_title,
            "description": res.DATA[0].recentwork[x].recentwork_desc,
            "images": res.DATA[0].recentwork[x].recentwork_image,
            "images_file": rw_img_file,
            "title_shown": title_shown,
            "description_shown": desc_shown,
            "chosen_bg": 0,
            "cost": 0,
            "id": res.DATA[0].recentwork[x].recentwork_id,
            "rw_image_url": rw_img_url
          }

          rw_array.push(rw);

        }


        this.recent_works = rw_array;

        var gallery = [];

        for(var y = 0; y < res.DATA[0].gallery_images.length; y++){
          gallery.push('http://jaxoftrades.com/api/' + res.DATA[0].gallery_images[y] + '?' + this.getRandomInt(90000000000, 100000000000));
        }


        this.gallery = gallery;
        this.company_logo = null;
        this.company_street = null;
        this.company_province = null;
        this.company_postal_code = null;
        this.company_number = null;
        this.company_rating = null;

        if(res.DATA[0].company.length != 0){
          this.company_logo = 'http://jaxoftrades.com/api/' + res.DATA[0].company[0].c_photo + '?' + this.getRandomInt(90000000000, 100000000000);
          this.company_street = res.DATA[0].company[0].c_add;
          this.company_province = res.DATA[0].company[0].c_city + ', ' + res.DATA[0].company[0].c_province;
          this.company_postal_code = res.DATA[0].company[0].c_postalcode.toUpperCase();
          this.company_number = '+1 ' + res.DATA[0].company[0].c_number;
        }
        this.fb_link = res.DATA[0].facebook_link;
        if((this.fb_link.substr(0,7) != "http://" && this.fb_link.substr(0,8) != "https://") && this.fb_link.length != 0){
          this.fb_link = "http://" + this.fb_link;
        }
        this.tw_link = res.DATA[0].twitter_link;
        if((this.tw_link.substr(0,7) != "http://" && this.fb_link.substr(0,8) != "https://") && this.tw_link.length != 0){
          this.tw_link = "http://" + this.tw_link;
        }
        this.ln_link = res.DATA[0].linked_in;
        if((this.ln_link.substr(0,7) != "http://" && this.fb_link.substr(0,8) != "https://") && this.ln_link.length != 0){
          this.ln_link = "http://" + this.ln_link;
        }
        this.web_link = res.DATA[0].website;
        if((this.web_link.substr(0,7) != "http://" && this.fb_link.substr(0,8) != "https://") && this.web_link.length != 0){
          this.web_link = "http://" + this.web_link;
        }



        for(var c = 0; c < res.DATA[0].review.length; c++){
          var review = {
            "name": res.DATA[0].review[c].h_first_name + " " + res.DATA[0].review[c].h_last_name,
            "rating": parseFloat(res.DATA[0].review[c].avg_rating),
            "date": res.DATA[0].review[c].date,
            "title": res.DATA[0].review[c].review_title,
            "review": res.DATA[0].review[c].review_desc,
            "images": res.DATA[0].review[c].review_image
          }

          this.reviews.push(review);
        }
    }
    /*this.first_name = this.datatransfer.get_first_name();
    this.last_name = this.datatransfer.get_last_name();
    this.image = this.datatransfer.get_image();
    this.company = this.datatransfer.get_company();
    this.availability = this.datatransfer.get_availability();
    this.rating = parseFloat(this.datatransfer.get_rating());
    this.location = this.datatransfer.get_location();
    this.sub_category = this.datatransfer.get_sub_category();
    this.category = this.datatransfer.get_tradesman_cat();
    this.company_rating = this.getRandomInt(0, 500)/100;
    this.type = parseInt(this.datatransfer.get_view_profile_type(),10);*/
  }

  compute_width(rating, index){
    if(index <= rating){
      return{
        "width": "98%",
        "margin-left": "1%",
        "margin-right": "1%"
      };
    }else{
      if(rating- index < -1){
        return{
          "width": "0",
          "margin-left": "1%",
          "margin-right": "1%"
        };
      }else{
        var value = (1 + rating- index)*98;
        return{
          "width": value + "%",
          "margin-left": "1%",
          "margin-right": "1%"
        };
      }
    }
  }

   background_selector(bg, gallery){
     if(gallery){
       return{
         "background-image": "url(" + bg + ")"
       }
     }else{
       if(bg.images.length == 0){
         return{
           "background-image": "none",
         }
       }else{
         return{
           "background-image": "url(" + bg.images[0] + ")",
         }
       }
     }
   }

   company_logo_bg(){
     return{
       "background-image": "url(" + this.company_logo + ")",
     }
   }

   getRandomInt(min, max){
         return Math.floor(Math.random() * (max - min + 1)) + min;
   }

   compute_company_rating(percentage){
     var value = 0;

     if(percentage > 4){
       value = 102;
       percentage -= 4;
     }else if (percentage > 3){
       value = 77;
       percentage -= 3;
     }else if (percentage > 2){
       value = 52;
       percentage -= 2;
     }else if (percentage > 1){
       value = 27;
       percentage -= 1;
     }else{
       value = 3;
     }

     value += percentage * 17;

     return{
       "width": value + "px"
     }
   }

   remove_item(){
     setTimeout(() => {
       this.datatransfer.add_id_to_delete(this.datatransfer.get_index());
       this.router.navigate(['./tradesmanlist']);
     },350);
   }

   request_pop(){
     this.datatransfer.set_blur(true);
     this.overlay_id="overlay_active";
     this.tradesman_container_id="tradesman_container_blurred";
     this.request.request_pop();
   }

   close_request(){
     if(this.request.element.nativeElement.querySelector('#request_container').style.display == "none"){
       this.close();
     }
   }

   close(){
     this.request.close_request();
     setTimeout(() => {
       this.overlay_id="overlay";
       this.tradesman_container_id="tradesman_container";
       this.datatransfer.set_blur(false);
     },350);
   }

   box_closed(event){
     if(event){
       this.close();
     }
   }

   rw_overlay_pop(i){
     var box = this.element.nativeElement.querySelector('#image_overlay');
     this.overlay_array_1_id = 0;

     this.overlay_1_id="overlay_1_active";
     this.tradesman_container_id="tradesman_container_blurred";
     this.overlay_array_id = i;
     box.style.opacity = "0";
     setTimeout(() => {
       box.style.opacity = "1";
     },10);

   }

   review_overlay_pop(i, j){
     var box = this.element.nativeElement.querySelector('#image_overlay_2');
     this.overlay_array_4_id = j;

     this.overlay_3_id="overlay_3_active";
     this.tradesman_container_id="tradesman_container_blurred";
     this.overlay_array_3_id = i;
     box.style.opacity = "0";
     setTimeout(() => {
       box.style.opacity = "1";
     },10);

   }

   overlay_image(){
     return this.recent_works[this.overlay_array_id].images[this.overlay_array_1_id];
   }

   overlay_image_3(){
     return this.reviews[this.overlay_array_3_id].images[this.overlay_array_4_id];
   }

   gallery_overlay_pop(i){
     var box = this.element.nativeElement.querySelector('#gallery_overlay');

     this.overlay_2_id="overlay_2_active";
     this.tradesman_container_id="tradesman_container_blurred";
     this.overlay_array_2_id = i;
     box.style.opacity = "0";
     setTimeout(() => {
       box.style.opacity = "1";
     },10);

   }

   overlay_image_pop(){
     return this.gallery[this.overlay_array_2_id];
   }

   remove_image_overlay(){
     var box = this.element.nativeElement.querySelector('#image_overlay');

     box.style.opacity = "0";
     setTimeout(() => {
       this.overlay_1_id="overlay_1";
       this.tradesman_container_id="tradesman_container";
     },350);
   }

   remove_image_overlay_2(){
     var box = this.element.nativeElement.querySelector('#gallery_overlay');

     box.style.opacity = "0";
     setTimeout(() => {
       this.overlay_2_id="overlay_2";
       this.tradesman_container_id="tradesman_container";
     },350);
   }

   remove_image_overlay_3(){
     var box = this.element.nativeElement.querySelector('#image_overlay_2');

     box.style.opacity = "0";
     setTimeout(() => {
       this.overlay_3_id="overlay_3";
       this.tradesman_container_id="tradesman_container";
     },350);
   }

   next_image(){
     this.overlay_array_1_id++;
   }

   prev_image(){
     this.overlay_array_1_id--;
   }

   next_image_1(){
     this.overlay_array_1_id = 0;
     this.overlay_array_id++;
   }

   prev_image_1(){
     this.overlay_array_1_id = 0;
     this.overlay_array_id--;
   }

   next_image_2(){
     this.overlay_array_2_id++;
   }

   prev_image_2(){
     this.overlay_array_2_id--;
   }

   next_image_3(){
     this.overlay_array_4_id++;
   }

   prev_image_3(){
     this.overlay_array_4_id--;
   }

   click_event(event){
     event.stopPropagation();
   }

   check_second_skill(i){
     if(i%2 == 0 && this.specialize[i+1] != null){
       return true;
     }else{
       return false;
     }
   }

   view_more_gallery(){
     var box = this.element.nativeElement.querySelector('#gallery_wrapper');
     var new_num_rows;
     if(this.gallery_num_rows * 4  < this.gallery.length){
       new_num_rows = this.gallery_num_rows+ 1;
     }else{
       new_num_rows = 1;
     }

    box.style.height = ((box.clientHeight + 3 )/(this.gallery_num_rows)) * new_num_rows;
    this.gallery_num_rows = new_num_rows;
    if(new_num_rows * 4  >= this.gallery.length){
      this.gallery_view_more_less = "less";
    }else{
      this.gallery_view_more_less = "more";
    }
   }

   view_more_rw(){
     var box = this.element.nativeElement.querySelector('#recent_works_wrapper');
     var new_num_rows;
     if(this.recent_work_num_rows * 3  < this.recent_works.length){
       new_num_rows = this.recent_work_num_rows + 1;
     }else{
      new_num_rows = 1;
     }

    box.style.height = ((box.clientHeight + 3)/(this.recent_work_num_rows)) * new_num_rows;
    this.recent_work_num_rows = new_num_rows;
    if(new_num_rows * 3  >= this.recent_works.length){
      this.rw_view_more_less = "less";
    }else{
      this.rw_view_more_less = "more";
    }
   }

   next_arrow_2(){
     if(this.gallery_num_rows * 4 > this.gallery.length){
       if(this.overlay_array_2_id == this.gallery.length - 1){
         return false;
       }else{
         return true;
       }
     }else{
       if((this.gallery_num_rows * 4) - 1== this.overlay_array_2_id){
         return false;
       }else{
         return true;
       }
     }
   }

   next_arrow_1(){
    if(this.recent_work_num_rows * 3 > this.recent_works.length){
       if(this.overlay_array_id == this.recent_works.length - 1){
         return false;
       }else{
         return true;
       }
     }else{
       if((this.recent_work_num_rows * 3) - 1 == this.overlay_array_id){
         return false;
       }else{
         return true;
       }
     }
   }

   set_dot_bg(overlay_array_id, i){
     if(i == overlay_array_id){
       return{
         "background-color": "#fff"
       };
     }else{
       return{
         "background-color": "transparent"
       };
     }
   }

   change_recent_work_image(i){
     this.overlay_array_1_id = i;
   }

   change_review_image(i){
     this.overlay_array_4_id = i;
   }
}
