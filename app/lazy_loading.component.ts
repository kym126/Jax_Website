import { Component, Input } from '@angular/core';

@Component({
  selector: 'image-loader',
  template: `<img *ngIf="!loaded" src="./assets/images/loading.gif" style="height: 60%; width: 60%; left: 50%; top: 50%; position: absolute; transform: translateX(-50%) translateY(-50%);"/>
    <img [hidden]="!loaded" (load)="loaded = true" [src]="src" style="height: 100%; width: 100%;"/>`
})
export class ImageLoaderComponent {
  @Input() src;
}
