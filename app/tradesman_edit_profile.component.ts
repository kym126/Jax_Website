import { Component, ElementRef, Input, Output, EventEmitter, ViewChild, NgZone } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';
import { WindowRef } from './window.service';
import { EditProfileComponent } from './editprofile.component';

@Component({
  selector: 'tradesman-edit-profile',
  templateUrl: './app/HTML/tradesman_edit_profile.html',
  styleUrls: ['css/tradesman_edit_profile.css'],
  host: {'(window:scroll)': 'change_nav_btns($event)'}
})

export class TradesmanEditProfileComponent {
  window: any;
  jobs = [];
  container_id = "container";
  content_id = "content";
  overlay_id = "overlay_hide"

  save_text = "Save";
  testimonial_text = "Write a testimonial";

  max_trades = 2;

  testimonial_title = "";
  testimonial_content = "";
  old_testimonial_title = "";
  old_testimonial_content = "";
  testimonial_error = "*Please provide a content for your testimonial*";

  @ViewChild(EditProfileComponent) profile: EditProfileComponent;

  constructor(private element: ElementRef, public router: Router, private datatransfer:DataTransferService, private jsonfetch: JsonFetchService, win: WindowRef, private zone: NgZone) {
    if(!this.datatransfer.is_logged_in() || this.datatransfer.get_user_type() ==  "1"){
        this.router.navigate(['./home']);
    }
    this.window = win.nativeWindow;
    this.scrollTo(0,0);
    this.jobs = JSON.parse(this.datatransfer.get_job_json()).jobs;
  }

  ngAfterViewInit(){
    var btn1 = this.element.nativeElement.querySelector('#profile_nav_btn');
    var btn2 = this.element.nativeElement.querySelector('#about_you_nav_btn');
    var btn3 = this.element.nativeElement.querySelector('#company_nav_btn');
    var btn4 = this.element.nativeElement.querySelector('#sub_category_nav_btn');
    var btn5 = this.element.nativeElement.querySelector('#skills_nav_btn');
    var btn6 = this.element.nativeElement.querySelector('#default_daily_time_availability_nav_btn');
    var btn7 = this.element.nativeElement.querySelector('#default_location_nav_btn');
    var btn8 = this.element.nativeElement.querySelector('#recent_works_nav_btn');
    var btn9 = this.element.nativeElement.querySelector('#gallery_nav_btn');
    var btn10 = this.element.nativeElement.querySelector('#add_social_media_links_nav_btn');

    this.set_nav_btn(btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10);
    setTimeout(() => {
      if(this.datatransfer.is_edit_default_daily_time_availability()){
        this.smoothScroll('default_daily_time_availability_title');
      }
    },350);

    setTimeout(() => {
      if(this.profile.job_specific_fields.length > 1){
        this.save_text = "Save all trades";
      }
    },1);

    var testimonial_json = this.datatransfer.get_testimonial();
    if(testimonial_json != null){
      this.testimonial_content = testimonial_json;
      this.testimonial_text = "Edit testimonial";
      this.profile.testimonial_text = "Edit testimonial";
    }

  }

  change_nav_btns(event){
    var offset = 100;
    var section1 = elmYPosition('profile_title') - offset;
    var section2 = elmYPosition('about_you_title') - offset;
    var section3 = elmYPosition('company_title') - offset;
    var section4 = elmYPosition('sub_category_title') - offset;
    var section5 = elmYPosition('skills_title') - offset;
    var section6 = elmYPosition('default_daily_time_availability_title') - offset;
    var section7 = elmYPosition('default_location_title') - offset;
    var section8 = elmYPosition('recent_works_title') - offset;
    var section9 = elmYPosition('gallery_title') - offset;
    var section10 = elmYPosition('add_social_media_links_title') - offset;

    var btn1 = this.element.nativeElement.querySelector('#profile_nav_btn');
    var btn2 = this.element.nativeElement.querySelector('#about_you_nav_btn');
    var btn3 = this.element.nativeElement.querySelector('#company_nav_btn');
    var btn4 = this.element.nativeElement.querySelector('#sub_category_nav_btn');
    var btn5 = this.element.nativeElement.querySelector('#skills_nav_btn');
    var btn6 = this.element.nativeElement.querySelector('#default_daily_time_availability_nav_btn');
    var btn7 = this.element.nativeElement.querySelector('#default_location_nav_btn');
    var btn8 = this.element.nativeElement.querySelector('#recent_works_nav_btn');
    var btn9 = this.element.nativeElement.querySelector('#gallery_nav_btn');
    var btn10 = this.element.nativeElement.querySelector('#add_social_media_links_nav_btn');


    if(currentYPosition() <= section2){
      this.set_nav_btn(btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10);
    }else if(currentYPosition() >= section2 && currentYPosition() <= section3 && this.jobs.length > 1){
      this.set_nav_btn(btn2, btn1, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10);
    }else if(currentYPosition() >= section2 && currentYPosition() <= section5 && this.jobs.length <= 1){
      this.set_nav_btn(btn2, btn1, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10);
    }else if(currentYPosition() >= section3 && currentYPosition() <= section4){
      this.set_nav_btn(btn3, btn1, btn2, btn4, btn5, btn6, btn7, btn8, btn9, btn10);
    }else if(currentYPosition() >= section4 && currentYPosition() <= section5){
      this.set_nav_btn(btn4, btn1, btn2, btn3, btn5, btn6, btn7, btn8, btn9, btn10);
    }else if(currentYPosition() >= section5 && currentYPosition() <= section6){
      this.set_nav_btn(btn5, btn1, btn2, btn3, btn4, btn6, btn7, btn8, btn9, btn10);
    }else if(currentYPosition() >= section6 && currentYPosition() <= section7){
      this.set_nav_btn(btn6, btn1, btn2, btn3, btn4, btn5, btn7, btn8, btn9, btn10);
    }else if(currentYPosition() >= section7 && currentYPosition() <= section8){
      this.set_nav_btn(btn7, btn1, btn2, btn3, btn4, btn5, btn6, btn8, btn9, btn10);
    }else if(currentYPosition() >= section8 && currentYPosition() <= section9){
      this.set_nav_btn(btn8, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn9, btn10);
    }else if(currentYPosition() >= section9 && currentYPosition() <= section10){
      this.set_nav_btn(btn9, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn10);
    }else if(currentYPosition() >= section10){
      this.set_nav_btn(btn10, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9);
    }else{
      this.set_nav_btn(btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10);
    }
  }

  set_nav_btn(btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10){
    btn1.style.color = "#292C38";
    btn1.style.fontSize = "11pt";
    btn2.style.color = "#8C8C8C";
    btn2.style.fontSize = "9pt";
    btn3.style.color = "#8C8C8C";
    btn3.style.fontSize = "9pt";
    btn4.style.color = "#8C8C8C";
    btn4.style.fontSize = "9pt";
    btn5.style.color = "#8C8C8C";
    btn5.style.fontSize = "9pt";
    btn6.style.color = "#8C8C8C";
    btn6.style.fontSize = "9pt";
    btn7.style.color = "#8C8C8C";
    btn7.style.fontSize = "9pt";
    btn8.style.color = "#8C8C8C";
    btn8.style.fontSize = "9pt";
    btn9.style.color = "#8C8C8C";
    btn9.style.fontSize = "9pt";
    btn10.style.color = "#8C8C8C";
    btn10.style.fontSize = "9pt";
  }

  smoothScroll(eID) {
      var startY = currentYPosition();
      var stopY = elmYPosition(eID) - 63;
      var distance = stopY > startY ? stopY - startY : startY - stopY;
      if (distance < 100) {
          this.window.window.scrollTo(0, stopY); return;
      }
      var speed = Math.round(distance / 100);
      if (speed >= 20) speed = 20;
      var step = Math.round(distance / 100);
      var leapY = stopY > startY ? startY + step : startY - step;
      var timer = 0;
      if (stopY > startY) {
          for (var i = startY; i < stopY; i += step) {
              this.scrollTo(leapY, timer * speed);
              leapY += step; if (leapY > stopY) leapY = stopY; timer++;
          } return;
      }
      for (var i = startY; i > stopY; i -= step) {
          this.scrollTo(leapY, timer * speed);
          leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
      }
  }
  scrollTo(yPoint: number, duration: number) {
      setTimeout(() => {
          this.window.window.scrollTo(0, yPoint)
      }, duration);
      return;
  }

  save_profile(){
    this.profile.save_all();
  }

  set_blur(event){
    if(event){
      this.container_id = "container_blur";
    }else{
      this.container_id = "container";
    }
  }

  refresh_page(event){
   this.jobs = JSON.parse(this.datatransfer.get_job_json()).jobs;
   this.smoothScroll('profile_title');
   this.save_text = "Save";
  }

  set_bottom_height(){
    var height = window.innerHeight;
    var value = height - 296;

    if(this.profile.job_specific_fields.length > 1){
      value = height - 320;
    }

    if(window.innerWidth > 800){
      return{
        "min-height": value + "px"
      }
    }else{
      return{
        "min-height": "3vw"
      }
    }
  }

  open_testimonial(){
    this.container_id="container_blur";
    this.content_id="content_blur";
    this.datatransfer.set_blur(true);
    this.overlay_id="overlay_active";
    this.old_testimonial_title = this.testimonial_title;
    this.old_testimonial_content = this.testimonial_content;

    var box = this.element.nativeElement.querySelector('#testimonial');
    box.style.opacity = 0;
    setTimeout(() => {
      box.style.opacity = 1;
    },10);
  }

  close_overlay(){
    var box = this.element.nativeElement.querySelector('#testimonial');
    var box_2 = this.element.nativeElement.querySelector('#error_strings');
    this.testimonial_error = "";
    box.style.opacity = 0;
    setTimeout(() => {
      this.container_id="container";
      this.content_id="content";
      this.datatransfer.set_blur(false);
      this.overlay_id="overlay_hide";
      box_2.style.opacity = 0;
    },350);
  }

  stop_prop(event){
    event.stopPropagation();
  }

  save_testimonial(){
    var box = this.element.nativeElement.querySelector('#error_strings');
    box.style.opacity = 0;

    if(this.testimonial_content.length == 0){
      this.testimonial_error = "*Please provide a content for your testimonial*";
      box.style.opacity = 1;
      return;
    }

    this.datatransfer.set_load(true);
    this.jsonfetch.testimonial (2, this.datatransfer.get_user_id(), this.testimonial_content, this.datatransfer.get_user_first_name(), this.datatransfer.get_user_last_name())
      .map(res => res)
          .subscribe(
              res => this.check_result(res),
              error => this.error_happened(error)
          );
  }

  error_happened(res){
    this.datatransfer.set_load(false);
    this.testimonial_error = "*Please check your internet connection and try again*";
  }

  check_result(res){
    this.datatransfer.set_load(false);
    console.log(res);
    if(res.STATUS == "OK"){
      this.datatransfer.set_testimonial(this.testimonial_content);
      this.testimonial_text = "Edit testimonial";
      this.profile.testimonial_text = "Edit testimonial";
      this.close_overlay();
    }else{
      this.testimonial_error = "*" + res.ERR_MSG + "*";
    }
  }

  cancel_testimonial(){
    this.testimonial_title = this.old_testimonial_title;
    this.testimonial_content = this.old_testimonial_content;
    this.close_overlay();
  }
}

function currentYPosition() {
    // Firefox, Chrome, Opera, Safari
    if (self.pageYOffset) return self.pageYOffset;
    // Internet Explorer 6 - standards mode
    if (document.documentElement && document.documentElement.scrollTop)
        return document.documentElement.scrollTop;
    // Internet Explorer 6, 7 and 8
    if (document.body.scrollTop) return document.body.scrollTop;
    return 0;
}
function elmYPosition(eID) {
    var elm = document.getElementById(eID);
    var y = elm.offsetTop;
    var node = elm;
    while (node.offsetParent && node.offsetParent != document.body) {
        node = <HTMLElement><any>node.offsetParent;
        y += node.offsetTop;
    } return y;
}
