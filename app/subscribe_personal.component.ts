import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';

@Component({
  selector: 'subscribe-personal',
  templateUrl: './app/HTML/subscribe_personal.html',
  styleUrls: ['css/subscribe_personal.css'],
})

export class SubscribePersonalComponent {
  package_1 = "Go ahead, dip your toe into our 3 Month Introductory Level and watch your business grow! This is a great option for tradesmen looking to start and grow their business with effective advertising. ";
  package_2 = "Advertising for one year on Jax of Trades will ensure that your business is top of mind for home and commercial property owners who are looking to hire pros like you!.";
  package_3 = "Advertising  your business for six months on Jax of Trades can add that extra level of publicity you’ve been searching for! ";
  constructor(public router: Router, private datatransfer:DataTransferService){
    if(!this.datatransfer.is_logged_in() || this.datatransfer.get_user_type() ==  "1"){
        this.router.navigate(['./home']);
    }
  }
}
