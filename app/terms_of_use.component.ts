import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { WindowRef } from './window.service';

@Component({
  selector: 'terms-of-use',
  templateUrl: './app/HTML/terms_of_use.html',
  styleUrls: ['css/misc_pages.css']
})

export class TermsOfUseComponent {
  constructor() {
    window.scrollTo(0, 0);
  }
}
