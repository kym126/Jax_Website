import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions, Jsonp } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import {Md5} from 'ts-md5/dist/md5'

@Injectable()
export class JsonFetchService {
  offline_signup = false;
  offline_login = false;
  constructor (private http: Http, private jsonp: Jsonp) {
  }

  getPages (Url): Observable<Pages> {
    return this.http.get(Url)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  getTradesman (Url): Observable<Tradesman> {
    return this.http.get(Url)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  get_testimonials(){
    return this.http.get("http://jaxoftrades.com/api/testimonials_hl.php")
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  get_testimonials_2(){
    return this.http.get("http://jaxoftrades.com/api/testimonials_tl.php")
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  send_post (Url, body): Observable<Response> {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
   let options = new RequestOptions({ headers: headers });
    return this.http.post(Url, body, options)
                    .map(this.extractData_1)
                    .catch(this.handleError);
  }

  send_forgot_password (email): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {
          let res = {
            STATUS: "OK",
            DATA: "candidate registered"
          }

          let formData: any = new FormData()
          let xhr = new XMLHttpRequest()

          formData.append("email", email.toLowerCase());

          if(this.offline_signup == false){
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://jaxoftrades.com/api/forgotpassword.php", true);
            xhr.send(formData);
          }else{
            resolve(res);
          }
      }));
  }

  send_xhr (url, type, email, first, last, password, contact, c_name, job, sub_categs, lat, long): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {
          let res = {
            STATUS: "OK",
            DATA: "candidate registered"
          }

          let formData: any = new FormData()
          let xhr = new XMLHttpRequest()

          var encrypted_pass = Md5.hashStr(password);

          if(type == 1){
            formData.append("h_first_name", first);
            formData.append("h_last_name", last);
          }else{
            formData.append("tr_first_name", first);
            formData.append("tr_last_name", last);
          }

          formData.append("type", type);
          formData.append("email", email.toLowerCase());
          formData.append("password", encrypted_pass);
          formData.append("contact_number", contact);
          formData.append("company_name", c_name);
          formData.append("job_type", job);
          formData.append("job_sub_type", sub_categs);
          formData.append("geo_lat", lat);
          formData.append("geo_long", long);

          if(this.offline_signup == false){
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/signup.php", true);
            xhr.send(formData);
          }else{
            resolve(res);
          }
      }));
  }

  send_login (url, email, pass, req_home): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let res = {
            STATUS: "OK",
            DATA: "login successful"
          }

          let formData: any = new FormData()
          let xhr = new XMLHttpRequest()

          var encrypted_pass = Md5.hashStr(pass);

          formData.append("email", email.toLowerCase());
          formData.append("password", encrypted_pass);
          formData.append("key", req_home);

          if(this.offline_login == false){
            xhr.onreadystatechange = function () {
                  if (xhr.readyState === 4) {
                      if (xhr.status === 200) {
                          resolve(JSON.parse(xhr.response));
                      } else {
                          reject(xhr.response);
                      }
                  }
              }
              xhr.open("POST", "http://www.jaxoftrades.com/api/login.php", true);
              xhr.send(formData);
        }else{
          resolve(res);
        }

    }));
  }

  send_homeowner_edit_profile (id, email, first, last, contact, image, image_url): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let res = {
            STATUS: "OK",
            DATA: "login successful"
          }

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          formData.append("homeowner_id", id);
          formData.append("email", email.toLowerCase());
          formData.append("h_first_name", first);
          formData.append("h_last_name", last);
          formData.append("contact_number", contact);
          formData.append("profile_image", image);
          formData.append("profile_url", image_url);

          console.log(image);

          if(this.offline_login == false){
            xhr.onreadystatechange = function () {
                  if (xhr.readyState === 4) {
                      if (xhr.status === 200) {
                          resolve(JSON.parse(xhr.response));
                      } else {
                          reject(xhr.response);
                      }
                  }
              }
              xhr.open("POST", "http://www.jaxoftrades.com/api/editprofile_h.php", true);
              xhr.send(formData);
        }else{
          resolve(res);
        }

    }));
  }

  send_tradesman_new_password (id, old, new_pass): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let res = {
            STATUS: "OK",
            DATA: "login successful"
          }

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          var encrypted_pass = Md5.hashStr(old);

          var encrypted_pass2 = Md5.hashStr(new_pass);

          formData.append("tradesmen_id", id);
          formData.append("old_password", encrypted_pass);
          formData.append("new_password", encrypted_pass2);

          if(this.offline_login == false){
            xhr.onreadystatechange = function () {
                  if (xhr.readyState === 4) {
                      if (xhr.status === 200) {
                          resolve(JSON.parse(xhr.response));
                      } else {
                          reject(xhr.response);
                      }
                  }
              }
              xhr.open("POST", "http://www.jaxoftrades.com/api/settings_changepassword.php", true);
              xhr.send(formData);
        }else{
          resolve(res);
        }

    }));
  }

  send_new_password (id, old, new_pass): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let res = {
            STATUS: "OK",
            DATA: "login successful"
          }

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          var encrypted_pass = Md5.hashStr(old);

          var encrypted_pass2 = Md5.hashStr(new_pass);

          formData.append("homeowner_id", id);
          formData.append("old_password", encrypted_pass);
          formData.append("new_password", encrypted_pass2);

          if(this.offline_login == false){
            xhr.onreadystatechange = function () {
                  if (xhr.readyState === 4) {
                      if (xhr.status === 200) {
                          resolve(JSON.parse(xhr.response));
                      } else {
                          reject(xhr.response);
                      }
                  }
              }
              xhr.open("POST", "http://www.jaxoftrades.com/api/cpassword_h.php", true);
              xhr.send(formData);
        }else{
          resolve(res);
        }

    }));
  }

  testimonial (type, id, text, firstname, lastname): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          formData.append("type", type);
          if(type == 1){
            formData.append("homeowner_id", id);
            formData.append("t_text", text);
            formData.append("h_first_name", firstname);
            formData.append("h_last_name", lastname);
          }else{
            formData.append("tradesmen_id", id);
            formData.append("t_text", text);
            formData.append("tr_first_name", firstname);
            formData.append("tr_last_name", lastname);
          }

          xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/testimonials.php", true);
            xhr.send(formData);

    }));
  }

  tradesman_list(job, sub, time, long, lat, postal_code): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          formData.append("job_type", job);
          formData.append("job_sub_type", sub);

          if(time != null){
            formData.append("service_time", time);
          }

          if(long != null && lat != null){
            formData.append("geo_long", long);
            formData.append("geo_lat", lat);
          }

          if(postal_code != null){
            formData.append("postal_code", postal_code);
          }

          xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/tr_list.php", true);
            xhr.send(formData);

    }));
  }

  request_rating(id): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          formData.append("request_id", id);

          xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/req_rating.php", true);
            xhr.send(formData);

    }));
  }

  fetch_t_settings(id): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          formData.append("tradesmen_id", id);

          xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                      console.log(xhr.response);
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/d_notifications.php", true);
            xhr.send(formData);

    }));
  }

  send_t_settings(id, v_email, e_davail, e_ireq, e_other, v_sms, s_davail, s_ireq, s_other): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          formData.append("tradesmen_id", id);
          formData.append("viaemail", v_email);
          formData.append("daily_aval", e_davail);
          formData.append("incom_req", e_ireq);
          formData.append("other", e_other);
          formData.append("viasms", v_sms);
          formData.append("sdaily_aval", s_davail);
          formData.append("sincom_req", s_ireq);
          formData.append("sother", s_other);

          xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/s_notification.php", true);
            xhr.send(formData);

    }));
  }

  best_tradesman_list_home(long, lat): Observable<{}> {
    return Observable.fromPromise(new Promise((resolve, reject) => {

        let formData: any = new FormData();
        let xhr = new XMLHttpRequest();

        formData.append("type", "home");
        if(long != null && lat != null){
          formData.append("geo_long", long);
          formData.append("geo_lat", lat);
        }

        xhr.onreadystatechange = function () {
              if (xhr.readyState === 4) {
                  if (xhr.status === 200) {
                      resolve(JSON.parse(xhr.response));
                  } else {
                      reject(xhr.response);
                  }
              }
          }
          xhr.open("POST", "http://www.jaxoftrades.com/api/tr_list.php", true);
          xhr.send(formData);

  }));
  }

  delete_trade(id, job): Observable<{}> {
    return Observable.fromPromise(new Promise((resolve, reject) => {

        let formData: any = new FormData();
        let xhr = new XMLHttpRequest();

        formData.append("tradesmen_id", id);
        formData.append("job_type", job);

        xhr.onreadystatechange = function () {
              if (xhr.readyState === 4) {
                  if (xhr.status === 200) {
                      resolve(JSON.parse(xhr.response));
                  } else {
                      reject(xhr.response);
                  }
              }
          }
          xhr.open("POST", "http://www.jaxoftrades.com/api/delete_trade.php", true);
          xhr.send(formData);

  }));
  }

  best_tradesman_list(): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          formData.append("type", "home");

          xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/tr_list.php", true);
            xhr.send(formData);

    }));
  }

  get_postal_code(lon, lat): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          formData.append("geo_long", lon);
          formData.append("geo_lat", lat);

          xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                      console.log(xhr.response);
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/postalcode.php", true);
            xhr.send(formData);

    }));
  }

  request(h_id, t_id, f_name, l_name, email, contact, desc, job, sub_cat): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          formData.append("homeowner_id", h_id);
          formData.append("tradesmen_id", t_id);
          formData.append("h_first_name", f_name);
          formData.append("h_last_name", l_name);
          formData.append("email", email.toLowerCase());
          formData.append("contact_number", contact);
          formData.append("short_desc", desc);
          formData.append("job_type", job);
          formData.append("job_sub_type", sub_cat);

          xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/request.php", true);
            xhr.send(formData);

    }));
  }

  check_request_status(h_id, t_id){
    return Observable.fromPromise(new Promise((resolve, reject) => {

        let formData: any = new FormData();
        let xhr = new XMLHttpRequest();

        formData.append("homeowner_id", h_id);
        formData.append("tradesmen_id", t_id);

        xhr.onreadystatechange = function () {
              if (xhr.readyState === 4) {
                  if (xhr.status === 200) {
                      resolve(JSON.parse(xhr.response));
                  } else {
                      reject(xhr.response);
                  }
              }
          }
          xhr.open("POST", "http://www.jaxoftrades.com/api/h_req_status.php", true);
          xhr.send(formData);

  }));
  }

  cancel_request(h_id, t_id){
    return Observable.fromPromise(new Promise((resolve, reject) => {

        let formData: any = new FormData();
        let xhr = new XMLHttpRequest();

        formData.append("homeowner_id", h_id);
        formData.append("tradesmen_id", t_id);

        xhr.onreadystatechange = function () {
              if (xhr.readyState === 4) {
                  if (xhr.status === 200) {
                      resolve(JSON.parse(xhr.response));
                  } else {
                      reject(xhr.response);
                  }
              }
          }
          xhr.open("POST", "http://www.jaxoftrades.com/api/cancel_request.php", true);
          xhr.send(formData);

  }));
  }

  view_tr_profile(id, job, long, lat){
    return Observable.fromPromise(new Promise((resolve, reject) => {

        let formData: any = new FormData();
        let xhr = new XMLHttpRequest();

        formData.append("tradesmen_id", id);
        formData.append("job_type", job);

        if(long != null && lat != null){
          formData.append("geo_long", long);
          formData.append("geo_lat", lat);
        }

        xhr.onreadystatechange = function () {
              if (xhr.readyState === 4) {
                  if (xhr.status === 200) {
                      resolve(JSON.parse(xhr.response));
                  } else {
                      reject(xhr.response);
                  }
              }
          }
          xhr.open("POST", "http://www.jaxoftrades.com/api/view_tr.php", true);
          xhr.send(formData);

  }));
  }

  fetch_h_history(id){
    return Observable.fromPromise(new Promise((resolve, reject) => {

        let formData: any = new FormData();
        let xhr = new XMLHttpRequest();

        formData.append("homeowner_id", id);

        xhr.onreadystatechange = function () {
              if (xhr.readyState === 4) {
                  if (xhr.status === 200) {
                      resolve(JSON.parse(xhr.response));
                  } else {
                      reject(xhr.response);
                  }
              }
          }
          xhr.open("POST", "http://www.jaxoftrades.com/api/h_history.php", true);
          xhr.send(formData);

  }));
  }

  clear_t_notif(r_id){
    return Observable.fromPromise(new Promise((resolve, reject) => {

        let formData: any = new FormData();
        let xhr = new XMLHttpRequest();

        formData.append("request_id[]", r_id);

        xhr.onreadystatechange = function () {
              if (xhr.readyState === 4) {
                  if (xhr.status === 200) {
                      resolve(JSON.parse(xhr.response));
                  } else {
                      reject(xhr.response);
                  }
              }
          }
          xhr.open("POST", "http://www.jaxoftrades.com/api/cle_notifications.php", true);
          xhr.send(formData);

  }));
  }

  send_reset_password(password, encrypt, user_type){
    return Observable.fromPromise(new Promise((resolve, reject) => {

        let res = {
          STATUS: "OK",
          DATA: "login successful"
        }

        let formData: any = new FormData();
        let xhr = new XMLHttpRequest();

        var encrypted_pass = Md5.hashStr(password);

        formData.append("password", encrypted_pass);
        formData.append("encrypt", encrypt);
        formData.append("user_type", user_type);

        if(this.offline_login == false){
          xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/passwordset.php", true);
            xhr.send(formData);
      }else{
        resolve(res);
      }

    }));
  }

  clear_h_history(r_id){
    return Observable.fromPromise(new Promise((resolve, reject) => {

        let formData: any = new FormData();
        let xhr = new XMLHttpRequest();

        formData.append("request_id[]", r_id);

        xhr.onreadystatechange = function () {
              if (xhr.readyState === 4) {
                  if (xhr.status === 200) {
                      resolve(JSON.parse(xhr.response));
                  } else {
                      reject(xhr.response);
                  }
              }
          }
          xhr.open("POST", "http://www.jaxoftrades.com/api/h_cle_history.php", true);
          xhr.send(formData);

  }));
  }

  clear_t_history(r_id){
    return Observable.fromPromise(new Promise((resolve, reject) => {

        let formData: any = new FormData();
        let xhr = new XMLHttpRequest();

        formData.append("request_id[]", r_id);

        xhr.onreadystatechange = function () {
              if (xhr.readyState === 4) {
                  if (xhr.status === 200) {
                      resolve(JSON.parse(xhr.response));
                  } else {
                      reject(xhr.response);
                  }
              }
          }
          xhr.open("POST", "http://www.jaxoftrades.com/api/tr_cle_history.php", true);
          xhr.send(formData);

  }));
  }

  get_inc_request(id, email){
    return Observable.fromPromise(new Promise((resolve, reject) => {

        let formData: any = new FormData();
        let xhr = new XMLHttpRequest();

        formData.append("tradesmen_id", id);
        formData.append("email", email.toLowerCase());

        xhr.onreadystatechange = function () {
              if (xhr.readyState === 4) {
                  if (xhr.status === 200) {
                    //console.log(xhr.response);
                    resolve(JSON.parse(xhr.response));
                  } else {
                      reject(xhr.response);
                  }
              }
          }
          xhr.open("POST", "http://www.jaxoftrades.com/api/login_after_req.php", true);
          xhr.send(formData);

  }));
  }

  req_status_1(id, value){
    return Observable.fromPromise(new Promise((resolve, reject) => {

        let formData: any = new FormData();
        let xhr = new XMLHttpRequest();

        formData.append("request_id", id);
        formData.append("request_status", value);

        xhr.onreadystatechange = function () {
              if (xhr.readyState === 4) {
                  if (xhr.status === 200) {
                      resolve(JSON.parse(xhr.response));
                  } else {
                      reject(xhr.response);
                  }
              }
          }
          xhr.open("POST", "http://www.jaxoftrades.com/api/request_status.php", true);
          xhr.send(formData);

  }));
  }

  req_status_2(id, value){
    return Observable.fromPromise(new Promise((resolve, reject) => {

        let formData: any = new FormData();
        let xhr = new XMLHttpRequest();

        formData.append("request_id", id);
        formData.append("request_status", value);

        xhr.onreadystatechange = function () {
              if (xhr.readyState === 4) {
                  if (xhr.status === 200) {
                      resolve(JSON.parse(xhr.response));
                  } else {
                      reject(xhr.response);
                  }
              }
          }
          xhr.open("POST", "http://www.jaxoftrades.com/api/incoming_req_status.php", true);
          xhr.send(formData);

  }));
  }

  review_a_tradesman(h_id, t_id, r1, r2, r3, r4, avg_rating, job, title, desc, img, r_id): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          formData.append("homeowner_id", h_id);
          formData.append("tradesmen_id", t_id);
          formData.append("request_id", r_id);
          formData.append("rate1", r1);
          formData.append("rate2", r2);
          formData.append("rate3", r3);
          formData.append("rate4", r4);
          formData.append("avg_rating", avg_rating);
          formData.append("job_type", job);
          formData.append("review_title", title);
          formData.append("review_desc", desc);

          for (var x = 0; x < img.length; x++) {
            formData.append("review_image[]", img[x]);
          }

          xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/rating.php", true);
            xhr.send(formData);

    }));
  }

  initial_tr_profile(id, job, about_you, image, pd_skills, a_skills, time, loc, fb, tw, linked_in, web, gallery, rw_title, rw_desc, rw_img, sub_categ, company, rw_id, rw_index): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          formData.append("tradesmen_id", id);
          formData.append("profile_image", image);
          formData.append("job_type", job);
          formData.append("about_you", about_you);
          formData.append("predefined_skills", pd_skills);
          formData.append("skills", a_skills);
          formData.append("daily_time", time);
          formData.append("default_location", loc);
          formData.append("facebook_link", fb);
          formData.append("twitter_link", tw);
          formData.append("linked_in", linked_in);
          formData.append("website", web);
          formData.append("job_sub_type", sub_categ);
          formData.append("company_name", company);

          for (var x = 0; x < rw_id.length; x++) {
            formData.append("recentwork_id[]", rw_id[x]);
          }

          for (var x = 0; x < gallery.length; x++) {
            formData.append("gallery_image[]", gallery[x]);
          }

          for (var x = 0; x < rw_title.length; x++) {
            formData.append("recentwork_title[]", rw_title[x]);
          }

          for (var x = 0; x < rw_desc.length; x++) {
            formData.append("recentwork_desc[]", rw_desc[x]);
          }

          for (var x = 0; x < rw_img.length; x++) {
            for(var y = 0; y < rw_img[x].length; y++){
              var key =  'recentwork_image' + x + '[]';
              formData.append(key, rw_img[x][y]);
            }
          }

          for (var x = 0; x < rw_index.length; x++) {
            for(var y = 0; y < rw_index[x].length; y++){
              var key =  'rw' + x + '[]';
              formData.append(key, rw_index[x][y]);
            }
          }

          xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/update_tr.php", true);
            xhr.send(formData);

    }));
  }

  update_tr_profile(id, job, about_you, image, pd_skills, a_skills, time, loc, fb, tw, linked_in, web, gallery, rw_title, rw_desc, rw_img, sub_categ, company, rw_id, contact, email, fname, lname, profile_url, rw_index): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {
          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          formData.append("contact_number", contact);
          formData.append("email", email.toLowerCase());
          formData.append("tr_first_name", fname);
          formData.append("tr_last_name", lname);
          formData.append("tradesmen_id", id);
          formData.append("profile_image", image);
          formData.append("profile_url", profile_url);
          formData.append("job_type", job);
          formData.append("about_you", about_you);
          formData.append("predefined_skills", pd_skills);
          formData.append("skills", a_skills);
          formData.append("daily_time", time);
          formData.append("default_location", loc);
          formData.append("facebook_link", fb);
          formData.append("twitter_link", tw);
          formData.append("linked_in", linked_in);
          formData.append("website", web);
          formData.append("job_sub_type", sub_categ);
          formData.append("company_name", company);

          for (var x = 0; x < rw_id.length; x++) {
            formData.append("recentwork_id[]", rw_id[x]);
          }

          for (var x = 0; x < gallery.length; x++) {
            formData.append("gallery_image[]", gallery[x]);
          }

          for (var x = 0; x < rw_title.length; x++) {
            formData.append("recentwork_title[]", rw_title[x]);
          }

          for (var x = 0; x < rw_desc.length; x++) {
            formData.append("recentwork_desc[]", rw_desc[x]);
          }

          for (var x = 0; x < rw_img.length; x++) {
            for(var y = 0; y < rw_img[x].length; y++){
              var key =  'recentwork_image' + x + '[]';
              formData.append(key, rw_img[x][y]);
            }
          }

          for (var x = 0; x < rw_index.length; x++) {
            for(var y = 0; y < rw_index[x].length; y++){
              var key =  'rw' + x + '[]';
              formData.append(key, rw_index[x][y]);
            }
          }

          xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/dupdate_tr.php", true);
            xhr.send(formData);

    }));
  }

  add_trade(id, job, categs, a_skills, p_skills, company, time, loc, fb, tw, linked_in, web, gallery, rw_title, rw_desc, rw_img, rw_id): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          formData.append("tradesmen_id", id);
          formData.append("job_type", job);
          formData.append("job_sub_type", categs);
          formData.append("predefined_skills", p_skills);
          formData.append("skills", a_skills);
          formData.append("company_name", company);
          formData.append("daily_time", time);
          formData.append("default_location", loc);
          formData.append("facebook_link", fb);
          formData.append("twitter_link", tw);
          formData.append("linked_in", linked_in);
          formData.append("website", web);

          for (var x = 0; x < rw_id.length; x++) {
            formData.append("recentwork_id[]", rw_id[x]);
          }

          for (var x = 0; x < gallery.length; x++) {
            console.log(gallery[x]);
            formData.append("gallery_image[]", gallery[x]);
          }

          for (var x = 0; x < rw_title.length; x++) {
            formData.append("recentwork_title[]", rw_title[x]);
          }

          for (var x = 0; x < rw_desc.length; x++) {
            formData.append("recentwork_desc[]", rw_desc[x]);
          }

          for (var x = 0; x < rw_img.length; x++) {
            for(var y = 0; y < rw_img[x].length; y++){
              var key =  'recentwork_image' + x + '[]';
              formData.append(key, rw_img[x][y]);
            }
          }

          xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/trade.php", true);
            xhr.send(formData);

    }));
  }

  dnd_toggle(id, job, dnd): Observable<{}> {
        return Observable.fromPromise(new Promise((resolve, reject) => {

            console.log(id, job, dnd);
            let formData: any = new FormData();
            let xhr = new XMLHttpRequest();

            formData.append("tradesmen_id", id);
            formData.append("job_type", job);
            formData.append("dnd", dnd);

            xhr.onreadystatechange = function () {
                  if (xhr.readyState === 4) {
                      if (xhr.status === 200) {
                          resolve(JSON.parse(xhr.response));
                      } else {
                          reject(xhr.response);
                      }
                  }
              }
              xhr.open("POST", "http://www.jaxoftrades.com/api/dnd.php", true);
              xhr.send(formData);

      }));
  }

  start_work(id, status): Observable<{}> {
        return Observable.fromPromise(new Promise((resolve, reject) => {

            console.log(id,status);
            let formData: any = new FormData();
            let xhr = new XMLHttpRequest();

            formData.append("request_id", id);
            formData.append("work_status", status);

            xhr.onreadystatechange = function () {
                  if (xhr.readyState === 4) {
                      if (xhr.status === 200) {
                          resolve(JSON.parse(xhr.response));
                      } else {
                          reject(xhr.response);
                      }
                  }
              }
              xhr.open("POST", "http://www.jaxoftrades.com/api/incoming_req_status.php", true);
              xhr.send(formData);

      }));
  }

  history_status(id, status): Observable<{}> {
        return Observable.fromPromise(new Promise((resolve, reject) => {

            let formData: any = new FormData();
            let xhr = new XMLHttpRequest();

            formData.append("request_id", id);
            formData.append("job_status", status);

            xhr.onreadystatechange = function () {
                  if (xhr.readyState === 4) {
                      if (xhr.status === 200) {
                          resolve(JSON.parse(xhr.response));
                      } else {
                          reject(xhr.response);
                      }
                  }
              }
              xhr.open("POST", "http://www.jaxoftrades.com/api/history_status.php", true);
              xhr.send(formData);

      }));
  }

  initial_company_profile(id, address, about_you, image, city, a_skills, province, loc, fb, tw, linked_in, web, gallery, rw_title, rw_desc, rw_img, contact, company, email, rw_id, prof_url, rw_index): Observable<{}> {
      return Observable.fromPromise(new Promise((resolve, reject) => {

          let formData: any = new FormData();
          let xhr = new XMLHttpRequest();

          formData.append("tradesmen_id", id);
          formData.append("profile_image", image);
          formData.append("profile_url", prof_url);
          formData.append("c_add", address);
          formData.append("c_about", about_you);
          formData.append("c_city", city);
          formData.append("c_spec", a_skills);
          formData.append("c_province", province);
          formData.append("c_postalcode", loc);
          formData.append("facebook_link", fb);
          formData.append("twitter_link", tw);
          formData.append("linked_in", linked_in);
          formData.append("website", web);
          formData.append("c_name", company);
          formData.append("c_number", contact);
          formData.append("c_email", email.toLowerCase());

          for (var x = 0; x < rw_id.length; x++) {
            formData.append("recentwork_id[]", rw_id[x]);
          }

          for (var x = 0; x < gallery.length; x++) {
            formData.append("gallery_image[]", gallery[x]);
          }

          for (var x = 0; x < rw_title.length; x++) {
            formData.append("recentwork_title[]", rw_title[x]);
          }

          for (var x = 0; x < rw_desc.length; x++) {
            formData.append("recentwork_desc[]", rw_desc[x]);
          }

          for (var x = 0; x < rw_img.length; x++) {
            for(var y = 0; y < rw_img[x].length; y++){
              var key =  'recentwork_image' + x + '[]';
              formData.append(key, rw_img[x][y]);
            }
          }

          for (var x = 0; x < rw_index.length; x++) {
            for(var y = 0; y < rw_index[x].length; y++){
              var key =  'rw' + x + '[]';
              formData.append(key, rw_index[x][y]);
            }
          }

          xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", "http://www.jaxoftrades.com/api/company_tr.php", true);
            xhr.send(formData);

    }));
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }

  private extractData_1(res: Response) {
    return res;
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

}

export interface Pages {
    pages: string[],
}

export interface h_l_testimonial {
    "STATUS": string,
    "DATA": [
      {
        "homeowner_id": string,
        "t_title": string,
        "t_text": string,
        "first_name": string,
        "last_name": string,
        "profile_imgpath": string
      }
    ],
    "ERR_MSG": string
}

export interface Tradesman {
  results: [
  {
    gender: string,
    name: {
      title: string,
      first: string,
      last: string
    },
    location: {
      street: string,
      city: string,
      state: string,
      postcode: string
    },
    email: string,
    login: {
      username: string,
      password: string,
      salt: string,
      md5: string,
      sha1: string,
      sha256: string
    },
    dob: string,
    registered: string,
    phone: string,
    cell: string,
    id: {
      name: string,
      value: string
      },
      picture: {
        large: string,
        medium: string,
        thumbnail: string
      },
      nat: string
    }
  ],
  info: {
    seed: string,
    results: string,
    page: string,
    version: string
  }
}
