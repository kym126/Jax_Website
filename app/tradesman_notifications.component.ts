import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';
import { RequestService } from './request.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'tradesman-notifications',
  templateUrl: './app/HTML/tradesman_notifications.html',
  styleUrls: ['css/tradesman_notifications.css'],
})

export class TradesmanNotificationsComponent {
    notifications = [];
    subscription: Subscription;
    date =  new Date();
    months = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"];

    constructor(private element: ElementRef, private datatransfer:DataTransferService, public router: Router,  private request_service: RequestService,  private jsonfetch: JsonFetchService) {
      if(!this.datatransfer.is_logged_in() || this.datatransfer.get_user_type() ==  "1"){
          this.router.navigate(['./home']);
      }
      this.subscription = this.datatransfer.notif
         .subscribe(value => this.sub(value));
    }

    sub(value){
      var notif = JSON.parse(this.datatransfer.get_tradesman_notifications());
      if(notif == null){
        this.notifications = [];
      }else{
        this.notifications = notif;
      }
    }

    ngAfterViewInit(){
      var notif = JSON.parse(this.datatransfer.get_tradesman_notifications());
      console.log(notif);
      if(notif == null){
        this.notifications = [];
      }else{
        this.notifications = notif;
      }
    }

    clear_notification(){
      var r_id = [];
      for(var i = 0; i < this.notifications.length; i++){
        r_id.push(this.notifications[i].id);
      }

      this.datatransfer.set_load(true);
      this.jsonfetch.clear_t_notif(r_id).subscribe(
           res => this.clear_t_notif(res),
           error =>  this.clear_notif_err(error)
         );
    }

    clear_notif_err(error){
      this.datatransfer.set_load(false);
    }

    clear_t_notif(res){
      this.datatransfer.set_load(false);
      if(res.STATUS == "OK"){
        this.notifications.length = 0;
        this.datatransfer.set_tradesman_notifications(JSON.stringify(this.notifications));
        this.datatransfer.set_notif();
      }
    }

    notif_btn(i){
      this.datatransfer.set_load(true);
      if(this.notifications[i].button == "Mark as start work"){
        this.jsonfetch.start_work(this.notifications[i].id, 1)
          .map(res => res)
              .subscribe(
                  res => this.add_to_history_2(res, i),
                  error => this.error_happened(error)
              );
      }else if(this.notifications[i].button == "Mark as completed"){
        this.jsonfetch.history_status(this.notifications[i].id, 1)
          .map(res => res)
              .subscribe(
                  res => this.completed_2(res, i),
                  error => this.error_happened(error)
              );
      }else if(this.notifications[i].button == "Turn off DND mode"){

      }
    }

    error_happened(res){
      this.datatransfer.set_load(false);
      console.log(res);
    }

    add_to_history_2(res, i){
      console.log(res);
      this.datatransfer.set_load(false);
      if(res.STATUS == "OK"){
        var day;

        switch(this.date.getDate()){
          case 1:
            day = this.date.getDate() + "st";
            break;
          case 2:
            day = this.date.getDate() + "nd";
            break;
          case 3:
            day = this.date.getDate() + "rd";
            break;
          case 21:
            day = this.date.getDate() + "st";
            break;
          case 22:
            day = this.date.getDate() + "nd";
            break;
          case 23:
            day = this.date.getDate() + "rd";
            break;
          case 31:
            day = this.date.getDate() + "st";
            break;
          default:
            day = this.date.getDate() + "th";
            break;
        }

        for(var x = 0; x < this.request_service.requests.length; x++){
          if(this.notifications[i].id == this.request_service.requests[x].id){
            break;
          }
        }

        var history = {
          "state": 0,
          "first_name": this.request_service.requests[x].first_name,
          "last_name": this.request_service.requests[x].last_name,
          "date": day + " of " + this.months[this.date.getMonth()]+ " " + this.date.getFullYear(),
          "job": this.request_service.requests[x].job,
          "h_id": this.request_service.requests[x].h_id,
          "id": this.request_service.requests[x].id
        }

        setTimeout(() => {
          this.request_service.delete_req(x);
        }, 100);
        var histories = JSON.parse(this.datatransfer.get_tradesman_history());
        histories.splice(0,0, history);
        this.datatransfer.set_tradesman_history(JSON.stringify(histories));
        this.request_service.drop_reqs();
        this.datatransfer.set_load(true);
        var r_id = [];
        r_id.push(this.notifications[i].id);
        this.jsonfetch.clear_t_notif(r_id).subscribe(
             res => this.clear_t_notif_2(res, i),
             error =>  this.clear_notif_err(error)
           );
      }
    }

    completed_2(res, i){
      this.datatransfer.set_load(false);
      var histories = JSON.parse(this.datatransfer.get_tradesman_history());
      for(var x = 0; x < histories.length; x++){
        if(this.notifications[i].id == histories[x].id){
          break;
        }
      }
      if(res.STATUS == "OK"){
        histories[x].state = 1;
      }
      this.datatransfer.set_tradesman_history(JSON.stringify(histories));
      this.datatransfer.set_load(true);
      var r_id = [];
      r_id.push(this.notifications[i].id);
      this.jsonfetch.clear_t_notif(r_id).subscribe(
           res => this.clear_t_notif_2(res, i),
           error =>  this.clear_notif_err(error)
         );
    }

    clear_t_notif_2(res, i){
      this.datatransfer.set_load(false);
      if(res.STATUS == "OK"){
        this.notifications.splice(i, 1);
        this.datatransfer.set_tradesman_notifications(JSON.stringify(this.notifications));
        this.datatransfer.set_notif();
      }
    }
}
