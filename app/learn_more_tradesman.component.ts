import { Component, ElementRef, Input, trigger, state, style, transition, animate } from '@angular/core';
import './rxjs-operators';
import { JsonFetchService } from './JsonFetch.service'
import { Router} from '@angular/router';
import { DataTransferService } from './data_transfer.service';;

@Component({
  selector: 'learn-more-tradesman',
  templateUrl: './app/HTML/learn_more_tradesman.html',
  styleUrls: ['css/learn_more_tradesman.css'],
})

export class LearnMoreTradesman {
  title = "Homeowners are looking for pros like you";
  sub_title = "Jax of Trades puts your business in front of local homeowners and commercial property owners who are searching for professional tradesmen – like you!"
  perks = [
    {
      "image": "./assets/images/icons/skills.png",
      "title": "Showcase your Skills",
      "description": "Fill local homeowners and commercial property owners in on your business, what you do, and what you specialize in. It’s time to make your business shine!"
    },
    {
      "image": "./assets/images/icons/company.png",
      "title": "Add your Company",
      "description": "Fill local homeowners in on your business, what you do, and what you specialize in. It’s time to make your business shine!"
    },
    {
      "image": "./assets/images/icons/dashboard.png",
      "title": "The Tradesman's Dashboard",
      "description": "Update your business at any time with Jax of Trades’ interactive Tradesman’s Dashboard. Here you can build your profile page, add your specialties, view your customer ratings, change your daily availability, and add links to your website and social media pages. One great feature of the Tradesman’s Dashboard is that you will be alerted when a customer requests a quote, giving you the chance to respond back."
    },
    {
      "image": "./assets/images/icons/advertise.png",
      "title": "Advertise",
      "description": "Jax of Trades offers affordable and effective advertising opportunities. Be the king of your category by ensuring your business listing is seen at the very top of the results."
    },
  ]

  constructor(private datatransfer:DataTransferService, public router: Router) {
    if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "2"){
      if(this.datatransfer.get_tradesman_values_json() == null){
        this.router.navigate(['./edit-profile']);
      }else{
        this.router.navigate(['./dashboard']);
      }
    }
  }

  register_as_tradesman(){
    this.datatransfer.register_as_tradesman();
    this.router.navigate(['./sign-up']);
  }
}
