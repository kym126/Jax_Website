import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }   from './app.component';
import { LoginComponent } from './login.component';
import { ResetPasswordComponent } from './reset_password.component';
import { SignUpComponent } from './signup.component';
import { routing } from './app.routing';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home.component';
import { HttpModule, JsonpModule } from '@angular/http';
import { TradesmanListComponent } from './tradesmanlist.component';
import { TradesmanComponent } from './tradesman.component';
import { SearchPipe } from './search.pipe';
import { DataTransferService } from './data_transfer.service';
import { JsonFetchService } from './JsonFetch.service';
import { SubCategoriesService } from './subcategories.service';
import { AdsComponent } from './ads.component';
import { UserHistoryComponent } from './userhistory.component';
import { LoginFloatBoxComponent } from './login_float_box.component';
import { RequestFloatBoxComponent } from './request_float_box.component';
import { ImageLoaderComponent } from './lazy_loading.component';
import { EditProfileComponent } from './editprofile.component';
import { Ng2SliderComponent } from "ng2-slider-component/ng2-slider.component";
import { SlideAbleDirective } from 'ng2-slideable-directive/slideable.directive';
import { Ng2StyledDirective } from 'ng2-styled-directive/ng2-styled.directive';
import { PushNotificationsModule } from 'angular2-notifications';
import { EditProfileComponent_Personal } from './editprofilepersonal.component';
import { TimeInputComponent } from './time_input.component';
import { DashboardComponent } from './dashboard.component';
import { RequestService } from './request.service';
import { TradesmanSettingsComponent } from './tradesman_settings.component';
import { WindowRef } from './window.service';
import { TradesmanHistoryComponent } from './tradesman_history.component';
import { TradesmanNotificationsComponent } from './tradesman_notifications.component';
import { CompanyProfileComponent } from './company_profile.component';
import { JobSpecificFieldsComponent } from './job_specific_fields.component';
import { TradesmanEditProfileComponent } from './tradesman_edit_profile.component';
import { SelectJobComponent } from './select_job.component';
import { AddTradeComponent } from './add_trade.component';
import { SubscribeCompanyComponent } from './subscribe_company.component';
import { SubscribePersonalComponent } from './subscribe_personal.component';
import { LearnMoreTradesman } from './learn_more_tradesman.component';
import { LearnMoreHomeowner } from './learn_more_homeowner.component';
import { BestTradesmanComponent } from './best_tradesman.component';
import { LocationStrategy, HashLocationStrategy} from '@angular/common';
import { AboutUsComponent } from './about_us.component';
import { OurTeamComponent } from './our_team.component';
import { ContactUsComponent } from './contact_us.component';
import { PrivacyPolicyComponent } from './privacy_policy.component';
import { TermsOfUseComponent } from './terms_of_use.component';
import {ImageCropperComponent, CropperSettings, Bounds} from "ng2-img-cropper";
import { ViewCompanyComponent } from './view_company.component';

@NgModule({
  imports:      [ BrowserModule, routing, FormsModule, HttpModule, JsonpModule, PushNotificationsModule ],
  declarations: [ AppComponent, LoginComponent, SignUpComponent, HomeComponent,
                  TradesmanListComponent, SearchPipe, TradesmanComponent, AdsComponent,
                  UserHistoryComponent, LoginFloatBoxComponent, RequestFloatBoxComponent, ImageLoaderComponent,
                  EditProfileComponent, Ng2SliderComponent, Ng2StyledDirective, SlideAbleDirective,
                  EditProfileComponent_Personal, TimeInputComponent, DashboardComponent, TradesmanSettingsComponent,
                  TradesmanHistoryComponent, TradesmanNotificationsComponent, CompanyProfileComponent,
                  JobSpecificFieldsComponent, TradesmanEditProfileComponent, SelectJobComponent, AddTradeComponent,
                  SubscribeCompanyComponent, SubscribePersonalComponent, LearnMoreTradesman, LearnMoreHomeowner,
                  BestTradesmanComponent, AboutUsComponent, OurTeamComponent, ContactUsComponent, PrivacyPolicyComponent,
                  TermsOfUseComponent, ImageCropperComponent, ResetPasswordComponent, ViewCompanyComponent],
  providers: [ JsonFetchService, DataTransferService, RequestService, WindowRef, SubCategoriesService,
               {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap:    [ AppComponent, ]
})
export class AppModule { }
