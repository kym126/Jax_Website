import { Component, ElementRef, ViewChildren, QueryList} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import { Router} from '@angular/router';
import { DataTransferService } from './data_transfer.service';
import { RequestService } from './request.service';
import { Observable } from 'rxjs/Rx';
import { TimeInputComponent } from './time_input.component';
import { JsonFetchService } from './JsonFetch.service';

@Component({
  selector: 'dashboard',
  templateUrl: './app/HTML/dashboard.html',
  styleUrls: ['css/dashboard.css'],
})
export class DashboardComponent {
  history_sub: Subscription;
  dnd:boolean;
  overlay_id = "overlay_hidden";
  container_id = "container";
  pointer_choice = 0;
  pending_msg = "";
  name = "";
  req_length_sub: Subscription;
  req_length = 0;
  date =  new Date();
  path="M165,94 L165,14 A94,94 1 0,1 165,14 z";
  path2="M165,94 L165,147 A94,94 1 0,1 165,147 z";
  ave_rating: any;
  num_rating: any;

  months = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"];

  requests = [];
  timer: Observable<number>;
  histories = [];
  jobs_array = [];

  start_hour = "11";
  start_minute = "00";
  start_am_pm = "am";

  start_time = "";

  end_hour = "05";
  end_minute = "00";
  end_am_pm = "pm";

  end_time = "";

  old_start_hour = "11";
  old_start_minute = "00";
  old_start_am_pm = "am";


  old_end_hour = "05";
  old_end_minute = "00";
  old_end_am_pm = "pm";

  job_array = [];
  dnd_array = [];

  dnd_job = "";

  suffix = "th";

  setup_company_profile_id = "top_elements_btn_text_hidden";
  subscribe_company_id = "top_elements_btn_text_hidden";
  subscribe_profile_id = "top_elements_btn_text_hidden";

  @ViewChildren(TimeInputComponent) Time_Components: QueryList<TimeInputComponent>;

  constructor(private element: ElementRef, private datatransfer:DataTransferService, public router: Router, private request_service: RequestService, private jsonfetch: JsonFetchService) {
    if(!this.datatransfer.is_logged_in() || this.datatransfer.get_user_type() ==  "1"){
        this.router.navigate(['./home']);
    }
    this.name = this.datatransfer.get_user_first_name();
    this.calculate_circle(this.ave_rating);
    this.requests = this.request_service.get_requests();
    this.req_length_sub = this.request_service.req_length
       .subscribe(value => this.change_req_length(value));
    this.history_sub = this.datatransfer.history
      .subscribe(value => this.history_sub_fxn(value));
    this.histories = JSON.parse(this.datatransfer.get_tradesman_history());
    this.jobs_array = JSON.parse(this.datatransfer.get_tradesman_history());
    if(this.jobs_array == null){
      this.jobs_array = [];
    }
    for(var i = 0; this.jobs_array.length; i++){
      if(this.jobs_array[i].state == 1 || this.jobs_array[i].state == 3){
        this.jobs_array.splice(i, 1);
        i--;
      }
    }
    if(this.histories == null){
      this.histories = [];
    }

    var time = JSON.parse(this.datatransfer.get_tradesman_values_json()).jobs[0].default_time_availability;

    this.num_rating = this.datatransfer.get_tradesman_rating_num();
    this.ave_rating = this.datatransfer.get_tradesman_rating();
    this.start_time = time.start_hour + ":" + time.start_minute + " " + time.start_am_pm;
    this.end_time = time.end_hour + ":" + time.end_minute + " " + time.end_am_pm;

    var day = this.date.getDate();

    if(day < 10 || day > 20){
      day = day % 10;
      switch(day){
        case 1:
          this.suffix = "st";
          break;
        case 2:
          this.suffix = "nd";
          break;
        case 3:
          this.suffix = "rd";
          break;
        default:
          this.suffix = "th";
          break;
      }
    }
  }

  history_sub_fxn(value){
    this.histories = JSON.parse(this.datatransfer.get_tradesman_history());
    this.jobs_array = JSON.parse(this.datatransfer.get_tradesman_history());
    if(this.jobs_array == null){
      this.jobs_array = [];
    }
    for(var i = 0; this.jobs_array.length; i++){
      if(this.jobs_array[i].state == 1 || this.jobs_array[i].state == 3){
        this.jobs_array.splice(i, 1);
        i--;
      }
    }
  }

  valid_ratings(){
    if(this.datatransfer.get_tradesman_rating() != null && parseInt(this.datatransfer.get_tradesman_rating_num()) > 5){
      return true;
    }else{
      return false;
    }
  }

  ngAfterViewInit(){
    if(this.valid_ratings()){
      var canvas = this.element.nativeElement.querySelector("#can");
      var canvas2 = this.element.nativeElement.querySelector("#can2");
      var ctx = canvas.getContext("2d");
      var ctx2 = canvas2.getContext("2d");
      var lastend = 0;
      var other = 5.0 - this.ave_rating;
      var data = [this.ave_rating,other];
      var myTotal = 0;
      var myColor = ['#6abd45','transparent'];

      for(var e = 0; e < data.length; e++)
      {
        myTotal += data[e];
      }

      for (var i = 0; i < data.length; i++) {
        ctx.fillStyle = myColor[i];
        ctx.beginPath();
        ctx.moveTo(canvas.width/2,canvas.height/2);
        ctx.arc(canvas.width/2,canvas.height/2,canvas.height/2,lastend,lastend+(Math.PI*2*(data[i]/myTotal)),false);
        ctx.lineTo(canvas.width/2,canvas.height/2);
        ctx.fill();
        ctx2.fillStyle = myColor[i];
        ctx2.beginPath();
        ctx2.moveTo(canvas.width/2,canvas.height/2);
        ctx2.arc(canvas.width/2,canvas.height/2,canvas.height/2,lastend,lastend+(Math.PI*2*(data[i]/myTotal)),false);
        ctx2.lineTo(canvas.width/2,canvas.height/2);
        ctx2.fill();
        lastend += Math.PI*2*(data[i]/myTotal);
      }
    }

    var job_array = JSON.parse(this.datatransfer.get_job_json()).jobs;
    for(i = 0; i < job_array.length; i++){
      this.job_array.push(job_array[i].job);
      console.log(this.job_array);
    }

    if(!this.datatransfer.is_dnd()){
      this.dnd_array.push(false);
      this.datatransfer.set_dnd(this.dnd_array);
    }else{
      var dnd_array_2 = JSON.parse(this.datatransfer.get_dnd());
      for(var i = 0; i < dnd_array_2.length; i++){
        if(dnd_array_2[i] == true){
          this.dnd_array.push(true);
        }else{
          this.dnd_array.push(false);
        }
      }
      for(; i < this.job_array.length; i++){
        this.dnd_array.push(false);
      }
      this.datatransfer.set_dnd(JSON.stringify(this.dnd_array));
    }

    var time_text = this.element.nativeElement.querySelectorAll('#default_time');
    var icon_holder_1 = this.element.nativeElement.querySelector('#edit_time_icon_holder');
    var icon_holder_2 = this.element.nativeElement.querySelector('.default_time_icon_holder');

    for(var i = 0; i < this.dnd_array.length; i++){
      if(this.dnd_array[i]){
        break;
      }
    }

    if(i == this.dnd_array.length){
      for(var i = 0; i < time_text.length; i++){
        time_text[i].style.color = "#323230";
      }
      icon_holder_1.style.backgroundColor = "#313642";
      icon_holder_2.style.backgroundColor = "#313642";
    }else{
      for(var i = 0; i < time_text.length; i++){
        time_text[i].style.color = "#acacad";
      }
      icon_holder_1.style.backgroundColor = "#7a7e82";
      icon_holder_2.style.backgroundColor = "#7a7e82";
    }
  }

  change_req_length(value){
    this.req_length = value;
  }

  show_pending(){
    if(this.req_length == 0 && this.requests.length != 0){
      this.pending_msg = "No pending requests";
      return true;
    }else if(this.req_length == 0 && this.requests.length == 0){
      this.pending_msg = "";
      return true;
    }else{
      return false;
    }
  }

  calculate_circle(rating){
    var val = (2*3.1415 * (5-rating)/5) - 3.1415/2;
    var val_1 = 0;
    var val_2 = 0;
    var width = window.innerWidth;
    var x = width/2;
    var y = 15*100/width;
    var a = 16*100/width;
    var b = a + y;
    var c = -2*100/width;


    if(window.innerWidth < 800){
      if(val <= 3.1415/2){
        this.path2= "M"+ x +","+ y +" L"+ x +","+ b +" A"+ a +","+ a +" 1 0,1 "+ x +","+ y +" z";
        val_1 = x + a * Math.cos(val);
        val_2 = y + a * Math.sin(val);
        this.path = "M"+ x +","+ y +" L"+ x +","+ c +" A"+ a +","+ a +" 1 0,1 " + val_1 + "," + val_2 + " z";
      }else{
        this.path="M"+ x +","+ y +" L"+ x +","+ c +" A"+ a +","+ a +" 1 0,1 "+ x +","+ b +" z";
        val_1 = x + a * Math.cos(val);
        val_2 = y + a * Math.sin(val);
        this.path2 = "M"+ x +","+ y +" L"+ x +","+ c +" A"+ a +","+ a +" 1 0,1 " + val_1 + "," + val_2 + " z";
      }
    }else{
      if(val <= 3.1415/2){
        this.path2= "M165,94 L165,194 A100,100 1 0,1 165,194 z";
        val_1 = 165 + 100 * Math.cos(val);
        val_2 = 94 + 100 * Math.sin(val);
        this.path = "M165,94 L165,-9 A100,100 1 0,1 " + val_1 + "," + val_2 + " z";
      }else{
        this.path="M165,94 L165,-9 A100,100 1 0,1 165,194 z";
        val_1 = 165 + 100* Math.cos(val);
        val_2 = 94 + 100 * Math.sin(val);
        this.path2 = "M165,94 L165,194 A100,100 1 0,1 " + val_1 + "," + val_2 + " z";
      }
    }
  }

  request_rating(i){
    this.datatransfer.set_load(true);
    this.jsonfetch.request_rating(this.histories[i].id)
      .map(res => res)
          .subscribe(
              res => this.request_rating_2(res, i),
              error => this.error_happened(error)
          );
  }

  request_rating_2(res, i){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.histories[i].state = 1;
    }
    this.datatransfer.set_tradesman_history(JSON.stringify(this.histories));
    this.jobs_array = JSON.parse(this.datatransfer.get_tradesman_history());
    if(this.jobs_array == null){
      this.jobs_array = [];
    }
    for(var j = 0; this.jobs_array.length; j++){
      if(this.jobs_array[j].state == 1 || this.jobs_array[j].state == 3){
        this.jobs_array.splice(j, 1);
        j--;
      }
    }
  }

  I_m_turning_down_the_job(i){
    this.datatransfer.set_load(true);
    console.log("hello");
    this.jsonfetch.start_work(this.requests[i].id, 2)
      .map(res => res)
          .subscribe(
              res => this.I_m_turning_down_the_job_2(res, i),
              error => this.error_happened(error)
          );
  }

  I_m_turning_down_the_job_2(res, i){
    this.datatransfer.set_load(false);
    console.log(res);
    if(res.STATUS == "OK"){
      this.set_state(i, 3)
    }
  }

  turned_down_job(i){
    this.datatransfer.set_load(true);
    this.jsonfetch.history_status(this.histories[i].id, 2)
      .map(res => res)
          .subscribe(
              res => this.turned_down_job_2(res, i),
              error => this.error_happened(error)
          );
  }

  turned_down_job_2(res, i){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.histories.splice(i, 1);
    }
    this.datatransfer.set_tradesman_history(JSON.stringify(this.histories));
    this.jobs_array = JSON.parse(this.datatransfer.get_tradesman_history());
    if(this.jobs_array == null){
      this.jobs_array = [];
    }
    for(var j = 0; this.jobs_array.length; j++){
      if(this.jobs_array[j].state == 1 || this.jobs_array[j].state == 3){
        this.jobs_array.splice(j, 1);
        i--;
      }
    }
  }

  completed(i){
    this.datatransfer.set_load(true);
    this.jsonfetch.history_status(this.histories[i].id, 1)
      .map(res => res)
          .subscribe(
              res => this.completed_2(res, i),
              error => this.error_happened(error)
          );
  }

  completed_2(res, i){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.histories[i].state = 1;
    }
    this.datatransfer.set_tradesman_history(JSON.stringify(this.histories));
    this.jobs_array = JSON.parse(this.datatransfer.get_tradesman_history());
    if(this.jobs_array == null){
      this.jobs_array = [];
    }
    for(var j = 0; this.jobs_array.length; j++){
      if(this.jobs_array[j].state == 1 || this.jobs_array[j].state == 3){
        this.jobs_array.splice(j, 1);
        j--;
      }
    }
  }

  give_margin_bottom(i){
    if(i == 4){
      return {
          "margin-bottom": "0"
      }
    }else{
      return {
          "margin-bottom": "1px"
      }
    }
  }

  give_margin_bottom_1(i){
    if(i == this.requests.length - 1){
      return {
          "margin-bottom": "0"
      }
    }else{
      return {
          "margin-bottom": "3px"
      }
    }
  }

  set_height(i, j){
    if(this.requests[i].state == j){
      if(j == 1){
        if(window.innerWidth < 800){
          return{
            "max-height": "20vw"
          }
        }else{
          return{
            "max-height": "78px"
          }
        }
      }else{
        if(window.innerWidth < 800){
          return{
            "max-height": "24vw"
          }
        }else{
          return{
            "max-height": "105px"
          }
        }
      }
    }else{
        return{
          "max-height": "0"
        }
    }
  }

  set_state(i, j){
    this.request_service.set_state(i, j);
  }

  show_desc_btn(i){
    if((this.requests[i].state == 0 || this.requests[i].state == 5) && this.requests[i].description.length != 0){
      return true;
    }else{
      return false;
    }
  }

  delete_req(i){
    setTimeout(() => {
      this.request_service.delete_req(i);
    }, 100);
  }

  set_bg(i){
    return{
      "background-color": this.requests[i].bg_color
    }
  }

  add_to_history(i){
    this.datatransfer.set_load(true);
    this.jsonfetch.start_work(this.requests[i].id, 1)
      .map(res => res)
          .subscribe(
              res => this.add_to_history_2(res, i),
              error => this.error_happened(error)
          );
  }

  error_happened(res){
    this.datatransfer.set_load(false);
    console.log(res);
  }

  add_to_history_2(res, i){
    console.log(res);
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      var day;

      switch(this.date.getDate()){
        case 1:
          day = this.date.getDate() + "st";
          break;
        case 2:
          day = this.date.getDate() + "nd";
          break;
        case 3:
          day = this.date.getDate() + "rd";
          break;
        case 21:
          day = this.date.getDate() + "st";
          break;
        case 22:
          day = this.date.getDate() + "nd";
          break;
        case 23:
          day = this.date.getDate() + "rd";
          break;
        case 31:
          day = this.date.getDate() + "st";
          break;
        default:
          day = this.date.getDate() + "th";
          break;
      }

      var history = {
        "state": 0,
        "first_name": this.requests[i].first_name,
        "last_name": this.requests[i].last_name,
        "date": day + " of " + this.months[this.date.getMonth()]+ " " + this.date.getFullYear(),
        "job": this.requests[i].job,
        "h_id": this.requests[i].h_id,
        "id": this.requests[i].id
      }

      this.delete_req(i);
      this.histories.splice(0,0, history);
      this.datatransfer.set_tradesman_history(JSON.stringify(this.histories));
      this.jobs_array = JSON.parse(this.datatransfer.get_tradesman_history());
      if(this.jobs_array == null){
        this.jobs_array = [];
      }
      for(var j = 0; this.jobs_array.length; j++){
        if(this.jobs_array[j].state == 1 || this.jobs_array[j].state == 3){
          this.jobs_array.splice(j, 1);
          j--;
        }
      }
      this.request_service.drop_reqs();
    }
  }

  pop_up_close(){
    var popup = this.element.nativeElement.querySelector('#pop_overlay');

    popup.style.opacity = 0;
    setTimeout(() => {
      this.datatransfer.set_blur(false);
      this.container_id = "container";
      this.overlay_id = "overlay_hidden";

      var T_comp = this.Time_Components.toArray();

      T_comp[0].hour  = this.old_start_hour;
      T_comp[0].minute = this.old_start_minute;
      T_comp[0].am_pm = this.old_start_am_pm;

      T_comp[1].hour = this.old_end_hour;
      T_comp[1].minute = this.old_end_minute;
      T_comp[1].am_pm = this.old_end_am_pm;
    },350);
  }

  pop_up_close_go_edit_profile(){
    var popup = this.element.nativeElement.querySelector('#pop_overlay');
    this.datatransfer.edit_default_daily_time_availability();
    popup.style.opacity = 0;
    setTimeout(() => {
      this.datatransfer.set_blur(false);
      this.container_id = "container";
      this.overlay_id = "overlay_hidden";
      this.router.navigate(['./tradesman-edit-profile']);
    },350);
  }

  check_result(res, value, j){
    this.datatransfer.set_load(false);
    console.log(res);
    if(res.STATUS == "OK"){

      var dnd_toggle = this.element.nativeElement.querySelector('#dnd_toggle');
      var popup = this.element.nativeElement.querySelector('#pop_overlay');

      var time_text = this.element.nativeElement.querySelectorAll('#default_time');
      var icon_holder_1 = this.element.nativeElement.querySelector('#edit_time_icon_holder');
      var icon_holder_2 = this.element.nativeElement.querySelector('.default_time_icon_holder');


      this.dnd_array[j] = !this.dnd_array[j];
      this.datatransfer.set_dnd(JSON.stringify(this.dnd_array));
      this.dnd_job = this.job_array[j];

      for(var i = 0; i < this.dnd_array.length; i++){
        if(this.dnd_array[i]){
          break;
        }
      }

      if(i == this.dnd_array.length){
        for(var i = 0; i < time_text.length; i++){
          time_text[i].style.color = "#323230";
        }
        icon_holder_1.style.backgroundColor = "#313642";
        icon_holder_2.style.backgroundColor = "#313642";
      }else{
        for(var i = 0; i < time_text.length; i++){
          time_text[i].style.color = "#acacad";
        }
        icon_holder_1.style.backgroundColor = "#7a7e82";
        icon_holder_2.style.backgroundColor = "#7a7e82";
      }

      if(this.dnd_array[j] == false){
        var profile_json = JSON.parse(this.datatransfer.get_tradesman_values_json());
        var num_false = 0;
        for(var i = 0; i < profile_json.jobs.length; i++){
          if(profile_json.jobs[i].active == false){
            num_false++;
          }
        }

        if(i == num_false){
          for(var i = 0; i < profile_json.jobs.length; i++){
            profile_json.jobs[i].active = true;
          }
          this.datatransfer.save_tradesman_values_json(JSON.stringify(profile_json));
          value = 2;
        }
      }

      if(value == 1 && this.dnd_array[j] == true){
        setTimeout(() => {
          popup.style.opacity = 1;
        },100);
        this.datatransfer.set_blur(true);
        this.container_id = "container_blurred";
        this.overlay_id = "overlay_active";
      }
    }
  }

  popup(value, j){
    var dnd_toggle = this.element.nativeElement.querySelector('#dnd_toggle');
    var popup = this.element.nativeElement.querySelector('#pop_overlay');
    if(value == 1){
      this.datatransfer.set_load(true);
      var dnd = 0;
      if(!this.dnd_array[j]){
        dnd = 2;
      }else{
        dnd = 1;
      }
      this.jsonfetch.dnd_toggle(this.datatransfer.get_user_id(), JSON.parse(this.datatransfer.get_tradesman_values_json()).jobs[j].job ,dnd)
        .map(res => res)
            .subscribe(
                res => this.check_result(res, value, j),
                error => this.error_happened(error)
            );
    }

    this.pointer_choice = value;

    console.log(this.dnd_array.length)

    if(value != 1){
      for(var i = 0; i < this.dnd_array.length; i++){
        if(this.dnd_array[i]){
          break;
        }
      }
      if(i == this.dnd_array.length){
        setTimeout(() => {
          popup.style.opacity = 1;
        },100);
        this.datatransfer.set_blur(true);
        this.container_id = "container_blurred";
        this.overlay_id = "overlay_active";
      }
    }

    this.old_start_hour = this.start_hour;
    this.old_start_minute = this.start_minute;
    this.old_start_am_pm = this.start_am_pm;

    this.old_end_hour = this.end_hour;
    this.old_end_minute = this.end_minute;
    this.old_end_am_pm = this.end_am_pm;
  }

  stop_prop(event){
    event.stopPropagation();
  }

  remove_history_item(i){
    var r_id = [];
    r_id.push(this.histories[i].r_id);
    this.jsonfetch.clear_t_history(r_id).subscribe(
         res => this.clear_history_3(res, i),
         error =>  this.clear_history_err(error)
       );
  }

  clear_history_3(res, i){
    console.log(res);
    if(res.STATUS == "OK"){
      this.histories.splice(i,1);
      this.datatransfer.set_tradesman_history(JSON.stringify(this.histories));
      this.jobs_array = JSON.parse(this.datatransfer.get_tradesman_history());
      if(this.jobs_array == null){
        this.jobs_array = [];
      }
      for(var j = 0; this.jobs_array.length; j++){
        if(this.jobs_array[j].state == 1 || this.jobs_array[j].state == 3){
          this.jobs_array.splice(j, 1);
          j--;
        }
      }
    }
  }

  clear_history_err(error){
    console.log(error);
  }

  pop_up_save_time(){
    var T_comp = this.Time_Components.toArray();

    this.start_hour =  T_comp[0].hour;
    this.start_minute = T_comp[0].minute;
    this.start_am_pm = T_comp[0].am_pm;

    this.end_hour =  T_comp[1].hour;
    this.end_minute = T_comp[1].minute;
    this.end_am_pm = T_comp[1].am_pm;

    this.old_start_hour = this.start_hour;
    this.old_start_minute = this.start_minute;
    this.old_start_am_pm = this.start_am_pm;

    this.old_end_hour = this.end_hour;
    this.old_end_minute = this.end_minute;
    this.old_end_am_pm = this.end_am_pm;

    this.start_time = this.start_hour + ":" + this.start_minute + " " + this.start_am_pm;
    this.end_time = this.end_hour + ":" + this.end_minute + " " + this.end_am_pm;

    this.pop_up_close();
  }

  setup_company_profile_click(){
    if(this.setup_company_profile_id == 'top_elements_btn_text_active'){
      this.router.navigate(['/company-profile']);
    }else{
      this.setup_company_profile_id = 'top_elements_btn_text_active';
      this.subscribe_company_id = 'top_elements_btn_text_hidden';
      this.subscribe_profile_id = 'top_elements_btn_text_hidden';
    }
  }

  subscribe_company_click(){
    if(this.subscribe_company_id == 'top_elements_btn_text_active'){
      this.router.navigate(['/subscribe-company']);
    }else{
      this.setup_company_profile_id = 'top_elements_btn_text_hidden';
      this.subscribe_company_id = 'top_elements_btn_text_active';
      this.subscribe_profile_id = 'top_elements_btn_text_hidden';
    }
  }

  subscribe_profile_click(){
    if(this.subscribe_profile_id == 'top_elements_btn_text_active'){
      this.router.navigate(['/subscribe-personal']);
    }else{
      this.setup_company_profile_id = 'top_elements_btn_text_hidden';
      this.subscribe_company_id = 'top_elements_btn_text_hidden';
      this.subscribe_profile_id = 'top_elements_btn_text_active';
    }
  }
}
