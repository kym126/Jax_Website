import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class DataTransferService {

  private _login = new BehaviorSubject<boolean>(false);
  login = this._login.asObservable();
  private _blur = new BehaviorSubject<boolean>(false);
  blur = this._blur.asObservable();
  private _load = new BehaviorSubject<boolean>(false);
  load = this._load.asObservable();
  private _t_man_profile = new BehaviorSubject<boolean>(false);
  t_man_profile = this._t_man_profile.asObservable();
  private _ad_login = new BehaviorSubject<string>("");
  ad_login = this._ad_login.asObservable();
  private _ad_load = new BehaviorSubject<boolean>(false);
  ad_load = this._ad_load.asObservable();
  private _notif = new BehaviorSubject<boolean>(false);
  notif = this._notif.asObservable();
  private _history = new BehaviorSubject<boolean>(false);
  history = this._history.asObservable();
  array_id_delete = [];

  constructor(){
    if(localStorage.getItem("login") == null){
      localStorage.setItem("login", "false");
      this._login.next(false);
    }else if(localStorage.getItem("login") == "false"){
      this._login.next(false);
    }else if(localStorage.getItem("login") == "true"){
      this._login.next(true);
    }
  }

  set_inc_req_transfer(req){
    localStorage.setItem('inc_req_transfer', req);
  }

  get_inc_req_transfer(){
    if(localStorage.getItem('inc_req_transfer') != null){
      var data = localStorage.getItem('inc_req_transfer');
      localStorage.removeItem('inc_req_transfer');
      return data;
    }
  }

  set_data(type, first_name, last_name, image, company, availability, rating, location, sub_category, id, tradesman_cat){
    localStorage.setItem('view_profile_type', type);
    localStorage.setItem('first_name', first_name);
    localStorage.setItem('last_name', last_name);
    localStorage.setItem('image', image);
    localStorage.setItem('company', company);
    localStorage.setItem('availability', availability);
    localStorage.setItem('rating', rating);
    localStorage.setItem('location', location);
    localStorage.setItem('sub_category', sub_category);
    localStorage.setItem('id', id);
    localStorage.setItem('tradesman_cat', tradesman_cat);
  }

  get_tr_id(){
    return localStorage.getItem('id');
  }

  get_view_profile_type(){
    return localStorage.getItem('view_profile_type');
  }

  is_data_from_list(){
    if(localStorage.getItem('first_name') == null){
      return false;
    }else{
      return true;
    }
  }

  is_category(){
    if(localStorage.getItem('category') == null){
      return false;
    }else{
      return true;
    }
  }

  delete_data_local_storage(){
    localStorage.removeItem('view_profile_type');
    localStorage.removeItem('first_name');
    localStorage.removeItem('last_name');
    localStorage.removeItem('image');
    localStorage.removeItem('company');
    localStorage.removeItem('availability');
    localStorage.removeItem('rating');
    localStorage.removeItem('location');
    localStorage.removeItem('sub_category');
    localStorage.removeItem('id');
    localStorage.removeItem('tradesman_cat');
  }

  user_login(id, first, last, email, type, contact_no){
    localStorage.setItem("user_id", id);
    localStorage.setItem("user_first_name", first);
    localStorage.setItem("user_last_name", last);
    localStorage.setItem("user_email", email);
    localStorage.setItem("type", type);
    localStorage.setItem("contact_no", contact_no);
    localStorage.setItem("login", "true");
    this._login.next(true);
  }

  set_homeowner_profile_image(value){
    localStorage.setItem("homeowner_profile_image", value);
  }

  get_homeowner_profile_image(){
    return localStorage.getItem("homeowner_profile_image");
  }

  is_homeowner_profile_image(){
    if(localStorage.getItem("homeowner_profile_image") == null){
      return false;
    }else{
      return true;
    }
  }

  user_logout(){
    if(localStorage.getItem("type") == "2"){
      this.remove_extra_info();
    }else{
      localStorage.removeItem("sub_categories");
    }
    if(localStorage.getItem("category") != null){
      localStorage.removeItem("category");
    }
    localStorage.removeItem("user_id");
    localStorage.removeItem("user_first_name");
    localStorage.removeItem("user_last_name");
    localStorage.removeItem("user_email");
    localStorage.removeItem("type");
    localStorage.removeItem("contact_no");
    this.remove_gps();
    if(this.is_homeowner_profile_image()){
      localStorage.removeItem("homeowner_profile_image");
    }
    localStorage.setItem("login", "false");
    this.delete_data_local_storage();
    this._login.next(false);
  }

  remove_extra_info(){
    localStorage.removeItem("company_name");
    localStorage.removeItem("job");
    localStorage.removeItem('tradesman_profile');
    localStorage.removeItem("job_string");
    localStorage.removeItem("tradesman_history");
    localStorage.removeItem("tradesman_notifications");
    localStorage.removeItem("company_profile");
    localStorage.removeItem("office_number");
    localStorage.removeItem('tradesman_rating');
    localStorage.removeItem('tradesman_rating_num');
    localStorage.removeItem('inc_req_transfer');
    localStorage.removeItem("DND");
    localStorage.removeItem("requests");
    this.remove_edit_profile_cache_data(10);
    if(this.get_testimonial() != null){
      this.remove_testimonial();
    }
    this._t_man_profile.next(false);
  }

  set_SU_extra_info(c_name, job_json){
    localStorage.setItem("company_name", c_name);
    localStorage.setItem("job", job_json);
  }

  get_user_id(){
    return localStorage.getItem("user_id");
  }

  get_company_name(){
    return localStorage.getItem("company_name");
  }

  get_job_json(){
    return localStorage.getItem("job");
  }

  is_logged_in(){
    if(localStorage.getItem("login") == "true"){
      return true;
    }else{
      return false;
    }
  }

  get_user_contact(){
    return localStorage.getItem('contact_no');
  }

  get_user_type(){
    return localStorage.getItem('type');
  }

  get_user_first_name(){
    return localStorage.getItem('user_first_name');
  }

  get_user_last_name(){
    return localStorage.getItem('user_last_name');
  }

  get_user_email(){
    return localStorage.getItem('user_email');
  }

  clear_delete_id(){
   this.array_id_delete.length = 0;
  }

  add_id_to_delete(id){
   this.array_id_delete.push(id);
  }

  get_id_to_delete(){
   return this.array_id_delete;
  }

  set_category(category){
    localStorage.setItem('category', category);
  }

  set_gps(val){
    localStorage.setItem('gps', val);
  }

  get_gps(){
    return localStorage.getItem('gps');
  }

  remove_gps(){
    if(this.get_gps() != null){
      localStorage.removeItem('gps');
    }
  }

  get_first_name(){
    return localStorage.getItem('first_name');
  }

  get_index(){
    return localStorage.getItem('index');
  }

  get_last_name(){
    return localStorage.getItem('last_name');
  }

  get_image(){
    return localStorage.getItem('image');
  }

  get_company(){
    return localStorage.getItem('company');
  }

  get_availability(){
    return localStorage.getItem('availability');
  }

  get_rating(){
    return localStorage.getItem('rating');
  }

  set_tradesman_rating(value){
    localStorage.setItem('tradesman_rating', value);
  }

  get_tradesman_rating(){
    return localStorage.getItem('tradesman_rating');
  }

  set_tradesman_rating_num(value){
    localStorage.setItem('tradesman_rating_num', value);
  }

  get_tradesman_rating_num(){
    return localStorage.getItem('tradesman_rating_num');
  }

  get_location(){
    return localStorage.getItem('location');
  }

  get_sub_category(){
    return localStorage.getItem('sub_category');
  }

  get_tradesman_cat(){
    return localStorage.getItem('tradesman_cat');
  }

  get_category(){
    return localStorage.getItem('category');
  }

  set_blur(value){
    this._blur.next(value);
  }

  set_load(value){
    this._load.next(value);
  }

  save_tradesman_values_json(value){
    localStorage.setItem("tradesman_profile", value);
    this._t_man_profile.next(true);
  }

  get_tradesman_values_json(){
    return localStorage.getItem('tradesman_profile');
  }

  set_job_string(value){
    localStorage.setItem("job_string", value);
  }

  get_job_string(){
    return localStorage.getItem("job_string");
  }

  is_job_string(){
    if(localStorage.getItem("job_string") == null){
      return false;
    }else{
      return true;
    }
  }

  set_tradesman_history(value){
    localStorage.setItem("tradesman_history", value);
  }

  get_tradesman_history(){
    return localStorage.getItem("tradesman_history");
  }

  set_tradesman_notifications(value){
    localStorage.setItem("tradesman_notifications", value);
  }

  get_tradesman_notifications(){
    return localStorage.getItem("tradesman_notifications");
  }

  set_company_profile(value){
    localStorage.setItem("company_profile", value);
  }

  get_company_profile(){
    return localStorage.getItem("company_profile");
  }

  is_company_profile(){
    if(localStorage.getItem("company_profile") == null){
      return false;
    }else{
      return true;
    }
  }

  is_tradesman_profile(){
    if(localStorage.getItem("tradesman_profile") == null){
      return false;
    }else{
      return true;
    }
  }

  set_office_number(value){
    localStorage.setItem("office_number", value);
  }

  is_office_number(){
    if(localStorage.getItem("office_number") == null){
      return false;
    }else{
      return true;
    }
  }

  get_office_number(){
    return localStorage.getItem("office_number");
  }

  set_dnd(value){
    localStorage.setItem("DND", value);
  }

  is_dnd(){
    if(localStorage.getItem("DND") == null){
      return false;
    }else{
      return true;
    }
  }

  get_dnd(){
    return localStorage.getItem("DND");
  }

  register_as_tradesman(){
    localStorage.setItem("register_as_tradesman", "yes");
  }

  delete_register_as_tradesman(){
    localStorage.removeItem("register_as_tradesman");
  }

  is_register_as_tradesman(){
    if(localStorage.getItem("register_as_tradesman") == null){
      return false;
    }else{
      this.delete_register_as_tradesman();
      return true;
    }
  }

  set_ad_open_login(value){
    this._ad_login.next(value);
  }

  set_notif(){
    this._notif.next(true);
  }

  set_history_notif(){
    this._history.next(true);
  }

  set_ad_load(){
    this._ad_load.next(true);
  }

  set_sub_categories(value){
    localStorage.setItem("sub_categories", value);
  }

  get_sub_categories(){
    return localStorage.getItem("sub_categories");
  }

  set_service_time(value){
    localStorage.setItem("service_time", value);
  }

  get_service_time(){
    return localStorage.getItem("service_time");
  }

  remove_service_time(){
    localStorage.removeItem("service_time");
  }

  set_testimonial(value){
    localStorage.setItem("testimonial", value);
  }

  get_testimonial(){
    return localStorage.getItem("testimonial");
  }

  remove_testimonial(){
    localStorage.removeItem("testimonial");
  }

  is_service_time(){
    if(localStorage.getItem("service_time") == null){
      return false;
    }else{
      return true;
    }
  }

  edit_default_daily_time_availability(){
    localStorage.setItem("edit_default_daily_time_availability", "yes");
  }

  delete_edit_default_daily_time_availability(){
    localStorage.removeItem("edit_default_daily_time_availability");
  }

  is_edit_default_daily_time_availability(){
    if(localStorage.getItem("edit_default_daily_time_availability") == null){
      return false;
    }else{
      this.delete_edit_default_daily_time_availability();
      return true;
    }
  }

  remove_request_item(){
    localStorage.removeItem("request_item");
  }

  get_request_item(){
    return localStorage.getItem("request_item");
  }

  set_request_item(value){
    localStorage.setItem("request_item", value);
  }

  set_requests(value){
    localStorage.setItem("requests", value);
  }

  get_requests(){
    return localStorage.getItem("requests");
  }

  set_redir_forgot(){
    localStorage.setItem("redir_forgot", "true");
  }

  get_redir_forgot(){
    if(localStorage.getItem("redir_forgot") != null){
      localStorage.removeItem("redir_forgot");
      return true;
    }else{
      return false;
    }
  }

  set_new_tm_loc(val){
    localStorage.setItem("new_tm_loc", val);
  }

  get_new_tm_loc(){
    if(localStorage.getItem("new_tm_loc") != null){
      var data = localStorage.getItem("new_tm_loc");
      localStorage.removeItem("new_tm_loc");
      return data;
    }else{
      return null;
    }
  }

  set_edit_profile_cache_data(i, data){
    var name = "edit_profile_cache_data_" + i;
    localStorage.setItem(name, data);
  }

  get_edit_profile_cache_data(i){
    var name = "edit_profile_cache_data_" + i;
    return localStorage.getItem(name);
  }

  remove_edit_profile_cache_data(length){
    for(var i = 0; i < length; i++){
      var name = "edit_profile_cache_data_" + i;
      if(localStorage.getItem(name) != null){
        localStorage.removeItem(name);
      }
    }
  }

}
