import { Component, ElementRef, Input, trigger, state, style, transition, animate } from '@angular/core';
import { Router} from '@angular/router';
import { DataTransferService } from './data_transfer.service';

@Component({
  selector: 'login',
  templateUrl: './app/HTML/login.html',
  styleUrls: ['css/login.css'],
  /*host: {
     '[@routeAnimation]': "active",
     '[style.display]': "'block'",
     '[style.position]': "'absolute'",
     '[style.height]': "'100%'",
     '[style.width]': "'100%'"
   },
   animations: [
     trigger('routeAnimation', [
       state('*', style({transform: 'translateX(0)'})),
       transition('void => *', [
         style({transform: 'translateX(-100%)'}),
         animate(700)
       ]),
       transition('* => void', animate(700, style({transform: 'translateX(-100%)'})))
     ])
   ]*/
})

export class LoginComponent {

  public BoundFunction: Function;
  public SignupFunction: Function;

  constructor(private datatransfer:DataTransferService, private router:Router) {
    if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "2"){
      if(this.datatransfer.get_tradesman_values_json() == null){
        this.router.navigate(['./edit-profile']);
      }else{
        this.router.navigate(['./dashboard']);
      }
    }else if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "1"){
      this.router.navigate(['./home']);
    }else{
      this.BoundFunction= this.correct.bind(this);
      this.SignupFunction= this.signup.bind(this);
    }
  }

  correct(){
    if(this.datatransfer.get_user_type() == "1"){
      this.router.navigate(['./home']);
    }else{
      this.router.navigate(['./dashboard']);
    }

    return;
  }

  signup(){
    this.router.navigate(['./sign-up']);
  }

}
