import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';

@Component({
  selector: 'subscribe-company',
  templateUrl: './app/HTML/subscribe_company.html',
  styleUrls: ['css/subscribe_company.css'],
})

export class SubscribeCompanyComponent {
  lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tellus eros, sollicitudin ac fringilla id, interdum dictum augue. Quisque ornare est eget pellentesque ornare. Cras tempor sem ut tortor sodales, a sodales sem pretium.";
  constructor(public router: Router, private datatransfer:DataTransferService){
    if(!this.datatransfer.is_logged_in() || this.datatransfer.get_user_type() ==  "1"){
        this.router.navigate(['./home']);
    }
  }
}
