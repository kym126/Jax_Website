"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var data_transfer_service_1 = require("./data_transfer.service");
var window_service_1 = require("./window.service");
var subcategories_service_1 = require("./subcategories.service");
var request_float_box_component_1 = require("./request_float_box.component");
var JsonFetch_service_1 = require("./JsonFetch.service");
var Rx_1 = require("rxjs/Rx");
var HomeComponent = (function () {
    function HomeComponent(element, router, datatransfer, win, sub_categories, jsonfetch) {
        var _this = this;
        this.element = element;
        this.router = router;
        this.datatransfer = datatransfer;
        this.win = win;
        this.sub_categories = sub_categories;
        this.jsonfetch = jsonfetch;
        this.show_other_widgets = false;
        this.jobs = ["PLUMBER", "ELECTRICIAN", "GARDENER", "PAINTER", "CARPENTER", "CLEANERS", "MOVERS", "FLOORING", "ROOFING", "HANDYMAN"];
        this.bg_images = ["plumber.jpg", "electrician.jpg", "gardener.jpg", "painter.jpg", "carpenter.jpg", "cleaner.jpg", "movers.jpg", "flooring.jpg", "roofing.jpg", "general.jpg"];
        this.tradesmen_container_id = "tradesmen_container";
        this.sub_categories_id = "sub_categories";
        this.overlay_id = "overlay";
        this.main_category = 0;
        this.sub_cat_array = [];
        this.cat_array = [];
        this.gps = false;
        this.lon = 0;
        this.lat = 0;
        this.overlay_title = "";
        this.filter_1_id = "filter_1";
        this.filter_2_id = "filter_2_hide";
        this.chosen_tradesman = 0;
        this.chosen_company = false;
        this.opening_msg = "JAX OF TRADES HAS A DECK FULL OF THE BEST LOCAL TRADESMEN IN THE GTA";
        this.homeowner_msg = "Reputable, local, and available to serve you, Jax of Trades puts you in contact with the very best tradesmen in the GTA. From plumbers to gardeners, you can find the right tradesman for the job on Jax of Trades!";
        this.tradesman_msg = "Promoting your business just got easier with Jax of Trades. Homeowners and commercial property owners are searching for local professionals – just like you – to fix, build, maintain, and beautify their properties. Make sure your business gets noticed by showcasing your skills on Jax of Trades!";
        this.homeowner_title = "Find the very best local tradesmen in the GTA";
        this.tradesman_title = "People are looking for pros like you";
        this.homeowner_see_more = "See more";
        this.tradesman_see_more = "See more";
        this.t_id = 0;
        this.images = [
            "./assets/images/landing_page_images/cover_1.jpg",
            "./assets/images/landing_page_images/cover_2.jpg",
            "./assets/images/landing_page_images/cover_3.jpg",
            "./assets/images/landing_page_images/cover_4.jpg"
        ];
        this.best_tradesmen = []; /*[
          {
            "id": 0,
            "name": "Oliver Queen",
            "first_name": "Oliver",
            "last_name": "Queen",
            "sub_category": "Home/Commercial",
            "company": "Jax of Trades",
            "image": "./assets/images/icons/avatar/oliver_queen.jpg",
            "rating": 4.8,
            "location": 2,
            "availability": "Available from 6am to 5pm",
            "job": "Plumber"
          },
          {
            "id": 0,
            "name": "Harrison Wells",
            "first_name": "Harrison",
            "last_name": "Wells",
            "sub_category": "Home/Commercial",
            "company": "Jax of Trades",
            "image": "./assets/images/icons/avatar/harrison_wells.png",
            "rating": 4.8,
            "location": 2,
            "availability": "Available from 6am to 5pm",
            "job": "Electrician"
          },
          {
            "id": 0,
            "name": "John Diggle",
            "first_name": "John",
            "last_name": "Diggle",
            "sub_category": "Home/Commercial",
            "company": "",
            "image": "./assets/images/icons/avatar/john_diggle.png",
            "rating": 4.8,
            "location": 2,
            "availability": "Available from 6am to 5pm",
            "job": "Electrician"
          },
          {
            "id": 0,
            "name": "Felicity Smoak",
            "first_name": "Felicity",
            "last_name": "Smoak",
            "sub_category": "Home/Commercial",
            "company": "Jax of Trades",
            "image": "./assets/images/icons/avatar/felicity_smoak.jpg",
            "rating": 4.7,
            "location": 3,
            "availability": "Available from 6am to 5pm",
            "job": "Carpenter"
          },
          {
            "id": 0,
            "name": "Barry Allen",
            "first_name": "Barry",
            "last_name": "Allen",
            "sub_category": "Home/Commercial",
            "company": "Jax of Trades",
            "image": "./assets/images/icons/avatar/barry_allen.png",
            "rating": 4.9,
            "location": 2,
            "availability": "Available from 6am to 5pm",
            "job": "Painter"
          },
          {
            "id": 0,
            "name": "Joe West",
            "first_name": "Joe",
            "last_name": "West",
            "sub_category": "Home/Commercial",
            "company": "",
            "image": "./assets/images/icons/avatar/joe_west.png",
            "rating": 4.8,
            "location": 2,
            "availability": "Available from 6am to 5pm",
            "job": "Mover"
          }
        ];*/
        this.companies = [
            {
                "state": 0,
                "image": "./assets/images/icons/company_logos/north_end_plumbers.png",
                "name": "North End Plumbers",
                "bg": "./assets/images/ads_bg/WoodenTexture-1.jpg",
                "street_address_1": "234 Bayview Avenue",
                "street_address_2": "Toronto, ON, M3H 5J4",
                "address_1": "234 Bayview Avenue",
                "address_2": "Toronto, ON",
                "address_3": "M3H 5J4",
                "contact": "+1(312)351-3865"
            },
            {
                "state": 0,
                "image": "./assets/images/icons/company_logos/d&g_plumbing.png",
                "name": "D&G Plumbing",
                "bg": "./assets/images/ads_bg/PlumbingBCk.jpg",
                "street_address_1": "899 Danforth Rd",
                "street_address_2": "Toronto, ON, M1G 1A5",
                "address_1": "899 Danforth Rd",
                "address_2": "Toronto, ON",
                "address_3": "M1G 1A5",
                "contact": "+1(345)765-3212"
            },
            {
                "state": 0,
                "image": "./assets/images/icons/company_logos/john's_moving.png",
                "name": "John's Moving",
                "bg": "./assets/images/ads_bg/PlumbingBCk.jpg",
                "street_address_1": "899 Danforth Rd",
                "street_address_2": "Toronto, ON, M1G 1A5",
                "address_1": "899 Danforth Rd",
                "address_2": "Toronto, ON",
                "address_3": "M1G 1A5",
                "contact": "+1(345)765-3212"
            },
            {
                "state": 0,
                "image": "./assets/images/icons/company_logos/maple_leaf_plumbing.png",
                "name": "Maple Leaf Plumbing",
                "bg": "./assets/images/ads_bg/WoodenTexture-1.jpg",
                "street_address_1": "234 Bayview Avenue",
                "street_address_2": "Toronto, ON, M3H 5J4",
                "address_1": "234 Bayview Avenue",
                "address_2": "Toronto, ON",
                "address_3": "M3H 5J4",
                "contact": "+1(312)351-3865"
            }
        ];
        this.testimonial_text = "Sed et velit ullamcorper, finibus augue ullamcorper, volutpat neque.";
        this.home_owner_testimonials = []; /*= [
          {
            "image": "./assets/images/icons/avatar/selina_kyle.png",
            "name": "Selina Kyle",
            "message": this.testimonial_text
          },
          {
            "image": "./assets/images/icons/avatar/john_blake.png",
            "name": "John Blake",
            "message": this.testimonial_text
          },
          {
            "image": "./assets/images/icons/avatar/miranda_tate.png",
            "name": "Miranda Tate",
            "message": this.testimonial_text
          },
          {
            "image": "./assets/images/icons/avatar/rachel_dawes.png",
            "name": "Rachel Dawes",
            "message": this.testimonial_text
          }
        ];*/
        this.tradesman_testimonials = []; /*= [
          {
            "image": "./assets/images/icons/avatar/barry_no_border.png",
            "name": "Barry Allen",
            "message": this.testimonial_text
          },
          {
            "image": "./assets/images/icons/avatar/harrison_no_border.png",
            "name": "Harrison Wells",
            "message": this.testimonial_text
          },
          {
            "image": "./assets/images/icons/avatar/catlin_snow.png",
            "name": "Caitlin Snow",
            "message": this.testimonial_text
          },
          {
            "image": "./assets/images/icons/avatar/iriswest.png",
            "name": "Iris West",
            "message": this.testimonial_text
          }
        ];*/
        this.time_error = "";
        this.fade_img = "";
        this.stick_img = "";
        this.fade_img_index = 0;
        if (this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() == "2") {
            if (this.datatransfer.get_tradesman_values_json() == null) {
                this.router.navigate(['./edit-profile']);
            }
            else {
                this.router.navigate(['./dashboard']);
            }
        }
        this.window = win.nativeWindow;
        this.HideFunction = this.correct.bind(this);
        this.SignupFunction = this.signup.bind(this);
        this.window.window.scrollTo(0, 0);
        this.datatransfer.set_blur(false);
        this.jsonfetch.get_testimonials().map(function (res) { return res; })
            .subscribe(function (res) { return _this.check_result(res); }, function (error) { return _this.error_happened(error); });
        /*this.jsonfetch.get_testimonials_2().map(res => res)
            .subscribe(
                res => this.check_result_2(res),
                error => this.error_happened(error)
            );*/
        var timeoutVal = 10 * 1000 * 1000;
        navigator.geolocation.getCurrentPosition(function (position) { return _this.gps_found_2(position); }, function (error) { return _this.gps_error_2(error); }, { enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 });
        this.fade_img = this.images[this.fade_img_index];
        this.stick_img = this.images[this.fade_img_index];
        this.timer = Rx_1.Observable.timer(0, 10);
        this.subscription = this.timer.subscribe(function (t) { return _this.change_fade_img(t); });
    }
    HomeComponent.prototype.change_fade_img = function (t) {
        var current_value = (t / 10) % 80;
        var box = this.element.nativeElement.querySelector('#faded_img');
        var box2 = this.element.nativeElement.querySelector('#sticky_img');
        if (current_value == 0) {
            box.style.opacity = 1;
        }
        else if (current_value == 8) {
            box2.style.opacity = 0;
        }
        else if (current_value == 57) {
            box2.style.opacity = 1;
            if (this.fade_img_index == this.images.length - 1) {
                this.stick_img = this.images[0];
            }
            else {
                this.stick_img = this.images[this.fade_img_index + 1];
            }
        }
        else if (current_value == 60) {
            box.style.opacity = 0;
        }
        else if (current_value == 65) {
            if (this.fade_img_index == this.images.length - 1) {
                this.fade_img_index = 0;
                this.fade_img = this.images[0];
            }
            else {
                this.fade_img_index++;
                this.fade_img = this.images[this.fade_img_index];
            }
        }
    };
    HomeComponent.prototype.gps_found_2 = function (position) {
        var _this = this;
        this.jsonfetch.best_tradesman_list_home(position.coords.longitude, position.coords.latitude).map(function (res) { return res; })
            .subscribe(function (res) { return _this.best_tradesman_fetch(res); }, function (error) { return _this.error_happened(error); });
    };
    HomeComponent.prototype.gps_error_2 = function (error) {
        var _this = this;
        this.jsonfetch.best_tradesman_list().map(function (res) { return res; })
            .subscribe(function (res) { return _this.best_tradesman_fetch(res); }, function (error) { return _this.error_happened(error); });
    };
    HomeComponent.prototype.best_tradesman_fetch = function (res) {
        console.log(res);
        if (res.STATUS == "OK") {
            for (var i = 0; i < res.DATA.length; i++) {
                var sub_category = "";
                var location = null;
                var company = null;
                var rating = null;
                if (res.DATA[i].rating != null) {
                    rating = parseFloat(res.DATA[i].rating);
                }
                if (res.DATA[i].company_name != null) {
                    company = res.DATA[i].company_name;
                }
                if (res.DATA[i].location != null) {
                    location = res.DATA[i].location;
                }
                for (var y = 0; y < res.DATA[i].job_sub_type.length; y++) {
                    if (y != 0) {
                        sub_category += " | ";
                    }
                    sub_category += res.DATA[i].job_sub_type[y];
                }
                var tradesman = {
                    "id": res.DATA[i].tradesmen_id,
                    "name": res.DATA[i].tr_first_name + " " + res.DATA[i].tr_last_name,
                    "first_name": res.DATA[i].tr_first_name,
                    "last_name": res.DATA[i].tr_last_name,
                    "company": company,
                    "image": res.DATA[i].profile_imgpath,
                    "sub_category": sub_category,
                    "rating": rating,
                    "location": location,
                    "availability": "",
                    "job": res.DATA[i].job_type
                };
                this.best_tradesmen.push(tradesman);
            }
        }
    };
    HomeComponent.prototype.getRandomInt = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    HomeComponent.prototype.check_result = function (res) {
        var chosen_indeces = [];
        var index = 0;
        for (var x = 0; x < res.DATA.length && x < 4; x++) {
            do {
                index = this.getRandomInt(0, res.DATA.length - 1);
            } while (chosen_indeces.includes(index));
            chosen_indeces.push(index);
            var img = 'http://jaxoftrades.com/api/' + res.DATA[index].profile_imgpath;
            if (res.DATA[index].profile_imgpath.length == 0) {
                var img = "./assets/images/icons/default_profile_pic.png";
            }
            var testi = {
                "image": img,
                "name": res.DATA[index].first_name + " " + res.DATA[index].last_name,
                "message": res.DATA[index].t_text
            };
            this.home_owner_testimonials.push(testi);
        }
    };
    HomeComponent.prototype.check_result_2 = function (res) {
        var chosen_indeces = [];
        var index = 0;
        for (var x = 0; x < res.DATA.length && x < 4; x++) {
            do {
                index = this.getRandomInt(0, res.DATA.length - 2);
            } while (chosen_indeces.includes(index));
            chosen_indeces.push(index);
            var img = 'http://jaxoftrades.com/api/' + res.DATA[index].profile_imgpath;
            if (res.DATA[index].profile_imgpath.length == 0) {
                var img = "./assets/images/icons/default_profile_pic.png";
            }
            var testi = {
                "image": img,
                "name": res.DATA[index].first_name + " " + res.DATA[x].last_name,
                "message": res.DATA[index].t_text
            };
            this.tradesman_testimonials.push(testi);
        }
    };
    HomeComponent.prototype.error_happened = function (res) {
        console.log(res);
    };
    HomeComponent.prototype.ngAfterViewInit = function () {
        var box = this.element.nativeElement.querySelector('#fixed_box');
        box.style.top = "80%";
    };
    HomeComponent.prototype.toggle_widget = function () {
        var y = currentYPosition();
        if (y > 0) {
            return {
                "right": "-370px"
            };
        }
        else if (y == 0) {
            return {
                "right": "0px"
            };
        }
    };
    /*top_image_set_scroll(){
      var y = currentYPosition();
      if( 0 <= y && 200 >= y){
        return{
          "position": "fixed",
          "top": "0"
        }
      }else{
        return{
          "position": "relative",
          "top": "202"
        }
      }
    }*/
    HomeComponent.prototype.change_fixed_box = function (event) {
        var height = window.innerHeight;
        var width = window.innerWidth;
        var offset = .8 * (height) * (100 / width);
        if (width < 800) {
            offset = .7 * (height) * (100 / width);
        }
        var y = currentYPosition() * (100 / width);
        var curr_pos = offset - y;
        var box = this.element.nativeElement.querySelector('#fixed_box');
        var box2 = this.element.nativeElement.querySelector('#top_image');
        var max = offset - .3 * (height) * (100 / width);
        var max_2 = offset - max;
        if (y < 0) {
            box2.style.position = "fixed";
            box2.style.top = "0";
        }
        else if (0 <= y && .3 * (height) * (100 / width) >= y) {
            box.style.top = curr_pos + "vw";
            box2.style.position = "fixed";
            box2.style.top = "0";
        }
        else {
            box.style.top = max + "vw";
            box2.style.position = "absolute";
            box2.style.top = max_2 + "vw";
        }
    };
    HomeComponent.prototype.set_scroll_opacity = function () {
        var height = window.innerHeight;
        var width = window.innerWidth;
        var y = currentYPosition() * (100 / width);
        var opacity = 1 - y / (.3 * (height) * (100 / width));
        if (y < 0) {
            return {
                "opacity": 1
            };
        }
        else if (0 <= y && .3 * (height) * (100 / width) >= y) {
            return {
                "opacity": opacity
            };
        }
        else {
            return {
                "opacity": 0
            };
        }
    };
    HomeComponent.prototype.background_selector = function (index) {
        return {
            "background-image": "url('/assets/images/category_images/" + this.bg_images[index] + "')"
        };
    };
    HomeComponent.prototype.sub_category_pop = function (category) {
        var box = this.element.nativeElement.querySelector('#sub_categories');
        var box_2 = this.element.nativeElement.querySelector('#login');
        if (this.main_category != category) {
            this.sub_cat_array.length = 0;
            for (var i = 0; i < this.sub_categories.get_row_length(this.main_category); i++) {
                var check_box_id = "#check_box" + i;
                var check_box = this.element.nativeElement.querySelector(check_box_id);
                check_box.src = "./assets/images/uncheck_box.png";
            }
        }
        this.main_category = category;
        this.tradesmen_container_id = "tradesmen_container_blurred";
        box.style.display = "block";
        box.style.opacity = "0";
        box_2.style.display = "none";
        this.datatransfer.set_blur(true);
        this.overlay_id = "overlay_active";
        setTimeout(function () {
            box.style.opacity = "1";
        }, 50);
        this.overlay_title = "Select subcategory";
    };
    HomeComponent.prototype.close_sub_category = function () {
        var _this = this;
        setTimeout(function () {
            _this.datatransfer.set_blur(false);
        }, 350);
        this.close();
    };
    HomeComponent.prototype.close = function () {
        var _this = this;
        var box = this.element.nativeElement.querySelector('#sub_categories');
        var box_2 = this.element.nativeElement.querySelector('#login');
        box.style.opacity = "0";
        box_2.style.opacity = "0";
        box_2.style.display = "block";
        setTimeout(function () {
            _this.tradesmen_container_id = "tradesmen_container";
            _this.overlay_id = "overlay";
            box.style.display = "none";
        }, 350);
        this.gps = false;
        this.lon = 0;
        this.lat = 0;
        setTimeout(function () {
            _this.filter_1_id = "filter_1";
            _this.filter_2_id = "filter_2_hide";
            _this.overlay_title = "Select subcategory";
            setTimeout(function () {
                var box1 = _this.element.nativeElement.querySelector("#" + _this.filter_1_id);
                box1.style.opacity = 1;
                var box2 = _this.element.nativeElement.querySelector("#" + _this.filter_2_id);
                box2.style.opacity = 0;
            }, 50);
        }, 350);
    };
    HomeComponent.prototype.toggle_checkbox = function (index) {
        var check_box_id = "#check_box" + index;
        var check_box = this.element.nativeElement.querySelector(check_box_id);
        for (var i = 0; i < this.sub_cat_array.length; i++) {
            if (this.sub_cat_array[i] == index) {
                check_box.src = "./assets/images/uncheck_box.png";
                this.sub_cat_array.splice(i, 1);
                return;
            }
        }
        check_box.src = "./assets/images/check_box_v2.png";
        this.sub_cat_array.push(index);
    };
    HomeComponent.prototype.search = function () {
        var _this = this;
        var box1 = this.element.nativeElement.querySelector("#filter_1");
        var date = new Date();
        this.cat_array.length = 0;
        this.overlay_title = "Select service hours";
        box1.style.opacity = 0;
        this.start_hour = date.getHours();
        this.start_minute = date.getMinutes();
        if (this.start_minute < 10) {
            this.start_minute = "0" + this.start_minute.toString();
        }
        this.start_am_pm = "am";
        this.end_hour = date.getHours() + 5;
        this.end_minute = date.getMinutes();
        if (this.end_minute < 10) {
            this.end_minute = "0" + this.end_minute.toString();
        }
        this.end_am_pm = "am";
        if (this.start_hour > 11) {
            if (this.start_hour != 12) {
                this.start_hour = this.start_hour - 12;
            }
            this.start_am_pm = "pm";
        }
        if (this.end_hour > 23) {
            this.end_hour = this.end_hour - 24;
            this.end_am_pm = "am";
        }
        else if (this.end_hour > 11) {
            if (this.end_hour != 12) {
                this.end_hour = this.end_hour - 12;
            }
            this.end_am_pm = "pm";
        }
        this.start_hour = this.start_hour.toString();
        this.start_minute = this.start_minute.toString();
        this.end_hour = this.end_hour.toString();
        this.end_minute = this.end_minute.toString();
        setTimeout(function () {
            _this.filter_1_id = "filter_1_hide";
            _this.filter_2_id = "filter_2";
            setTimeout(function () {
                var box2 = _this.element.nativeElement.querySelector("#" + _this.filter_2_id);
                box2.style.opacity = 1;
            }, 50);
        }, 350);
    };
    HomeComponent.prototype.back = function () {
        var _this = this;
        var box2 = this.element.nativeElement.querySelector("#" + this.filter_2_id);
        var box1 = this.element.nativeElement.querySelector("#" + this.filter_1_id);
        this.overlay_title = "Select subcategory";
        box2.style.opacity = 0;
        setTimeout(function () {
            _this.filter_2_id = "filter_2_hide";
            _this.filter_1_id = "filter_1";
            box1.style.opacity = 0;
            setTimeout(function () {
                box1.style.opacity = 1;
            }, 50);
        }, 350);
    };
    HomeComponent.prototype.check_hour = function (val) {
        switch (val) {
            case "0":
            case "00":
            case "1":
            case "01":
            case "2":
            case "02":
            case "3":
            case "03":
            case "4":
            case "04":
            case "5":
            case "05":
            case "6":
            case "06":
            case "7":
            case "07":
            case "8":
            case "08":
            case "9":
            case "09":
            case "10":
            case "11":
            case "12":
                return true;
            default: return false;
        }
    };
    HomeComponent.prototype.check_minute = function (val) {
        switch (val) {
            case "0":
            case "00":
            case "1":
            case "01":
            case "2":
            case "02":
            case "3":
            case "03":
            case "4":
            case "04":
            case "5":
            case "05":
            case "6":
            case "06":
            case "7":
            case "07":
            case "8":
            case "08":
            case "9":
            case "09":
                return true;
            default:
                if (parseInt(val) != null && parseInt(val) < 61) {
                    return true;
                }
                else {
                    return false;
                }
        }
    };
    HomeComponent.prototype.service_time = function () {
        var _this = this;
        var box = this.element.nativeElement.querySelector('#time_error');
        box.style.opacity = 0;
        if (!(this.check_hour(this.start_hour) && this.check_hour(this.end_hour) && this.check_minute(this.start_minute) && this.check_minute(this.end_minute))) {
            box.style.opacity = 1;
            return;
        }
        this.time = true;
        var timeoutVal = 10 * 1000 * 1000;
        navigator.geolocation.getCurrentPosition(function (position) { return _this.gps_found(position); }, function (error) { return _this.gps_error(error); }, { enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 });
        this.open_load();
    };
    HomeComponent.prototype.no_service_time = function () {
        var _this = this;
        this.time = false;
        var timeoutVal = 10 * 1000 * 1000;
        this.datatransfer.remove_gps();
        navigator.geolocation.getCurrentPosition(function (position) { return _this.gps_found(position); }, function (error) { return _this.gps_error(error); }, { enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 });
        this.open_load();
    };
    HomeComponent.prototype.open_load = function () {
        var _this = this;
        this.close();
        setTimeout(function () {
            _this.datatransfer.set_load(true);
        }, 350);
    };
    HomeComponent.prototype.gps_found = function (position) {
        this.gps = true;
        this.lon = position.coords.longitude;
        this.lat = position.coords.latitude;
        var gps = {
            "lon": this.lon,
            "lat": this.lat,
            "postal_code": null
        };
        this.datatransfer.set_gps(JSON.stringify(gps));
        this.handle_sub_cat();
    };
    HomeComponent.prototype.gps_error = function (error) {
        this.gps = false;
        this.handle_sub_cat();
    };
    HomeComponent.prototype.handle_sub_cat = function () {
        for (var i = 0; i < this.sub_cat_array.length; i++) {
            this.cat_array.push(this.sub_categories.get_sub_cat(this.main_category, this.sub_cat_array[i]));
        }
        this.call_backend();
    };
    HomeComponent.prototype.call_backend = function () {
        var _this = this;
        this.datatransfer.clear_delete_id();
        var job_category = this.jobs[this.main_category][0].toUpperCase() + this.jobs[this.main_category].toLowerCase().slice(1);
        this.datatransfer.set_category(job_category);
        this.datatransfer.set_sub_categories(JSON.stringify(this.cat_array));
        this.datatransfer.delete_data_local_storage();
        this.datatransfer.remove_service_time();
        if (this.time) {
            var time = {
                "start_hour": this.start_hour,
                "start_minute": this.start_minute,
                "start_am_pm": this.start_am_pm,
                "end_hour": this.end_hour,
                "end_minute": this.end_minute,
                "end_am_pm": this.end_am_pm,
            };
            this.datatransfer.set_service_time(JSON.stringify(time));
        }
        setTimeout(function () {
            _this.router.navigate(['./tradesmanlist']);
        }, 350);
    };
    HomeComponent.prototype.go_to_tradesman_profile = function (i, val) {
        var _this = this;
        this.chosen_tradesman = i;
        this.chosen_company = val;
        if (this.datatransfer.is_logged_in()) {
            this.datatransfer.delete_data_local_storage();
            if (val) {
                this.datatransfer.set_data(1, this.companies[i].name, this.companies[i].contact, this.companies[i].image, "", this.companies[i].address_1, "", "", this.companies[i].address_3, 0, this.companies[i].address_2);
            }
            else {
                this.datatransfer.set_data(0, this.best_tradesmen[i].first_name, this.best_tradesmen[i].last_name, this.best_tradesmen[i].image, this.best_tradesmen[i].company, this.best_tradesmen[i].availability, this.best_tradesmen[i].rating, this.best_tradesmen[i].location, this.best_tradesmen[i].sub_category, this.best_tradesmen[i].id, this.best_tradesmen[i].job);
                var job_category = this.best_tradesmen[i].job[0].toUpperCase() + this.best_tradesmen[i].job.toLowerCase().slice(1);
                this.datatransfer.clear_delete_id();
                this.cat_array.length = 0;
                this.datatransfer.set_sub_categories(JSON.stringify(this.cat_array));
                this.datatransfer.set_category(job_category);
                this.datatransfer.remove_service_time();
                this.t_id = this.best_tradesmen[i].id;
            }
            this.router.navigate(['./tradesman']);
        }
        else {
            var box = this.element.nativeElement.querySelector('#login');
            this.tradesmen_container_id = "tradesmen_container_blurred";
            this.datatransfer.set_blur(true);
            this.overlay_id = "overlay_active";
            box.style.opacity = "0";
            setTimeout(function () {
                box.style.opacity = "1";
                _this.request.request_pop();
            }, 10);
        }
    };
    HomeComponent.prototype.stop_propagation = function (event) {
        event.stopPropagation();
    };
    HomeComponent.prototype.correct = function (event) {
        this.go_to_tradesman_profile(this.chosen_tradesman, this.chosen_company);
    };
    HomeComponent.prototype.give_border = function (i) {
        if (i == 0) {
            return {
                "border-left": "none"
            };
        }
        else {
            return {
                "border-left": "1px solid #e0dede"
            };
        }
    };
    HomeComponent.prototype.smoothScroll = function () {
        var startY = currentYPosition();
        var stopY = .3 * (window.innerHeight);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            this.window.window.scrollTo(0, stopY);
            return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20)
            speed = 20;
        var step = Math.round(distance / 100);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for (var i = startY; i < stopY; i += step) {
                this.scrollTo(leapY, timer * speed);
                leapY += step;
                if (leapY > stopY)
                    leapY = stopY;
                timer++;
            }
            return;
        }
        for (var i = startY; i > stopY; i -= step) {
            this.scrollTo(leapY, timer * speed);
            leapY -= step;
            if (leapY < stopY)
                leapY = stopY;
            timer++;
        }
    };
    HomeComponent.prototype.scrollTo = function (yPoint, duration) {
        var _this = this;
        setTimeout(function () {
            _this.window.window.scrollTo(0, yPoint);
        }, duration);
        return;
    };
    HomeComponent.prototype.toggle_drop_down_start = function (event) {
        event.stopPropagation();
        var drop_down_box = this.element.nativeElement.querySelector('#drop_down_start');
        var am_pm = this.element.nativeElement.querySelector('#am_pm_start');
        if (drop_down_box.style.height == "30px") {
            drop_down_box.style.height = "0";
            am_pm.style.borderBottomLeftRadius = "3px";
        }
        else {
            drop_down_box.style.height = "30px";
            am_pm.style.borderBottomLeftRadius = "0";
        }
    };
    HomeComponent.prototype.close_drop_down_start = function () {
        var drop_down_box = this.element.nativeElement.querySelector('#drop_down_start');
        var am_pm = this.element.nativeElement.querySelector('#am_pm_start');
        drop_down_box.style.height = "0";
        am_pm.style.borderBottomLeftRadius = "0";
    };
    HomeComponent.prototype.change_value_start = function (value) {
        this.start_am_pm = value;
    };
    HomeComponent.prototype.toggle_drop_down_end = function (event) {
        event.stopPropagation();
        var drop_down_box = this.element.nativeElement.querySelector('#drop_down_end');
        var am_pm = this.element.nativeElement.querySelector('#am_pm_end');
        if (drop_down_box.style.height == "30px") {
            drop_down_box.style.height = "0";
            am_pm.style.borderBottomLeftRadius = "3px";
        }
        else {
            drop_down_box.style.height = "30px";
            am_pm.style.borderBottomLeftRadius = "0";
        }
    };
    HomeComponent.prototype.close_drop_down_end = function () {
        var drop_down_box = this.element.nativeElement.querySelector('#drop_down_end');
        var am_pm = this.element.nativeElement.querySelector('#am_pm_end');
        drop_down_box.style.height = "0";
        am_pm.style.borderBottomLeftRadius = "0";
    };
    HomeComponent.prototype.change_value_end = function (value) {
        this.end_am_pm = value;
    };
    HomeComponent.prototype.signup = function () {
        this.router.navigate(['./sign-up']);
    };
    HomeComponent.prototype.see_more = function (value) {
        var text = "#" + value;
        var box = this.element.nativeElement.querySelector(text);
        if (box.style.maxHeight == "104vw") {
            box.style.maxHeight = "52vw";
        }
        else {
            box.style.maxHeight = "104vw";
        }
        if (value == "homeowner_testimonial") {
            if (box.style.maxHeight == "104vw") {
                this.homeowner_see_more = "See less";
            }
            else {
                this.homeowner_see_more = "See more";
            }
        }
        else {
            if (box.style.maxHeight == "104vw") {
                this.tradesman_see_more = "See less";
            }
            else {
                this.tradesman_see_more = "See more";
            }
        }
    };
    __decorate([
        core_1.ViewChild(request_float_box_component_1.RequestFloatBoxComponent),
        __metadata("design:type", request_float_box_component_1.RequestFloatBoxComponent)
    ], HomeComponent.prototype, "request", void 0);
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'home',
            templateUrl: './app/HTML/home.html',
            styleUrls: ['css/home.css'],
            host: { '(window:scroll)': 'change_fixed_box($event)' }
            /*host: {
               '[@routeAnimation]': "active",
               '[style.display]': "'block'",
               '[style.position]': "'absolute'",
               '[style.height]': "'100%'",
               '[style.width]': "'100%'"
             },
             animations: [
               trigger('routeAnimation', [
                 state('*', style({transform: 'translateX(0)'})),
                 transition('void => *', [
                   style({transform: 'translateX(-100%)'}),
                   animate(500)
                 ]),
                 transition('* => void', animate(500, style({transform: 'translateX(-100%)'})))
               ])
             ]*/
        }),
        __metadata("design:paramtypes", [core_1.ElementRef, router_1.Router, data_transfer_service_1.DataTransferService, window_service_1.WindowRef, subcategories_service_1.SubCategoriesService, JsonFetch_service_1.JsonFetchService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
function currentYPosition() {
    // Firefox, Chrome, Opera, Safari
    if (self.pageYOffset)
        return self.pageYOffset;
    // Internet Explorer 6 - standards mode
    if (document.documentElement && document.documentElement.scrollTop)
        return document.documentElement.scrollTop;
    // Internet Explorer 6, 7 and 8
    if (document.body.scrollTop)
        return document.body.scrollTop;
    return 0;
}
//# sourceMappingURL=home.component.js.map