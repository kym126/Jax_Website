import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';

@Component({
  selector: 'tradesman-history',
  templateUrl: './app/HTML/tradesman_history.html',
  styleUrls: ['css/tradesman_history.css'],
})

export class TradesmanHistoryComponent {
  histories = []

  constructor(private element: ElementRef, private datatransfer:DataTransferService, public router: Router, private jsonfetch: JsonFetchService) {
    if(!this.datatransfer.is_logged_in() || this.datatransfer.get_user_type() ==  "1"){
        this.router.navigate(['./home']);
    }
    this.histories = JSON.parse(this.datatransfer.get_tradesman_history());
    if (this.histories == null){
      this.histories = [];
    }
  }

  error_happened(res){
    this.datatransfer.set_load(false);
    console.log(res);
  }

  request_rating(i){
    this.datatransfer.set_load(true);
    this.jsonfetch.request_rating(this.histories[i].id)
      .map(res => res)
          .subscribe(
              res => this.request_rating_2(res, i),
              error => this.error_happened(error)
          );
  }

  request_rating_2(res, i){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.histories[i].state = 1;
    }
    this.datatransfer.set_tradesman_history(JSON.stringify(this.histories));
  }

  turned_down_job(i){
    this.datatransfer.set_load(true);
    this.jsonfetch.history_status(this.histories[i].id, 2)
      .map(res => res)
          .subscribe(
              res => this.turned_down_job_2(res, i),
              error => this.error_happened(error)
          );
  }

  turned_down_job_2(res, i){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.histories.splice(i, 1);
    }
    this.datatransfer.set_tradesman_history(JSON.stringify(this.histories));
  }

  completed(i){
    this.datatransfer.set_load(true);
    this.jsonfetch.history_status(this.histories[i].id, 1)
      .map(res => res)
          .subscribe(
              res => this.completed_2(res, i),
              error => this.error_happened(error)
          );
  }

  completed_2(res, i){
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.histories[i].state = 1;
    }
    this.datatransfer.set_tradesman_history(JSON.stringify(this.histories));
  }

  give_margin_bottom(i){
    if(i == this.histories.length - 1){
      return {
          "margin-bottom": "0"
      }
    }else{
      return {
          "margin-bottom": "1px"
      }
    }
  }

  remove_history_item(i){
    var r_id = [];
    this.datatransfer.set_load(true);
    r_id.push(this.histories[i].r_id);
    this.jsonfetch.clear_t_history(r_id).subscribe(
         res => this.clear_history_3(res, i),
         error =>  this.clear_history_err(error)
       );
  }

  clear_history_3(res, i){
    console.log(res);
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      this.histories.splice(i,1);
      this.datatransfer.set_tradesman_history(JSON.stringify(this.histories));
    }
  }

  clear_history(){
    var r_id = [];
    for(var i = 0; i < this.histories.length; i++){
      if(this.histories[i].state == 3){
        r_id.push(this.histories[i].r_id);
        //this.histories.splice(i, 1);
      }
    }

    this.datatransfer.set_load(true);
    this.jsonfetch.clear_t_history(r_id).subscribe(
         res => this.clear_history_2(res),
         error =>  this.clear_history_err(error)
       );
  }

  clear_history_2(res){

    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      for(var i = 0; i < this.histories.length; i++){
        if(this.histories[i].state == 3){
          this.histories.splice(i,1);
          i--;
        }
      }
      this.datatransfer.set_tradesman_history(JSON.stringify(this.histories));
    }
  }

  clear_history_err(error){
    console.log(error);
  }

}
