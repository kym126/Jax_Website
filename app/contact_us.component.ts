import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { WindowRef } from './window.service';
import { DataTransferService } from './data_transfer.service';

@Component({
  selector: 'contact-us',
  templateUrl: './app/HTML/contact_us.html',
  styleUrls: ['css/misc_pages.css']
})

export class ContactUsComponent {
  banner_img = "./assets/images/misc_pages_banners/contact_us_banner.jpg";
  fullname = "";
  fullname_focus = false;
  email = "";
  email_focus = false;
  phone = "";
  phone_focus = false;
  msg = "";
  msg_focus = false;

  constructor(private element: ElementRef, public router: Router, private datatransfer:DataTransferService) {
    if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "2"){
      if(this.datatransfer.get_tradesman_values_json() == null){
        this.router.navigate(['./edit-profile']);
      }else{
        this.router.navigate(['./dashboard']);
      }
    }
    window.scrollTo(0, 0);
  }

  hide_name(input, input_2){
    if(input.length == 0 && input_2 == false){
      if(window.innerWidth < 800){
        return{
          "font-size": "2.8vw",
          "top": "1.8vw"
        }
      }else{
        return{
          "font-size": "10pt",
          "top": "13px"
        }
      }
    }else{
      if(window.innerWidth < 800){
        return{
          "font-size": "1.7vw",
          "top": "0vw"
        }
      }else{
        return{
          "font-size": "7pt",
          "top": "3px"
        }
      }
    }
  }

  remove_style(id){
    this.element.nativeElement.querySelector(id).removeAttribute("style");
  }
}
