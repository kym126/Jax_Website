import { Component, ElementRef, Input, trigger, state, style, transition, animate, ViewChild } from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';
import { RequestFloatBoxComponent } from './request_float_box.component';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'view-company',
  templateUrl: './app/HTML/view_company.html',
  styleUrls: ['css/tradesman.css'],
  /*host: {
     '[@routeAnimation]': "active",
     '[style.display]': "'block'",
     '[style.position]': "'absolute'",
     '[style.height]': "'100%'",
     '[style.width]': "'100%'"
   },
   animations: [
     trigger('routeAnimation', [
       state('*', style({transform: 'translateX(0)'})),
       transition('void => *', [
         style({transform: 'translateX(-100%)'}),
         animate(700)
       ]),
       transition('* => void', animate(700, style({transform: 'translateX(-100%)'})))
     ])
   ]*/
})

export class ViewCompanyComponent {
   id = 0;
   gallery_num_rows = 1;
   recent_work_num_rows = 1;
   type = 0;
   fb_link = "";
   tw_link = "";
   ln_link = "";
   web_link = "";
   first_name = "";
   last_name = "";
   image = "";
   company = "";
   availability = "";
   rating = null;
   location = null;
   sub_category = "";
   category = "";

   about = "";
   specialize = [];/*["consectetur adipiscing elit", "Donec accumsan tincidunt", "quis aliquam ex porttitor a", "consectetur adipiscing elit", "Donec accumsan tincidunt", "quis aliquam ex porttitor a"];*/

   recent_works = [];/*[
     {
       "title": "Lorem",
       "description": "Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor",
       "images":
         [
            "http://images.wisegeek.com/plumber-installing-fixture.jpg",
            "http://www.emergencyplumber247.com/wp-content/uploads/2016/04/DIY-Plumbing-vs.-Hiring-Contractors.jpg"
         ],
       "title_shown": "Lorem",
       "description_shown": "Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dol...",
       "cost": "400.21"
     },

     {
       "title": "Ipsum",
       "description": "Lorem ipsum dolor",
       "images":
         [
            "http://www.emergencyplumber247.com/wp-content/uploads/2016/04/DIY-Plumbing-vs.-Hiring-Contractors.jpg",
            "http://images.wisegeek.com/plumber-installing-fixture.jpg",
            "http://www.theplumbingworksnc.com/image/17002778.jpeg"
         ],
       "title_shown": "Ipsum",
       "description_shown": "Lorem ipsum dolor",
       "cost": "212"
     },

     {
       "title": "Dolor",
       "description": "Lorem ipsum dolor",
       "images":
         [
            "http://www.emergencyplumber247.com/wp-content/uploads/2016/04/DIY-Plumbing-vs.-Hiring-Contractors.jpg"
         ],
       "title_shown": "Dolor",
       "description_shown": "Lorem ipsum dolor",
       "cost": "10"
     },
     {
       "title": "Lorem",
       "description": "Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor",
       "images":
         [
           "http://images.wisegeek.com/plumber-installing-fixture.jpg",
            "http://www.theplumbingworksnc.com/image/17002778.jpeg",
            "http://www.emergencyplumber247.com/wp-content/uploads/2016/04/DIY-Plumbing-vs.-Hiring-Contractors.jpg"
         ],
       "title_shown": "Lorem",
       "description_shown": "Lorem ipsum dolor Lorem ipsum dolor Lorem ipsu...",
       "cost": "40"
     },

     {
       "title": "Ipsum",
       "description": "Lorem ipsum dolor",
       "images":
         [
           "http://images.wisegeek.com/plumber-installing-fixture.jpg",
            "http://www.theplumbingworksnc.com/image/17002778.jpeg",
            "http://www.emergencyplumber247.com/wp-content/uploads/2016/04/DIY-Plumbing-vs.-Hiring-Contractors.jpg"
         ],
       "title_shown": "Ipsum",
       "description_shown": "Lorem ipsum dolor",
       "cost": "3000"
     },
  ];*/

   //recent_works = ["http://images.wisegeek.com/plumber-installing-fixture.jpg", "http://www.theplumbingworksnc.com/image/17002778.jpeg", "http://www.emergencyplumber247.com/wp-content/uploads/2016/04/DIY-Plumbing-vs.-Hiring-Contractors.jpg"];
   gallery = [];/*["http://plumberlouisvilleky.com/wp-content/uploads/2012/12/Plumbing-Contractor.jpg", "http://www.tubliners.com/wp-content/uploads/bigstock-Plumber-16576664.jpg",
              "http://www.porkdisco.com/wp-content/uploads/2015/11/plumber.jpg",
              "http://nextech2u.com/megajayarenovation2.com/images/Plumbing_Works.jpg", "http://www.prestigeplumbinganddrain.com/wp-content/uploads/2014/03/plumbing-works.jpg",
              "http://plumberlouisvilleky.com/wp-content/uploads/2012/12/Plumbing-Contractor.jpg", "http://www.tubliners.com/wp-content/uploads/bigstock-Plumber-16576664.jpg",
              "http://www.porkdisco.com/wp-content/uploads/2015/11/plumber.jpg",
              "http://nextech2u.com/megajayarenovation2.com/images/Plumbing_Works.jpg", "http://www.prestigeplumbinganddrain.com/wp-content/uploads/2014/03/plumbing-works.jpg",
              "http://plumberlouisvilleky.com/wp-content/uploads/2012/12/Plumbing-Contractor.jpg", "http://www.tubliners.com/wp-content/uploads/bigstock-Plumber-16576664.jpg",
              "http://www.porkdisco.com/wp-content/uploads/2015/11/plumber.jpg",
              "http://nextech2u.com/megajayarenovation2.com/images/Plumbing_Works.jpg", "http://www.prestigeplumbinganddrain.com/wp-content/uploads/2014/03/plumbing-works.jpg"
            ];*/
   company_logo = "";//"http://logok.org/wp-content/uploads/2014/04/Apple-Logo-black.png";
   company_street = null;//"899 Danforth Rd";
   company_province = "";//"Toronto, ON";
   company_postal_code = "";//"M1G 1A5";
   company_number = "";//"+1-345-765-3212";
   company_rating = 0;

   overlay_id= "overlay";
   tradesman_container_id="tradesman_container";
   overlay_1_id = "overlay_1";
   overlay_2_id = "overlay_2";
   overlay_3_id = "overlay_3";
   overlay_array_id = 0;
   overlay_array_1_id = 0;
   overlay_array_2_id = 0;
   overlay_array_3_id = 0;
   overlay_array_4_id = 0;

   ad_load: Subscription;

   rw_view_more_less = "more";

   gallery_view_more_less = "more";

   public DeleteFunction: Function;

   reviews = [];/*[
     {
       "name": "Walter White",
       "rating": 2.4,
       "date": "3rd November 2016",
       "title": "If you don't know who I am - then maybe your best course is to tread lightly.",
       "review": this.about,
       "images": []
     },
     {
       "name": "Saul Goodman",
       "rating": 4.3,
       "date": "21st October 2016",
       "title": "Better call Saul!",
       "review": this.about + this.about + this.about,
       "images": ["http://www.porkdisco.com/wp-content/uploads/2015/11/plumber.jpg"]
     },
     {
       "name": "Jesse Pinkman",
       "rating": 3.2,
       "date": "12th October 2016",
       "title": "Yeah Mr. White! You really do have a plan! Yeah science!",
       "review": this.about,
       "images": ["http://www.tubliners.com/wp-content/uploads/bigstock-Plumber-16576664.jpg",
                  "http://www.porkdisco.com/wp-content/uploads/2015/11/plumber.jpg",
                  "http://nextech2u.com/megajayarenovation2.com/images/Plumbing_Works.jpg"]
     }
   ];*/

   @ViewChild(RequestFloatBoxComponent) request:RequestFloatBoxComponent;

   ngOnInit(){
     window.scrollTo(0,0);
   }

  constructor(private element: ElementRef, private jsonfetch: JsonFetchService, public router: Router, private datatransfer:DataTransferService) {

    if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "2"){
      if(this.datatransfer.get_tradesman_values_json() == null){
        this.router.navigate(['./edit-profile']);
      }else{
        this.router.navigate(['./dashboard']);
      }
    }else if(!this.datatransfer.is_logged_in()){
      this.router.navigate(['./home']);
    }else if(this.datatransfer.is_data_from_list()){
      this.set_values();
      this.DeleteFunction= this.remove_item.bind(this);
      this.datatransfer.set_ad_open_login("");
      this.ad_load = this.datatransfer.ad_load
        .subscribe(value => this.set_ad_load(value));
    }else{
      if(this.datatransfer.is_category()){
        this.router.navigate(['./tradesmanlist']);
      }else{
        this.router.navigate(['./tradesmen']);
      }
    }
  }

  set_ad_load(value){
    if(value){
      this.set_values();
    }
  }

  set_off_on(id){
    if(id%2 == 0){
      return{
        "background-color": "#47b649"
      };
    }else{
      return{
        "background-color": "#f5845d"
      };
    }
  }

  set_values(){
    this.datatransfer.set_load(true);
    this.jsonfetch.view_tr_profile(this.datatransfer.get_tr_id(), this.datatransfer.get_tradesman_cat(), null, null)
       .subscribe(
         tradesman => this.lol(tradesman),
         error =>  this.error_happened(error));
  }

  error_happened(error){
    console.log(error);
  }

  lol(res){
    console.log(res);
    this.datatransfer.set_load(false);
    if(res.STATUS == "OK"){
      var rw_array = [];
      var image = 'http://jaxoftrades.com/api/' + res.DATA[0].company[0].c_photo + "?" + this.getRandomInt(90000000000, 100000000000);

      if(res.DATA[0].company[0].profile_imgpath == 0){
        image = "./assets/images/icons/default_profile_pic.png";
      }

        this.id = parseInt(this.datatransfer.get_tr_id());
        this.first_name =  res.DATA[0].company[0].c_name;
        this.last_name = res.DATA[0].company[0].c_number;
        this.company = "";
        this.image = image;
        this.sub_category = res.DATA[0].company[0].c_postalcode.toUpperCase();
        this.category = res.DATA[0].company[0].c_city + ', ' + res.DATA[0].company[0].c_province;
        this.rating = null;
        this.location = null;
        this.availability = res.DATA[0].company[0].c_add;
        this.type = 1;
        this.about = res.DATA[0].company[0].c_about;
        this.specialize = res.DATA[0].company[0].c_spec;
        for(var x = 0; x < res.DATA[0].company[0].recentwork.length; x++){
          var title_shown = "";
          var desc_shown = "";
          var rw_img_file = [];
          var rw_img_url = [];

          if(res.DATA[0].company[0].recentwork[x].recentwork_title.length > 15){
            title_shown = res.DATA[0].company[0].recentwork[x].recentwork_title.substr(0, 11) + "...";
          }else{
            title_shown = res.DATA[0].company[0].recentwork[x].recentwork_title;
          }

          if(res.DATA[0].company[0].recentwork[x].recentwork_desc.length > 100){
            desc_shown = res.DATA[0].company[0].recentwork[x].recentwork_desc.substr(0, 96) + "...";
          }else{
            desc_shown = res.DATA[0].company[0].recentwork[x].recentwork_desc;
          }

          for(var y = 0; y < res.DATA[0].company[0].recentwork[x].recentwork_image.length; y++){
            rw_img_url.push(res.DATA[0].company[0].recentwork[x].recentwork_image[y]);
            res.DATA[0].company[0].recentwork[x].recentwork_image[y] = 'http://jaxoftrades.com/api/' + res.DATA[0].company[0].recentwork[x].recentwork_image[y] + "?" + this.getRandomInt(90000000000, 100000000000);
            rw_img_file.push(res.DATA[0].company[0].recentwork[x].recentwork_image[y]);
          }

          var rw = {
            "title": res.DATA[0].company[0].recentwork[x].recentwork_title,
            "description": res.DATA[0].company[0].recentwork[x].recentwork_desc,
            "images": res.DATA[0].company[0].recentwork[x].recentwork_image,
            "images_file": rw_img_file,
            "title_shown": title_shown,
            "description_shown": desc_shown,
            "chosen_bg": 0,
            "cost": 0,
            "id": res.DATA[0].company[0].recentwork[x].recentwork_id,
            "rw_image_url": rw_img_url
          }

          rw_array.push(rw);

        }


        this.recent_works = rw_array;

        var gallery = [];

        for(var y = 0; y < res.DATA[0].company[0].gallery_images.length; y++){
          gallery.push('http://jaxoftrades.com/api/' + res.DATA[0].company[0].gallery_images[y] + '?' + this.getRandomInt(90000000000, 100000000000));
        }


        this.gallery = gallery;
        this.company_logo = null;
        this.company_street = null;
        this.company_province = null;
        this.company_postal_code = null;
        this.company_number = null;
        this.company_rating = null;
        this.fb_link = res.DATA[0].company[0].facebook_link;
        if(this.fb_link.substr(0,7) != "http://"){
          this.fb_link = "http://" + this.fb_link;
        }
        this.tw_link = res.DATA[0].company[0].twitter_link;
        if(this.tw_link.substr(0,7) != "http://"){
          this.tw_link = "http://" + this.tw_link;
        }
        this.ln_link = res.DATA[0].company[0].linked_in;
        if(this.ln_link.substr(0,7) != "http://"){
          this.ln_link = "http://" + this.ln_link;
        }
        this.web_link = res.DATA[0].company[0].website;
        if(this.web_link.substr(0,7) != "http://"){
          this.web_link = "http://" + this.web_link;
        }
    }
    /*this.first_name = this.datatransfer.get_first_name();
    this.last_name = this.datatransfer.get_last_name();
    this.image = this.datatransfer.get_image();
    this.company = this.datatransfer.get_company();
    this.availability = this.datatransfer.get_availability();
    this.rating = parseFloat(this.datatransfer.get_rating());
    this.location = this.datatransfer.get_location();
    this.sub_category = this.datatransfer.get_sub_category();
    this.category = this.datatransfer.get_tradesman_cat();
    this.company_rating = this.getRandomInt(0, 500)/100;
    this.type = parseInt(this.datatransfer.get_view_profile_type(),10);*/
  }

  compute_width(rating, index){
    if(index <= rating){
      return{
        "width": "98%",
        "margin-left": "1%",
        "margin-right": "1%"
      };
    }else{
      if(rating- index < -1){
        return{
          "width": "0",
          "margin-left": "1%",
          "margin-right": "1%"
        };
      }else{
        var value = (1 + rating- index)*98;
        return{
          "width": value + "%",
          "margin-left": "1%",
          "margin-right": "1%"
        };
      }
    }
  }

   background_selector(bg, gallery){
     if(gallery){
       return{
         "background-image": "url(" + bg + ")"
       }
     }else{
       if(bg.images.length == 0){
         return{
           "background-image": "none",
         }
       }else{
         return{
           "background-image": "url(" + bg.images[0] + ")",
         }
       }
     }
   }

   company_logo_bg(){
     return{
       "background-image": "url(" + this.company_logo + ")",
     }
   }

   getRandomInt(min, max){
         return Math.floor(Math.random() * (max - min + 1)) + min;
   }

   compute_company_rating(percentage){
     var value = 0;

     if(percentage > 4){
       value = 102;
       percentage -= 4;
     }else if (percentage > 3){
       value = 77;
       percentage -= 3;
     }else if (percentage > 2){
       value = 52;
       percentage -= 2;
     }else if (percentage > 1){
       value = 27;
       percentage -= 1;
     }else{
       value = 3;
     }

     value += percentage * 17;

     return{
       "width": value + "px"
     }
   }

   remove_item(){
     setTimeout(() => {
       this.datatransfer.add_id_to_delete(this.datatransfer.get_index());
       this.router.navigate(['./tradesmanlist']);
     },350);
   }

   request_pop(){
     this.datatransfer.set_blur(true);
     this.overlay_id="overlay_active";
     this.tradesman_container_id="tradesman_container_blurred";
     this.request.request_pop();
   }

   close_request(){
     if(this.request.element.nativeElement.querySelector('#request_container').style.display == "none"){
       this.close();
     }
   }

   close(){
     this.request.close_request();
     setTimeout(() => {
       this.overlay_id="overlay";
       this.tradesman_container_id="tradesman_container";
       this.datatransfer.set_blur(false);
     },350);
   }

   box_closed(event){
     if(event){
       this.close();
     }
   }

   rw_overlay_pop(i){
     var box = this.element.nativeElement.querySelector('#image_overlay');
     this.overlay_array_1_id = 0;

     this.overlay_1_id="overlay_1_active";
     this.tradesman_container_id="tradesman_container_blurred";
     this.overlay_array_id = i;
     box.style.opacity = "0";
     setTimeout(() => {
       box.style.opacity = "1";
     },10);

   }

   review_overlay_pop(i, j){
     var box = this.element.nativeElement.querySelector('#image_overlay_2');
     this.overlay_array_4_id = j;

     this.overlay_3_id="overlay_3_active";
     this.tradesman_container_id="tradesman_container_blurred";
     this.overlay_array_3_id = i;
     box.style.opacity = "0";
     setTimeout(() => {
       box.style.opacity = "1";
     },10);

   }

   overlay_image(){
     return this.recent_works[this.overlay_array_id].images[this.overlay_array_1_id];
   }

   overlay_image_3(){
     return this.reviews[this.overlay_array_3_id].images[this.overlay_array_4_id];
   }

   gallery_overlay_pop(i){
     var box = this.element.nativeElement.querySelector('#gallery_overlay');

     this.overlay_2_id="overlay_2_active";
     this.tradesman_container_id="tradesman_container_blurred";
     this.overlay_array_2_id = i;
     box.style.opacity = "0";
     setTimeout(() => {
       box.style.opacity = "1";
     },10);

   }

   overlay_image_pop(){
     return this.gallery[this.overlay_array_2_id];
   }

   remove_image_overlay(){
     var box = this.element.nativeElement.querySelector('#image_overlay');

     box.style.opacity = "0";
     setTimeout(() => {
       this.overlay_1_id="overlay_1";
       this.tradesman_container_id="tradesman_container";
     },350);
   }

   remove_image_overlay_2(){
     var box = this.element.nativeElement.querySelector('#gallery_overlay');

     box.style.opacity = "0";
     setTimeout(() => {
       this.overlay_2_id="overlay_2";
       this.tradesman_container_id="tradesman_container";
     },350);
   }

   remove_image_overlay_3(){
     var box = this.element.nativeElement.querySelector('#image_overlay_2');

     box.style.opacity = "0";
     setTimeout(() => {
       this.overlay_3_id="overlay_3";
       this.tradesman_container_id="tradesman_container";
     },350);
   }

   next_image(){
     this.overlay_array_1_id++;
   }

   prev_image(){
     this.overlay_array_1_id--;
   }

   next_image_1(){
     this.overlay_array_1_id = 0;
     this.overlay_array_id++;
   }

   prev_image_1(){
     this.overlay_array_1_id = 0;
     this.overlay_array_id--;
   }

   next_image_2(){
     this.overlay_array_2_id++;
   }

   prev_image_2(){
     this.overlay_array_2_id--;
   }

   next_image_3(){
     this.overlay_array_4_id++;
   }

   prev_image_3(){
     this.overlay_array_4_id--;
   }

   click_event(event){
     event.stopPropagation();
   }

   check_second_skill(i){
     if(i%2 == 0 && this.specialize[i+1] != null){
       return true;
     }else{
       return false;
     }
   }

   view_more_gallery(){
     var box = this.element.nativeElement.querySelector('#gallery_wrapper');
     var new_num_rows;
     if(this.gallery_num_rows * 4  < this.gallery.length){
       new_num_rows = this.gallery_num_rows+ 1;
     }else{
       new_num_rows = 1;
     }

    box.style.height = (box.clientHeight/(this.gallery_num_rows)) * new_num_rows;
    this.gallery_num_rows = new_num_rows;
    if(new_num_rows * 4  > this.gallery.length){
      this.gallery_view_more_less = "less";
    }else{
      this.gallery_view_more_less = "more";
    }
   }

   view_more_rw(){
     var box = this.element.nativeElement.querySelector('#recent_works_wrapper');
     var new_num_rows;
     if(this.recent_work_num_rows * 3  < this.recent_works.length){
       new_num_rows = this.recent_work_num_rows + 1;
     }else{
      new_num_rows = 1;
     }

    box.style.height = (box.clientHeight/(this.recent_work_num_rows)) * new_num_rows;
    this.recent_work_num_rows = new_num_rows;
    if(new_num_rows * 3  > this.recent_works.length){
      this.rw_view_more_less = "less";
    }else{
      this.rw_view_more_less = "more";
    }
   }

   next_arrow_2(){
     if(this.gallery_num_rows * 4 > this.gallery.length){
       if(this.overlay_array_2_id == this.gallery.length - 1){
         return false;
       }else{
         return true;
       }
     }else{
       if((this.gallery_num_rows * 4) - 1== this.overlay_array_2_id){
         return false;
       }else{
         return true;
       }
     }
   }

   next_arrow_1(){
    if(this.recent_work_num_rows * 3 > this.recent_works.length){
       if(this.overlay_array_id == this.recent_works.length - 1){
         return false;
       }else{
         return true;
       }
     }else{
       if((this.recent_work_num_rows * 3) - 1 == this.overlay_array_id){
         return false;
       }else{
         return true;
       }
     }
   }

   set_dot_bg(overlay_array_id, i){
     if(i == overlay_array_id){
       return{
         "background-color": "#fff"
       };
     }else{
       return{
         "background-color": "transparent"
       };
     }
   }

   change_recent_work_image(i){
     this.overlay_array_1_id = i;
   }

   change_review_image(i){
     this.overlay_array_4_id = i;
   }
}
