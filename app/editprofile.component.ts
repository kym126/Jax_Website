import { Component, ElementRef, Input, Output, EventEmitter, ViewChildren, QueryList, ViewChild, NgZone} from '@angular/core';
import { Router} from '@angular/router';
import './rxjs-operators';
import { Observable } from 'rxjs/Rx';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';
import { LoginFloatBoxComponent } from './login_float_box.component';
import { TimeInputComponent } from './time_input.component';
import { EditProfileComponent_Personal } from './editprofilepersonal.component';
import { JobSpecificFieldsComponent } from './job_specific_fields.component';
import { WindowRef } from './window.service';

@Component({
  selector: 'edit-profile',
  templateUrl: './app/HTML/edit_profile.html',
  styleUrls: ['css/edit_profile.css'],
})

export class EditProfileComponent {

  @ViewChild(EditProfileComponent_Personal) profile: EditProfileComponent_Personal;
  @ViewChildren(JobSpecificFieldsComponent) job_specific: QueryList<JobSpecificFieldsComponent>;
  @Output()
  blur = new EventEmitter();

  @Output()
  refresh = new EventEmitter();

  @Output()
  testimonial = new EventEmitter();

  profile_pic = "./assets/images/icons/default_profile_pic.png";
  job = "";
  sub_categs = [];
  sub_categ = "";
  c_name = "";
  about_you = "";
  job_specific_fields = [];
  container_id = "container";
  overlay_id = "overlay_hide";
  shown_tab = 0;
  error = false;
  window: any;
  testimonial_text = "Add testimonial";
  tab_id = "tab";
  profile_url = null;

  container_2_id = "container_2";
  container_3_id = "container_3";


  constructor(private element: ElementRef, public router: Router, private datatransfer:DataTransferService, private jsonfetch: JsonFetchService, private zone: NgZone, private win: WindowRef) {
    if(this.datatransfer.is_logged_in() && this.datatransfer.get_user_type() ==  "2"){
      this.job = JSON.parse(this.datatransfer.get_job_json()).jobs[0].job;
      this.sub_categs = JSON.parse(this.datatransfer.get_job_json()).jobs[0].sub_categs;
      if(this.sub_categs.length != 0){
        this.sub_categ = "( ";
        for(var i = 0; i < this.sub_categs.length; i++){
          this.sub_categ += this.sub_categs[i];
          if(i != this.sub_categs.length - 1){
            this.sub_categ += ", ";
          }
        }
        this.sub_categ += " )";
      }
      this.c_name = this.datatransfer.get_company_name();

      if(this.datatransfer.is_tradesman_profile()){
        this.job_specific_fields = JSON.parse(this.datatransfer.get_tradesman_values_json()).jobs;
        this.about_you = JSON.parse(this.datatransfer.get_tradesman_values_json()).about_you;
        var profile_pic = JSON.parse(this.datatransfer.get_tradesman_values_json()).profile_pic;
        if(profile_pic != null){
          this.profile_pic = profile_pic;
        }
        var profile_url = JSON.parse(this.datatransfer.get_tradesman_values_json()).profile_url;
        if(profile_url != null){
          this.profile_url = profile_url;
        }
      }

      this.window = win.nativeWindow;
    }else{
      this.router.navigate(['./home']);
    }

    if(!this.datatransfer.is_logged_in() || this.datatransfer.get_user_type() ==  "1"){
        this.router.navigate(['./home']);
    }
  }

  ngAfterViewInit(){
    var testimonial_json = this.datatransfer.get_testimonial();
    if(testimonial_json != null){
      this.testimonial_text = "Edit testimonial";
    }
  }

  set_width(){
    if(window.innerWidth > 800){
      if(this.router.url == '/tradesman-edit-profile'){
        return{
          "width": "auto;",
          "margin-top": "10px",
          "margin-left": "0px",
          "margin-right": "0px",
          "margin-bottom": "0"
        }
      }else{
        return{
          "width": "75%",
          "margin-top": "87.5px",
          "margin-left": "auto",
          "margin-right": "auto",
          "margin-bottom": "36.5px"
        }
      }
    }else{
      if(this.router.url == '/tradesman-edit-profile'){
        return{
          "width": "91%",
          "margin-top": "5vw",
          "margin-left": "auto",
          "margin-right": "auto",
          "margin-bottom": "0px"
        }
      }else{
        return{
          "width": "91%",
          "margin-top": "87.5px",
          "margin-left": "auto",
          "margin-right": "auto",
          "margin-bottom": "36.5px"
        }
      }
    }
  }

  getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  save_profile(){
    var jobs;
    var job_specific_array = this.job_specific.toArray();
    var job_category = "";
    var sub_categs = [];
    var job_json;
    var i = 0;
    var company_name = "";
    var office_number = "";
    var active: boolean;
    var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    var testPhone = /^\(+([0-9]{3})+\)+\ +([0-9]{3})[-]?([0-9]{4})$/;

    this.error = false;
    this.profile.profile_error = "";

    if(!job_specific_array[i].check_time()){
      this.error = true;
      this.smoothScroll('default_daily_time_availability_title');
    }

    if(!job_specific_array[i].check_location()){
      this.error = true;
      this.smoothScroll('default_location_title');
    }

    if(!testEmail.test(this.profile.email)){
      this.error = true;
      this.smoothScroll('profile_title');
      this.profile.error_2();
    }

    if(!testPhone.test(this.profile.mobile_number)){
      this.error = true;
      this.smoothScroll('profile_title');
      this.profile.error_3();
    }

    if(this.profile.first_name.length == 0 || this.profile.last_name.length == 0 || this.profile.email.length == 0  || this.profile.mobile_number.length == 0){
      this.error = true;
      this.smoothScroll('profile_title');
      this.profile.error_1();
    }

    if(this.error){
      return;
    }

    if(this.datatransfer.is_tradesman_profile()){
      jobs = JSON.parse(this.datatransfer.get_tradesman_values_json()).jobs;
    }else{
      jobs = [];
    }

    this.datatransfer.user_login(this.datatransfer.get_user_id(), this.profile.first_name, this.profile.last_name, this.profile.email, this.datatransfer.get_user_type(), this.profile.mobile_number);
    if(this.job_specific_fields.length < 2){
      job_json = {
        jobs: [
          {
            job: this.profile.job,
            sub_categs: this.profile.sub_categs
          }
        ]
      }
    }else{
      var job_array = JSON.parse(this.datatransfer.get_job_json()).jobs;
      var job_item = {
        job: job_specific_array[i].job,
        sub_categs: job_specific_array[i].sub_categs
      }

      job_array.splice(this.shown_tab,1,job_item);

      job_json = {
        jobs: job_array
      }

    }
    this.datatransfer.set_SU_extra_info(this.profile.c_name, JSON.stringify(job_json));
    this.datatransfer.set_office_number(this.profile.office_number);

    var T_comp = job_specific_array[i].Time_Components.toArray();
    if(this.job_specific_fields.length < 2){
      job_category = this.profile.job;
      sub_categs = this.profile.sub_categs;
      company_name = this.profile.c_name;
      office_number = this.profile.office_number;
      active = true;
    }else{
      job_category = job_specific_array[i].job;
      sub_categs = job_specific_array[i].sub_categs;
      company_name = job_specific_array[i].company_name;
      office_number = job_specific_array[i].office_number;
    }
    var job = {
      "company_name": company_name,
      "office_number": office_number,
      "active": active,
      "job": job_category,
      "sub_categories": sub_categs,
      "pre_defined_skills": job_specific_array[i].chosen_pre_skill,
      "added_skills": job_specific_array[i].added_skills,
      "default_time_availability":{
          "start_time": T_comp[0].hour + ":" + T_comp[0].minute + T_comp[0].am_pm,
          "end_time": T_comp[1].hour + ":" + T_comp[1].minute + T_comp[1].am_pm,
          "start_hour": T_comp[0].hour,
          "start_minute": T_comp[0].minute,
          "start_am_pm": T_comp[0].am_pm,
          "end_hour": T_comp[1].hour,
          "end_minute": T_comp[1].minute,
          "end_am_pm": T_comp[1].am_pm
        },
      "location": job_specific_array[i].location,
      "recent_works": job_specific_array[i].rw_array,
      "gallery": job_specific_array[i].gallery_images,
      "facebook": job_specific_array[i].facebook,
      "linkedin": job_specific_array[i].linkedin,
      "twitter": job_specific_array[i].twitter,
      "website": job_specific_array[i].website
    }

    var profile={
      "profile_pic": this.profile.get_image(),
      "about_you": this.about_you,
      "jobs":jobs
    }

    var rw_title = [];
    var rw_id = [];
    var rw_desc = [];
    var rw_images = [];
    var time = T_comp[0].hour + ":" + T_comp[0].minute + T_comp[0].am_pm + "-" + T_comp[1].hour + ":" + T_comp[1].minute + T_comp[1].am_pm;
    var rw_images_file_index = [];

    for(var z = 0; z < job_specific_array[i].rw_array.length; z++){
      rw_title.push(job_specific_array[i].rw_array[z].title);
      rw_desc.push(job_specific_array[i].rw_array[z].description);
      if(job_specific_array[i].rw_array[z].id == null){
        rw_id.push(0);
      }else{
        rw_id.push(job_specific_array[i].rw_array[z].id);
      }
      var rw_image_small = new Array();
      var rw_images_file_index_small = new Array();
      for(var y = 0; y < job_specific_array[i].rw_array[z].images.length; y++){
        if(typeof job_specific_array[i].rw_array[z].images_file[y] == "string"){
          rw_image_small.push(job_specific_array[i].rw_array[z].rw_image_url[y]);
          rw_images_file_index_small.push(0);
        }else{
          rw_image_small.push(job_specific_array[i].rw_array[z].images_file[y]);
          rw_images_file_index_small.push(1);
        }
      }

      for(var x = rw_images_file_index_small.length; x < 3; x++){
        rw_images_file_index_small.push(null);
      }

      rw_images_file_index.push(rw_images_file_index_small);
      rw_images.push(rw_image_small);
    }

    if(this.router.url == "/edit-profile"){
      this.datatransfer.set_load(true);
      this.jsonfetch.initial_tr_profile(this.datatransfer.get_user_id(), job_category, this.about_you, this.profile.get_image(), job_specific_array[i].chosen_pre_skill,
           job_specific_array[i].added_skills, time, job_specific_array[i].location, job_specific_array[i].facebook, job_specific_array[i].twitter, job_specific_array[i].linkedin,
           job_specific_array[i].website, job_specific_array[i].gallery_images_file, rw_title, rw_desc, rw_images, sub_categs, company_name, rw_id, rw_images_file_index)
        .map(res => res)
            .subscribe(
                res => this.check_result(res, profile),
                error => this.error_happened(error)
            );
    }else{
      this.datatransfer.set_load(true);

      var pic = this.profile.get_image();

      if(pic != null){
        if(pic.substr(0,4) == "data"){
          this.profile_url = null;
        }else{
          pic = null;
        }
      }

      this.jsonfetch.update_tr_profile(this.datatransfer.get_user_id(), job_category, this.about_you, pic, job_specific_array[i].chosen_pre_skill,
           job_specific_array[i].added_skills, time, job_specific_array[i].location, job_specific_array[i].facebook, job_specific_array[i].twitter, job_specific_array[i].linkedin,
           job_specific_array[i].website, job_specific_array[i].gallery_images_file, rw_title, rw_desc, rw_images, sub_categs, company_name, rw_id, this.profile.mobile_number,
           this.profile.email, this.profile.first_name, this.profile.last_name, this.profile_url, rw_images_file_index)
        .map(res => res)
            .subscribe(
                res => this.check_result2(res, profile),
                error => this.error_happened(error)
            );

    }
  }

  error_happened(res){
    this.datatransfer.set_load(false);
    console.log(res);
  }

  check_result2(res, profile){
    this.datatransfer.set_load(false);
    console.log(res);
    if(res.STATUS == "OK"){
      var start_am_pm = res.DATA[0].start_am_pm
      var start_minute = res.DATA[0].start_minute
      var end_am_pm = res.DATA[0].end_am_pm
      var end_minute = res.DATA[0].end_minute
      var rw_array = [];
      var jobs = JSON.parse(this.datatransfer.get_tradesman_values_json()).jobs;

      for(var x = 0; x < res.DATA[0].recentwork.length; x++){
        var title_shown = "";
        var desc_shown = "";
        var rw_img_file = [];
        var rw_img_url = [];

        if(res.DATA[0].recentwork[x].recentwork_title.length > 15){
          title_shown = res.DATA[0].recentwork[x].recentwork_title.substr(0, 11) + "...";
        }else{
          title_shown = res.DATA[0].recentwork[x].recentwork_title;
        }

        if(res.DATA[0].recentwork[x].recentwork_desc.length > 100){
          desc_shown = res.DATA[0].recentwork[x].recentwork_desc.substr(0, 96) + "...";
        }else{
          desc_shown = res.DATA[0].recentwork[x].recentwork_desc;
        }

        for(var y = 0; y < res.DATA[0].recentwork[x].recentwork_image.length; y++){
          rw_img_url.push(res.DATA[0].recentwork[x].recentwork_image[y]);
          res.DATA[0].recentwork[x].recentwork_image[y] = 'http://jaxoftrades.com/api/' + res.DATA[0].recentwork[x].recentwork_image[y] + "?" + this.getRandomInt(90000000000, 100000000000);
          rw_img_file.push(res.DATA[0].recentwork[x].recentwork_image[y]);
        }

        var rw = {
          "title": res.DATA[0].recentwork[x].recentwork_title,
          "description": res.DATA[0].recentwork[x].recentwork_desc,
          "images": res.DATA[0].recentwork[x].recentwork_image,
          "images_file": rw_img_file,
          "title_shown": title_shown,
          "description_shown": desc_shown,
          "chosen_bg": 0,
          "cost": 0,
          "id": res.DATA[0].recentwork[x].recentwork_id,
          "rw_image_url": rw_img_url
        }

        rw_array.push(rw);

      }

      var gallery = [];

      for(var y = 0; y < res.DATA[0].gallery_images.length; y++){
        gallery.push(res.DATA[0].gallery_images[y]);
      }

      var job = {
        "company_name": res.DATA[0].company_name,
        "office_number": res.DATA[0].contact_number,
        "active": true,
        "job": res.DATA[0].job_type,
        "sub_categories": res.DATA[0].job_sub_type,
        "pre_defined_skills": res.DATA[0].predefined_skills,
        "added_skills": res.DATA[0].skills,
        "default_time_availability":{
            "start_time": res.DATA[0].start_hour + ":" + start_minute + start_am_pm,
            "end_time": res.DATA[0].end_hour + ":" + end_minute + end_am_pm,
            "start_hour": res.DATA[0].start_hour,
            "start_minute": start_minute,
            "start_am_pm": start_am_pm,
            "end_hour": res.DATA[0].end_hour,
            "end_minute": end_minute,
            "end_am_pm": end_am_pm
          },
        "location": res.DATA[0].default_location,
        "recent_works": rw_array,
        "gallery": gallery,
        "facebook": res.DATA[0].facebook_link,
        "linkedin": res.DATA[0].linked_in,
        "twitter": res.DATA[0].twitter_link,
        "website": res.DATA[0].website
      }

      jobs.splice(this.shown_tab, 1, job);

      var profile2={
        "profile_pic": 'http://jaxoftrades.com/api/' + res.DATA[0].profile_imgpath + "?" + this.getRandomInt(90000000000, 100000000000),
        "profile_url": res.DATA[0].profile_imgpath,
        "about_you": res.DATA[0].about_you,
        "jobs":jobs
      }
      this.datatransfer.save_tradesman_values_json(JSON.stringify(profile2));
      this.router.navigate(['./dashboard']);
    }
  }

  check_result(res, profile){
    this.datatransfer.set_load(false);
    console.log(res);
    if(res.STATUS == "OK"){
      var start_am_pm = res.DATA[0].start_am_pm
      var start_minute = res.DATA[0].start_minute
      var end_am_pm = res.DATA[0].end_am_pm
      var end_minute = res.DATA[0].end_minute
      var rw_array = [];
      var jobs = [];

      for(var x = 0; x < res.DATA[0].recentwork.length; x++){
        var title_shown = "";
        var desc_shown = "";
        var rw_img_file = [];
        var rw_img_url = [];

        if(res.DATA[0].recentwork[x].recentwork_title.length > 15){
          title_shown = res.DATA[0].recentwork[x].recentwork_title.substr(0, 11) + "...";
        }else{
          title_shown = res.DATA[0].recentwork[x].recentwork_title;
        }

        if(res.DATA[0].recentwork[x].recentwork_desc.length > 100){
          desc_shown = res.DATA[0].recentwork[x].recentwork_desc.substr(0, 96) + "...";
        }else{
          desc_shown = res.DATA[0].recentwork[x].recentwork_desc;
        }

        for(var y = 0; y < res.DATA[0].recentwork[x].recentwork_image.length; y++){
          rw_img_url.push(res.DATA[0].recentwork[x].recentwork_image[y]);
          res.DATA[0].recentwork[x].recentwork_image[y] = 'http://jaxoftrades.com/api/' + res.DATA[0].recentwork[x].recentwork_image[y] + "?" + this.getRandomInt(90000000000, 100000000000);
          rw_img_file.push(res.DATA[0].recentwork[x].recentwork_image[y]);
        }

        var rw = {
          "title": res.DATA[0].recentwork[x].recentwork_title,
          "description": res.DATA[0].recentwork[x].recentwork_desc,
          "images": res.DATA[0].recentwork[x].recentwork_image,
          "images_file": rw_img_file,
          "title_shown": title_shown,
          "description_shown": desc_shown,
          "chosen_bg": 0,
          "cost": 0,
          "id": res.DATA[0].recentwork[x].recentwork_id,
          "rw_image_url": rw_img_url
        }

        rw_array.push(rw);

      }

      var gallery = [];

      for(var y = 0; y < res.DATA[0].gallery_images.length; y++){
        gallery.push(res.DATA[0].gallery_images[y]);
      }

      var job = {
        "company_name": res.DATA[0].company_name,
        "office_number": profile.office_number,
        "active": profile.active,
        "job": res.DATA[0].job_type,
        "sub_categories": res.DATA[0].job_sub_type,
        "pre_defined_skills": res.DATA[0].predefined_skills,
        "added_skills": res.DATA[0].skills,
        "default_time_availability":{
            "start_time": res.DATA[0].start_hour + ":" + start_minute + start_am_pm,
            "end_time": res.DATA[0].end_hour + ":" + end_minute + end_am_pm,
            "start_hour": res.DATA[0].start_hour,
            "start_minute": start_minute,
            "start_am_pm": start_am_pm,
            "end_hour": res.DATA[0].end_hour,
            "end_minute": end_minute,
            "end_am_pm": end_am_pm
          },
        "location": res.DATA[0].default_location,
        "recent_works": rw_array,
        "gallery": gallery,
        "facebook": res.DATA[0].facebook_link,
        "linkedin": res.DATA[0].linked_in,
        "twitter": res.DATA[0].twitter_link,
        "website": res.DATA[0].website
      }

      jobs.push(job);

      var profile2={
        "profile_pic": 'http://jaxoftrades.com/api/' + res.DATA[0].profile_imgpath + "?" + this.getRandomInt(90000000000, 100000000000),
        "about_you": res.DATA[0].about_you,
        "jobs":jobs
      }
      this.datatransfer.save_tradesman_values_json(JSON.stringify(profile2));
      this.router.navigate(['./dashboard']);
    }
  }

  save_profile_recursive(i, j){
    if(this.error == true){
      return;
    }else if(i < this.job_specific_fields.length){
      this.shown_tab = i;
      setTimeout(() => {
        this.save_profile();
        i++;
        this.save_profile_recursive(i, j);
        return;
      },50);
    }else{
      this.shown_tab = j;
      this.router.navigate(['./dashboard']);
    }
  }

  save_all(){
    var init_shown_tab = this.shown_tab;
    this.error = false;
    this.save_profile_recursive(0, init_shown_tab);
  }

  set_tab(i){
    var width = 100/this.job_specific_fields.length - .1;
    if(i == 0){
      if(i == this.shown_tab){
        return{
          "width": width + "%",
          "overflow": "visible",
          "float": "left",
          "background-color": "transparent",
          "border-bottom": "none",
          "border-right": "1px solid #b6b7b7",
          "border-top-left-radius": "0",
          "border-top-right-radius": "0"
        };
      }else{
        return{
          "width": width + "%",
          "overflow": "visible",
          "float": "left",
          "background-color": "#efefef",
          "border-bottom": "1px solid #b6b7b7",
          "border-right": "1px solid #b6b7b7",
          "border-top-left-radius": "7px",
          "border-top-right-radius": "0"
        };
      }
    }else if(i == this.job_specific_fields.length - 1){
      if(i == this.shown_tab){
        return{
          "width": "auto",
          "overflow": "hidden",
          "float": "none",
          "background-color": "transparent",
          "border-bottom": "none",
          "border-right": "none",
          "border-top-left-radius": "0",
          "border-top-right-radius": "0"
        };
      }else{
        return{
          "width": "auto",
          "overflow": "hidden",
          "float": "none",
          "background-color": "#efefef",
          "border-bottom": "1px solid #b6b7b7",
          "border-right": "none",
          "border-top-left-radius": "0",
          "border-top-right-radius": "7px"
        };
      }
    }else{
      if(i == this.shown_tab){
        return{
          "width": width + "%",
          "overflow": "visible",
          "float": "left",
          "background-color": "transparent",
          "border-bottom": "none",
          "border-right": "1px solid #b6b7b7",
          "border-top-left-radius": "0",
          "border-top-right-radius": "0"
        };
      }else{
        return{
          "width": width + "%",
          "overflow": "visible",
          "float": "left",
          "background-color": "#efefef",
          "border-bottom": "1px solid #b6b7b7",
          "border-right": "1px solid #b6b7b7",
          "border-top-left-radius": "0",
          "border-top-right-radius": "0"
        };
      }
    }
  }

  set_blur(event){
    this.blur.emit(event);
    if(event){
      this.container_id = "container_blur";
    }else{
      this.container_id = "container";
    }
  }

  set_blur_personal(event){
    this.blur.emit(event);
    this.datatransfer.set_blur(event);
    if(event){
      this.container_2_id = "container_2_blur";
      this.container_3_id = "container_3_blur";
    }else{
      this.container_2_id = "container_2";
      this.container_3_id = "container_3";
    }
  }

  stop_prop(event){
    event.stopPropagation();
  }

  close_pop(){
    var pop = this.element.nativeElement.querySelector('#edit_rw_overlay_3');
    var job_specific_array = this.job_specific.toArray();

    pop.style.opacity = 0;
    setTimeout(() => {
      this.tab_id = "tab";
      this.overlay_id = "overlay_hide";
      this.set_blur(false);
      this.datatransfer.set_blur(false);
      for(var i = 0; i < job_specific_array.length; i++){
        job_specific_array[i].set_blur(false);
      }
    },350);
  }

  open_pop(){
    this.tab_id = "tab_blur";
    this.overlay_id="overlay_active";
    var pop = this.element.nativeElement.querySelector('#edit_rw_overlay_3');
    this.datatransfer.set_blur(true);
    this.set_blur(true);
    var job_specific_array = this.job_specific.toArray();
    for(var i = 0; i < job_specific_array.length; i++){
      job_specific_array[i].set_blur(true);
    }

    pop.style.opacity = 0;
    setTimeout(() => {
      pop.style.opacity = 1;
    },10);
  }

  remove_trade(){
    this.datatransfer.set_load(true);
    this.jsonfetch.delete_trade(this.datatransfer.get_user_id(), JSON.parse(this.datatransfer.get_job_json()).jobs[this.shown_tab].job)
      .map(res => res)
          .subscribe(
              res => this.check_result3(res),
              error => this.error_happened(error)
          );
  }


check_result3(res){
  this.datatransfer.set_load(false);
  console.log(res);
  if(res.STATUS == "OK"){
    this.close_pop();
    setTimeout(() => {
      var job_array = JSON.parse(this.datatransfer.get_job_json());
      job_array.jobs.splice(this.shown_tab, 1);
      this.datatransfer.set_SU_extra_info(this.profile.c_name, JSON.stringify(job_array));
      var profile_json = JSON.parse(this.datatransfer.get_tradesman_values_json());
      profile_json.jobs.splice(this.shown_tab, 1);
      this.datatransfer.save_tradesman_values_json(JSON.stringify(profile_json));
      this.refresh.emit(true);
      this.job_specific_fields = JSON.parse(this.datatransfer.get_tradesman_values_json()).jobs;
      if(this.job_specific_fields.length == 1){
        this.profile.job = this.job_specific_fields[0].job;
        this.profile.populate_sub_cat();
        this.profile.sub_categs = this.job_specific_fields[0].sub_categories;
        setTimeout(() => {
          this.profile.set_checkboxes();
        },50);
        var profile_json = JSON.parse(this.datatransfer.get_tradesman_values_json());
        profile_json.jobs[0].active = true;
        this.datatransfer.save_tradesman_values_json(JSON.stringify(profile_json));
      }
    },350);
  }
}
  add_border_multiple_trades(){
    if(this.job_specific_fields.length > 1){
      return{
        "border": "1px solid #b6b7b7",
        "margin-top": "10px",
        "padding-bottom": "30px"
      };
    }else{
      return{
        "border": "none",
        "margin-top": "0",
        "padding-bottom": "0"
      };
    }
  }

  smoothScroll(eID) {
      var startY = currentYPosition();
      var stopY = elmYPosition(eID) - 63;
      var distance = stopY > startY ? stopY - startY : startY - stopY;
      if (distance < 100) {
          this.window.window.scrollTo(0, stopY); return;
      }
      var speed = Math.round(distance / 100);
      if (speed >= 20) speed = 20;
      var step = Math.round(distance / 100);
      var leapY = stopY > startY ? startY + step : startY - step;
      var timer = 0;
      if (stopY > startY) {
          for (var i = startY; i < stopY; i += step) {
              this.scrollTo(leapY, timer * speed);
              leapY += step; if (leapY > stopY) leapY = stopY; timer++;
          } return;
      }
      for (var i = startY; i > stopY; i -= step) {
          this.scrollTo(leapY, timer * speed);
          leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
      }
  }
  scrollTo(yPoint: number, duration: number) {
      setTimeout(() => {
          this.window.window.scrollTo(0, yPoint)
      }, duration);
      return;
  }
}

function currentYPosition() {
    // Firefox, Chrome, Opera, Safari
    if (self.pageYOffset) return self.pageYOffset;
    // Internet Explorer 6 - standards mode
    if (document.documentElement && document.documentElement.scrollTop)
        return document.documentElement.scrollTop;
    // Internet Explorer 6, 7 and 8
    if (document.body.scrollTop) return document.body.scrollTop;
    return 0;
}

function elmYPosition(eID) {
    var elm = document.getElementById(eID);
    var y = elm.offsetTop;
    var node = elm;
    while (node.offsetParent && node.offsetParent != document.body) {
        node = <HTMLElement><any>node.offsetParent;
        y += node.offsetTop;
    } return y;
}
