import { Injectable} from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Rx';
import { PushNotificationsService } from 'angular2-notifications';
import { JsonFetchService } from './JsonFetch.service';
import { DataTransferService } from './data_transfer.service';

@Injectable()
export class RequestService {

  private _req_length = new BehaviorSubject<number>(0);
  req_length = this._req_length.asObservable();

  private _missed_reqs = new BehaviorSubject<number>(0);
  missed_reqs = this._missed_reqs.asObservable();

  missed_reqs_var = 0;

  requests = [];
  inc_requests = [];
  timer: Observable<number>;

  constructor(private push_notif: PushNotificationsService, private jsonfetch: JsonFetchService, private datatransfer:DataTransferService){
    if(this.datatransfer.get_requests() != null){
      var data = JSON.parse(this.datatransfer.get_requests());
      for(var x = 0; x < data.length; x++){
        if(data[x].state > 1){
          this.requests.push(data[x]);
        }
      }
      var requests_array = [];
      for(var z = 0; z < this.requests.length; z++){
        if(this.requests[z].state > 1){
          var request = {
            "pop_state": this.requests[z].pop_state,
            "state": this.requests[z].state,
            "first_name":  this.requests[z].first_name,
            "last_name": this.requests[z].last_name,
            "job": this.requests[z].job,
            "location": this.requests[z].location,
            "precise_loc": this.requests[z].precise_loc,
            "time": this.requests[z].time,
            "description": this.requests[z].description,
            "phone_number": this.requests[z].phone_number,
            "email": this.requests[z].email,
            "time_left": this.requests[z].time_left,
            "subscription": null,
            "bg_color": this.requests[z].bg_color,
            "id": this.requests[z].id,
            "h_id": this.requests[z].h_id
          }
          requests_array.push(request);
        }
      }
      console.log(requests_array);
      this.datatransfer.set_requests(JSON.stringify(requests_array));
    }
  }

  get_requests(){
    return this.requests;
  }

  get_inc_requests(){
    return this.inc_requests;
  }

  count_reqs(){
    var req_length = 0;
    for(var i = 0; i < this.requests.length; i++){
      if(this.requests[i].state < 2){
        req_length++;
      }
    }
    this._req_length.next(req_length);
  }

  remove_from_inc_requests(){
    for(var i = 0; i < this.inc_requests.length; i++){
      if(this.inc_requests[i].time_left <= 0){
        this.inc_requests.splice(i,1);
        return;
      }
    }
  }

  add_request(first_name, last_name, job, location, precise_loc, time, description, phone_number, email, id, h_id){
    this.timer = Observable.timer(0,100);
    var subscription = this.timer.subscribe(t=>this.calculate_time_remaining(t, request));

    var request = {
      "pop_state": 0,
      "state": 0,
      "first_name":  first_name,
      "last_name": last_name,
      "job": job,
      "location": location,
      "precise_loc": precise_loc,
      "time": time,
      "description": description,
      "phone_number": phone_number,
      "email": email,
      "time_left": 0,
      "subscription": subscription,
      "bg_color": "#7dc469",
      "id": id,
      "h_id": h_id
    }

    console.log("HELLO");

    this.requests.splice(0,0, request);
    this.inc_requests.splice(0,0, request);
    this.count_reqs();
  }

  add_request_no_pop(first_name, last_name, job, location, precise_loc, time, description, phone_number, email, id, h_id){
    this.timer = Observable.timer(0,100);
    var subscription = this.timer.subscribe(t=>this.calculate_time_remaining(t, request));

    var request = {
      "pop_state": 0,
      "state": 0,
      "first_name":  first_name,
      "last_name": last_name,
      "job": job,
      "location": location,
      "precise_loc": precise_loc,
      "time": time,
      "description": description,
      "phone_number": phone_number,
      "email": email,
      "time_left": 0,
      "subscription": subscription,
      "bg_color": "#7dc469",
      "id": id,
      "h_id": h_id
    }

    this.requests.splice(0,0, request);
    this.count_reqs();
  }

  calculate_time_remaining(t, request){
    var time_left = request.time-t/10;
    request.time_left = Math.ceil(time_left);

    if(time_left <= 0){
      request.subscription.unsubscribe();
      request.state = 5;
      request.bg_color = "#fcb431";
      this.count_reqs();
      this.missed_reqs_var++;
      this._missed_reqs.next(this.missed_reqs_var);
      this.remove_from_inc_requests();
      this.push_notif.create('Jax of Trades', {body: "You missed a request from "+request.first_name+" "+request.last_name+".", icon: './assets/images/logo_box.png'}).subscribe(
        res => console.log(res),
        err => console.log(err)
      );
    }else if(time_left <= 10){
      if((100-time_left*10)%8 <= 3){
        request.bg_color = "#EF5350";
      }else{
        request.bg_color = "#f16a67"
      }
    }else if(time_left <= 30){
      request.bg_color = "#EF5350";
    }else if(time_left <= 60){
      request.bg_color = "#FFC609";
    }
  }

  delete_req(i){
    this.requests.splice(i,1);
    this.count_reqs();
    var requests_array = [];
    for(var z = 0; z < this.requests.length; z++){
      if(this.requests[z].state > 1){
        var request = {
          "pop_state": this.requests[z].pop_state,
          "state": this.requests[z].state,
          "first_name":  this.requests[z].first_name,
          "last_name": this.requests[z].last_name,
          "job": this.requests[z].job,
          "location": this.requests[z].location,
          "precise_loc": this.requests[z].precise_loc,
          "time": this.requests[z].time,
          "description": this.requests[z].description,
          "phone_number": this.requests[z].phone_number,
          "email": this.requests[z].email,
          "time_left": this.requests[z].time_left,
          "subscription": null,
          "bg_color": this.requests[z].bg_color,
          "id": this.requests[z].id,
          "h_id": this.requests[z].h_id
        }
        requests_array.push(request);
      }
    }
    console.log(requests_array);
    this.datatransfer.set_requests(JSON.stringify(requests_array));
  }

  drop_reqs(){
    this.requests.length = 0;
    this._req_length.next(0);
    var requests_array = [];
    for(var z = 0; z < this.requests.length; z++){
      if(this.requests[z].state > 1){
        var request = {
          "pop_state": this.requests[z].pop_state,
          "state": this.requests[z].state,
          "first_name":  this.requests[z].first_name,
          "last_name": this.requests[z].last_name,
          "job": this.requests[z].job,
          "location": this.requests[z].location,
          "precise_loc": this.requests[z].precise_loc,
          "time": this.requests[z].time,
          "description": this.requests[z].description,
          "phone_number": this.requests[z].phone_number,
          "email": this.requests[z].email,
          "time_left": this.requests[z].time_left,
          "subscription": null,
          "bg_color": this.requests[z].bg_color,
          "id": this.requests[z].id,
          "h_id": this.requests[z].h_id
        }
        requests_array.push(request);
      }
    }
    console.log(requests_array);
    this.datatransfer.set_requests(JSON.stringify(requests_array));
  }

  getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  set_state(i, j){
    if(j > 1 && this.requests[i].subscription != null){
      this.requests[i].subscription.unsubscribe();
    }

    this.requests[i].state = j;
    if(j == 2){
      this.datatransfer.set_load(true);
      this.jsonfetch.req_status_1(this.requests[i].id, 1)
         .subscribe(
           data => this.set_state_accept(data, i, j),
           error =>  this.inc_req_error(error));
    }else if(j == 3){
      this.datatransfer.set_load(true);
      this.jsonfetch.req_status_1(this.requests[i].id, 2)
         .subscribe(
           data => this.set_state_reject(data, i, j),
           error =>  this.inc_req_error(error));
    }else if(j > 3){
      this.requests[i].bg_color = "#fcb431";
      this.count_reqs();
      var requests_array = [];
      for(var z = 0; z < this.requests.length; z++){
        if(this.requests[z].state > 1){
          var request = {
            "pop_state": this.requests[z].pop_state,
            "state": this.requests[z].state,
            "first_name":  this.requests[z].first_name,
            "last_name": this.requests[z].last_name,
            "job": this.requests[z].job,
            "location": this.requests[z].location,
            "precise_loc": this.requests[z].precise_loc,
            "time": this.requests[z].time,
            "description": this.requests[z].description,
            "phone_number": this.requests[z].phone_number,
            "email": this.requests[z].email,
            "time_left": this.requests[z].time_left,
            "subscription": null,
            "bg_color": this.requests[z].bg_color,
            "id": this.requests[z].id,
            "h_id": this.requests[z].h_id
          }
          requests_array.push(request);
        }
      }
      console.log(requests_array);
      this.datatransfer.set_requests(JSON.stringify(requests_array));
    }


  }

  set_state_accept(data, i, j){
    console.log(data);
    this.datatransfer.set_load(false);
    this.requests[i].bg_color = "#7dc469";
    this.count_reqs();
    var requests_array = [];
    for(var z = 0; z < this.requests.length; z++){
      if(this.requests[z].state > 1){
        var request = {
          "pop_state": this.requests[z].pop_state,
          "state": this.requests[z].state,
          "first_name":  this.requests[z].first_name,
          "last_name": this.requests[z].last_name,
          "job": this.requests[z].job,
          "location": this.requests[z].location,
          "precise_loc": this.requests[z].precise_loc,
          "time": this.requests[z].time,
          "description": this.requests[z].description,
          "phone_number": this.requests[z].phone_number,
          "email": this.requests[z].email,
          "time_left": this.requests[z].time_left,
          "subscription": null,
          "bg_color": this.requests[z].bg_color,
          "id": this.requests[z].id,
          "h_id": this.requests[z].h_id
        }
        requests_array.push(request);
      }
    }
    console.log(requests_array);
    this.datatransfer.set_requests(JSON.stringify(requests_array));
  }

  set_state_reject(data, i, j){
    console.log(data);
    this.datatransfer.set_load(false);
    this.requests[i].bg_color = "#ea5b5b";
    this.count_reqs();
    var requests_array = [];
    for(var z = 0; z < this.requests.length; z++){
      if(this.requests[z].state > 1){
        var request = {
          "pop_state": this.requests[z].pop_state,
          "state": this.requests[z].state,
          "first_name":  this.requests[z].first_name,
          "last_name": this.requests[z].last_name,
          "job": this.requests[z].job,
          "location": this.requests[z].location,
          "precise_loc": this.requests[z].precise_loc,
          "time": this.requests[z].time,
          "description": this.requests[z].description,
          "phone_number": this.requests[z].phone_number,
          "email": this.requests[z].email,
          "time_left": this.requests[z].time_left,
          "subscription": null,
          "bg_color": this.requests[z].bg_color,
          "id": this.requests[z].id,
          "h_id": this.requests[z].h_id
        }
        requests_array.push(request);
      }
    }
    console.log(requests_array);
    this.datatransfer.set_requests(JSON.stringify(requests_array));
  }

  inc_req_error(error){
    console.log(error);
  }

  pop_set_state(i, j){
    if(j > 1){
      this.inc_requests[i].subscription.unsubscribe();
    }

    this.inc_requests[i].state = j;

    if(j == 2){
      this.datatransfer.set_load(true);
      this.jsonfetch.req_status_1(this.requests[i].id, 1)
         .subscribe(
           data => this.set_state_accept(data, i, j),
           error =>  this.inc_req_error(error));
    }else if(j == 3){
      this.datatransfer.set_load(true);
      this.jsonfetch.req_status_1(this.requests[i].id, 2)
         .subscribe(
           data => this.set_state_reject(data, i, j),
           error =>  this.inc_req_error(error));
    }else if(j > 3){
      this.requests[i].bg_color = "#fcb431";
      this.count_reqs();
    }

  }

  drop_inc_reqs(){
    this.inc_requests.length = 0;
    this.missed_reqs_var = 0
    this._missed_reqs.next(0);
  }
}
