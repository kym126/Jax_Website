import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pipe_search'
})

export class SearchPipe {
  transform(value: any[], str): any {

    value.sort((a, b) => {
      if (a.location < b.location) {
        return -1;
      } else if (a.location > b.location) {
        return 1;
      } else {
        if (a.rating < b.rating) {
          return 1;
        } else if (a.rating > b.rating) {
          return -1;
        } else {
          return 0;
        }
      }
    });

    if(str == ""){
      return value;
    }

    return value.filter(item => (item.name.toLowerCase()).includes(str.toLowerCase()) == true);
  }

}
